/*
 * ss_session_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_SS_SESSION_TBL_T_H_
#define STRUCTURES_SS_SESSION_TBL_T_H_



struct ss_session_tbl_t{
	struct session_ex_t **data;

	unsigned long long data_size;
	unsigned long long cnt;
	signed long long shift;

	struct min_max repl_id;
	struct min_max sess_id;

	struct session_ex_t *current;
};

#endif /* STRUCTURES_SS_SESSION_TBL_T_H_ */
