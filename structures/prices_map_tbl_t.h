/*
 * prices_map_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_PRICES_MAP_TBL_T_H_
#define STRUCTURES_PRICES_MAP_TBL_T_H_

struct prices_map_tbl_t{
	char **data;

	unsigned long long data_size;
//	unsigned long long cnt;
	signed long long shift;

	struct min_max price;

	struct min_max range;
};

#endif /* STRUCTURES_PRICES_MAP_TBL_T_H_ */
