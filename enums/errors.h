/*
 * errors.h
 *
 *  Created on: Feb 4, 2017
 *      Author: user
 */

#ifndef ENUMS_ERRORS_H_
#define ENUMS_ERRORS_H_


enum {
	THREAD_CREATION_ERROR = 1,
	MEMORY_ALLOC_ERROR = 2,
	ORDLOG_DEBUG_ERROR = 3,
	WRONG_REALLOC_CALL = 4,
	CG_PUB_MSGNEW_ERROR = 5,
	R_DATA_MISSED_ERROR = 6
};


#endif /* ENUMS_ERRORS_H_ */
