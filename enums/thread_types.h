/*
 * thread_types.h
 *
 *  Created on: Feb 4, 2017
 *      Author: user
 */

#ifndef ENUMS_THREAD_TYPES_H_
#define ENUMS_THREAD_TYPES_H_


enum {
	TRANSACTIONS = 1,
	FORTS_ORDLOG = 2,
	FORTS_OTHER = 3,
	LOGGER = 4
};


#endif /* ENUMS_THREAD_TYPES_H_ */
