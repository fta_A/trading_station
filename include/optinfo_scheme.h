#pragma pack(push, 4)

// Scheme "CustReplScheme" description




    struct opt_sess_contents
    {
        signed long long replID; // i8
        signed long long replRev; // i8
        signed long long replAct; // i8
        signed int sess_id; // i4
        signed int isin_id; // i4
        char isin[26]; // c25
        signed int roundto; // i4
        char min_step[11]; // d16.5
        char step_price[11]; // d16.5
        signed int signs; // i4
        signed char is_limited; // i1
        char limit_up[11]; // d16.5
        char limit_down[11]; // d16.5
        char last_cl_quote[11]; // d16.5
        char old_kotir[11]; // d16.5
        signed int fut_isin_id; // i4
        signed char europe; // i1
        signed char put; // i1
        char strike[11]; // d16.5
        
    };

    const size_t sizeof_opt_sess_contents = 160;
    const size_t opt_sess_contents_index = 0;




#pragma pack(pop)
