/*
 * cg_msgs_data_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_CG_MSGS_DATA_TBL_T_H_
#define STRUCTURES_CG_MSGS_DATA_TBL_T_H_




struct cg_msgs_data_tbl_t{
	struct cg_msg_data_ex_t *msg_data;
	struct cg_msg_data_ex_t *msg_data_last;
};

#endif /* STRUCTURES_CG_MSGS_DATA_TBL_T_H_ */
