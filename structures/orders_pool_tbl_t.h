/*
 * orders_pool_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_ORDERS_POOL_TBL_T_H_
#define STRUCTURES_ORDERS_POOL_TBL_T_H_



struct orders_pool_tbl_t{
	struct active_order_t *first;
	struct active_order_t *last;
};

#endif /* STRUCTURES_ORDERS_POOL_TBL_T_H_ */
