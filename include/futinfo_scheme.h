#pragma pack(push, 4)

// Scheme "CustReplScheme" description




    struct session
    {
        signed long long replID; // i8
        signed long long replRev; // i8
        signed long long replAct; // i8
        signed int sess_id; // i4
        struct cg_time_t begin; // t
        struct cg_time_t end; // t
        signed int state; // i4
        signed int opt_sess_id; // i4
        struct cg_time_t inter_cl_begin; // t
        struct cg_time_t inter_cl_end; // t
        signed int inter_cl_state; // i4
        signed char eve_on; // i1
        struct cg_time_t eve_begin; // t
        struct cg_time_t eve_end; // t
        signed char mon_on; // i1
        struct cg_time_t mon_begin; // t
        struct cg_time_t mon_end; // t
        struct cg_time_t pos_transfer_begin; // t
        struct cg_time_t pos_transfer_end; // t
        
    };

    const size_t sizeof_session = 144;
    const size_t session_index = 0;


    struct fut_sess_contents
        {
            signed long long replID; // i8
            signed long long replRev; // i8
            signed long long replAct; // i8
            signed int sess_id; // i4
            signed int isin_id; // i4
            char isin[26]; // c25
            signed int roundto; // i4
            char min_step[11]; // d16.5
            char step_price[11]; // d16.5
            signed int signs; // i4
            signed char is_limited; // i1
            char limit_up[11]; // d16.5
            char limit_down[11]; // d16.5
            char last_cl_quote[11]; // d16.5
            char step_price_curr[11]; // d16.5

        };

        const size_t sizeof_fut_sess_contents = 140;
        const size_t fut_sess_contents_index = 1;




#pragma pack(pop)
