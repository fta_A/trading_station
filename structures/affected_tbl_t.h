/*
 * affected_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_AFFECTED_TBL_T_H_
#define STRUCTURES_AFFECTED_TBL_T_H_


struct affected_tbl_t{
	signed int *data;
	signed int data_size;
	signed int cnt;
};


#endif /* STRUCTURES_AFFECTED_TBL_T_H_ */
