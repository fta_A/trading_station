/*
 * sesscont_ex_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_SESSCONT_EX_T_H_
#define STRUCTURES_SESSCONT_EX_T_H_

struct sesscont_ex_t{
	struct sess_contents origin;

	int64_t minstep_int;
	int8_t minstep_scale;

	int64_t step_price_int;
	int8_t step_price_scale;

	int64_t step_price_curr_int;
	int8_t step_price_curr_scale;

	int64_t limit_up_int;
	int8_t limit_up_scale;

	int64_t limit_down_int;
	int8_t limit_down_scale;

	int64_t last_cl_quote_int;
	int8_t last_cl_quote_scale;

	int64_t limit_down_adj;
	int64_t minstep_adj;
	int8_t scale_adj;

	struct sess_data_row_t *sess_data;
};

#endif /* STRUCTURES_SESSCONT_EX_T_H_ */
