/*
 * quotes_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_QUOTES_TBL_T_H_
#define STRUCTURES_QUOTES_TBL_T_H_


struct quotes_tbl_t{
	struct quote_t **data;
	uint cnt;
	uint data_size;

	uint qnt_cnt;
};

#endif /* STRUCTURES_QUOTES_TBL_T_H_ */
