/*
 * trans_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_TRANS_TBL_T_H_
#define STRUCTURES_TRANS_TBL_T_H_


struct trans_tbl_t{
	struct trans_t **data;

	unsigned long long data_size;
	unsigned long long cnt;
	signed long long shift;

#ifdef RTT_STAT
	struct min_max commit_rtt;
	struct min_max begin_rtt;
	struct min_max sent_rtt;
#endif
};


#endif /* STRUCTURES_TRANS_TBL_T_H_ */
