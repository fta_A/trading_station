/*
 * price_aggr_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_PRICE_AGGR_T_H_
#define STRUCTURES_PRICE_AGGR_T_H_


struct price_aggr_t{
	signed int amount_rest;

#ifdef ORDLOG_DEBUG
	int agp_position;
#endif

//	signed int lc_amount_rest;
//	signed int fr_amount_rest;
};


#endif /* STRUCTURES_PRICE_AGGR_T_H_ */
