/*
 * prices_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_PRICES_TBL_T_H_
#define STRUCTURES_PRICES_TBL_T_H_



struct prices_tbl_t{
	struct price_aggr_t **data;
	int data_size;

	int first;
	int last;

};


#endif /* STRUCTURES_PRICES_TBL_T_H_ */
