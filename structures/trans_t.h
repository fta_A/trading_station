/*
 * trans_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_TRANS_T_H_
#define STRUCTURES_TRANS_T_H_


struct trans_t{
	struct active_order_t *active_order;
	struct active_order_t *active_order2;
	struct cg_msg_data_ex_t *p2msg;

	char ttype;

#ifdef RTT_STAT
	struct timespec ordlog_listener_TN_COMMIT;
	struct timespec ordlog_listener_TN_BEGIN;
	struct timespec sent_moment;
#endif
};

#endif /* STRUCTURES_TRANS_T_H_ */
