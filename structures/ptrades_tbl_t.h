/*
 * ptrades_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_PTRADES_TBL_T_H_
#define STRUCTURES_PTRADES_TBL_T_H_


struct ptrades_tbl_t{
	int *data;
	int64_t limit_up_int;
	int64_t limit_down_int;
};

#endif /* STRUCTURES_PTRADES_TBL_T_H_ */
