/*
 * portfolio_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_PORTFOLIO_T_H_
#define STRUCTURES_PORTFOLIO_T_H_


struct portfolio_t{
	int portfolio_id;
	struct quotes_tbl_t *quotes_tbl;

	int64_t wbid;
	int64_t wask;

	int64_t lbid;
	int64_t lask;

	int64_t rbid;
	int64_t rask;

	int64_t best_buy;
	int64_t best_sell;

	int coeff;
	int64_t buy_at;
	int64_t sell_at;

	int sl_cnt;
	int trd_amt;
	int trd_limit;
	char used;
	int sl;
	char sl_dir;
	char sl_order;

	int same_inn;
	char same_inn_ch;
	char same_inn_dir;

	int64_t best_buy_trd;
	int64_t best_sell_trd;

	int active_bids;
	int active_asks;

	int need_buy_amt;
	int need_sell_amt;

	int killed_buy_amt;
	int killed_sell_amt;

	int amount;

	struct active_order_t *active_orders_tbl;

	int work_type;
	char state;

	int divisor;
	int zero_wbids_num;
	int zero_wasks_num;

	int8_t scale;

	int spread;
	int shift;

	char not_affected;

	int type_a_cnt;			// 0-inf
	int type_b_cnt;			// 0-inf
	int type_c_cnt;			// 0-inf
	int self_type;			// 0 | 1 | 2
	int type_id;			// 0-inf

	int mkt_spread;
	struct ptrades_tbl_t *trades;

	char trade;
	char buy;
	char sell;

	int iceberg_cnt;
	int iceberg_limit;

	int hedge_portfolio_id;
	struct portfolio_t *hedge_portfolio;
	int hedge_mltp;

#ifdef RTT_STAT
	struct timespec ordlog_listener_TN_COMMIT;
#endif

#ifdef PORTFOLIOSTAT_LOG
	int64_t pstat_wbid;
	int64_t pstat_wask;
#endif

#ifdef UNITTEST
	int64_t ut_volume;
#endif
};

#endif /* STRUCTURES_PORTFOLIO_T_H_ */
