#pragma pack(push, 4)


// Scheme "CustReplScheme" description




  struct orders_log
    {
        signed long long replID; // i8
        signed long long replRev; // i8
        signed long long replAct; // i8
        signed long long id_ord; // i8
        signed int amount_rest; // i4
        signed int sess_id; // i4
        signed int isin_id; // i4
        signed char dir; // i1
        signed char action; // i1
        signed long long xstatus; // i8
        char price[11]; // d16.5

    };

    const size_t sizeof_orders_log = 68;
    const size_t orders_log_index = 0;






#pragma pack(pop)
