/*
 * ordref_ex_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_ORDREF_EX_T_H_
#define STRUCTURES_ORDREF_EX_T_H_

struct ordref_ex_t{
	signed long long replID;
	signed long long replRev;
	signed int amount_rest;
	int agp_position;

	struct active_order_t *active;

#ifdef ORDLOG_DEBUG
	signed long long order_id;
#endif

	//signed char action;
	struct sess_data_row_t *sess_data;
	struct price_aggr_t *price_aggr;
};


#endif /* STRUCTURES_ORDREF_EX_T_H_ */
