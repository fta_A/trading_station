/*
 * ordref_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_ORDREF_TBL_T_H_
#define STRUCTURES_ORDREF_TBL_T_H_


struct ordref_tbl_t{
	struct ordref_ex_t **data;

	unsigned long long data_size;
	unsigned long long cnt;
	signed long long shift;

	signed long long replRev;
	signed long long sync_replRev;
	char sync;
	struct active_order_t *movingorder;
};

#endif /* STRUCTURES_ORDREF_TBL_T_H_ */
