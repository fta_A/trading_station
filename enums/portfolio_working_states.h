/*
 * portfolio_working_states.h
 *
 *  Created on: Feb 4, 2017
 *      Author: user
 */

#ifndef ENUMS_PORTFOLIO_WORKING_STATES_H_
#define ENUMS_PORTFOLIO_WORKING_STATES_H_


enum{
	WAIT_STATE = 0,
	MM_STATE = 1,
	INN_STATE = 2,
	SL_STATE = 3,
	SLFIN_STATE = 4,
	NEED_STOP_STATE = 5,
	STOP_AND_CLOSE_STATE = 6,
	RESET_AND_STOP_STATE = 7,
	NEED_START_STATE = 8
};



#endif /* ENUMS_PORTFOLIO_WORKING_STATES_H_ */
