/*
 * active_order_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_ACTIVE_ORDER_T_H_
#define STRUCTURES_ACTIVE_ORDER_T_H_


struct active_order_t{
	signed long long order_id;

	int state;
	int order_type;

	char option;

	signed int type;
	signed int amount;
	signed int amount_rest;

	signed int dir;

	int64_t price;

	//struct ordref_ex_t *ordref;


	struct quote_t *quote;

	struct active_order_t *next;
	struct active_order_t *prev;


//	struct active_order_t *next_aotbl;
//	struct active_order_t *prev_aotbl;

};

#endif /* STRUCTURES_ACTIVE_ORDER_T_H_ */
