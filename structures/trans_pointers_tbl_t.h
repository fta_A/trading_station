/*
 * trans_pointers_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_TRANS_POINTERS_TBL_T_H_
#define STRUCTURES_TRANS_POINTERS_TBL_T_H_

struct trans_pointers_tbl_t{
	long *data;
	uint data_size;
	uint first;

	char release;
	char free;
};


#endif /* STRUCTURES_TRANS_POINTERS_TBL_T_H_ */
