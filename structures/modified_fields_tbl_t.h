/*
 * modified_fields_tbl_t.h
 *
 *  Created on: Apr 29, 2017
 *      Author: user
 */

#ifndef STRUCTURES_MODIFIED_FIELDS_TBL_T_H_
#define STRUCTURES_MODIFIED_FIELDS_TBL_T_H_

struct modified_fields_tbl_t{
	struct field_value_t **data;
	uint data_size;
	uint cnt;
};


#endif /* STRUCTURES_MODIFIED_FIELDS_TBL_T_H_ */
