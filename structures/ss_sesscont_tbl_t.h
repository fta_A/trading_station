/*
 * ss_sesscont_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_SS_SESSCONT_TBL_T_H_
#define STRUCTURES_SS_SESSCONT_TBL_T_H_



struct ss_sesscont_tbl_t{
	struct sesscont_ex_t **data;

	unsigned long long data_size;
	unsigned long long cnt;
	signed long long shift;

	struct min_max repl_id;
	struct min_max isin_id;
	struct min_max sess_id;
};

#endif /* STRUCTURES_SS_SESSCONT_TBL_T_H_ */
