/*
 * kernel_helpers.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef MACROSES_KERNEL_HELPERS_H_
#define MACROSES_KERNEL_HELPERS_H_



#define RELEASE_ACTIVE_ORDER \
{ \
	active->prev = NULL; \
	active->next = NULL; \
	orders_pool_tbl->last->next = active; \
	orders_pool_tbl->last = active; \
	trans->active_order = NULL; \
}

#define RELEASE_ADD_TRANS \
{ \
	active->quote->isin_data->msg_data_last->next = trans->p2msg; \
	active->quote->isin_data->msg_data_last = trans->p2msg; \
	trans->p2msg->next = NULL; \
	trans->p2msg = NULL; \
}

#define RELEASE_MOVE_TRANS \
{ \
	if(active->option){ \
		optmovemsgs_tbl->msg_data_last->next = trans->p2msg; \
		optmovemsgs_tbl->msg_data_last = trans->p2msg; \
		trans->p2msg->next = NULL; \
		trans->p2msg = NULL; \
	}else{ \
		futmovemsgs_tbl->msg_data_last->next = trans->p2msg; \
		futmovemsgs_tbl->msg_data_last = trans->p2msg; \
		trans->p2msg->next = NULL; \
		trans->p2msg = NULL; \
	} \
	trans->active_order = NULL; \
	trans->active_order2 = NULL; \
}

#define RELEASE_DEL_TRANS \
{ \
	if(active->option){ \
		optdelmsgs_tbl->msg_data_last->next = trans->p2msg; \
		optdelmsgs_tbl->msg_data_last = trans->p2msg; \
		trans->p2msg->next = NULL; \
		trans->p2msg = NULL; \
	}else{ \
		futdelmsgs_tbl->msg_data_last->next = trans->p2msg; \
		futdelmsgs_tbl->msg_data_last = trans->p2msg; \
		trans->p2msg->next = NULL; \
		trans->p2msg = NULL; \
	} \
	trans->active_order = NULL; \
}

#define EXIT_MEMORY_ALLOC_ERROR(line, func) \
{ \
	printf("MEMORY_ALLOC_ERROR: %d, %s\n", line, func); \
	exit (MEMORY_ALLOC_ERROR); \
}


#define CHECK_FAIL(f, msg) \
	{ \
	uint32_t cf_res = f; \
	if (cf_res != CG_ERR_OK) \
		{ \
		cg_log_error("Client gate error (%s): %x\n", msg, cf_res); \
		exit(1); \
		} \
	}

#define WARN_FAIL(f, msg) \
	{ \
	uint32_t wf_res = f; \
	if (wf_res != CG_ERR_OK) \
		cg_log_error("Error (%s): %x\n", msg, wf_res); \
	}

#define BREAK_FAIL(f, msg) \
	{ \
	uint32_t res = f; \
	if (res != CG_ERR_OK) \
		{ \
		cg_log_error("Client gate error (%s): %x\n", msg, res); \
		continue; \
		} \
	}

#endif /* MACROSES_KERNEL_HELPERS_H_ */
