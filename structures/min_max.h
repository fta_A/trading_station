/*
 * min_max.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_MIN_MAX_H_
#define STRUCTURES_MIN_MAX_H_



struct min_max{
	signed long long min;
	signed long long max;

#ifdef RTT_STAT
	signed long long avg;
	signed long long cnt;
#endif
};

#endif /* STRUCTURES_MIN_MAX_H_ */
