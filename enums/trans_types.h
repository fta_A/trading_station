/*
 * trans_types.h
 *
 *  Created on: Feb 4, 2017
 *      Author: user
 */

#ifndef ENUMS_TRANS_TYPES_H_
#define ENUMS_TRANS_TYPES_H_



enum{
	ADDORDER_TTYPE = 1,
	MOVEORDER_TTYPE = 2,
	DELORDER_TTYPE = 3
};


#endif /* ENUMS_TRANS_TYPES_H_ */
