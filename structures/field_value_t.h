/*
 * field_value_t.h
 *
 *  Created on: Apr 29, 2017
 *      Author: user
 */

#ifndef STRUCTURES_FIELD_VALUE_T_H_
#define STRUCTURES_FIELD_VALUE_T_H_

struct field_value_t{
	uint owner;			/// Message type
	uint owner_id;
	uint field;

	size_t data_size;	/// Data size
	void* data;			/// Data pointer
};


#endif /* STRUCTURES_FIELD_VALUE_T_H_ */
