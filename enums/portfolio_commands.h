/*
 * portfolio_commands.h
 *
 *  Created on: Apr 29, 2017
 *      Author: user
 */

#ifndef ENUMS_PORTFOLIO_COMMANDS_H_
#define ENUMS_PORTFOLIO_COMMANDS_H_

enum {
	UNKNOWN_COMMAND = 0,
	NEED_START_COMMAND = 1,
	NEED_STOP_COMMAND = 2,
	STOP_AND_CLOSE_COMMAND = 3,
	RESET_AND_STOP_COMMAND = 4,
	SET_MM_WORKTYPE_COMMAND = 5,
	SET_FR_WORKTYPE_COMMAND = 6,
	SYSTEM_START_COMMAND = 7,
	SYSTEM_STOP_COMMAND = 8,
	SYNCHRONIZE_COMMAND = 9
};


#endif /* ENUMS_PORTFOLIO_COMMANDS_H_ */
