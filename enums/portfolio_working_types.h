/*
 * portfolio_working_types.h
 *
 *  Created on: Feb 4, 2017
 *      Author: user
 */

#ifndef ENUMS_PORTFOLIO_WORKING_TYPES_H_
#define ENUMS_PORTFOLIO_WORKING_TYPES_H_




enum{
	PORTFOLIOWORKTYPE_SLEEP = 0,
	PORTFOLIOWORKTYPE_FOLLOW_TREND = 1,
	PORTFOLIOWORKTYPE_PATTERN = 2,
	PORTFOLIOWORKTYPE_PINGPONG = 3,
	PORTFOLIOWORKTYPE_RTT = 4,
//	PORTFOLIOWORKTYPE_FOLLOW_TREND_EX = 5
};

#endif /* ENUMS_PORTFOLIO_WORKING_TYPES_H_ */
