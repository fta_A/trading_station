/*
 * p2msgs_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_P2MSGS_T_H_
#define STRUCTURES_P2MSGS_T_H_



struct p2msgs_t{
	struct cg_msg_data_ex_t **data;

	unsigned long long data_size;
	unsigned long long cnt;
	signed long long shift;
};

#endif /* STRUCTURES_P2MSGS_T_H_ */
