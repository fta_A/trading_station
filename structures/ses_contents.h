/*
 * ses_contents.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_SES_CONTENTS_H_
#define STRUCTURES_SES_CONTENTS_H_


#pragma pack(push, 4)
struct sess_contents{
	// Future часть
	signed long long replID; // i8
	signed long long replRev; // i8
	signed long long replAct; // i8
	signed int sess_id; // i4
	signed int isin_id; // i4
	char isin[26]; // c25
	signed int roundto; // i4
	char min_step[11]; // d16.5
	char step_price[11]; // d16.5
	signed int signs; // i4
	signed char is_limited; // i1
	char limit_up[11]; // d16.5
	char limit_down[11]; // d16.5
	char last_cl_quote[11]; // d16.5
	char step_price_curr[11]; // d16.5

	// Option часть
    signed int fut_isin_id; // i4
    signed char europe; // i1
    signed char put; // i1
    char strike[11]; // d16.5

    char option; // i1
};
#pragma pack(pop)


#endif /* STRUCTURES_SES_CONTENTS_H_ */
