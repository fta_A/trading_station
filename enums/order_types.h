/*
 * order_types.h
 *
 *  Created on: Feb 4, 2017
 *      Author: user
 */

#ifndef ENUMS_ORDER_TYPES_H_
#define ENUMS_ORDER_TYPES_H_


enum{
	ORDERTYPE_MARKET_MAKER = 1,
	ORDERTYPE_STOP_LOSS = 2,
	ORDERTYPE_COMMON = 3,
	ORDERTYPE_TAKE_PROFIT = 4
};

#endif /* ENUMS_ORDER_TYPES_H_ */
