/*
 * sesscont_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_SESSCONT_TBL_T_H_
#define STRUCTURES_SESSCONT_TBL_T_H_



struct sesscont_tbl_t{
	struct sesscont_ex_t **data;

	unsigned long long data_size;
	unsigned long long cnt;
	signed long long shift;
};

#endif /* STRUCTURES_SESSCONT_TBL_T_H_ */
