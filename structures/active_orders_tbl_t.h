/*
 * active_orders_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_ACTIVE_ORDERS_TBL_T_H_
#define STRUCTURES_ACTIVE_ORDERS_TBL_T_H_



struct active_orders_tbl_t{
	struct active_order_t **data;

	unsigned long long data_size;
	unsigned long long cnt;
	signed long long shift;
};

#endif /* STRUCTURES_ACTIVE_ORDERS_TBL_T_H_ */
