#pragma pack(push, 4)

#ifndef _message_H_
#define _message_H_

// Scheme "message" description




    struct FutAddOrder
    {
        char broker_code[5]; // c4
        char isin[26]; // c25
        char client_code[4]; // c3
        signed int type; // i4
        signed int dir; // i4
        signed int amount; // i4
        char price[18]; // c17
        char comment[21]; // c20
        char broker_to[21]; // c20
        signed int ext_id; // i4
        signed int du; // i4
        char date_exp[9]; // c8
        signed int hedge; // i4
        signed int dont_check_money; // i4
        struct cg_time_t local_stamp; // t
        char match_ref[11]; // c10
        
    };

    const size_t sizeof_FutAddOrder = 160;
    const size_t FutAddOrder_index = 0;

    const unsigned int FutAddOrder_msgid = 64;


    struct FORTS_MSG101
    {
        signed int code; // i4
        char message[256]; // c255
        signed long long order_id; // i8
        
    };

    const size_t sizeof_FORTS_MSG101 = 268;
    const size_t FORTS_MSG101_index = 1;

    const unsigned int FORTS_MSG101_msgid = 101;


    struct FutAddMultiLegOrder
    {
        char broker_code[5]; // c4
        signed int sess_id; // i4
        signed int isin_id; // i4
        char client_code[4]; // c3
        signed int type; // i4
        signed int dir; // i4
        signed int amount; // i4
        char price[18]; // c17
        char rate_price[18]; // c17
        char comment[21]; // c20
        signed int hedge; // i4
        char broker_to[21]; // c20
        signed int ext_id; // i4
        signed int trust; // i4
        char date_exp[9]; // c8
        signed int trade_mode; // i4
        signed int dont_check_money; // i4
        struct cg_time_t local_stamp; // t
        char match_ref[11]; // c10
        
    };

    const size_t sizeof_FutAddMultiLegOrder = 172;
    const size_t FutAddMultiLegOrder_index = 2;

    const unsigned int FutAddMultiLegOrder_msgid = 65;


    struct FORTS_MSG129
    {
        signed int code; // i4
        char message[256]; // c255
        signed long long order_id; // i8
        
    };

    const size_t sizeof_FORTS_MSG129 = 268;
    const size_t FORTS_MSG129_index = 3;

    const unsigned int FORTS_MSG129_msgid = 129;


    struct FutDelOrder
    {
        char broker_code[5]; // c4
        signed long long order_id; // i8
        struct cg_time_t local_stamp; // t
        
    };

    const size_t sizeof_FutDelOrder = 28;
    const size_t FutDelOrder_index = 4;

    const unsigned int FutDelOrder_msgid = 37;


    struct FORTS_MSG102
    {
        signed int code; // i4
        char message[256]; // c255
        signed int amount; // i4
        
    };

    const size_t sizeof_FORTS_MSG102 = 264;
    const size_t FORTS_MSG102_index = 5;

    const unsigned int FORTS_MSG102_msgid = 102;


    struct FutDelUserOrders
    {
        char broker_code[5]; // c4
        signed int buy_sell; // i4
        signed int non_system; // i4
        char code[4]; // c3
        char code_vcb[26]; // c25
        signed int ext_id; // i4
        signed int work_mode; // i4
        char isin[26]; // c25
        struct cg_time_t local_stamp; // t
        
    };

    const size_t sizeof_FutDelUserOrders = 92;
    const size_t FutDelUserOrders_index = 6;

    const unsigned int FutDelUserOrders_msgid = 38;


    struct FORTS_MSG103
    {
        signed int code; // i4
        char message[256]; // c255
        signed int num_orders; // i4
        
    };

    const size_t sizeof_FORTS_MSG103 = 264;
    const size_t FORTS_MSG103_index = 7;

    const unsigned int FORTS_MSG103_msgid = 103;


    struct FutMoveOrder
    {
        char broker_code[5]; // c4
        signed int regime; // i4
        signed long long order_id1; // i8
        signed int amount1; // i4
        char price1[18]; // c17
        signed int ext_id1; // i4
        signed long long order_id2; // i8
        signed int amount2; // i4
        char price2[18]; // c17
        signed int ext_id2; // i4
        struct cg_time_t local_stamp; // t
        
    };

    const size_t sizeof_FutMoveOrder = 96;
    const size_t FutMoveOrder_index = 8;

    const unsigned int FutMoveOrder_msgid = 39;


    struct FORTS_MSG105
    {
        signed int code; // i4
        char message[256]; // c255
        signed long long order_id1; // i8
        signed long long order_id2; // i8
        
    };

    const size_t sizeof_FORTS_MSG105 = 276;
    const size_t FORTS_MSG105_index = 9;

    const unsigned int FORTS_MSG105_msgid = 105;


    struct OptAddOrder
    {
        char broker_code[5]; // c4
        char isin[26]; // c25
        char client_code[4]; // c3
        signed int type; // i4
        signed int dir; // i4
        signed int amount; // i4
        char price[18]; // c17
        char comment[21]; // c20
        char broker_to[21]; // c20
        signed int ext_id; // i4
        signed int du; // i4
        signed int check_limit; // i4
        char date_exp[9]; // c8
        signed int hedge; // i4
        signed int dont_check_money; // i4
        struct cg_time_t local_stamp; // t
        char match_ref[11]; // c10
        
    };

    const size_t sizeof_OptAddOrder = 164;
    const size_t OptAddOrder_index = 10;

    const unsigned int OptAddOrder_msgid = 66;


    struct FORTS_MSG109
    {
        signed int code; // i4
        char message[256]; // c255
        signed long long order_id; // i8
        
    };

    const size_t sizeof_FORTS_MSG109 = 268;
    const size_t FORTS_MSG109_index = 11;

    const unsigned int FORTS_MSG109_msgid = 109;


    struct OptDelOrder
    {
        char broker_code[5]; // c4
        signed long long order_id; // i8
        struct cg_time_t local_stamp; // t
        
    };

    const size_t sizeof_OptDelOrder = 28;
    const size_t OptDelOrder_index = 12;

    const unsigned int OptDelOrder_msgid = 42;


    struct FORTS_MSG110
    {
        signed int code; // i4
        char message[256]; // c255
        signed int amount; // i4
        
    };

    const size_t sizeof_FORTS_MSG110 = 264;
    const size_t FORTS_MSG110_index = 13;

    const unsigned int FORTS_MSG110_msgid = 110;


    struct OptDelUserOrders
    {
        char broker_code[5]; // c4
        signed int buy_sell; // i4
        signed int non_system; // i4
        char code[4]; // c3
        char code_vcb[26]; // c25
        signed int ext_id; // i4
        signed int work_mode; // i4
        char isin[26]; // c25
        struct cg_time_t local_stamp; // t
        
    };

    const size_t sizeof_OptDelUserOrders = 92;
    const size_t OptDelUserOrders_index = 14;

    const unsigned int OptDelUserOrders_msgid = 43;


    struct FORTS_MSG111
    {
        signed int code; // i4
        char message[256]; // c255
        signed int num_orders; // i4
        
    };

    const size_t sizeof_FORTS_MSG111 = 264;
    const size_t FORTS_MSG111_index = 15;

    const unsigned int FORTS_MSG111_msgid = 111;


    struct OptMoveOrder
    {
        char broker_code[5]; // c4
        signed int regime; // i4
        signed long long order_id1; // i8
        signed int amount1; // i4
        char price1[18]; // c17
        signed int ext_id1; // i4
        signed int check_limit; // i4
        signed long long order_id2; // i8
        signed int amount2; // i4
        char price2[18]; // c17
        signed int ext_id2; // i4
        struct cg_time_t local_stamp; // t
        
    };

    const size_t sizeof_OptMoveOrder = 100;
    const size_t OptMoveOrder_index = 16;

    const unsigned int OptMoveOrder_msgid = 44;


    struct FORTS_MSG113
    {
        signed int code; // i4
        char message[256]; // c255
        signed long long order_id1; // i8
        signed long long order_id2; // i8
        
    };

    const size_t sizeof_FORTS_MSG113 = 276;
    const size_t FORTS_MSG113_index = 17;

    const unsigned int FORTS_MSG113_msgid = 113;


    struct FutChangeClientMoney
    {
        char broker_code[5]; // c4
        signed int mode; // i4
        char code[4]; // c3
        char limit_money[18]; // c17
        char limit_pledge[18]; // c17
        char coeff_liquidity[18]; // c17
        char coeff_go[18]; // c17
        signed int is_auto_update_limit; // i4
        signed int no_fut_discount; // i4
        signed int check_limit; // i4
        
    };

    const size_t sizeof_FutChangeClientMoney = 100;
    const size_t FutChangeClientMoney_index = 18;

    const unsigned int FutChangeClientMoney_msgid = 67;


    struct FORTS_MSG104
    {
        signed int code; // i4
        char message[256]; // c255
        
    };

    const size_t sizeof_FORTS_MSG104 = 260;
    const size_t FORTS_MSG104_index = 19;

    const unsigned int FORTS_MSG104_msgid = 104;


    struct FutChangeBFMoney
    {
        char broker_code[5]; // c4
        signed int mode; // i4
        char code[3]; // c2
        char limit_money[18]; // c17
        char limit_pledge[18]; // c17
        
    };

    const size_t sizeof_FutChangeBFMoney = 52;
    const size_t FutChangeBFMoney_index = 20;

    const unsigned int FutChangeBFMoney_msgid = 7;


    struct FORTS_MSG107
    {
        signed int code; // i4
        char message[256]; // c255
        
    };

    const size_t sizeof_FORTS_MSG107 = 260;
    const size_t FORTS_MSG107_index = 21;

    const unsigned int FORTS_MSG107_msgid = 107;


    struct OptChangeExpiration
    {
        char broker_code[5]; // c4
        signed int mode; // i4
        signed int order_id; // i4
        char code[4]; // c3
        char isin[26]; // c25
        signed int amount; // i4
        
    };

    const size_t sizeof_OptChangeExpiration = 52;
    const size_t OptChangeExpiration_index = 22;

    const unsigned int OptChangeExpiration_msgid = 12;


    struct FORTS_MSG112
    {
        signed int code; // i4
        char message[256]; // c255
        signed int order_id; // i4
        
    };

    const size_t sizeof_FORTS_MSG112 = 264;
    const size_t FORTS_MSG112_index = 23;

    const unsigned int FORTS_MSG112_msgid = 112;


    struct FutChangeClientProhibit
    {
        char broker_code[5]; // c4
        signed int mode; // i4
        char code[4]; // c3
        char code_vcb[26]; // c25
        char isin[26]; // c25
        signed int state; // i4
        signed int state_mask; // i4
        
    };

    const size_t sizeof_FutChangeClientProhibit = 76;
    const size_t FutChangeClientProhibit_index = 24;

    const unsigned int FutChangeClientProhibit_msgid = 15;


    struct FORTS_MSG115
    {
        signed int code; // i4
        char message[256]; // c255
        
    };

    const size_t sizeof_FORTS_MSG115 = 260;
    const size_t FORTS_MSG115_index = 25;

    const unsigned int FORTS_MSG115_msgid = 115;


    struct OptChangeClientProhibit
    {
        char broker_code[5]; // c4
        signed int mode; // i4
        char code[4]; // c3
        char code_vcb[26]; // c25
        char isin[26]; // c25
        signed int state; // i4
        signed int state_mask; // i4
        
    };

    const size_t sizeof_OptChangeClientProhibit = 76;
    const size_t OptChangeClientProhibit_index = 26;

    const unsigned int OptChangeClientProhibit_msgid = 17;


    struct FORTS_MSG117
    {
        signed int code; // i4
        char message[256]; // c255
        
    };

    const size_t sizeof_FORTS_MSG117 = 260;
    const size_t FORTS_MSG117_index = 27;

    const unsigned int FORTS_MSG117_msgid = 117;


    struct FutExchangeBFMoney
    {
        char broker_code[5]; // c4
        signed int mode; // i4
        char code_from[3]; // c2
        char code_to[3]; // c2
        char amount_money[18]; // c17
        char amount_pledge[18]; // c17
        
    };

    const size_t sizeof_FutExchangeBFMoney = 56;
    const size_t FutExchangeBFMoney_index = 28;

    const unsigned int FutExchangeBFMoney_msgid = 35;


    struct FORTS_MSG130
    {
        signed int code; // i4
        char message[256]; // c255
        
    };

    const size_t sizeof_FORTS_MSG130 = 260;
    const size_t FORTS_MSG130_index = 29;

    const unsigned int FORTS_MSG130_msgid = 130;


    struct OptRecalcCS
    {
        char broker_code[5]; // c4
        signed int isin_id; // i4
        
    };

    const size_t sizeof_OptRecalcCS = 12;
    const size_t OptRecalcCS_index = 30;

    const unsigned int OptRecalcCS_msgid = 45;


    struct FORTS_MSG132
    {
        signed int code; // i4
        char message[256]; // c255
        
    };

    const size_t sizeof_FORTS_MSG132 = 260;
    const size_t FORTS_MSG132_index = 31;

    const unsigned int FORTS_MSG132_msgid = 132;


    struct FutTransferClientPosition
    {
        char broker_code[5]; // c4
        char code_from[8]; // c7
        char code_to[8]; // c7
        char isin[26]; // c25
        signed int amount; // i4
        
    };

    const size_t sizeof_FutTransferClientPosition = 52;
    const size_t FutTransferClientPosition_index = 32;

    const unsigned int FutTransferClientPosition_msgid = 61;


    struct FORTS_MSG137
    {
        signed int code; // i4
        char message[256]; // c255
        
    };

    const size_t sizeof_FORTS_MSG137 = 260;
    const size_t FORTS_MSG137_index = 33;

    const unsigned int FORTS_MSG137_msgid = 137;


    struct OptTransferClientPosition
    {
        char broker_code[5]; // c4
        char code_from[8]; // c7
        char code_to[8]; // c7
        char isin[26]; // c25
        signed int amount; // i4
        
    };

    const size_t sizeof_OptTransferClientPosition = 52;
    const size_t OptTransferClientPosition_index = 34;

    const unsigned int OptTransferClientPosition_msgid = 62;


    struct FORTS_MSG138
    {
        signed int code; // i4
        char message[256]; // c255
        
    };

    const size_t sizeof_FORTS_MSG138 = 260;
    const size_t FORTS_MSG138_index = 35;

    const unsigned int FORTS_MSG138_msgid = 138;


    struct OptChangeRiskParameters
    {
        char broker_code[5]; // c4
        char client_code[4]; // c3
        signed int num_clr_2delivery; // i4
        signed char use_broker_num_clr_2delivery; // i1
        char exp_weight[5]; // c4
        signed char use_broker_exp_weight; // i1
        
    };

    const size_t sizeof_OptChangeRiskParameters = 24;
    const size_t OptChangeRiskParameters_index = 36;

    const unsigned int OptChangeRiskParameters_msgid = 69;


    struct FORTS_MSG140
    {
        signed int code; // i4
        char message[256]; // c255
        
    };

    const size_t sizeof_FORTS_MSG140 = 260;
    const size_t FORTS_MSG140_index = 37;

    const unsigned int FORTS_MSG140_msgid = 140;


    struct FutTransferRisk
    {
        char broker_code[5]; // c4
        char code_from[8]; // c7
        char isin[26]; // c25
        signed int amount; // i4
        
    };

    const size_t sizeof_FutTransferRisk = 44;
    const size_t FutTransferRisk_index = 38;

    const unsigned int FutTransferRisk_msgid = 68;


    struct FORTS_MSG139
    {
        signed int code; // i4
        char message[256]; // c255
        signed long long deal_id1; // i8
        signed long long deal_id2; // i8
        
    };

    const size_t sizeof_FORTS_MSG139 = 276;
    const size_t FORTS_MSG139_index = 39;

    const unsigned int FORTS_MSG139_msgid = 139;


    struct CODHeartbeat
    {
        signed int seq_number; // i4
        
    };

    const size_t sizeof_CODHeartbeat = 4;
    const size_t CODHeartbeat_index = 40;

    const unsigned int CODHeartbeat_msgid = 10000;


    struct FORTS_MSG99
    {
        signed int queue_size; // i4
        signed int penalty_remain; // i4
        char message[129]; // c128
        
    };

    const size_t sizeof_FORTS_MSG99 = 140;
    const size_t FORTS_MSG99_index = 41;

    const unsigned int FORTS_MSG99_msgid = 99;


    struct FORTS_MSG100
    {
        signed int code; // i4
        char message[256]; // c255
        
    };

    const size_t sizeof_FORTS_MSG100 = 260;
    const size_t FORTS_MSG100_index = 42;

    const unsigned int FORTS_MSG100_msgid = 100;



#endif //_message_H_

#pragma pack(pop)
