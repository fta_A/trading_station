/*
 * order_states.h
 *
 *  Created on: Feb 4, 2017
 *      Author: user
 */

#ifndef ENUMS_ORDER_STATES_H_
#define ENUMS_ORDER_STATES_H_


enum{
	ORDER_ACTIVE = 0,
	ORDER_MOVING = 1,
	ORDER_KILLING = 2,
	ORDER_ADDING = 3,
	ORDER_KILLED = 4,
//	MOVE_NOT_FOUND = 5,
//	KILL_NOT_FOUND = 6
};

#endif /* ENUMS_ORDER_STATES_H_ */
