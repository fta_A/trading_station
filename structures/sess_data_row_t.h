/*
 * sess_data_row_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_SESS_DATA_ROW_T_H_
#define STRUCTURES_SESS_DATA_ROW_T_H_

struct sess_data_row_t{
	struct prices_tbl_t *bidEx;
	struct prices_tbl_t *askEx;

	int64_t limit_down;
	int64_t limit_up;

	int ords_cnt;
	struct sesscont_ex_t *cont;
	char used;

	char destroyed;
};

#endif /* STRUCTURES_SESS_DATA_ROW_T_H_ */
