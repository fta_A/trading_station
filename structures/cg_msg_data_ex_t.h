/*
 * cg_msg_data_ex_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_CG_MSG_DATA_EX_T_H_
#define STRUCTURES_CG_MSG_DATA_EX_T_H_




struct cg_msg_data_ex_t{
	struct cg_msg_data_t *origin;

    signed int *type; // i4
    signed int *dir; // i4
    signed int *amount; // i4
    char *price; // c17
    signed long long *order_id;


    signed long long *order_id2;
    signed int *amount2; // i4
    char *price2; // c17

    signed int *regime; // i4

    char option;

    struct cg_msg_data_ex_t *next;
};

#endif /* STRUCTURES_CG_MSG_DATA_EX_T_H_ */
