/*
 * log_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_LOG_T_H_
#define STRUCTURES_LOG_T_H_


struct log_t{
	int log_id;
	char *msg;
	size_t msg_size;
};

#endif /* STRUCTURES_LOG_T_H_ */
