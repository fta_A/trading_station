/*
 * fields_list.h
 *
 *  Created on: Apr 29, 2017
 *      Author: user
 */

#ifndef ENUMS_FIELDS_LIST_H_
#define ENUMS_FIELDS_LIST_H_

enum FIELDS_LIST{
	COMMAND_FIELD = 0,
	TRADE_FIELD = 1,
	STATE_FIELD = 2,
	WORKTYPE_FIELD = 3
};


#endif /* ENUMS_FIELDS_LIST_H_ */
