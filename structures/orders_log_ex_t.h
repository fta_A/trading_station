/*
 * orders_log_ex_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_ORDERS_LOG_EX_T_H_
#define STRUCTURES_ORDERS_LOG_EX_T_H_




#pragma pack(push, 4)
struct orders_log_ex_t{
	signed long long replID; // i8
	signed long long replRev; // i8
	signed long long replAct; // i8
	signed long long id_ord; // i8
	signed int amount_rest; // i4
	signed int sess_id; // i4
    signed int isin_id; // i4
	signed char dir; // i1

	int64_t price_int;
	int8_t price_scale;



	struct orders_log_ex_t *next;

};
#pragma pack(pop)

#endif /* STRUCTURES_ORDERS_LOG_EX_T_H_ */
