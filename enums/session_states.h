/*
 * session_states.h
 *
 *  Created on: Feb 4, 2017
 *      Author: user
 */

#ifndef ENUMS_SESSION_STATES_H_
#define ENUMS_SESSION_STATES_H_


enum{
	UNKNOWN_SESST = -1,
	SETTED_SESST = 0,
	ACTIVE_SESST = 1,
	SUSPENDED_SESST = 2,
	FORCEDFINISHED_SESST = 3,
	FINISHED_SESST = 4
};


#endif /* ENUMS_SESSION_STATES_H_ */
