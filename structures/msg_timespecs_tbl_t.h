/*
 * msg_timespecs_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_MSG_TIMESPECS_TBL_T_H_
#define STRUCTURES_MSG_TIMESPECS_TBL_T_H_


struct msg_timespecs_tbl_t{
	struct timespec *data;
	uint first;
	uint data_size;
};

#endif /* STRUCTURES_MSG_TIMESPECS_TBL_T_H_ */
