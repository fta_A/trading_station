/*
 * debug_functions.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef MACROSES_DEBUG_FUNCTIONS_H_
#define MACROSES_DEBUG_FUNCTIONS_H_

#ifdef ORDLOG_DEBUG
#define DEBUG_FUNC(f) \
	{ \
	 f; \
	}
#else
#define DEBUG_FUNC(f)
#endif


#if defined(LOG_SAVER) && defined(TRANS_LOG)
#define TRANSLOG_FUNC(f) \
	{ \
	 f; \
	}
#else
#define TRANSLOG_FUNC(f)
#endif

#if defined(LOG_SAVER) && defined(LOGIC_LOG)
#define LOGICLOG_FUNC(f) \
	{ \
	 f; \
	}
#else
#define LOGICLOG_FUNC(f)
#endif

#if defined(LOG_SAVER) && defined(ORDERS_LOG)
#define ORDERSLOG_FUNC(f) \
	{ \
	 f; \
	}
#else
#define ORDERSLOG_FUNC(f)
#endif


#if defined(LOG_SAVER) && defined(FUTINFO_LOG)
#define FUTINFO_FUNC(f) \
	{ \
	 f; \
	}
#else
#define FUTINFO_FUNC(f)
#endif

#if defined(LOG_SAVER) && defined(KERNEL_LOG)
#define KERNEL_FUNC(f) \
	{ \
	 f; \
	}
#else
#define KERNEL_FUNC(f)
#endif

#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
#define STREAMLOG_FUNC(f) \
	{ \
	 f; \
	}
#else
#define STREAMLOG_FUNC(f)
#endif

#if defined(LOG_SAVER) && defined(PORTFOLIOSTAT_LOG)
#define PSTAT_FUNC(f) \
	{ \
	 f; \
	}
#else
#define PSTAT_FUNC(f)
#endif


#endif /* MACROSES_DEBUG_FUNCTIONS_H_ */
