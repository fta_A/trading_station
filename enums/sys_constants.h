/*
 * sys_constants.h
 *
 *  Created on: Feb 4, 2017
 *      Author: user
 */

#ifndef ENUMS_SYS_CONSTANTS_H_
#define ENUMS_SYS_CONSTANTS_H_


enum {
	ORDLOGSIZE_INCREMENT = 100000,

	SESSCONTSIZE_INCREMENT = 100,
	ISINSSIZE_INCREMENT = 100,
	SESSSIZE_INCREMENT = 1,

	MAX_PRICE_SCALE = 10,

	SIMULTANEOUSLY_SENT_TRANS = 1000,

	AFFECTED_ISINS = 1000,

	FLOOD_PERIOD_SEC = 1,
	MAX_FLOOD_MSGS = 30,

	MAX_QUOTES_PER_PORTFOLIO = 50,
	MAX_PORTFOLIOS = 100,

	TRANSACTIONS_CNT = 100000,
	SIMULTANEOUSLY_ACTIVE_ORDERS = 10000,


	LOGS_CNT = 10000,
	MAX_LOGSTRING_SIZE = 511,

	MAX_PINGPONG_TRADES = 5,
	MAX_RND_VALUES = 1000000,

	EXTERNAL_EVENTS_NUM = 100
};

#endif /* ENUMS_SYS_CONSTANTS_H_ */
