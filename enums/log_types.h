/*
 * log_types.h
 *
 *  Created on: Feb 4, 2017
 *      Author: user
 */

#ifndef ENUMS_LOG_TYPES_H_
#define ENUMS_LOG_TYPES_H_


enum{
	UNKNOWN_LOG_ID = 0,
	TRANS_LOG_ID = 1,
	LOGIC_LOG_ID = 2,
	ORDERS_LOG_ID = 3,
	FUTINFO_LOG_ID = 4,
	KERNEL_LOG_ID = 5,
	FUTINFO_STREAM_LOG_ID = 6,
	ORDERS_STREAM_LOG_ID = 7,
	PORTFOLIOSTAT_LOG_ID = 8,
	TRANS_STREAM_LOG_ID = 9,

	MAX_LOG_ID = 10
};

#endif /* ENUMS_LOG_TYPES_H_ */
