/*
 * ordref_list_t.h
 *
 *  Created on: Mar 14, 2017
 *      Author: user
 */

#ifndef STRUCTURES_ORDREF_LIST_T_H_
#define STRUCTURES_ORDREF_LIST_T_H_

struct ordref_list_t{
	struct ordref_ex_t *ordref;
	struct ordref_list_t *next;
	struct ordref_list_t *prev;
};

#endif /* STRUCTURES_ORDREF_LIST_T_H_ */
