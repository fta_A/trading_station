/*
 * ss_ordref_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_SS_ORDREF_TBL_T_H_
#define STRUCTURES_SS_ORDREF_TBL_T_H_



struct ss_ordref_tbl_t{
	struct orders_log_ex_t **data;

	unsigned long long data_size;
	unsigned long long cnt;
	signed long long shift;

	struct min_max order_id;
//	struct min_max repl_rev;
};

#endif /* STRUCTURES_SS_ORDREF_TBL_T_H_ */
