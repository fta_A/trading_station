/*
 * quote_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_QUOTE_T_H_
#define STRUCTURES_QUOTE_T_H_



struct quote_t{
	int quote_id;
	int portfolio_id;

	signed int isin_id;

	int weight;

	int64_t wbid;
	int64_t wask;

	int64_t ask_tgt;
	int64_t bid_tgt;

	int64_t pt_value;

	int8_t price_scale;
	int8_t ptv_scale;

	int64_t minstep;

	char is_left;
	char is_quanto;
	char qnt_type;

	struct quote_t *next;
	struct portfolio_t *portfolio;
	struct isin_data_tbl_t *isin_data;
};


#endif /* STRUCTURES_QUOTE_T_H_ */
