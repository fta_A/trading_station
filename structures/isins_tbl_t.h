/*
 * isins_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_ISINS_TBL_T_H_
#define STRUCTURES_ISINS_TBL_T_H_


struct isins_tbl_t{
	struct isin_data_tbl_t **data;

	unsigned long data_size;
	unsigned long cnt;
	signed int shift;

	struct min_max sess_id;
	unsigned long ext_links;
//	unsigned long long bytes_used;
};

#endif /* STRUCTURES_ISINS_TBL_T_H_ */
