/*
 * isin_data_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_ISIN_DATA_TBL_T_H_
#define STRUCTURES_ISIN_DATA_TBL_T_H_


struct isin_data_tbl_t{
	struct sess_data_row_t **data;

	struct quotes_tbl_t *quotes_tbl;
	struct quotes_tbl_t *qnt_quotes_tbl;

	struct cg_msg_data_ex_t *msg_data;
	struct cg_msg_data_ex_t *msg_data_last;

	unsigned long data_size;
	unsigned long cnt;
	signed int shift;
};

#endif /* STRUCTURES_ISIN_DATA_TBL_T_H_ */
