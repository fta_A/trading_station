/*
 * settings_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_SETTINGS_T_H_
#define STRUCTURES_SETTINGS_T_H_


struct settings_t{
	char ccode[4];
	char port[6];
};

#endif /* STRUCTURES_SETTINGS_T_H_ */
