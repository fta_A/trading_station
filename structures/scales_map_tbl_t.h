/*
 * scales_map_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_SCALES_MAP_TBL_T_H_
#define STRUCTURES_SCALES_MAP_TBL_T_H_


struct scales_map_tbl_t{
	struct prices_map_tbl_t **data;

	int data_size;
//	int cnt;
	int shift;

	int size_of_price;
};

#endif /* STRUCTURES_SCALES_MAP_TBL_T_H_ */
