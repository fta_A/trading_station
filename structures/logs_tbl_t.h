/*
 * logs_tbl_t.h
 *
 *  Created on: Feb 3, 2017
 *      Author: user
 */

#ifndef STRUCTURES_LOGS_TBL_T_H_
#define STRUCTURES_LOGS_TBL_T_H_


struct logs_tbl_t{
	struct log_t **data;
	uint data_size;
	uint first;
};

#endif /* STRUCTURES_LOGS_TBL_T_H_ */
