#define ORDLOG_DEBUG

//#define RTT_STAT
//#define UNITTEST

//#define REPLY

//#define STREAMS_LOG


//#define PORTFOLIOSTAT_LOG
#define LOG_SAVER
#define TRANS_LOG
#define LOGIC_LOG
#define FUTINFO_LOG
//#define KERNEL_LOG
//#define ORDERS_LOG
#define NO_OPTIONS
#define IGNORE_UNKNOWN_ORDERS

//#define TERMINAL_LOG

//#define RANDOM_MOVE


//////////////////////////////////////////////////////////////
// System headers
//////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <limits.h>
#include <math.h>
//#include <numa.h>
#include <libconfig.h>

#include <termios.h>
#include <unistd.h>


//////////////////////////////////////////////////////////////
// CGate headers
//////////////////////////////////////////////////////////////
#include <cgate.h>
#include "include/ordlog_scheme.h"
#include "include/futinfo_scheme.h"
#include "include/optinfo_scheme.h"
#include "include/messages_scheme.h"

//////////////////////////////////////////////////////////////
// Internal structures headers
//////////////////////////////////////////////////////////////
#include "structures/structures.h"

//////////////////////////////////////////////////////////////
// Macroses
//////////////////////////////////////////////////////////////
#include "macroses/macroses.h"

//////////////////////////////////////////////////////////////
// Constants
//////////////////////////////////////////////////////////////
#include "enums/enums.h"

#define STRUCT_ORDLOG_SNAPSHOT_SIZE 45
#define STRUCT_ORDLOG_ONLINE_SIZE 36






//
//Переменные
//

//Признак завершения работы всем нитям

char proceed = 1;
char ordlog_proceed = 1;
char alive = 3;

//Параметры ПО
//const char* cfg_cli_env = {"ini=ini/send.ini;key=nbb4LVPcISIc4skeEOCMuEpnJv6uuZyB"};
const char* cfg_cli_env = {"ini=ini/send.ini;key=11111111"};

//Параметры соединения со шлюзом
char cfg_ordlog_conn[] = {"p2tcp://127.0.0.1:4001;app_name=ordlog"};
cg_conn_t* ordlog_conn;

//p2tcp
char cfg_p2msg_conn[] = {"p2tcp://127.0.0.1:4001;app_name=send"};
cg_conn_t* p2msg_conn;

//Параметры получения данных от шлюза

//Таблица заявок по всем инструментам
const char* cfg_ordlog_listener = {"p2repl://FORTS_ORDLOG_REPL;scheme=|FILE|ini/ordLog_trades.ini|CustReplScheme"};
cg_listener_t* ordlog_listener;
uint32_t ordlog_listener_state;
struct timespec ordlog_listener_TN_BEGIN;
struct timespec ordlog_listener_TN_COMMIT;
char *ordlog_replstate;

//Таблица фьючерсов
const char* cfg_futinfo_listener = {"p2repl://FORTS_FUTINFO_REPL;scheme=|FILE|ini/fut_info.ini|CustReplScheme"};
cg_listener_t* futinfo_listener;
uint32_t futinfo_listener_state;
struct timespec futinfo_listener_TN_BEGIN;
struct timespec futinfo_listener_TN_COMMIT;
//int64_t futinfo_listener_lifenum;
char *futinfo_replstate;

const char* cfg_optinfo_listener = {"p2repl://FORTS_OPTINFO_REPL;scheme=|FILE|ini/opt_info.ini|CustReplScheme"};
cg_listener_t* optinfo_listener;
uint32_t optinfo_listener_state;
struct timespec optinfo_listener_TN_BEGIN;
struct timespec optinfo_listener_TN_COMMIT;

const char* cfg_repl_listener = {"p2mqreply://;ref=PUB"};
cg_listener_t* repl_listener;
uint32_t repl_listener_state;
struct timespec repl_listener_TN_BEGIN;
struct timespec repl_listener_TN_COMMIT;

const char* cfg_msg_publisher = {"p2mq://FORTS_SRV;category=FORTS_MSG;name=PUB;scheme=|FILE|ini/forts_messages.ini|message"};
cg_publisher_t* msg_publisher;
uint32_t msg_publisher_state;

//Хранилище данных


struct isins_tbl_t *isins_tbl;
struct sesscont_tbl_t *futsesscont_tbl;
struct ss_sesscont_tbl_t *ss_futsesscont_tbl;
struct sesscont_tbl_t *optsesscont_tbl;
struct ss_sesscont_tbl_t *ss_optsesscont_tbl;

struct session_tbl_t *session_tbl;
struct ss_session_tbl_t *ss_session_tbl;
struct affected_tbl_t *affected_tbl;
struct affected_tbl_t *qnt_affected_tbl;

struct ordref_tbl_t *ordref_tbl;
struct ss_ordref_tbl_t *ss_ordref_tbl;

#ifdef UNITTEST
struct ordref_list_t *ordref_tbl_UNITTEST;
#endif

struct quotes_tbl_t *quotes_tbl;
struct portfolios_tbl_t *portfolios_tbl;
struct affected_tbl_t *pts_affected_tbl;

struct trans_tbl_t *trans_tbl;
struct trans_pointers_tbl_t *new_trans_tbl;
struct trans_pointers_tbl_t *snt_trans_tbl;
struct trans_pointers_tbl_t *flood_trans_tbl;
struct trans_pointers_tbl_t *susp_trans_tbl;

struct msg_timespecs_tbl_t *snt_messages_tbl;
struct msg_timespecs_tbl_t *pp_snt_messages_tbl;

struct active_order_t *active_orders_tbl;
struct orders_pool_tbl_t *orders_pool_tbl;


struct active_orders_tbl_t *trans_thr_approved_orders_tbl;
struct active_orders_tbl_t *ordrs_thr_approved_orders_tbl;

struct ordref_tbl_t *modified_orders_tbl;

struct active_orders_tbl_t *ordrs_thr_empty_orders_tbl;
struct active_orders_tbl_t *trans_thr_empty_orders_tbl;

struct cg_msgs_data_tbl_t *futmovemsgs_tbl;
struct cg_msgs_data_tbl_t *futdelmsgs_tbl;
struct cg_msgs_data_tbl_t *optmovemsgs_tbl;
struct cg_msgs_data_tbl_t *optdelmsgs_tbl;

struct scales_map_tbl_t *price_to_str_tbl;


struct logs_tbl_t **new_logs_tbls_array;
struct logs_tbl_t **snt_logs_tbls_array;

struct modified_fields_tbl_t *new_modified_fields_tbl;
struct modified_fields_tbl_t *modified_fields_tbl;

struct trans_pointers_tbl_t *rnd_values_tbl;

int *pingpongtrades_tbl;
struct settings_t kern_settings;

#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
FILE *fp_futinfo_stream;
FILE *fp_ordlog_stream;
FILE *fp_IO_stream;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
long long io_event_id = 0;
#endif

//#if defined(REPLY)
pthread_mutex_t reply_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t reply_cond = PTHREAD_COND_INITIALIZER;
long long tr_events_cnt = 0;
//#endif

#if defined(STREAMS_LOG) && !defined(REPLY)
pthread_mutex_t io_order_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif

//
//

//
//Прототипы функций
//

void *thread_func(void *vptr_args);
void thread_func_forts_ordlog(void);
void thread_func_forts_ordlog_UNITTEST(void);
void thread_func_forts_ordlog_REPLY(void);

void thread_func_transactions(void);
void thread_func_transactions_UNITTEST(void);
void thread_func_transactions_REPLY(void);
void thread_func_transactions_loop_REPLY(void);
void thread_func_transactions_switch_approved_orders_REPLY(void);
void thread_func_logger(void);

inline void thread_func_forts_ordlog_other_job(void);


inline void thread_func_logger_save(int log_id, void *msg, size_t msg_size);
inline void thread_func_logger_flush(int log_id);

inline void thread_func_transactions_sendtrans(void);
inline void thread_func_transactions_sendtrans_UNITTEST(void);
inline void thread_func_transactions_sendtrans_REPLY(void);
inline void thread_func_transactions_clear_empty(void);
void thread_func_transactions_init_msgs(void);
void thread_func_transactions_killall(void);
void thread_func_transcations_free_msgs(void);
inline void thread_func_transactions_clear_suspended(void);

inline void fill_isins_tbl_entry(struct isin_data_tbl_t *isin_data, struct sess_data_row_t *sess_data);
inline void create_isins_tbl_entry(signed int *isin_id_p, signed int *sess_id_p);
inline void destroy_isins_tbl_entry(signed int isin_id, signed int sess_id);
inline void adjust_prices_map_tbl(int64_t limit_up_adj, int64_t limit_down_adj, int64_t scale_adj);
inline void rebuild_prices_map_tbl();
inline void portfolio_reset_trd_settings(struct portfolio_t *portfolio);

//Функция, в которой происходит первоначальное сохранение данных
CG_RESULT ordlog_event(cg_conn_t* conn, cg_listener_t* listener, struct cg_msg_t* msg, void* data);
inline void ordlog_event_snapshot(struct cg_msg_streamdata_t *replmsg);
inline void ordlog_event_initial_snapshot(struct cg_msg_streamdata_t *replmsg);
inline void ordlog_event_online(struct cg_msg_streamdata_t *replmsg);
inline void ordlog_event_new_session(signed int new_sess_id);
inline void ordlog_event_integrate_active();
inline void ordlog_event_integrate_active_UNITTEST();

inline void ordlog_event_set_online(void);
inline void ordlog_event_send_order(struct quote_t *quote, int amount, int dir, int64_t price, int order_type);
inline void ordlog_event_check_modified_orders(void);
inline void ordlog_event_check_active(void);
inline void ordlog_event_check_modified_portfolios(void);
inline void ordlog_event_portfolio_field_changed(struct field_value_t *filed_value);
inline void ordlog_event_external_events(void);

inline void ordlog_event_portfolio_stop_trade(struct portfolio_t *portfolio)__attribute__((always_inline));

inline void ordlog_event_commit(void)__attribute__((always_inline));
inline void ordlog_event_commit_quote_bid_change(struct quote_t *quote, int8_t bid_scale, int64_t bid_price, int64_t volume, signed int qty)__attribute__((always_inline));
inline void ordlog_event_commit_quote_ask_change(struct quote_t *quote, int8_t ask_scale, int64_t ask_price, int64_t volume, signed int qty)__attribute__((always_inline));
inline void ordlog_event_commit_quote_ptv_change(struct quote_t *quote, struct sesscont_ex_t *sess)__attribute__((always_inline));
inline void ordlog_event_commit_portfolios(void)__attribute__((always_inline));
inline void ordlog_event_send_portfolio_orders(struct portfolio_t *portfolio, int amount, signed int p2type, int order_type)__attribute__((always_inline));
inline void ordlog_event_commit_portfolios_send_portfolio_orders_minstep_trd(struct portfolio_t *portfolio)__attribute__((always_inline));
inline void ordlog_event_commit_portfolios_send_portfolio_orders_pingpong(struct portfolio_t *portfolio)__attribute__((always_inline));
inline void ordlog_event_commit_portfolios_send_portfolio_orders_followtrend(struct portfolio_t *portfolio)__attribute__((always_inline));
inline void ordlog_event_commit_portfolios_send_portfolio_orders_rtt(struct portfolio_t *portfolio)__attribute__((always_inline));


inline void ordlog_event_commit_portfolios_send_additional_portfolio_orders_minstep_trd(struct portfolio_t *portfolio);

inline int ordlog_event_kill_portfolio_orders(struct portfolio_t *portfolio, int64_t price, signed int dir);
inline int ordlog_event_kill_portfolio_orders_rev(struct portfolio_t *portfolio, int64_t price, signed int dir);
inline int ordlog_event_kill_portfolio_orders_range(struct portfolio_t *portfolio, int64_t from, int64_t till, signed int dir);
inline void ordlog_event_kill_order(struct active_order_t *active);
inline void ordlog_event_move_order(struct active_order_t *active, int64_t price);


inline void ordlog_event_deleted(struct cg_data_cleardeleted_t *replmsg);
inline void ordlog_event_lifenum(void);
inline void ordlog_event_close(void);
char ordlog_listen = 0;
//char ordlog_need_snapshot = 0;




CG_RESULT futinfo_event(cg_conn_t* conn, cg_listener_t* listener, struct cg_msg_t* msg, void* data);
inline void futinfo_event_snapshot(struct cg_msg_streamdata_t *replmsg);
inline void futinfo_event_online(struct cg_msg_streamdata_t *replmsg);
inline void futinfo_event_set_online(void);
inline void futinfo_event_deleted(struct cg_data_cleardeleted_t *replmsg);
inline void futinfo_event_lifenum(void);
inline void futinfo_event_set_online_session(void);
inline void futinfo_event_set_online_sescont(void);
inline void futinfo_event_online_sesstate_changed(struct session_ex_t *session_ex);


void futinfo_event_sescontlog(struct fut_sess_contents *sesscont);
void futinfo_event_seslog(struct session *sess);
char futinfo_listen = 0;

void clear_portfolios_trd_sate(void);
void clear_portfolio_trd_sate(struct portfolio_t *portfolio);
void clear_portfolios_market_data(void);


CG_RESULT optinfo_event(cg_conn_t* conn, cg_listener_t* listener, struct cg_msg_t* msg, void* data);
inline void optinfo_event_snapshot(struct cg_msg_streamdata_t *replmsg);
inline void optinfo_event_online(struct cg_msg_streamdata_t *replmsg);
inline void optinfo_event_set_online(void);
inline void optinfo_event_deleted(struct cg_data_cleardeleted_t *replmsg);
inline void optinfo_event_lifenum(void);
char optinfo_listen = 0;

CG_RESULT repl_event(cg_conn_t* conn, cg_listener_t* listener, struct cg_msg_t* msg, void* data);
inline void repl_event_data(struct cg_msg_data_t* replmsg);
void repl_event_timeout(struct cg_msg_data_t* replmsg);

void initialize(void);
void deinitialize(void);
void load_portfolios(void);
void save_portfolios(void);

void show_portfolios_menu(void);
void infinite_menu_cicle(void);
void manage_selected_portfolio(struct portfolio_t *portfolio);

struct quote_t *load_portfolios_new_quote(struct portfolio_t *portfolio, int quote_id, signed int isin_id, char is_left, int weight, char is_quanto);
struct portfolio_t *load_portfolios_new_portfolio(int portfolio_id, int divisor, int coeff, int quotes_num);
void integrate_portfolios(void);

void i64toa_branchlut_ex(int64_t value, char* buffer, int8_t scale);
//int u64toa_branchlut_ex(uint64_t value, char* buffer, int8_t scale);

inline void price_to_str_map(uint64_t value, char* buffer, int8_t scale)__attribute__((always_inline));

char started_tmp = 0;
char integr_state_tmp = 0;
char system_started = 0;

inline long long ipow(int base, int exp);


inline void printf_stream_newstate(char lst_code, uint32_t type, int reason);


void generate_random_data()
{
	FILE *fp_storage = fopen("r_data", "w");
	int it;
	for(it = 0; it < MAX_RND_VALUES; it ++){
		long rnd = rand();
		fwrite(&rnd, sizeof(long), 1, fp_storage);
	}
	fclose(fp_storage);
}

//Debug functions
void exit_memory_alloc_error(int line, const char *func)
{
	printf("MEMORY_ALLOC_ERROR: %d, %s\n", line, func);

	save_portfolios();

	/*
	FILE *fp_storage = fopen("storage", "w");
	int it;
	for(it = 0; it < portfolios_tbl->cnt; it++){
		struct portfolio_t *portfolio = portfolios_tbl->data[it];

		printf("P(%d, %d). sl_cnt = %d\n", portfolio->portfolio_id, portfolio->amount, portfolio->sl_cnt);

		fwrite(portfolio, sizeof(struct portfolio_t), 1, fp_storage);
	}
	fclose(fp_storage);
	*/

	exit (MEMORY_ALLOC_ERROR);
}

void exit_ordlog_debug_error(int line, const char *func)
{
	printf("ORDLOG_DEBUG_ERROR: %d, %s\n", line, func);

	save_portfolios();

	/*
	FILE *fp_storage = fopen("storage", "w");
	int it;
	for(it = 0; it < portfolios_tbl->cnt; it++){
		struct portfolio_t *portfolio = portfolios_tbl->data[it];

		printf("P(%d, %d). sl_cnt = %d\n", portfolio->portfolio_id, portfolio->amount, portfolio->sl_cnt);

		fwrite(portfolio, sizeof(struct portfolio_t), 1, fp_storage);
	}
	fclose(fp_storage);
	*/

	exit (ORDLOG_DEBUG_ERROR);
}


void save_portfolios(void)
{
	static const char *output_file = "settings.cfg";
	config_t cfg;
	config_setting_t *root, *setting, *group, *list, *sublist;

	config_init(&cfg);
	root = config_root_setting(&cfg);

	setting = config_setting_add(root, "name", CONFIG_TYPE_STRING);
	config_setting_set_string(setting, "settings");

	setting = config_setting_add(root, "ccode", CONFIG_TYPE_STRING);
	config_setting_set_string(setting, kern_settings.ccode);

	setting = config_setting_add(root, "port", CONFIG_TYPE_STRING);
	config_setting_set_string(setting, kern_settings.port);



	/* Add some settings to the configuration. */
	list = config_setting_add(root, "portfolios", CONFIG_TYPE_LIST);

	int it;
	for(it = 0; it < portfolios_tbl->cnt; it++){
	//for(it = 0; it < 1; it++){
		struct portfolio_t *portfolio = portfolios_tbl->data[it];
		switch(portfolio->work_type){
		case PORTFOLIOWORKTYPE_PATTERN:{
			//portfolio->amount = 0;
			portfolio->sell_at = portfolio->buy_at + portfolio->spread;
			//portfolio->iceberg_cnt = 0;
		}break;
		case PORTFOLIOWORKTYPE_FOLLOW_TREND:{
			portfolio->sell_at = portfolio->buy_at + portfolio->spread;
			//portfolio->amount = 0;
		}break;
		}


		group = config_setting_add(list, "", CONFIG_TYPE_GROUP);

		setting = config_setting_add(group, "id", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->portfolio_id);
		setting = config_setting_add(group, "buy_at", CONFIG_TYPE_INT64);
		config_setting_set_int64(setting, portfolio->buy_at);
		setting = config_setting_add(group, "sell_at", CONFIG_TYPE_INT64);
		config_setting_set_int64(setting, portfolio->sell_at);
		setting = config_setting_add(group, "work_type", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->work_type);
		setting = config_setting_add(group, "amount", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->amount);
		setting = config_setting_add(group, "coeff", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->coeff);
		setting = config_setting_add(group, "divisor", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->divisor);
		setting = config_setting_add(group, "shift", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->shift);
		setting = config_setting_add(group, "type_a_cnt", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->type_a_cnt);
		setting = config_setting_add(group, "type_b_cnt", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->type_b_cnt);
		setting = config_setting_add(group, "type_c_cnt", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->type_c_cnt);
		setting = config_setting_add(group, "self_type", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->self_type);
		setting = config_setting_add(group, "type_id", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->type_id);
		setting = config_setting_add(group, "sl_cnt", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->sl_cnt);
		setting = config_setting_add(group, "trade", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->trade);
		setting = config_setting_add(group, "buy", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->buy);
		setting = config_setting_add(group, "sell", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->sell);
		setting = config_setting_add(group, "trd_amt", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->trd_amt);
		setting = config_setting_add(group, "trd_limit", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->trd_limit);
		setting = config_setting_add(group, "iceberg_cnt", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->iceberg_cnt);
		setting = config_setting_add(group, "iceberg_limit", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->iceberg_limit);
		setting = config_setting_add(group, "hedge_portfolio_id", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->hedge_portfolio_id);
		setting = config_setting_add(group, "hedge_mltp", CONFIG_TYPE_INT);
		config_setting_set_int(setting, portfolio->hedge_mltp);

		sublist = config_setting_add(group, "quotes", CONFIG_TYPE_LIST);

		int jt;
		for(jt = 0; jt < portfolio->quotes_tbl->cnt; jt++){
		//for(jt = 0; jt < 1; jt++){
			struct quote_t *quote = portfolio->quotes_tbl->data[jt];

			group = config_setting_add(sublist, "", CONFIG_TYPE_GROUP);

			setting = config_setting_add(group, "id", CONFIG_TYPE_INT);
			config_setting_set_int(setting, quote->quote_id);
			setting = config_setting_add(group, "isin_id", CONFIG_TYPE_INT);
			config_setting_set_int(setting, quote->isin_id);
			setting = config_setting_add(group, "weight", CONFIG_TYPE_INT);
			config_setting_set_int(setting, quote->weight);
			setting = config_setting_add(group, "is_left", CONFIG_TYPE_INT);
			config_setting_set_int(setting, quote->is_left);
			setting = config_setting_add(group, "is_quanto", CONFIG_TYPE_INT);
			config_setting_set_int(setting, quote->is_quanto);
		}



	}

	/* Write out the new configuration. */
	if(!config_write_file(&cfg, output_file))
	{
		//fprintf(stderr, "Error while writing file.\n");
		config_destroy(&cfg);
		return;
	}

	config_destroy(&cfg);


#ifdef RTT_STAT
	FILE *fp_storage = fopen("rtt_stat", "w");
	fprintf(fp_storage, "begin,commit\n");

	for(it = 0; it < trans_tbl->cnt; it++){
		struct trans_t *trans = trans_tbl->data[it];
		long long begin = 0;
		long long commit = 0;

		begin = (trans->sent_moment.tv_sec - trans->ordlog_listener_TN_BEGIN.tv_sec) * 1000000000L + (trans->sent_moment.tv_nsec - trans->ordlog_listener_TN_BEGIN.tv_nsec);
		commit = (trans->sent_moment.tv_sec - trans->ordlog_listener_TN_COMMIT.tv_sec) * 1000000000L + (trans->sent_moment.tv_nsec - trans->ordlog_listener_TN_COMMIT.tv_nsec);

		fprintf(fp_storage, "%lld,%lld\n", begin, commit);
	}
	fclose(fp_storage);
#endif
}

void printf_stream_newstate(char lst_code, uint32_t type, int reason){

	uint32_t *lst_state;

	char buf[256];
	switch(lst_code){
	case 1:
		//fut
		lst_state = &futinfo_listener_state;
		strcpy(buf, "FUTINFO:");
		break;
	case 2:
		//opt
		lst_state = &optinfo_listener_state;
		strcpy(buf, "OPTINFO:");
		break;
	case 3:
		//ordlog
		lst_state = &ordlog_listener_state;
		strcpy(buf, "ORD LOG:");
		break;

	}

	switch(*lst_state){
	case CG_MSG_OPEN:
		strcpy(&buf[8], "CG_MSG_OPEN->");
	break;
	case CG_MSG_CLOSE:
		strcpy(&buf[8], "CG_MSG_CLOSE->");
	break;
	case CG_MSG_TN_BEGIN:
		strcpy(&buf[8], "CG_MSG_TN_BEGIN->");
	break;
	case CG_MSG_TN_COMMIT:
		strcpy(&buf[8], "CG_MSG_TN_COMMIT->");
	break;
	case CG_MSG_STREAM_DATA:
		strcpy(&buf[8], "CG_MSG_STREAM_DATA->");
	break;
	case CG_MSG_P2REPL_ONLINE:
		strcpy(&buf[8], "CG_MSG_P2REPL_ONLINE->");
	break;
	case CG_MSG_P2REPL_LIFENUM:
		strcpy(&buf[8], "CG_MSG_P2REPL_LIFENUM->");
	break;
	case CG_MSG_P2REPL_CLEARDELETED:
		strcpy(&buf[8], "CG_MSG_P2REPL_CLEARDELETED->");
	break;
	case CG_MSG_P2REPL_REPLSTATE:
		strcpy(&buf[8], "CG_MSG_P2REPL_REPLSTATE->");
	break;
	default:
		strcpy(&buf[8], "UNDEFINED->");
	break;
	}

	switch(type){
	case CG_MSG_OPEN:
		printf("%s%s\n", buf, "CG_MSG_OPEN");
	break;
	case CG_MSG_CLOSE:{
		switch(reason){
		case CG_REASON_UNDEFINED:
			printf("%s%s=%s\n", buf, "CG_MSG_CLOSE", "CG_REASON_UNDEFINED");
		break;
		case CG_REASON_USER:
			printf("%s%s=%s\n", buf, "CG_MSG_CLOSE", "CG_REASON_USER");
		break;
		case CG_REASON_ERROR:
			printf("%s%s=%s\n", buf, "CG_MSG_CLOSE", "CG_REASON_ERROR");
		break;
		case CG_REASON_DONE:
			printf("%s%s=%s\n", buf, "CG_MSG_CLOSE", "CG_REASON_DONE");
		break;
		case CG_REASON_SNAPSHOT_DONE:
			printf("%s%s=%s\n", buf, "CG_MSG_CLOSE", "CG_REASON_SNAPSHOT_DONE");
		break;
		}
	}
	break;
	case CG_MSG_TN_BEGIN:
		printf("%s%s\n", buf, "CG_MSG_TN_BEGIN");
	break;
	case CG_MSG_TN_COMMIT:
		printf("%s%s\n", buf, "CG_MSG_TN_COMMIT");
	break;
	case CG_MSG_STREAM_DATA:
		printf("%s%s\n", buf, "CG_MSG_STREAM_DATA");
	break;
	case CG_MSG_P2REPL_ONLINE:
		printf("%s%s\n", buf, "CG_MSG_P2REPL_ONLINE");
	break;
	case CG_MSG_P2REPL_LIFENUM:
		printf("%s%s\n", buf, "CG_MSG_P2REPL_LIFENUM");
	break;
	case CG_MSG_P2REPL_CLEARDELETED:
		printf("%s%s\n", buf, "CG_MSG_P2REPL_CLEARDELETED");
	break;
	case CG_MSG_P2REPL_REPLSTATE:
		printf("%s%s\n", buf, "CG_MSG_P2REPL_REPLSTATE");
	break;
	}
}


void infinite_menu_cicle()
{
	static struct termios oldt, newt;
	/*tcgetattr gets the parameters of the current terminal
	STDIN_FILENO will tell tcgetattr that it should write the settings
	of stdin to oldt*/
	tcgetattr( STDIN_FILENO, &oldt);
	/*now the settings will be copied*/
	newt = oldt;

	printf("%d\n", newt.c_lflag);
	/*ICANON normally takes care that one line at a time will be processed
	that means it will return if it sees a "\n" or an EOF or an EOL*/
	newt.c_lflag &= ~(ICANON | ECHO);

	/*Those new settings will be set to STDIN
	TCSANOW tells tcsetattr to change attributes immediately. */
	tcsetattr( STDIN_FILENO, TCSANOW, &newt);

	int c;
	/*This is your part:
	I choose 'e' to end input. Notice that EOF is also turned off
	in the non-canonical mode*/

	printf("\033[H\033[J");
	printf("----------------------------------------\n");
	printf("Main menu\n");
	printf("----------------------------------------\n");
	printf("2. Show portfolios\n");
	printf("q. Quit\n");
	printf("----------------------------------------\n");
	printf("----------------------------------------\n");

	while((c=getchar())!= 'q'){
		switch(c){
		case '2':
			show_portfolios_menu();
			break;
		}

		printf("\033[H\033[J");
		printf("----------------------------------------\n");
		printf("Main menu\n");
		printf("----------------------------------------\n");
		printf("2. Show portfolios\n");
		printf("q. Quit\n");
		printf("----------------------------------------\n");
		printf("----------------------------------------\n");
	}

	/*restore the old settings*/
	tcsetattr( STDIN_FILENO, TCSANOW, &oldt);

	proceed = 0;
}

void show_portfolios_menu()
{
	printf("\033[H\033[J");
	printf("----------------------------------------\n");
	printf("Portfolios menu\n");
	printf("----------------------------------------\n");
	printf("1. Show all portfolios\n");
	printf("2. Select portfolio\n");
	printf("q. Quit\n");
	printf("----------------------------------------\n");
	printf("----------------------------------------\n");

	int c;
	while((c=getchar())!= 'q'){
		//printf("Pressed %c key\r", c);

		switch(c){
		case '1':{
			printf("\033[H\033[J");
			printf("----------------------------------------\n");
			printf("Portfolios menu\n");
			printf("----------------------------------------\n");
			printf("1. Show all portfolios\n");
			printf("2. Select portfolio\n");
			printf("q. Quit\n");
			printf("----------------------------------------\n");
			printf("Portfolios List:\n");


			int it;
			for(it = 0; it < portfolios_tbl->cnt; it++){
				struct portfolio_t *portfolio = portfolios_tbl->data[it];
				printf("P(%d). wtype = %d, trade = %d, amount = %d, buy_at = %ld, sell_at = %ld, sl_cnt = %d\n", portfolio->portfolio_id, portfolio->work_type, portfolio->trade, portfolio->amount, portfolio->buy_at, portfolio->sell_at, portfolio->sl_cnt);
			}

			printf("----------------------------------------\n");
			printf("----------------------------------------\n");


		}break;
		case '2':{
			printf("\33[2K");
			printf("Enter portfolio id:");
			c=getchar();

			int portfolio_id = c - 48;

			if(portfolio_id >= 0 && portfolio_id < portfolios_tbl->cnt){
				manage_selected_portfolio(portfolios_tbl->data[portfolio_id]);

				printf("\033[H\033[J");
				printf("----------------------------------------\n");
				printf("Portfolios menu\n");
				printf("----------------------------------------\n");
				printf("1. Show all portfolios\n");
				printf("2. Select portfolio\n");
				printf("q. Quit\n");
				printf("----------------------------------------\n");
				printf("----------------------------------------\n");

			}else{

				printf("\033[H\033[J");
				printf("----------------------------------------\n");
				printf("Portfolios menu\n");
				printf("----------------------------------------\n");
				printf("1. Show all portfolios\n");
				printf("2. Select portfolio\n");
				printf("q. Quit\n");
				printf("----------------------------------------\n");
				printf("Portfolios List:\n");


				int it;
				for(it = 0; it < portfolios_tbl->cnt; it++){
					struct portfolio_t *portfolio = portfolios_tbl->data[it];
					printf("P(%d). wtype = %d, trade = %d, amount = %d, buy_at = %ld, sell_at = %ld, sl_cnt = %d\n", portfolio->portfolio_id, portfolio->work_type, portfolio->trade, portfolio->amount, portfolio->buy_at, portfolio->sell_at, portfolio->sl_cnt);
				}

				printf("----------------------------------------\n");
				printf("----------------------------------------\n");


				printf("You entered %d. No such portfolio!\r", portfolio_id);

			}

		}break;
		}
	}

}


void manage_selected_portfolio(struct portfolio_t *portfolio)
{
	printf("\033[H\033[J");
	printf("----------------------------------------\n");
	printf("Portfolio menu\n");
	printf("P(%d). wtype = %d, state = %d, trade = %d, buy = %d, sell = %d, amount = %d, buy_at = %ld, sell_at = %ld, iceburg_cnt = %d\n", portfolio->portfolio_id, portfolio->work_type, portfolio->state, portfolio->trade, portfolio->buy, portfolio->sell, portfolio->amount, portfolio->buy_at, portfolio->sell_at, portfolio->iceberg_cnt);
	printf("wbid = %ld, wask = %ld, bids_num = %d, asks_num = %d, z_bids = %d, z_ask = %d, need_buy = %d, need_sell = %d, killed_bids = %d, killed_asks = %d\n",
			portfolio->wbid, portfolio->wask, portfolio->active_bids, portfolio->active_asks, portfolio->zero_wbids_num, portfolio->zero_wasks_num, portfolio->need_buy_amt, portfolio->need_sell_amt, portfolio->killed_buy_amt, portfolio->killed_sell_amt);
	printf("sl = %d, sl_dir = %d, sl_order = %d, same_inn = %d, same_inn_ch = %d, same_inn_dir = %d\n",
			portfolio->sl, portfolio->sl_dir, portfolio->sl_order, portfolio->same_inn, portfolio->same_inn_ch, portfolio->same_inn_dir);
	printf("sync = %d, sync_replRev = %lld, replRev = %lld\n", ordref_tbl->sync, ordref_tbl->sync_replRev, ordref_tbl->replRev);

	printf("----------------------------------------\n");
	printf("W. System Start\n");
	printf("w. System Stop\n");
	printf("S. Start trade\n");
	printf("s. Stop trade\n");
	printf("m. Market Making Worktype\n");
	printf("f. Front Run Worktype\n");
	printf("c. Close current position\n");
	printf("r. Reset portfolio params\n");
	printf("u. Update portfolio params\n");
	printf("H. Synchronize\n");
	printf("q. Quit\n");
	printf("----------------------------------------\n");

	int c;
	while((c=getchar())!= 'q'){
		printf("Pressed %c key\r", c);

		char command = UNKNOWN_COMMAND;

		switch(c){
		case 'u':{
			printf("\033[H\033[J");
			printf("----------------------------------------\n");
			printf("Portfolio menu\n");
			printf("P(%d). wtype = %d, state = %d, trade = %d, buy = %d, sell = %d, amount = %d, buy_at = %ld, sell_at = %ld, iceburg_cnt = %d\n", portfolio->portfolio_id, portfolio->work_type, portfolio->state, portfolio->trade, portfolio->buy, portfolio->sell, portfolio->amount, portfolio->buy_at, portfolio->sell_at, portfolio->iceberg_cnt);
			printf("wbid = %ld, wask = %ld, bids_num = %d, asks_num = %d, z_bids = %d, z_ask = %d, need_buy = %d, need_sell = %d, killed_bids = %d, killed_asks = %d\n",
					portfolio->wbid, portfolio->wask, portfolio->active_bids, portfolio->active_asks, portfolio->zero_wbids_num, portfolio->zero_wasks_num, portfolio->need_buy_amt, portfolio->need_sell_amt, portfolio->killed_buy_amt, portfolio->killed_sell_amt);

			printf("sl = %d, sl_dir = %d, sl_order = %d, same_inn = %d, same_inn_ch = %d, same_inn_dir = %d\n",
					portfolio->sl, portfolio->sl_dir, portfolio->sl_order, portfolio->same_inn, portfolio->same_inn_ch, portfolio->same_inn_dir);

			printf("sync = %d, sync_replRev = %lld, replRev = %lld\n", ordref_tbl->sync, ordref_tbl->sync_replRev, ordref_tbl->replRev);

			printf("----------------------------------------\n");
			printf("W. System Start\n");
			printf("w. System Stop\n");
			printf("S. Start trade\n");
			printf("s. Stop trade\n");
			printf("m. Market Making Worktype\n");
			printf("f. Front Run Worktype\n");
			printf("c. Close current position\n");
			printf("r. Reset portfolio params\n");
			printf("u. Update portfolio params\n");
			printf("H. Synchronize\n");
			printf("q. Quit\n");
			printf("----------------------------------------\n");


		}break;
		case 'S':
			command = NEED_START_COMMAND;
			break;
		case 's':
			command = NEED_STOP_COMMAND;
			break;
		case 'm':
			command = SET_MM_WORKTYPE_COMMAND;
			break;
		case 'f':
			command = SET_FR_WORKTYPE_COMMAND;
			break;
		case 'c':
			command = STOP_AND_CLOSE_COMMAND;
			break;
		case 'r':
			command = RESET_AND_STOP_COMMAND;
			break;
		case 'W':
			command = SYSTEM_START_COMMAND;
			break;
		case 'w':
			command = SYSTEM_STOP_COMMAND;
			break;
		case 'H':
			command = SYNCHRONIZE_COMMAND;
			break;
		}

		if(command != UNKNOWN_COMMAND){
			struct field_value_t *field_value = new_modified_fields_tbl->data[new_modified_fields_tbl->cnt++];
			field_value->owner = PORTFOLIOS_OWNER;
			field_value->owner_id = portfolio->portfolio_id;
			field_value->field = COMMAND_FIELD;

			if(sizeof(char) > field_value->data_size){
				field_value->data = realloc(field_value->data, sizeof(char));
				field_value->data_size = sizeof(char);
				memcpy(field_value->data, &command, sizeof(char));
			}else{
				//memset(field_value->data, 0, field_value->data_size);
				memcpy(field_value->data, &command, sizeof(char));
			}


			if(modified_fields_tbl->cnt == 0){
				struct modified_fields_tbl_t *tbl_t = new_modified_fields_tbl;
				new_modified_fields_tbl = modified_fields_tbl;
				modified_fields_tbl = tbl_t;
			}
		}
	}
}



void manage_selected_portfolio_old(struct portfolio_t *portfolio)
{
	printf("\033[H\033[J");
	printf("----------------------------------------\n");
	printf("Portfolio menu\n");
	printf("P(%d). wtype = %d, trade = %d, amount = %d, buy_at = %ld, sell_at = %ld, sl_cnt = %d\n", portfolio->portfolio_id, portfolio->work_type, portfolio->trade, portfolio->amount, portfolio->buy_at, portfolio->sell_at, portfolio->sl_cnt);
	printf("----------------------------------------\n");
	printf("s. Start trade\n");
	printf("h. Stop trade\n");
	printf("m. Market Making Worktype\n");
	printf("f. Front Run Worktype\n");
	printf("c. Close current position\n");
	printf("r. Reset portfolio params\n");

	printf("u. Update portfolio params\n");
	printf("q. Quit\n");
	printf("----------------------------------------\n");

	int c;
	while((c=getchar())!= 'q'){
		printf("Pressed %c key\r", c);

		switch(c){
		case 'u':{
			printf("\033[H\033[J");
			printf("----------------------------------------\n");
			printf("Portfolio menu\n");
			printf("P(%d). wtype = %d, trade = %d, amount = %d, buy_at = %ld, sell_at = %ld, sl_cnt = %d\n", portfolio->portfolio_id, portfolio->work_type, portfolio->trade, portfolio->amount, portfolio->buy_at, portfolio->sell_at, portfolio->sl_cnt);
			printf("----------------------------------------\n");
			printf("W. System Start\n");
			printf("w. System Stop\n");
			printf("S. Start trade\n");
			printf("s. Stop trade\n");
			printf("m. Market Making Worktype\n");
			printf("f. Front Run Worktype\n");
			printf("c. Close current position\n");
			printf("r. Reset portfolio params\n");
			printf("u. Update portfolio params\n");
			printf("q. Quit\n");
			printf("----------------------------------------\n");


		}break;
		case 'r':{

		}break;
		case 'c':{
			struct field_value_t *field_value = new_modified_fields_tbl->data[new_modified_fields_tbl->cnt++];
			field_value->owner = PORTFOLIOS_OWNER;
			field_value->owner_id = portfolio->portfolio_id;
			field_value->field = STATE_FIELD;

			char state = STOP_AND_CLOSE_STATE;

			if(sizeof(char) > field_value->data_size){
				field_value->data = realloc(field_value->data, sizeof(char));
				field_value->data_size = sizeof(char);
				memcpy(field_value->data, &state, sizeof(char));
			}else{
				//memset(field_value->data, 0, field_value->data_size);
				memcpy(field_value->data, &state, sizeof(char));
			}


			if(modified_fields_tbl->cnt == 0){
				struct modified_fields_tbl_t *tbl_t = new_modified_fields_tbl;
				new_modified_fields_tbl = modified_fields_tbl;
				modified_fields_tbl = tbl_t;
			}
		}break;
		}
	}
}

int main_old()
{
	printf("!!!!!!!\n");
	printf("1. Limit up and limit down\n");
	printf("2. Price to string\n");
	printf("3. When killing by stop lose, check killed amount!!!! (it should be equal with planned!!!)\n");
	printf("4. Check hedge portfolio id. It should be inside bounds!!! And what is spread for hedge portfolio?\n");
	printf("5. If session is chaged it is necessary to reset portfolio bids and asks!!!\n");
	printf("__________________________\n\n");

	initialize();
	load_portfolios();

	//Создание объекта Окружение
	CHECK_FAIL(cg_env_open(cfg_cli_env), "env_open");

	//-----------------------------------------------------
	//Создание нитей для работы с внешней библиотекой
	//-----------------------------------------------------

	//Нить контроля за сделками
	pthread_t transactions_thread;
	char trans_th_id = TRANSACTIONS;
	if (pthread_create(&transactions_thread, NULL, thread_func, &trans_th_id) != 0){
        return THREAD_CREATION_ERROR;
    }

	//Нить потока репликации FORTS_ORDLOG_REPL
	pthread_t ordlog_repl_thread;
	char ordlog_thread_id = FORTS_ORDLOG;
	if (pthread_create(&ordlog_repl_thread, NULL, thread_func, &ordlog_thread_id) != 0){
        return THREAD_CREATION_ERROR;
    }

	//Нить остальных потоков
	pthread_t other_thread;
	char other_thread_id = FORTS_OTHER;
	if (pthread_create(&other_thread, NULL, thread_func, &other_thread_id) != 0){
        return THREAD_CREATION_ERROR;
    }

	pthread_t logger_thread;
	char logger_thread_id = LOGGER;
	if (pthread_create(&logger_thread, NULL, thread_func, &logger_thread_id) != 0){
        return THREAD_CREATION_ERROR;
    }

	char c;
	int choice = -1;
	do{
		printf("Menu\n\n");
		if(started_tmp)
			printf("1. Stop.\n");
		else
			printf("1. Start.\n");

		printf("2. Round up sync_replRev to 20000.\n");
		printf("3. Desynchronize.\n");
		printf("4. Show last replRev.\n");
#ifdef RTT_STAT
		printf("5. Print rtt stat.\n");
#endif
		printf("6. Portfolios.\n");
		printf("-------------------------------------\n");
		printf("0. Quit\n");

		scanf("%d", &choice);


		switch(choice){
		case 1:
			started_tmp = !started_tmp;
			break;
		case 2:
			ordref_tbl->sync_replRev = ((ordref_tbl->replRev / 20000) + 1) * 20000;
			printf("2. sync_replRev is rounded up to 20000 = %lld\n", ordref_tbl->sync_replRev);
			break;
		case 3:
			ordref_tbl->sync_replRev = LLONG_MAX;
			ordref_tbl->sync = 0;
			printf("3. sync_replRev is desynchronized\n");
			break;
		case 4:
			printf("4. Last replRev = %lld\n", ordref_tbl->replRev);
			break;
#ifdef RTT_STAT
		case 5:
			printf("5. Min, Max, Avg. Begin: %lld, %lld, %f. Commit: %lld, %lld, %f. RTT: %lld, %lld, %f\n", trans_tbl->begin_rtt.min,  trans_tbl->begin_rtt.max,  trans_tbl->begin_rtt.cnt == 0 ? 0 : ((float)trans_tbl->begin_rtt.avg) /  trans_tbl->begin_rtt.cnt, trans_tbl->commit_rtt.min,  trans_tbl->commit_rtt.max, trans_tbl->commit_rtt.cnt == 0 ? 0 : ((float)trans_tbl->commit_rtt.avg) /  trans_tbl->commit_rtt.cnt, trans_tbl->sent_rtt.min,  trans_tbl->sent_rtt.max, trans_tbl->sent_rtt.cnt == 0 ? 0 : ((float)trans_tbl->sent_rtt.avg) /  trans_tbl->sent_rtt.cnt);
			break;
#endif
		case 6:
			//set_portfolios_params();
			break;
		case 0: printf("Exiting program...\n"); proceed = 0;
			break;
		default:
			while((c = getchar()) != '\n' && c != EOF)
					/* discard */ ;
		}

	}while (choice != 0);

	pthread_join(transactions_thread, NULL);
	pthread_join(ordlog_repl_thread, NULL);
	pthread_join(other_thread, NULL);
	pthread_join(logger_thread, NULL);

	CHECK_FAIL(cg_env_close(), "env_close");


	deinitialize();

	return 0;
}

int main()
{
	//remove("trade_station");

	printf("!!!!!!!\n");
	printf("1. Limit up and limit down\n");
	printf("2. Price to string\n");
	printf("3. When killing by stop lose, check killed amount!!!! (it should be equal with planned!!!)\n");
	printf("4. Check hedge portfolio id. It should be inside bounds!!! And what is spread for hedge portfolio?\n");
	printf("5. If session is chaged it is necessary to reset portfolio bids and asks!!!\n");
	printf("__________________________\n\n");


	initialize();
	load_portfolios();

	char thread_ids[] = {TRANSACTIONS, FORTS_ORDLOG, FORTS_OTHER, LOGGER};



	//Создание объекта Окружение
	CHECK_FAIL(cg_env_open(cfg_cli_env), "env_open");


	pthread_t *threads = malloc(sizeof(pthread_t) * sizeof(thread_ids));
	int it;
	for(it = 0; it < sizeof(thread_ids); it ++)
		if(pthread_create(&threads[it], NULL, thread_func, &thread_ids[it]) != 0) exit(THREAD_CREATION_ERROR);


	infinite_menu_cicle();


	for(it = 0; it < sizeof(thread_ids); it ++)	pthread_join(threads[it], NULL);



	CHECK_FAIL(cg_env_close(), "env_close");
	deinitialize();

	return 0;
}


void deinitialize(void)
{
	free(ordlog_replstate);
	free(futinfo_replstate);

	int it;

	for(it = 0; it < quotes_tbl->data_size; it ++) free(quotes_tbl->data[it]);
	free(quotes_tbl->data);
	free(quotes_tbl);


	for(it = 0; it < isins_tbl->data_size; it ++){
		struct isin_data_tbl_t *isin_data = isins_tbl->data[it];

		if(isin_data){
			struct cg_msg_data_ex_t *msg_data = isin_data->msg_data;
			while(msg_data){
				struct cg_msg_data_ex_t *to_free = msg_data;
				msg_data = to_free->next;
				free(to_free);
			}



			if(isin_data->quotes_tbl){
				struct quotes_tbl_t *quotes_tbl = isin_data->quotes_tbl;
				//int jt;
				//for(jt = 0; jt < quotes_tbl->cnt; jt++) free(quotes_tbl->data[jt]);
				free(quotes_tbl->data);
				free(quotes_tbl);
			}

			if(isin_data->qnt_quotes_tbl){
				struct quotes_tbl_t *qnt_quotes_tbl = isin_data->qnt_quotes_tbl;
				//int jt;
				//for(jt = 0; jt < qnt_quotes_tbl->cnt; jt++) free(qnt_quotes_tbl->data[jt]);
				free(qnt_quotes_tbl->data);
				free(qnt_quotes_tbl);
			}

			int jt;
			for(jt = 0; jt < isin_data->data_size; jt++){
				struct sess_data_row_t *sess_data = isin_data->data[jt];

				if(sess_data && sess_data->bidEx){
					int kt;
					for(kt = 0; kt < sess_data->bidEx->data_size; kt++){
						free(sess_data->bidEx->data[kt]);
						free(sess_data->askEx->data[kt]);
					}

					free(sess_data->bidEx->data);
					free(sess_data->bidEx);
					free(sess_data->askEx->data);
					free(sess_data->askEx);
				}
				free(sess_data);
			}

			free(isin_data->data);
		}

		free(isins_tbl->data[it]);
	}
	free(isins_tbl->data);
	free(isins_tbl);

	for(it = 0; it < futsesscont_tbl->data_size; it ++) free(futsesscont_tbl->data[it]);
	free(futsesscont_tbl->data);
	free(futsesscont_tbl);

	for(it = 0; it < ss_futsesscont_tbl->data_size; it ++) free(ss_futsesscont_tbl->data[it]);
	free(ss_futsesscont_tbl->data);
	free(ss_futsesscont_tbl);

	for(it = 0; it < optsesscont_tbl->data_size; it ++) free(optsesscont_tbl->data[it]);
	free(optsesscont_tbl->data);
	free(optsesscont_tbl);

	for(it = 0; it < ss_optsesscont_tbl->data_size; it ++) free(ss_optsesscont_tbl->data[it]);
	free(ss_optsesscont_tbl->data);
	free(ss_optsesscont_tbl);

	for(it = 0; it < session_tbl->data_size; it ++) free(session_tbl->data[it]);
	free(session_tbl->data);
	free(session_tbl);

	for(it = 0; it < ss_session_tbl->data_size; it ++) free(ss_session_tbl->data[it]);
	free(ss_session_tbl->data);
	free(ss_session_tbl);

	//for(it = 0; it < affected_tbl->data_size; it ++) free(affected_tbl->data[it]);
	free(affected_tbl->data);
	free(affected_tbl);

	//for(it = 0; it < qnt_affected_tbl->data_size; it ++) free(qnt_affected_tbl->data[it]);
	free(qnt_affected_tbl->data);
	free(qnt_affected_tbl);

	//printf("ordref_tbl->data_size = %lld\n", ordref_tbl->data_size);
	for(it = 0; it < ordref_tbl->data_size; it ++) free(ordref_tbl->data[it]);
	free(ordref_tbl->data);
	free(ordref_tbl);


	for(it = 0; it < ss_ordref_tbl->data_size; it++) free(ss_ordref_tbl->data[it]);
	free(ss_ordref_tbl->data);
	free(ss_ordref_tbl);

#ifdef UNITTEST
	while(ordref_tbl_UNITTEST){
		free(ordref_tbl_UNITTEST->ordref);
		ordref_tbl_UNITTEST = ordref_tbl_UNITTEST->next;
	}
	free(ordref_tbl_UNITTEST);
#endif


	for(it = 0; it < portfolios_tbl->data_size; it ++){
		if(portfolios_tbl->data[it]){
			struct portfolio_t *portfolio = portfolios_tbl->data[it];
			if(portfolio->active_orders_tbl){
				struct active_order_t *active = portfolio->active_orders_tbl;
				while(active){
					struct active_order_t *to_free = active;
					active = active->next;
					free(to_free);
				}
			}

			struct quotes_tbl_t *quotes_tbl = portfolio->quotes_tbl;

			free(quotes_tbl->data);
			free(quotes_tbl);
			free(portfolios_tbl->data[it]);
		}
	}
	free(portfolios_tbl->data);
	free(portfolios_tbl);

	//for(it = 0; it < pts_affected_tbl->data_size; it ++) free(pts_affected_tbl->data[it]);
	free(pts_affected_tbl->data);
	free(pts_affected_tbl);

	for(it = 0; it < trans_tbl->data_size; it ++) free(trans_tbl->data[it]);
	free(trans_tbl->data);
	free(trans_tbl);

	//for(it = 0; it < new_trans_tbl->data_size; it ++) free(new_trans_tbl->data[it]);
	free(new_trans_tbl->data);
	free(new_trans_tbl);

	//for(it = 0; it < snt_trans_tbl->data_size; it ++) free(snt_trans_tbl->data[it]);
	free(snt_trans_tbl->data);
	free(snt_trans_tbl);

	free(flood_trans_tbl->data);
	free(flood_trans_tbl);

	free(susp_trans_tbl->data);
	free(susp_trans_tbl);

	free(rnd_values_tbl->data);
	free(rnd_values_tbl);


	//for(it = 0; it < snt_messages_tbl->data_size; it ++) free(snt_messages_tbl->data[it]);
	free(snt_messages_tbl->data);
	free(snt_messages_tbl);

	free(pp_snt_messages_tbl->data);
	free(pp_snt_messages_tbl);


	free(pingpongtrades_tbl);

	struct active_order_t *active = active_orders_tbl;
	while(active){
		struct active_order_t *to_free = active;
		active = active->next;
		free(to_free);
	}
	//free(active_orders_tbl);

	active = orders_pool_tbl->first;
	while(active){
		struct active_order_t *to_free = active;
		active = active->next;
		free(to_free);
	}
	free(orders_pool_tbl);

	for(it = 0; it < trans_thr_approved_orders_tbl->cnt; it ++) free(trans_thr_approved_orders_tbl->data[it]);
	free(trans_thr_approved_orders_tbl->data);
	free(trans_thr_approved_orders_tbl);

	for(it = 0; it < ordrs_thr_approved_orders_tbl->cnt; it ++) free(ordrs_thr_approved_orders_tbl->data[it]);
	free(ordrs_thr_approved_orders_tbl->data);
	free(ordrs_thr_approved_orders_tbl);

//	for(it = 0; it < modified_orders_tbl->data_size; it ++) free(modified_orders_tbl->data[it]);
	free(modified_orders_tbl->data);
	free(modified_orders_tbl);

	for(it = 0; it < ordrs_thr_empty_orders_tbl->cnt; it ++) free(ordrs_thr_empty_orders_tbl->data[it]);
	free(ordrs_thr_empty_orders_tbl->data);
	free(ordrs_thr_empty_orders_tbl);

	for(it = 0; it < trans_thr_empty_orders_tbl->cnt; it ++) free(trans_thr_empty_orders_tbl->data[it]);
	free(trans_thr_empty_orders_tbl->data);
	free(trans_thr_empty_orders_tbl);



	struct cg_msg_data_ex_t *msg_data;
	if(futmovemsgs_tbl){
		msg_data = futmovemsgs_tbl->msg_data;
		while(msg_data){
			struct cg_msg_data_ex_t *to_free = msg_data;
			msg_data = msg_data->next;

			free(to_free);
		}
		free(futmovemsgs_tbl);
	}

	if(futdelmsgs_tbl){
		msg_data = futdelmsgs_tbl->msg_data;
		while(msg_data){
			struct cg_msg_data_ex_t *to_free = msg_data;
			msg_data = msg_data->next;
			free(to_free);
		}
		free(futdelmsgs_tbl);
	}

	if(optmovemsgs_tbl){
		msg_data = optmovemsgs_tbl->msg_data;
		while(msg_data){
			struct cg_msg_data_ex_t *to_free = msg_data;
			msg_data = msg_data->next;
			free(to_free);
		}
		free(optmovemsgs_tbl);
	}

	if(optdelmsgs_tbl){
		msg_data = optdelmsgs_tbl->msg_data;
		while(msg_data){
			struct cg_msg_data_ex_t *to_free = msg_data;
			msg_data = msg_data->next;
			free(to_free);
		}
		free(optdelmsgs_tbl);
	}


	for(it = 0; it < price_to_str_tbl->data_size; it ++){
		struct prices_map_tbl_t *prices = price_to_str_tbl->data[it];
		if(prices){
			int jt;
			for(jt = 0; jt < prices->data_size; jt++){
				free(prices->data[jt]);
			}

			free(prices->data);
			free(prices);
		}
	}
	free(price_to_str_tbl->data);
	free(price_to_str_tbl);



	for(it = 0; it < new_modified_fields_tbl->data_size; it ++){
		free(new_modified_fields_tbl->data[it]->data);
		free(modified_fields_tbl->data[it]->data);

		free(new_modified_fields_tbl->data[it]);
		free(modified_fields_tbl->data[it]);
	}
	free(new_modified_fields_tbl->data);
	free(modified_fields_tbl->data);
	free(new_modified_fields_tbl);
	free(modified_fields_tbl);




#ifdef LOG_SAVER
	for(it = 0; it < MAX_LOG_ID; it++){
		struct logs_tbl_t *new_logs_tbl = new_logs_tbls_array[it];
		struct logs_tbl_t *snt_logs_tbl = snt_logs_tbls_array[it];


		int jt;
		for(jt = 0; jt < LOGS_CNT; jt++){
			free(new_logs_tbl->data[jt]->msg);
			free(new_logs_tbl->data[jt]);

			free(snt_logs_tbl->data[jt]->msg);
			free(snt_logs_tbl->data[jt]);
		}
		free(new_logs_tbl->data);
		free(new_logs_tbl);
		free(snt_logs_tbl->data);
		free(snt_logs_tbl);
	}

	free(new_logs_tbls_array);
	free(snt_logs_tbls_array);
#endif


#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
	fclose(fp_futinfo_stream);
	fclose(fp_ordlog_stream);
	fclose(fp_IO_stream);
#endif


}


void initialize(void)
{

	struct timespec curr_tm;
	clock_gettime (CLOCK_MONOTONIC, &curr_tm);
	srand(curr_tm.tv_sec);



#ifdef CLIENTCODE
	strcpy(kern_settings.ccode, CLIENTCODE);
#else
	strcpy(kern_settings.ccode, "001");
#endif



	ordlog_replstate = malloc(1);
	ordlog_replstate[0] = '\0';

	futinfo_replstate = malloc(1);
	futinfo_replstate[0] = '\0';

	isins_tbl = malloc(sizeof(struct isins_tbl_t));
	isins_tbl->cnt = 0;
	isins_tbl->shift = 0;
	isins_tbl->data_size = 0;
	isins_tbl->data = NULL;
	isins_tbl->ext_links = 0;
	//isins_tbl->bytes_used = 0;

	futsesscont_tbl = malloc(sizeof(struct sesscont_tbl_t));
	futsesscont_tbl->cnt = 0;
	futsesscont_tbl->data_size = 0;
	futsesscont_tbl->data = NULL;

	ss_futsesscont_tbl = malloc(sizeof(struct ss_sesscont_tbl_t));
	ss_futsesscont_tbl->cnt = 0;
	ss_futsesscont_tbl->data_size = 0;
	ss_futsesscont_tbl->data = NULL;
	ss_futsesscont_tbl->repl_id.min = LLONG_MAX;
	ss_futsesscont_tbl->sess_id.min = LLONG_MAX;
	ss_futsesscont_tbl->isin_id.min = LLONG_MAX;
	ss_futsesscont_tbl->repl_id.max = LLONG_MIN;
	ss_futsesscont_tbl->sess_id.max = LLONG_MIN;
	ss_futsesscont_tbl->isin_id.max = LLONG_MIN;


	optsesscont_tbl = malloc(sizeof(struct sesscont_tbl_t));
	optsesscont_tbl->cnt = 0;
	optsesscont_tbl->data_size = 0;
	optsesscont_tbl->data = NULL;


	ss_optsesscont_tbl = malloc(sizeof(struct ss_sesscont_tbl_t));
	ss_optsesscont_tbl->cnt = 0;
	ss_optsesscont_tbl->data_size = 0;
	ss_optsesscont_tbl->data = NULL;
	ss_optsesscont_tbl->repl_id.min = LLONG_MAX;
	ss_optsesscont_tbl->sess_id.min = LLONG_MAX;
	ss_optsesscont_tbl->isin_id.min = LLONG_MAX;
	ss_optsesscont_tbl->repl_id.max = LLONG_MIN;
	ss_optsesscont_tbl->sess_id.max = LLONG_MIN;
	ss_optsesscont_tbl->isin_id.max = LLONG_MIN;


	session_tbl = malloc(sizeof(struct session_tbl_t));
	session_tbl->cnt = 0;
	session_tbl->data_size = 0;
	session_tbl->data = NULL;
	session_tbl->current = NULL;
	session_tbl->state = SETTED_SESST;

	ss_session_tbl = malloc(sizeof(struct ss_session_tbl_t));
	ss_session_tbl->cnt = 0;
	ss_session_tbl->data_size = 0;
	ss_session_tbl->data = NULL;
	ss_session_tbl->repl_id.min = LLONG_MAX;
	ss_session_tbl->sess_id.min = LLONG_MAX;
	ss_session_tbl->repl_id.max = LLONG_MIN;
	ss_session_tbl->sess_id.max = LLONG_MIN;

	affected_tbl = malloc(sizeof(struct affected_tbl_t));
	affected_tbl->data = malloc(sizeof(signed int) * AFFECTED_ISINS);
	if(affected_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	affected_tbl->cnt = 0;
	affected_tbl->data_size = AFFECTED_ISINS;

	qnt_affected_tbl = malloc(sizeof(struct affected_tbl_t));
	qnt_affected_tbl->data = malloc(sizeof(signed int) * AFFECTED_ISINS);
	if(qnt_affected_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	qnt_affected_tbl->cnt = 0;
	qnt_affected_tbl->data_size = AFFECTED_ISINS;


	ordref_tbl = malloc(sizeof(struct ordref_tbl_t));
	ordref_tbl->cnt = 0;
	ordref_tbl->shift = 0;
	ordref_tbl->data_size = 0;
	ordref_tbl->data = NULL;
	ordref_tbl->replRev = 0;
	ordref_tbl->movingorder = NULL;
	ordref_tbl->sync_replRev = LLONG_MAX;
	ordref_tbl->sync = -1;

	ss_ordref_tbl = malloc(sizeof(struct ss_ordref_tbl_t));
	ss_ordref_tbl->data = NULL;
	ss_ordref_tbl->data_size = 0;
	ss_ordref_tbl->shift = 0;
	ss_ordref_tbl->order_id.min = LLONG_MAX;
	ss_ordref_tbl->order_id.max = LLONG_MIN;

#ifdef UNITTEST
	ordref_tbl_UNITTEST = NULL;
#endif


	int it;
	quotes_tbl = malloc(sizeof(struct quotes_tbl_t));
	quotes_tbl->data_size = MAX_PORTFOLIOS * MAX_QUOTES_PER_PORTFOLIO;
	quotes_tbl->data = malloc(sizeof(struct quote_t *) * quotes_tbl->data_size);
	if(quotes_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	for(it = 0; it < quotes_tbl->data_size; it++) quotes_tbl->data[it] = NULL;
	quotes_tbl->cnt = 0;

	portfolios_tbl = malloc(sizeof(struct portfolios_tbl_t));
	portfolios_tbl->data_size = MAX_PORTFOLIOS;
	portfolios_tbl->data = malloc(sizeof(struct portfolio_t *) * portfolios_tbl->data_size);
	if(portfolios_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	for(it = 0; it < portfolios_tbl->data_size; it++) portfolios_tbl->data[it] = NULL;
	portfolios_tbl->cnt = 0;


	pts_affected_tbl = malloc(sizeof(struct affected_tbl_t));
	pts_affected_tbl->data_size = MAX_PORTFOLIOS;
	pts_affected_tbl->cnt = 0;
	pts_affected_tbl->data = malloc(sizeof(signed int) * pts_affected_tbl->data_size);
	if(pts_affected_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);


	//------TRADES------------------
	trans_tbl = malloc(sizeof(struct trans_tbl_t));
	trans_tbl->cnt = 0;
	trans_tbl->shift = 0;
	trans_tbl->data_size = TRANSACTIONS_CNT;
	trans_tbl->data = malloc(sizeof(struct trans_t *) * trans_tbl->data_size);
	if(trans_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	for(it = 0; it < trans_tbl->data_size; it++){
		struct trans_t *trans = malloc(sizeof(struct trans_t));
		if(trans == NULL) exit_memory_alloc_error(__LINE__, __func__);

		trans->active_order = NULL;
		trans->active_order2 = NULL;
		trans->p2msg = NULL;

		trans_tbl->data[it] = trans;
	}

#ifdef RTT_STAT
	trans_tbl->commit_rtt.min = LLONG_MAX;
	trans_tbl->commit_rtt.max = LLONG_MIN;
	trans_tbl->commit_rtt.avg = 0;
	trans_tbl->commit_rtt.cnt = 0;
	trans_tbl->begin_rtt.min = LLONG_MAX;
	trans_tbl->begin_rtt.max = LLONG_MIN;
	trans_tbl->begin_rtt.avg = 0;
	trans_tbl->begin_rtt.cnt = 0;
	trans_tbl->sent_rtt.min = LLONG_MAX;
	trans_tbl->sent_rtt.max = LLONG_MIN;
	trans_tbl->sent_rtt.avg = 0;
	trans_tbl->sent_rtt.cnt = 0;
#endif


	new_trans_tbl = malloc(sizeof(struct trans_pointers_tbl_t));
	new_trans_tbl->data_size = 0;
	new_trans_tbl->first = 0;
	new_trans_tbl->data = malloc(sizeof(long) * TRANSACTIONS_CNT);
	if(new_trans_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

	snt_trans_tbl = malloc(sizeof(struct trans_pointers_tbl_t));
	snt_trans_tbl->data_size = 0;
	snt_trans_tbl->first = 0;
	snt_trans_tbl->data = malloc(sizeof(long) * TRANSACTIONS_CNT);
	if(snt_trans_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

	flood_trans_tbl = malloc(sizeof(struct trans_pointers_tbl_t));
	flood_trans_tbl->data_size = 0;
	flood_trans_tbl->first = 0;
	flood_trans_tbl->data = malloc(sizeof(long) * TRANSACTIONS_CNT);
	if(flood_trans_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

	susp_trans_tbl = malloc(sizeof(struct trans_pointers_tbl_t));
	susp_trans_tbl->data_size = 0;
	susp_trans_tbl->first = 0;
	susp_trans_tbl->data = malloc(sizeof(long) * TRANSACTIONS_CNT);
	if(susp_trans_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	susp_trans_tbl->free = 0;
	susp_trans_tbl->release = 0;


	snt_messages_tbl = malloc(sizeof(struct msg_timespecs_tbl_t));
	snt_messages_tbl->data_size = 0;
	snt_messages_tbl->first = 0;
	snt_messages_tbl->data = malloc(sizeof(struct timespec) * TRANSACTIONS_CNT);

	if(snt_messages_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	for(it = 0; it < TRANSACTIONS_CNT; it++){
		snt_messages_tbl->data[it].tv_nsec = 0;
		snt_messages_tbl->data[it].tv_sec = 0;
	}

	pp_snt_messages_tbl = malloc(sizeof(struct msg_timespecs_tbl_t));
	pp_snt_messages_tbl->data_size = 0;
	pp_snt_messages_tbl->first = 0;
	pp_snt_messages_tbl->data = malloc(sizeof(struct timespec) * TRANSACTIONS_CNT);

	if(pp_snt_messages_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	for(it = 0; it < TRANSACTIONS_CNT; it++){
		pp_snt_messages_tbl->data[it].tv_nsec = 0;
		pp_snt_messages_tbl->data[it].tv_sec = 0;
	}

	orders_pool_tbl = malloc(sizeof(struct orders_pool_tbl_t));
	if(orders_pool_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	orders_pool_tbl->first = NULL;
	orders_pool_tbl->last = NULL;
	for(it = 0; it < SIMULTANEOUSLY_ACTIVE_ORDERS; it++){
		struct active_order_t *order = malloc(sizeof(struct active_order_t));
		if(order == NULL) exit_memory_alloc_error(__LINE__, __func__);

		order->prev = NULL;
		order->order_id = 0;
		order->quote = NULL;
		order->next = orders_pool_tbl->first;
		order->state = ORDER_ADDING;

		if(orders_pool_tbl->last == NULL) orders_pool_tbl->last = order;
		orders_pool_tbl->first = order;
	}


	trans_thr_approved_orders_tbl = malloc(sizeof(struct active_orders_tbl_t));
	if(trans_thr_approved_orders_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	trans_thr_approved_orders_tbl->data_size = SIMULTANEOUSLY_ACTIVE_ORDERS;
	trans_thr_approved_orders_tbl->data = malloc(sizeof(struct active_order_t *) * trans_thr_approved_orders_tbl->data_size);
	if(trans_thr_approved_orders_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	trans_thr_approved_orders_tbl->cnt = 0;
	for (it = 0; it < trans_thr_approved_orders_tbl->data_size; it++){
		trans_thr_approved_orders_tbl->data[it] = NULL;
	}

	ordrs_thr_approved_orders_tbl = malloc(sizeof(struct active_orders_tbl_t));
	if(ordrs_thr_approved_orders_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	ordrs_thr_approved_orders_tbl->data_size = SIMULTANEOUSLY_ACTIVE_ORDERS;
	ordrs_thr_approved_orders_tbl->data = malloc(sizeof(struct active_order_t *) * ordrs_thr_approved_orders_tbl->data_size);
	if(ordrs_thr_approved_orders_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	ordrs_thr_approved_orders_tbl->cnt = 0;
	for (it = 0; it < ordrs_thr_approved_orders_tbl->data_size; it++){
		ordrs_thr_approved_orders_tbl->data[it] = NULL;
	}

	modified_orders_tbl = malloc(sizeof(struct ordref_tbl_t));
	if(modified_orders_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	modified_orders_tbl->data_size = SIMULTANEOUSLY_ACTIVE_ORDERS;
	modified_orders_tbl->data = malloc(sizeof(struct ordref_ex_t *) * modified_orders_tbl->data_size);
	if(modified_orders_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	modified_orders_tbl->cnt = 0;
	for (it = 0; it < modified_orders_tbl->data_size; it++){
		modified_orders_tbl->data[it] = NULL;
	}


	ordrs_thr_empty_orders_tbl = malloc(sizeof(struct active_orders_tbl_t));
	if(ordrs_thr_empty_orders_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	ordrs_thr_empty_orders_tbl->data_size = SIMULTANEOUSLY_ACTIVE_ORDERS;
	ordrs_thr_empty_orders_tbl->data = malloc(sizeof(struct active_order_t *) * ordrs_thr_empty_orders_tbl->data_size);
	if(ordrs_thr_empty_orders_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	ordrs_thr_empty_orders_tbl->cnt = 0;
	for (it = 0; it < ordrs_thr_empty_orders_tbl->data_size; it++){
		ordrs_thr_empty_orders_tbl->data[it] = NULL;
	}

	trans_thr_empty_orders_tbl = malloc(sizeof(struct active_orders_tbl_t));
	if(trans_thr_empty_orders_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	trans_thr_empty_orders_tbl->data_size = SIMULTANEOUSLY_ACTIVE_ORDERS;
	trans_thr_empty_orders_tbl->data = malloc(sizeof(struct active_order_t *) * trans_thr_empty_orders_tbl->data_size);
	if(trans_thr_empty_orders_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	trans_thr_empty_orders_tbl->cnt = 0;
	for (it = 0; it < trans_thr_empty_orders_tbl->data_size; it++){
		trans_thr_empty_orders_tbl->data[it] = NULL;
	}


	futmovemsgs_tbl = NULL;
	futdelmsgs_tbl = NULL;
	optmovemsgs_tbl = NULL;
	optdelmsgs_tbl = NULL;



	new_modified_fields_tbl = malloc(sizeof(struct modified_fields_tbl_t));
	if(new_modified_fields_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	new_modified_fields_tbl->data_size = EXTERNAL_EVENTS_NUM;
	new_modified_fields_tbl->data = malloc(sizeof(struct field_value_t *) * new_modified_fields_tbl->data_size);
	if(new_modified_fields_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	new_modified_fields_tbl->cnt = 0;
	for (it = 0; it < new_modified_fields_tbl->data_size; it++){
		new_modified_fields_tbl->data[it] = malloc(sizeof(struct field_value_t));
		new_modified_fields_tbl->data[it]->data_size = sizeof(int);
		new_modified_fields_tbl->data[it]->data = malloc(sizeof(int));
	}

	modified_fields_tbl = malloc(sizeof(struct modified_fields_tbl_t));
	if(modified_fields_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	modified_fields_tbl->data_size = EXTERNAL_EVENTS_NUM;
	modified_fields_tbl->data = malloc(sizeof(struct field_value_t *) * modified_fields_tbl->data_size);
	if(modified_fields_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
	modified_fields_tbl->cnt = 0;
	for (it = 0; it < modified_fields_tbl->data_size; it++){
		modified_fields_tbl->data[it] = malloc(sizeof(struct field_value_t));
		modified_fields_tbl->data[it]->data_size = sizeof(int);
		modified_fields_tbl->data[it]->data = malloc(sizeof(int));
	}


	//------------------------------

	active_orders_tbl = NULL;


	price_to_str_tbl = malloc(sizeof(struct scales_map_tbl_t));
	if(price_to_str_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	price_to_str_tbl->data_size = MAX_PRICE_SCALE;
	price_to_str_tbl->data = malloc(sizeof(struct prices_map_tbl_t *) * price_to_str_tbl->data_size);
	if(price_to_str_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
//	price_to_str_tbl->cnt = 0;
	price_to_str_tbl->shift = 0;
	for (it = 0; it < price_to_str_tbl->data_size; it++){
		price_to_str_tbl->data[it] = NULL;
	}


	rnd_values_tbl = malloc(sizeof(struct trans_pointers_tbl_t));
	rnd_values_tbl->data_size = MAX_RND_VALUES;
	rnd_values_tbl->data = malloc(sizeof(long) * rnd_values_tbl->data_size);
	rnd_values_tbl->first = 0;
	FILE *fp_storage = fopen("r_data", "r");
	if(fp_storage){
		int it;
		for(it = 0; it < rnd_values_tbl->data_size; it++){
			if(fread(&rnd_values_tbl->data[it], sizeof(long), 1, fp_storage) == 0){
				printf("r_data is corrupted!. Exiting...\n");
				exit(R_DATA_MISSED_ERROR);
			}
			rnd_values_tbl->data[it] = abs(rnd_values_tbl->data[it]);
		}
	}else{
		printf("r_data is missed!. Exiting...\n");
		exit(R_DATA_MISSED_ERROR);
	}

	fclose(fp_storage);

	pingpongtrades_tbl = malloc(sizeof(int) * MAX_PINGPONG_TRADES);
	if(pingpongtrades_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);

#ifdef LOG_SAVER
	new_logs_tbls_array = malloc(sizeof(struct logs_tbl_t *) * MAX_LOG_ID);
	for(it = 0; it < MAX_LOG_ID; it++){
		struct logs_tbl_t *new_logs_tbl = malloc(sizeof(struct logs_tbl_t));
		new_logs_tbl->data_size = 0;
		new_logs_tbl->first = 0;
		new_logs_tbl->data = malloc(sizeof(struct logs_t *) * LOGS_CNT);
		if(new_logs_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

		int jt;
		for(jt = 0; jt < LOGS_CNT; jt ++){
			new_logs_tbl->data[jt] = malloc(sizeof(struct log_t));
			new_logs_tbl->data[jt]->msg = malloc(sizeof(char) * MAX_LOGSTRING_SIZE);
		}

		new_logs_tbls_array[it] = new_logs_tbl;
	}

	snt_logs_tbls_array = malloc(sizeof(struct logs_tbl_t *) * MAX_LOG_ID);
	for(it = 0; it < MAX_LOG_ID; it++){
		struct logs_tbl_t *snt_logs_tbl = malloc(sizeof(struct logs_tbl_t));
		snt_logs_tbl->data_size = 0;
		snt_logs_tbl->first = 0;
		snt_logs_tbl->data = malloc(sizeof(struct logs_t *) * LOGS_CNT);
		if(snt_logs_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

		int jt;
		for(jt = 0; jt < LOGS_CNT; jt ++){
			snt_logs_tbl->data[jt] = malloc(sizeof(struct log_t));
			snt_logs_tbl->data[jt]->msg = malloc(sizeof(char) * MAX_LOGSTRING_SIZE);
		}

		snt_logs_tbls_array[it] = snt_logs_tbl;
	}
#endif


#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
	fp_futinfo_stream = fopen("log/futinfo_stream", "wb");
	fp_ordlog_stream = fopen("log/ordlog_stream", "wb");
	fp_IO_stream = fopen("log/io_stream", "wb");
#endif


}

void *thread_func(void *vptr_args)
{
	char thread_id = *(char*)vptr_args;

	switch(thread_id){
	case TRANSACTIONS:
#if defined(UNITTEST)
		thread_func_transactions_UNITTEST();

#elif defined(REPLY)
		thread_func_transactions_init_msgs();

		//thread_func_transactions_loop_REPLY();
#else
		thread_func_transactions();
#endif
		break;
	case FORTS_ORDLOG:
#if defined(UNITTEST)
		thread_func_forts_ordlog_UNITTEST();
#elif defined(REPLY)
		thread_func_forts_ordlog_REPLY();
#else
		thread_func_forts_ordlog();
#endif
		break;
	case FORTS_OTHER:
		//thread_func_forts_other();
		break;
	case LOGGER:
#ifdef LOG_SAVER
		thread_func_logger();
#endif
		break;
	}

    return NULL;
}

void thread_func_logger(void)
{
#ifdef FUTINFO_LOG
	FILE *fp_futinfo = fopen("log/futinfo", "w");
#endif

#ifdef TRANS_LOG
	FILE *fp_trans = fopen("log/trans", "w");
#endif

#ifdef LOGIC_LOG
	FILE *fp_logic = fopen("log/logic", "w");
#endif

#ifdef ORDERS_LOG
	FILE *fp_orders = fopen("log/orders", "w");
#endif

#ifdef PORTFOLIOSTAT_LOG
	FILE *fp_pstat = fopen("log/pstat", "w");
#endif

	/*
#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
	FILE *fp_futinfo_stream = fopen("log/futinfo_stream", "wb");
	FILE *fp_ordlog_stream = fopen("log/ordlog_stream", "wb");

#endif
	*/

	char logger_proceed = proceed;

	int it;
	int jt;
	while(logger_proceed){

		if(!proceed){
			for(it = 0; it < MAX_LOG_ID; it++) thread_func_logger_flush(it);
			logger_proceed = proceed;
		}

		for(it = 0; it < MAX_LOG_ID; it++){
			struct logs_tbl_t *snt_logs_tbl = snt_logs_tbls_array[it];


			if(snt_logs_tbl->data_size){
				for(jt = 0; jt < snt_logs_tbl->data_size; jt ++){
					struct log_t *log = snt_logs_tbl->data[jt];
					FILE *fp = NULL;


					switch(log->log_id){
					case UNKNOWN_LOG_ID:

						break;
					case TRANS_LOG_ID:
	#ifdef TRANS_LOG
						fp = fp_trans;
	#endif
						break;
					case LOGIC_LOG_ID:
	#ifdef LOGIC_LOG
						fp = fp_logic;
	#endif
						break;
					case ORDERS_LOG_ID:
	#ifdef ORDERS_LOG
						fp = fp_orders;
	#endif
						break;
					case FUTINFO_LOG_ID:
	#ifdef FUTINFO_LOG
						fp = fp_futinfo;
	#endif
						break;

					case PORTFOLIOSTAT_LOG_ID:
	#ifdef PORTFOLIOSTAT_LOG
						fp = fp_pstat;
	#endif
						break;

					}

					if(fp){
						fputs(log->msg, fp);
					}
				}

#ifdef FUTINFO_LOG
				fflush(fp_futinfo);
#endif

#ifdef PORTFOLIOSTAT_LOG
				fflush(fp_pstat);
#endif

	#ifdef TRANS_LOG
				fflush(fp_trans);
	#endif

	#ifdef LOGIC_LOG
				fflush(fp_logic);
	#endif

	#ifdef ORDERS_LOG
				fflush(fp_orders);
	#endif




				snt_logs_tbl->data_size = 0;
			}
		}


	}


#ifdef TRANS_LOG
	fclose(fp_trans);
#endif

#ifdef LOGIC_LOG
	fclose(fp_logic);
#endif

#ifdef ORDERS_LOG
	fclose(fp_orders);
#endif

#ifdef FUTINFO_LOG
	fclose(fp_futinfo);
#endif

#ifdef PORTFOLIOSTAT_LOG
	fclose(fp_pstat);
#endif


}


void thread_func_logger_save(int log_id, void *msg, size_t msg_size)
{
	if(msg_size == 0) return;

	struct logs_tbl_t *new_logs_tbl = new_logs_tbls_array[log_id];
	struct log_t *log = new_logs_tbl->data[new_logs_tbl->data_size++];
	log->log_id = log_id;
	log->msg_size = msg_size;
	if(msg_size > MAX_LOGSTRING_SIZE) log->msg = realloc(log->msg, msg_size);

	memcpy(log->msg, msg, msg_size);
	//strncpy(log->msg, msg, msg_size);
}



void thread_func_logger_save_immidiete(int log_id, void *msg, size_t msg_size)
{
	//if(msg_size == 0) return;

#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
	pthread_mutex_lock(&mutex);
	fwrite(&io_event_id, sizeof(long long), 1, fp_IO_stream);
	fwrite(&log_id, sizeof(int), 1, fp_IO_stream);
	fwrite(msg, msg_size, 1, fp_IO_stream);

	io_event_id++;
	pthread_mutex_unlock(&mutex);
#endif

/*
#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
	if(log_id == FUTINFO_STREAM_LOG_ID)
		fwrite(msg, msg_size, 1, fp_futinfo_stream);
	else if(log_id == ORDERS_STREAM_LOG_ID){
		fwrite(msg, msg_size, 1, fp_ordlog_stream);
	}
#endif
*/

}

void thread_func_logger_save_immidiete_duos(int log_id, void *msg1, size_t msg_size1, void *msg2, size_t msg_size2)
{
	//if(msg_size1 == 0) return;

#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
	pthread_mutex_lock(&mutex);
	fwrite(&io_event_id, sizeof(long long), 1, fp_IO_stream);
	fwrite(&log_id, sizeof(int), 1, fp_IO_stream);
	fwrite(msg1, msg_size1, 1, fp_IO_stream);
	fwrite(msg2, msg_size2, 1, fp_IO_stream);

	io_event_id++;
	pthread_mutex_unlock(&mutex);
#endif

/*
#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
	if(log_id == FUTINFO_STREAM_LOG_ID)
		fwrite(msg, msg_size, 1, fp_futinfo_stream);
	else if(log_id == ORDERS_STREAM_LOG_ID){
		fwrite(msg, msg_size, 1, fp_ordlog_stream);
	}
#endif
*/

}



void thread_func_logger_flush(int log_id)
{
	struct logs_tbl_t *snt_logs_tbl = snt_logs_tbls_array[log_id];

	if(snt_logs_tbl->data_size == 0){
		struct logs_tbl_t *new_logs_tbl = new_logs_tbls_array[log_id];
		new_logs_tbl->first = 0;


		struct logs_tbl_t *logs_tbl = new_logs_tbl;
	//	new_logs_tbl = snt_logs_tbl;
	//	snt_logs_tbl = logs_tbl;

		new_logs_tbls_array[log_id] = snt_logs_tbls_array[log_id];
		snt_logs_tbls_array[log_id] = logs_tbl;

	}
}

void thread_func_transactions_REPLY(void)
{
	if(snt_trans_tbl->data_size) thread_func_transactions_sendtrans_REPLY();
	if(trans_thr_empty_orders_tbl->cnt) thread_func_transactions_clear_empty();
	if(susp_trans_tbl->free && susp_trans_tbl->data_size) thread_func_transactions_clear_suspended();
}


void thread_func_transactions_loop_REPLY(void)
{
	struct timespec tim;
	tim.tv_sec = 0;
	tim.tv_nsec = 1000L;

	while(proceed){
		if(futmovemsgs_tbl == NULL) thread_func_transactions_init_msgs();
		if(snt_trans_tbl->data_size) thread_func_transactions_sendtrans_REPLY();
		if(trans_thr_empty_orders_tbl->cnt) thread_func_transactions_clear_empty();

		if(trans_thr_approved_orders_tbl->cnt && ordrs_thr_approved_orders_tbl->cnt == 0){
			struct active_orders_tbl_t *trans_tbl = trans_thr_approved_orders_tbl;
			trans_thr_approved_orders_tbl = ordrs_thr_approved_orders_tbl;
			ordrs_thr_approved_orders_tbl = trans_tbl;
		}

		if(susp_trans_tbl->free && susp_trans_tbl->data_size) thread_func_transactions_clear_suspended();

		nanosleep(&tim, NULL);
	}
	thread_func_transcations_free_msgs();
	thread_func_transactions_killall();

}

void thread_func_transactions_UNITTEST(void)
{
	struct timespec tim;
	tim.tv_sec = 0;
	tim.tv_nsec = 1000L;

	while(proceed){
		if(futmovemsgs_tbl == NULL) thread_func_transactions_init_msgs();
		if(snt_trans_tbl->data_size) thread_func_transactions_sendtrans_UNITTEST();
		if(trans_thr_empty_orders_tbl->cnt) thread_func_transactions_clear_empty();

		if(trans_thr_approved_orders_tbl->cnt && ordrs_thr_approved_orders_tbl->cnt == 0){
			struct active_orders_tbl_t *trans_tbl = trans_thr_approved_orders_tbl;
			trans_thr_approved_orders_tbl = ordrs_thr_approved_orders_tbl;
			ordrs_thr_approved_orders_tbl = trans_tbl;
		}

		if(susp_trans_tbl->free && susp_trans_tbl->data_size) thread_func_transactions_clear_suspended();

		nanosleep(&tim, NULL);
	}
	thread_func_transcations_free_msgs();
	thread_func_transactions_killall();

}

void thread_func_transactions_switch_approved_orders_REPLY(void)
{
	if(trans_thr_approved_orders_tbl->cnt && ordrs_thr_approved_orders_tbl->cnt == 0){
		struct active_orders_tbl_t *trans_tbl = trans_thr_approved_orders_tbl;
		trans_thr_approved_orders_tbl = ordrs_thr_approved_orders_tbl;
		ordrs_thr_approved_orders_tbl = trans_tbl;
	}
}

void thread_func_transactions(void)
{
	CHECK_FAIL(cg_conn_new(cfg_p2msg_conn, &p2msg_conn), "cg_conn_new");
	CHECK_FAIL(cg_pub_new(p2msg_conn, cfg_msg_publisher, &msg_publisher), "cg_pub_new");
	CHECK_FAIL(cg_lsn_new(p2msg_conn, cfg_repl_listener, &repl_event, 0, &repl_listener), "cg_lsn_new");


	while (proceed)
	{
		uint32_t state;

		// query connection state
		WARN_FAIL(cg_conn_getstate(p2msg_conn, &state), "cg_conn_getstate");

		if (state == CG_STATE_ERROR)
		{
			// if connection is in ERROR state
			// then close connection
			WARN_FAIL(cg_conn_close(p2msg_conn), "cg_conn_close");
		}
		else if (state == CG_STATE_CLOSED)
		{
			// if connection is in CLOSED state
			// then open connection
			WARN_FAIL(cg_conn_open(p2msg_conn, 0), "cg_conn_open");
		}
		else if (state == CG_STATE_ACTIVE)
		{
			// if connection is ACTIVE then
			// process with connection messages

			uint32_t result = cg_conn_process(p2msg_conn, 0, 0);
			if (result != CG_ERR_OK && result != CG_ERR_TIMEOUT)
			{
				cg_log_info("Warning: connection state request failed: %X", result);
			}

			// query publisher state
			WARN_FAIL(cg_pub_getstate(msg_publisher, &state), "cg_pub_getstate");

			switch (state)
			{
			case CG_STATE_CLOSED: /* closed */

				// publisher is CLOSED so open it
				WARN_FAIL(cg_pub_open(msg_publisher, ""), "cg_pub_open");
				/**/
				break;
			case CG_STATE_ERROR: /* error */
				// publisher is in ERROR state so close it
				WARN_FAIL(cg_pub_close(msg_publisher), "cg_pub_close");
				break;
			}
			if (state != CG_STATE_ACTIVE)
				continue;





			// query listener state
			WARN_FAIL(cg_lsn_getstate(repl_listener, &state), "cg_lsn_getstate");

			switch (state)
			{
			case CG_STATE_CLOSED: /* closed */

				// listener is CLOSED so open it
				WARN_FAIL(cg_lsn_open(repl_listener, ""), "cg_lsn_open");
				/**/
				break;
			case CG_STATE_ERROR: /* error */
				// listener is in ERROR state so close it
				WARN_FAIL(cg_lsn_close(repl_listener), "cg_lsn_close");
				break;
			}
			if (state != CG_STATE_ACTIVE)
				continue;


			if(futmovemsgs_tbl == NULL) thread_func_transactions_init_msgs();
			if(snt_trans_tbl->data_size) thread_func_transactions_sendtrans();
			if(trans_thr_empty_orders_tbl->cnt) thread_func_transactions_clear_empty();

			if(trans_thr_approved_orders_tbl->cnt && ordrs_thr_approved_orders_tbl->cnt == 0){
				struct active_orders_tbl_t *trans_tbl = trans_thr_approved_orders_tbl;
				trans_thr_approved_orders_tbl = ordrs_thr_approved_orders_tbl;
				ordrs_thr_approved_orders_tbl = trans_tbl;
			}

			if(susp_trans_tbl->free && susp_trans_tbl->data_size) thread_func_transactions_clear_suspended();

		}
	}

	thread_func_transcations_free_msgs();
	thread_func_transactions_killall();




	CHECK_FAIL(cg_conn_close(p2msg_conn), "cg_conn_close");

	CHECK_FAIL(cg_lsn_destroy(repl_listener), "cg_lsn_destroy");
	CHECK_FAIL(cg_pub_destroy(msg_publisher), "cg_pub_destroy");

	CHECK_FAIL(cg_conn_destroy(p2msg_conn), "cg_conn_destroy");
}

void thread_func_transcations_free_msgs(void)
{
	struct cg_msg_data_ex_t *msg_data;

	int it;
	for(it = 0; it < trans_tbl->data_size; it++){
		if(trans_tbl->data[it]->p2msg)
			cg_pub_msgfree(msg_publisher, (struct cg_msg_t*)trans_tbl->data[it]->p2msg->origin);
	}



	if(futmovemsgs_tbl){
		msg_data = futmovemsgs_tbl->msg_data;
		while(msg_data){
			cg_pub_msgfree(msg_publisher, (struct cg_msg_t*)msg_data->origin);
			msg_data = msg_data->next;
		}
	}

	if(futdelmsgs_tbl){
		msg_data = futdelmsgs_tbl->msg_data;
		while(msg_data){
			cg_pub_msgfree(msg_publisher, (struct cg_msg_t*)msg_data->origin);
			msg_data = msg_data->next;
		}
	}

	if(optmovemsgs_tbl){
		msg_data = optmovemsgs_tbl->msg_data;
		while(msg_data){
			cg_pub_msgfree(msg_publisher, (struct cg_msg_t*)msg_data->origin);
			msg_data = msg_data->next;
		}
	}

	if(optdelmsgs_tbl){
		msg_data = optdelmsgs_tbl->msg_data;
		while(msg_data){
			cg_pub_msgfree(msg_publisher, (struct cg_msg_t*)msg_data->origin);
			msg_data = msg_data->next;
		}
	}




	for(it = 0; it < isins_tbl->data_size; it++){
		struct isin_data_tbl_t *isin_data = isins_tbl->data[it];

		if(isin_data){
			msg_data = isin_data->msg_data;
			while(msg_data){
				cg_pub_msgfree(msg_publisher, (struct cg_msg_t*)msg_data->origin);
				msg_data = msg_data->next;
			}
		}
	}


}

void thread_func_transactions_init_msgs(void)
{
	futinfo_listen = 1;


	futmovemsgs_tbl = malloc(sizeof(struct cg_msgs_data_tbl_t));
	if(futmovemsgs_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	futmovemsgs_tbl->msg_data = NULL;
	futmovemsgs_tbl->msg_data_last = NULL;

	futdelmsgs_tbl = malloc(sizeof(struct cg_msgs_data_tbl_t));
	if(futdelmsgs_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	futdelmsgs_tbl->msg_data = NULL;
	futdelmsgs_tbl->msg_data_last = NULL;

	optmovemsgs_tbl = malloc(sizeof(struct cg_msgs_data_tbl_t));
	if(optmovemsgs_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	optmovemsgs_tbl->msg_data = NULL;
	optmovemsgs_tbl->msg_data_last = NULL;

	optdelmsgs_tbl = malloc(sizeof(struct cg_msgs_data_tbl_t));
	if(optdelmsgs_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
	optdelmsgs_tbl->msg_data = NULL;
	optdelmsgs_tbl->msg_data_last = NULL;




	int jt;
	for(jt = 0; jt < SIMULTANEOUSLY_SENT_TRANS; jt++){


		//FutMoveOrder
		{
			struct cg_msg_data_t *msg_data = NULL;
#if !defined(UNITTEST) && !defined(REPLY)
			if(cg_pub_msgnew(msg_publisher, CG_KEY_NAME, "FutMoveOrder", (struct cg_msg_t**)&msg_data) != CG_ERR_OK)
				exit(CG_PUB_MSGNEW_ERROR);
#else
			msg_data = malloc(sizeof(struct cg_msg_data_t));
			msg_data->data = malloc(sizeof(struct FutMoveOrder));
#endif

			struct cg_msg_data_ex_t *msg_data_ex = malloc(sizeof(struct cg_msg_data_ex_t));
			msg_data_ex->origin = msg_data;

			struct FutMoveOrder *ord = (struct FutMoveOrder*) msg_data->data;
			ord->regime = 0;
			msg_data_ex->regime = &ord->regime;
			msg_data_ex->order_id = &ord->order_id1;
			msg_data_ex->order_id2 = &ord->order_id2;
			msg_data_ex->amount = &ord->amount1;
			msg_data_ex->amount2 = &ord->amount2;
			msg_data_ex->price = ord->price1;
			msg_data_ex->price2 = ord->price2;

			msg_data_ex->next = futmovemsgs_tbl->msg_data;
			if(msg_data_ex->next == NULL) futmovemsgs_tbl->msg_data_last = msg_data_ex;
			futmovemsgs_tbl->msg_data = msg_data_ex;
		}

		//FutKillOrder
		{
			struct cg_msg_data_t *msg_data = NULL;
#if !defined(UNITTEST) && !defined(REPLY)
			if(cg_pub_msgnew(msg_publisher, CG_KEY_NAME, "FutDelOrder", (struct cg_msg_t**)&msg_data) != CG_ERR_OK)
				exit(CG_PUB_MSGNEW_ERROR);
#else
			msg_data = malloc(sizeof(struct cg_msg_data_t));
			msg_data->data = malloc(sizeof(struct FutDelOrder));
#endif

			struct cg_msg_data_ex_t *msg_data_ex = malloc(sizeof(struct cg_msg_data_ex_t));
			msg_data_ex->origin = msg_data;

			struct FutDelOrder *ord = (struct FutDelOrder*) msg_data->data;
			msg_data_ex->order_id = &ord->order_id;

			msg_data_ex->next = futdelmsgs_tbl->msg_data;
			if(msg_data_ex->next == NULL)	futdelmsgs_tbl->msg_data_last = msg_data_ex;
			futdelmsgs_tbl->msg_data = msg_data_ex;
		}


		//OptMoveOrder
		{
			struct cg_msg_data_t *msg_data = NULL;
#if !defined(UNITTEST) && !defined(REPLY)
			if(cg_pub_msgnew(msg_publisher, CG_KEY_NAME, "OptMoveOrder", (struct cg_msg_t**)&msg_data) != CG_ERR_OK)
				exit(CG_PUB_MSGNEW_ERROR);
#else
			msg_data = malloc(sizeof(struct cg_msg_data_t));
			msg_data->data = malloc(sizeof(struct OptMoveOrder));
#endif

			struct cg_msg_data_ex_t *msg_data_ex = malloc(sizeof(struct cg_msg_data_ex_t));
			msg_data_ex->origin = msg_data;
			struct OptMoveOrder *ord = (struct OptMoveOrder*) msg_data->data;
			ord->regime = 0;
			msg_data_ex->regime = &ord->regime;
			msg_data_ex->order_id = &ord->order_id1;
			msg_data_ex->order_id2 = &ord->order_id2;
			msg_data_ex->amount = &ord->amount1;
			msg_data_ex->amount2 = &ord->amount2;
			msg_data_ex->price = ord->price1;
			msg_data_ex->price2 = ord->price2;

			msg_data_ex->next = optmovemsgs_tbl->msg_data;
			if(msg_data_ex->next == NULL)	optmovemsgs_tbl->msg_data_last = msg_data_ex;
			optmovemsgs_tbl->msg_data = msg_data_ex;
		}

		//OptKillOrder
		{
			struct cg_msg_data_t *msg_data = NULL;
#if !defined(UNITTEST) && !defined(REPLY)
			if(cg_pub_msgnew(msg_publisher, CG_KEY_NAME, "OptDelOrder", (struct cg_msg_t**)&msg_data) != CG_ERR_OK)
				exit(CG_PUB_MSGNEW_ERROR);
#else
			msg_data = malloc(sizeof(struct cg_msg_data_t));
			msg_data->data = malloc(sizeof(struct OptDelOrder));
#endif

			struct cg_msg_data_ex_t *msg_data_ex = malloc(sizeof(struct cg_msg_data_ex_t));
			msg_data_ex->origin = msg_data;

			struct OptDelOrder *ord = (struct OptDelOrder*) msg_data->data;
			msg_data_ex->order_id = &ord->order_id;

			msg_data_ex->next = optdelmsgs_tbl->msg_data;
			if(msg_data_ex->next == NULL)	optdelmsgs_tbl->msg_data_last = msg_data_ex;
			optdelmsgs_tbl->msg_data = msg_data_ex;
		}
	}
}

void logsaver(int log_id, char *pattern, ...)
{
	struct timespec curr_tm;
	clock_gettime (CLOCK_MONOTONIC, &curr_tm);

	char buff[19];
	strftime(buff, 18, "%H:%M:%S", localtime(&curr_tm.tv_sec));

	va_list ap;
	va_start(ap, pattern); //Requires the last fixed parameter (to get the address)


#ifdef TERMINAL_LOG
	printf("%s.%09ld: ", buff, curr_tm.tv_nsec);
	vprintf(pattern, ap);
	va_end(ap);

/*
#endif
#ifndef ORDLOG_DEBUG

	if(log_id == LOGIC_LOG_ID){
		printf("%s.%09ld: ", buff, curr_tm.tv_nsec);
		vprintf(pattern, ap);
		va_end(ap);
	}else{
		char msg_log[MAX_LOGSTRING_SIZE];
		sprintf(msg_log, "%s.%09ld: ", buff, curr_tm.tv_nsec);
		size_t msg_size = vsprintf(&msg_log[20], pattern, ap);

		va_end(ap);
		thread_func_logger_save(log_id, msg_log, msg_size + 21);
	}



*/
#else
	char msg_log[MAX_LOGSTRING_SIZE];
	sprintf(msg_log, "%s.%09ld: ", buff, curr_tm.tv_nsec);
	size_t msg_size = vsprintf(&msg_log[20], pattern, ap);

	va_end(ap);
	thread_func_logger_save(log_id, msg_log, msg_size + 21);
#endif
}

void thread_func_transactions_killall(void)
{
	struct cg_msg_data_t *msgptr;
	CG_RESULT result;

	alive = 0;

	result = cg_pub_msgnew(msg_publisher, CG_KEY_NAME, "FutDelUserOrders", (struct cg_msg_t**)&msgptr);
	if(result == CG_ERR_OK){
		struct FutDelUserOrders *ord = (struct FutDelUserOrders *)msgptr->data;
		strcpy(ord->code, kern_settings.ccode);
		ord->buy_sell = 3;
		ord->non_system = 2;
		msgptr->user_id = -1;

		alive -= msgptr->user_id;

		result = cg_pub_post(msg_publisher, (struct cg_msg_t*)msgptr, CG_PUB_NEEDREPLY);
		if (result != CG_ERR_OK)
		{
			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "Failed to post message: 0x%X\n", result));
			alive = 0;
		}
		else{
			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID,  "Message posted\n"));
		}
	}else{
		TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "Failed to allocate message: 0x%X\n", result));
		alive = 0;
	}
	cg_pub_msgfree(msg_publisher, (struct cg_msg_t*)msgptr);


	////////////////
	/*
	result = cg_pub_msgnew(msg_publisher, CG_KEY_NAME, "OptDelUserOrders", (struct cg_msg_t**)&msgptr);
	if(result == CG_ERR_OK){
		struct OptDelUserOrders *ord = (struct OptDelUserOrders *)msgptr->data;
		strcpy(ord->code, kern_settings.ccode);
		ord->buy_sell = 3;
		ord->non_system = 2;
		msgptr->user_id = -2;

		alive -= msgptr->user_id;

		result = cg_pub_post(msg_publisher, (struct cg_msg_t*)msgptr, CG_PUB_NEEDREPLY);
		if (result != CG_ERR_OK)
		{
			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "Failed to post message: 0x%X\n", result));
			alive = 0;
		}
		else{
			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID,  "Message posted\n"));
		}
	}else{
		TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "Failed to allocate message: 0x%X\n", result));
		alive = 0;
	}
	cg_pub_msgfree(msg_publisher, (struct cg_msg_t*)msgptr);
	/////////
	 */

	TRANSLOG_FUNC(thread_func_logger_flush(TRANS_LOG_ID));


	while (alive)
	{
		uint32_t state;

		// query connection state
		WARN_FAIL(cg_conn_getstate(p2msg_conn, &state), "cg_conn_getstate");

		if (state == CG_STATE_ERROR)
		{
			// if connection is in ERROR state
			// then close connection
			WARN_FAIL(cg_conn_close(p2msg_conn), "cg_conn_close");
		}
		else if (state == CG_STATE_CLOSED)
		{
			// if connection is in CLOSED state
			// then open connection
			WARN_FAIL(cg_conn_open(p2msg_conn, 0), "cg_conn_open");
		}
		else if (state == CG_STATE_ACTIVE)
		{
			// if connection is ACTIVE then
			// process with connection messages

			uint32_t result = cg_conn_process(p2msg_conn, 0, 0);
			if (result != CG_ERR_OK && result != CG_ERR_TIMEOUT)
			{
				cg_log_info("Warning: connection state request failed: %X", result);
			}

			// query publisher state
			WARN_FAIL(cg_pub_getstate(msg_publisher, &state), "cg_pub_getstate");

			switch (state)
			{
			case CG_STATE_CLOSED: /* closed */

				// publisher is CLOSED so open it
				WARN_FAIL(cg_pub_open(msg_publisher, ""), "cg_pub_open");
				/**/
				break;
			case CG_STATE_ERROR: /* error */
				// publisher is in ERROR state so close it
				WARN_FAIL(cg_pub_close(msg_publisher), "cg_pub_close");
				break;
			}
			if (state != CG_STATE_ACTIVE)
				continue;

			// query listener state
			WARN_FAIL(cg_lsn_getstate(repl_listener, &state), "cg_lsn_getstate");

			switch (state)
			{
			case CG_STATE_CLOSED: /* closed */

				// listener is CLOSED so open it
				WARN_FAIL(cg_lsn_open(repl_listener, ""), "cg_lsn_open");
				/**/
				break;
			case CG_STATE_ERROR: /* error */
				// listener is in ERROR state so close it
				WARN_FAIL(cg_lsn_close(repl_listener), "cg_lsn_close");
				break;
			}
			if (state != CG_STATE_ACTIVE)
				continue;
		}
	}

}

void thread_func_transactions_clear_suspended(void)
{
	int it;
	for(it = 0; it < susp_trans_tbl->data_size; it++){
		long trans_id = susp_trans_tbl->data[it];
		struct trans_t *trans = trans_tbl->data[trans_id];
		struct active_order_t *active = trans->active_order;

		switch(trans->ttype){
		case ADDORDER_TTYPE:{
			orders_pool_tbl->last->next = active;
			orders_pool_tbl->last = active;
			trans->active_order = NULL;

			active->quote->isin_data->msg_data_last->next = trans->p2msg;
			active->quote->isin_data->msg_data_last = trans->p2msg;
			trans->p2msg->next = NULL;
			trans->p2msg = NULL;
		}break;

		case MOVEORDER_TTYPE:{
			if(active->option){
				optmovemsgs_tbl->msg_data_last->next = trans->p2msg;
				optmovemsgs_tbl->msg_data_last = trans->p2msg;
				trans->p2msg->next = NULL;
				trans->p2msg = NULL;
			}else{
				futmovemsgs_tbl->msg_data_last->next = trans->p2msg;
				futmovemsgs_tbl->msg_data_last = trans->p2msg;
				trans->p2msg->next = NULL;
				trans->p2msg = NULL;
			}

			trans->active_order = NULL;
			trans->active_order2 = NULL;
		}break;

		case DELORDER_TTYPE:{
			if(active->option){
				optdelmsgs_tbl->msg_data_last->next = trans->p2msg;
				optdelmsgs_tbl->msg_data_last = trans->p2msg;
				trans->p2msg->next = NULL;
				trans->p2msg = NULL;
			}else{
				futdelmsgs_tbl->msg_data_last->next = trans->p2msg;
				futdelmsgs_tbl->msg_data_last = trans->p2msg;
				trans->p2msg->next = NULL;
				trans->p2msg = NULL;
			}

			trans->active_order = NULL;
		}break;
		}


	}

	susp_trans_tbl->data_size = 0;
}

void thread_func_transactions_clear_empty(void)
{
//	printf("thread_func_transactions_clear_empty\n");
	int it;
	for(it = 0; it < trans_thr_empty_orders_tbl->cnt; it++){
		struct active_order_t *active = trans_thr_empty_orders_tbl->data[it];


#ifdef ORDLOG_DEBUG
		if(active->prev || active->next){
			printf("active->prev || active->next\n");
			//exit_ordlog_debug_error(__LINE__, __func__);
			continue;
		}
#else
		if(active->prev || active->next) continue;
#endif



		active->state = ORDER_ADDING;
		orders_pool_tbl->last->next = active;
		orders_pool_tbl->last = active;


	}

	trans_thr_empty_orders_tbl->cnt = 0;
}

void thread_func_transactions_sendtrans_REPLY(void)
{
	//pthread_mutex_lock(&reply_mutex);

	int it;
#if defined(LOG_SAVER) && defined(TRANS_LOG)
	CG_RESULT result = CG_ERR_OK;
#endif

	//uint msgs_cnt = MAX_FLOOD_MSGS * 100;
	uint msgs_cnt = tr_events_cnt;

	if(msgs_cnt > 0){


		if(msgs_cnt > snt_trans_tbl->data_size) msgs_cnt = snt_trans_tbl->data_size;
		snt_messages_tbl->data_size += msgs_cnt;

		for(it = 0; it < msgs_cnt; it++){
			struct timespec *snt_ts = &snt_messages_tbl->data[(snt_messages_tbl->first+it) % TRANSACTIONS_CNT];
			clock_gettime (CLOCK_MONOTONIC, snt_ts);

			long trans_id = snt_trans_tbl->data[snt_trans_tbl->first+it];

#ifdef RTT_STAT
			trans_tbl->data[trans_id]->sent_moment = *snt_ts;
#endif

			struct trans_t *trans = trans_tbl->data[trans_id];

#if defined(LOG_SAVER) && defined(TRANS_LOG)
			long long begin = 0;
			long long commit = 0;

#ifdef RTT_STAT
			begin = (snt_ts->tv_sec - trans->ordlog_listener_TN_BEGIN.tv_sec) * 1000000000L + (snt_ts->tv_nsec - trans->ordlog_listener_TN_BEGIN.tv_nsec);
			commit = (snt_ts->tv_sec - trans->ordlog_listener_TN_COMMIT.tv_sec) * 1000000000L + (snt_ts->tv_nsec - trans->ordlog_listener_TN_COMMIT.tv_nsec);
#endif

			if(result != CG_ERR_OK){
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%ld. Error = %d\n", trans_id, result));
			}

			if(trans->active_order->state == ORDER_ADDING){
				struct FutAddOrder *ord = (struct FutAddOrder *) trans->p2msg->origin->data;
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, " New order TR=%ld. Isin = %s, dir = %d, price = %s, amount = %d, ccode = %s. begin = %lld, commit = %lld\n", trans_id, ord->isin, ord->dir, ord->price, ord->amount, ord->client_code, begin, commit));
			}
			else if(trans->active_order->state == ORDER_MOVING){
				struct FutMoveOrder *ord = (struct FutMoveOrder *) trans->p2msg->origin->data;
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, " Move order TR=%ld, order_id = %lld, amount = %d, price = %s. begin = %lld, commit = %lld\n", trans_id, ord->order_id1, ord->amount1, ord->price1, begin, commit));
			}else{
				struct FutDelOrder *ord = (struct FutDelOrder *) trans->p2msg->origin->data;
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, " Del order TR=%ld, order_id = %lld. begin = %lld, commit = %lld\n", trans_id, ord->order_id, begin, commit));
			}
#endif
		}

		for(it = 0; it < flood_trans_tbl->data_size; it++){
			snt_trans_tbl->data[snt_trans_tbl->first + snt_trans_tbl->data_size] = flood_trans_tbl->data[it];
			snt_trans_tbl->data_size++;
		}
		flood_trans_tbl->data_size = 0;


		if(susp_trans_tbl->release){
			for(it = 0; it < susp_trans_tbl->data_size; it++){
				snt_trans_tbl->data[snt_trans_tbl->first + snt_trans_tbl->data_size] = susp_trans_tbl->data[it];
				snt_trans_tbl->data_size++;
			}
			susp_trans_tbl->data_size = 0;
		}

		snt_trans_tbl->first += msgs_cnt;
		snt_trans_tbl->data_size -= msgs_cnt;
	}

	TRANSLOG_FUNC(thread_func_logger_flush(TRANS_LOG_ID));


	tr_events_cnt -= msgs_cnt;
//	pthread_mutex_unlock(&reply_mutex);
//	pthread_cond_signal(&reply_cond);
}

void thread_func_transactions_sendtrans_UNITTEST(void)
{
	int it;
#if defined(LOG_SAVER) && defined(TRANS_LOG)
	CG_RESULT result = CG_ERR_OK;
#endif

	struct cg_msg_data_t* replmsg = malloc(sizeof(struct cg_msg_data_t));

	/*
	//Обработка новых транзакций.
	struct timespec curr_tm;
	curr_tm.tv_nsec = 0;
	curr_tm.tv_sec = 0;

	clock_gettime (CLOCK_MONOTONIC, &curr_tm);

	while(snt_messages_tbl->data_size != 0 && ((curr_tm.tv_sec - snt_messages_tbl->data[snt_messages_tbl->first].tv_sec > FLOOD_PERIOD_SEC) || ((curr_tm.tv_sec - snt_messages_tbl->data[snt_messages_tbl->first].tv_sec == FLOOD_PERIOD_SEC) && (curr_tm.tv_nsec > snt_messages_tbl->data[snt_messages_tbl->first].tv_nsec)))){
		snt_messages_tbl->first = (snt_messages_tbl->first + 1) % TRANSACTIONS_CNT;
		snt_messages_tbl->data_size--;
	}

	uint msgs_cnt = MAX_FLOOD_MSGS - snt_messages_tbl->data_size;
	*/

	uint msgs_cnt = MAX_FLOOD_MSGS * 100;

	if(msgs_cnt > 0){


		if(msgs_cnt > snt_trans_tbl->data_size) msgs_cnt = snt_trans_tbl->data_size;
		snt_messages_tbl->data_size += msgs_cnt;

		for(it = 0; it < msgs_cnt; it++){
			struct timespec *snt_ts = &snt_messages_tbl->data[(snt_messages_tbl->first+it) % TRANSACTIONS_CNT];
			clock_gettime (CLOCK_MONOTONIC, snt_ts);

			long trans_id = snt_trans_tbl->data[snt_trans_tbl->first+it];

#ifdef RTT_STAT
			trans_tbl->data[trans_id]->sent_moment = *snt_ts;
#endif

			struct trans_t *trans = trans_tbl->data[trans_id];

#if defined(LOG_SAVER) && defined(TRANS_LOG)
			long long begin = 0;
			long long commit = 0;

#ifdef RTT_STAT
			begin = (snt_ts->tv_sec - trans->ordlog_listener_TN_BEGIN.tv_sec) * 1000000000L + (snt_ts->tv_nsec - trans->ordlog_listener_TN_BEGIN.tv_nsec);
			commit = (snt_ts->tv_sec - trans->ordlog_listener_TN_COMMIT.tv_sec) * 1000000000L + (snt_ts->tv_nsec - trans->ordlog_listener_TN_COMMIT.tv_nsec);
#endif

			if(result != CG_ERR_OK){
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%ld. Error = %d\n", trans_id, result));
			}

			if(trans->active_order->state == ORDER_ADDING){
				struct FutAddOrder *ord = (struct FutAddOrder *) trans->p2msg->origin->data;
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, " New order TR=%ld. Isin = %s, dir = %d, price = %s, amount = %d, ccode = %s. begin = %lld, commit = %lld\n", trans_id, ord->isin, ord->dir, ord->price, ord->amount, ord->client_code, begin, commit));
			}
			else if(trans->active_order->state == ORDER_MOVING){
				struct FutMoveOrder *ord = (struct FutMoveOrder *) trans->p2msg->origin->data;
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, " Move order TR=%ld, order_id = %lld, amount = %d, price = %s. begin = %lld, commit = %lld\n", trans_id, ord->order_id1, ord->amount1, ord->price1, begin, commit));
			}else{
				struct FutDelOrder *ord = (struct FutDelOrder *) trans->p2msg->origin->data;
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, " Del order TR=%ld, order_id = %lld. begin = %lld, commit = %lld\n", trans_id, ord->order_id, begin, commit));
			}
#endif




			replmsg->user_id = trans_id;

			switch(trans->active_order->state){
			case ORDER_ADDING:{
				if(trans->active_order->option){
					replmsg->msg_id = 109;
					struct FORTS_MSG109 *msg = malloc(sizeof(struct FORTS_MSG109));
					replmsg->data = msg;
					msg->code = 0;
					msg->order_id = trans_id;
				}else{
					replmsg->msg_id = 101;
					struct FORTS_MSG101 *msg = malloc(sizeof(struct FORTS_MSG101));
					replmsg->data = msg;
					msg->code = 0;
					msg->order_id = trans_id;
				}

			}break;
			case ORDER_MOVING:{
				if(trans->active_order->option){
					replmsg->msg_id = 113;
					struct FORTS_MSG113 *msg = malloc(sizeof(struct FORTS_MSG113));
					replmsg->data = msg;
					msg->code = 0;
					msg->order_id1 = trans_id;
				}else{
					replmsg->msg_id = 105;
					struct FORTS_MSG105 *msg = malloc(sizeof(struct FORTS_MSG105));
					replmsg->data = msg;
					msg->code = 0;
					msg->order_id1 = trans_id;
				}

			}break;
			case ORDER_KILLING:{
				if(trans->active_order->option){
					replmsg->msg_id = 110;
					struct FORTS_MSG110 *msg = malloc(sizeof(struct FORTS_MSG110));
					replmsg->data = msg;
					msg->code = 0;
					msg->amount = trans->active_order->amount;
				}else{
					replmsg->msg_id = 102;
					struct FORTS_MSG102 *msg = malloc(sizeof(struct FORTS_MSG102));
					replmsg->data = msg;
					msg->code = 0;
					msg->amount = trans->active_order->amount;
				}

			}break;
			}

			if(replmsg->msg_id){
				repl_event_data(replmsg);
				free(replmsg->data);
			}

			replmsg->msg_id = 0;


		}

		for(it = 0; it < flood_trans_tbl->data_size; it++){
			snt_trans_tbl->data[snt_trans_tbl->first + snt_trans_tbl->data_size] = flood_trans_tbl->data[it];
			snt_trans_tbl->data_size++;
		}
		flood_trans_tbl->data_size = 0;


		if(susp_trans_tbl->release){
			for(it = 0; it < susp_trans_tbl->data_size; it++){
				snt_trans_tbl->data[snt_trans_tbl->first + snt_trans_tbl->data_size] = susp_trans_tbl->data[it];
				snt_trans_tbl->data_size++;
			}
			susp_trans_tbl->data_size = 0;
		}

		snt_trans_tbl->first += msgs_cnt;
		snt_trans_tbl->data_size -= msgs_cnt;
	}

	TRANSLOG_FUNC(thread_func_logger_flush(TRANS_LOG_ID));
}

void thread_func_transactions_sendtrans(void)
{
	int it;
#if defined(LOG_SAVER) && defined(TRANS_LOG)
	CG_RESULT result;
#endif

	//Обработка новых транзакций.
	struct timespec curr_tm;
	curr_tm.tv_nsec = 0;
	curr_tm.tv_sec = 0;

	clock_gettime (CLOCK_MONOTONIC, &curr_tm);

	while(snt_messages_tbl->data_size != 0 && ((curr_tm.tv_sec - snt_messages_tbl->data[snt_messages_tbl->first].tv_sec > FLOOD_PERIOD_SEC) || ((curr_tm.tv_sec - snt_messages_tbl->data[snt_messages_tbl->first].tv_sec == FLOOD_PERIOD_SEC) && (curr_tm.tv_nsec > snt_messages_tbl->data[snt_messages_tbl->first].tv_nsec)))){
		snt_messages_tbl->first = (snt_messages_tbl->first + 1) % TRANSACTIONS_CNT;
		snt_messages_tbl->data_size--;
	}

	uint msgs_cnt = MAX_FLOOD_MSGS - snt_messages_tbl->data_size;
	if(msgs_cnt > 0){


		if(msgs_cnt > snt_trans_tbl->data_size) msgs_cnt = snt_trans_tbl->data_size;
		snt_messages_tbl->data_size += msgs_cnt;

		for(it = 0; it < msgs_cnt; it++){
			struct timespec *snt_ts = &snt_messages_tbl->data[(snt_messages_tbl->first+it) % TRANSACTIONS_CNT];
			clock_gettime (CLOCK_MONOTONIC, snt_ts);

			long trans_id = snt_trans_tbl->data[snt_trans_tbl->first+it];


#if defined(LOG_SAVER) && defined(TRANS_LOG)
			result = cg_pub_post(msg_publisher, (struct cg_msg_t*)trans_tbl->data[trans_id]->p2msg->origin, CG_PUB_NEEDREPLY);
#else
			cg_pub_post(msg_publisher, (struct cg_msg_t*)trans_tbl->data[trans_id]->p2msg->origin, CG_PUB_NEEDREPLY);
#endif

#ifdef RTT_STAT
			trans_tbl->data[trans_id]->sent_moment = *snt_ts;
#endif

#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
			pthread_mutex_lock(&mutex);
			io_event_id++;
			pthread_mutex_unlock(&mutex);
#endif



#if defined(LOG_SAVER) && defined(TRANS_LOG)
			struct trans_t *trans = trans_tbl->data[trans_id];
			long long begin = 0;
			long long commit = 0;

#ifdef RTT_STAT
			begin = (snt_ts->tv_sec - trans->ordlog_listener_TN_BEGIN.tv_sec) * 1000000000L + (snt_ts->tv_nsec - trans->ordlog_listener_TN_BEGIN.tv_nsec);
			commit = (snt_ts->tv_sec - trans->ordlog_listener_TN_COMMIT.tv_sec) * 1000000000L + (snt_ts->tv_nsec - trans->ordlog_listener_TN_COMMIT.tv_nsec);
#endif

			if(result != CG_ERR_OK){
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%ld. Error = %d\n", trans_id, result));
			}

			if(trans->active_order->state == ORDER_ADDING){
				struct FutAddOrder *ord = (struct FutAddOrder *) trans->p2msg->origin->data;
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, " New order TR=%ld. Isin = %s, dir = %d, price = %s, amount = %d, ccode = %s. begin = %lld, commit = %lld\n", trans_id, ord->isin, ord->dir, ord->price, ord->amount, ord->client_code, begin, commit));
			}
			else if(trans->active_order->state == ORDER_MOVING){
				struct FutMoveOrder *ord = (struct FutMoveOrder *) trans->p2msg->origin->data;
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, " Move order TR=%ld, order_id = %lld, amount = %d, price = %s. begin = %lld, commit = %lld\n", trans_id, ord->order_id1, ord->amount1, ord->price1, begin, commit));
			}else{
				struct FutDelOrder *ord = (struct FutDelOrder *) trans->p2msg->origin->data;
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, " Del order TR=%ld, order_id = %lld. begin = %lld, commit = %lld\n", trans_id, ord->order_id, begin, commit));
			}
#endif
		}

		for(it = 0; it < flood_trans_tbl->data_size; it++){
			snt_trans_tbl->data[snt_trans_tbl->first + snt_trans_tbl->data_size] = flood_trans_tbl->data[it];
			snt_trans_tbl->data_size++;
		}
		flood_trans_tbl->data_size = 0;


		if(susp_trans_tbl->release){
			for(it = 0; it < susp_trans_tbl->data_size; it++){
				snt_trans_tbl->data[snt_trans_tbl->first + snt_trans_tbl->data_size] = susp_trans_tbl->data[it];
				snt_trans_tbl->data_size++;
			}
			susp_trans_tbl->data_size = 0;
		}

		snt_trans_tbl->first += msgs_cnt;
		snt_trans_tbl->data_size -= msgs_cnt;
	}

	TRANSLOG_FUNC(thread_func_logger_flush(TRANS_LOG_ID));
}

void thread_func_forts_ordlog_REPLY(void)
{
	FILE *fp_IO_stream = fopen("log/io_stream", "rb");

	long long prev_id = -1;
	long long io_event_id;

	long long row_id = 1;


	struct cg_msg_t *msg = malloc(sizeof(struct cg_msg_t));
	msg->data = NULL;
	msg->data_size = 0;

	fpos_t fp_curr;
	fgetpos(fp_IO_stream, &fp_curr);

	pthread_mutex_lock(&reply_mutex);
	while(fread(&io_event_id, sizeof(long long), 1, fp_IO_stream)){
		tr_events_cnt += io_event_id - prev_id - 1;

		if(tr_events_cnt < 0){
			printf("tr_events_cnt = %lld\n", tr_events_cnt);
		}

		prev_id = io_event_id;

		if(tr_events_cnt){
			thread_func_transactions_switch_approved_orders_REPLY();

			msg->type = CG_MSG_TN_COMMIT;
			ordlog_event(NULL, NULL, msg, NULL);


			printf("row_id = %lld, tr_events_cnt = %lld\n", row_id, tr_events_cnt);
			//pthread_cond_wait(&reply_cond, &reply_mutex);

			thread_func_transactions_REPLY();


			if(tr_events_cnt)
				printf("tr_events_cnt = %lld\n", tr_events_cnt);
		}

		int msg_src;
		fread(&msg_src, sizeof(int), 1, fp_IO_stream);

		fgetpos(fp_IO_stream, &fp_curr);
		fread(msg, sizeof(struct cg_msg_t), 1, fp_IO_stream);

		switch(msg->type){
		case CG_MSG_DATA:
		case CG_MSG_P2MQ_TIMEOUT:{
			msg = realloc(msg, sizeof(struct cg_msg_data_t));
			fsetpos(fp_IO_stream, &fp_curr);
			fread(msg, sizeof(struct cg_msg_data_t), 1, fp_IO_stream);
		}break;
		case CG_MSG_STREAM_DATA:{
			msg = realloc(msg, sizeof(struct cg_msg_streamdata_t));
			fsetpos(fp_IO_stream, &fp_curr);
			fread(msg, sizeof(struct cg_msg_streamdata_t), 1, fp_IO_stream);
		}break;
		}

		if(msg->data_size){
			msg->data = malloc(msg->data_size);
			fread(msg->data, msg->data_size, 1, fp_IO_stream);
		}

		switch(msg_src){
		case FUTINFO_STREAM_LOG_ID:
			futinfo_event(NULL, NULL, msg, NULL);
			break;
		case ORDERS_STREAM_LOG_ID:
			ordlog_event(NULL, NULL, msg, NULL);
			break;
		case TRANS_STREAM_LOG_ID:
			repl_event(NULL, NULL, msg, NULL);
			thread_func_transactions_switch_approved_orders_REPLY();
			break;
		default:
			printf("wrong\n");
			break;
		}


		free(msg->data);
		msg->data = NULL;
		msg->data_size = 0;


		row_id++;
		if(row_id == 10400646){
			//printf("!!!\n");
		}
	}
	pthread_mutex_unlock(&reply_mutex);

	fclose(fp_IO_stream);

	printf("FINITALYA\n");


	thread_func_transcations_free_msgs();
	thread_func_transactions_killall();
}


void thread_func_forts_ordlog_UNITTEST(void)
{
	FILE *fp_futinfo_stream = fopen("log/futinfo_stream", "rb");
	FILE *fp_ordlog_stream = fopen("log/ordlog_stream", "rb");

	if(futinfo_listen){
		struct cg_msg_t *msg = malloc(sizeof(struct cg_msg_t));
		msg->data = NULL;
		msg->data_size = 0;

		fpos_t fp_curr;
		fgetpos(fp_futinfo_stream, &fp_curr);

		while(fread(msg, sizeof(struct cg_msg_t), 1, fp_futinfo_stream)){

			if(msg->type == CG_MSG_STREAM_DATA){
				msg = realloc(msg, sizeof(struct cg_msg_streamdata_t));

				fsetpos(fp_futinfo_stream, &fp_curr);
				fread(msg, sizeof(struct cg_msg_streamdata_t), 1, fp_futinfo_stream);
			}

			if(msg->data_size){
				msg->data = malloc(msg->data_size);
				fread(msg->data, msg->data_size, 1, fp_futinfo_stream);
			}else if(msg->data)
				printf("msg->data");

			futinfo_event(NULL, NULL, msg, NULL);

			free(msg->data);
			msg->data = NULL;
			msg->data_size = 0;

			fgetpos(fp_futinfo_stream, &fp_curr);
		}

		free(msg);
	}

	long cnter = 0;

	if(ordlog_listen){
		struct cg_msg_t *msg = malloc(sizeof(struct cg_msg_t));
		msg->data = NULL;
		msg->data_size = 0;

		char is_offline = 1;

		struct timespec tim;
		tim.tv_sec = 0;
		tim.tv_nsec = 1000L;

		fpos_t fp_curr;
		fgetpos(fp_ordlog_stream, &fp_curr);


		while(fread(msg, sizeof(struct cg_msg_t), 1, fp_ordlog_stream)){

			if(msg->type == CG_MSG_STREAM_DATA){
				msg = realloc(msg, sizeof(struct cg_msg_streamdata_t));

				fsetpos(fp_ordlog_stream, &fp_curr);
				fread(msg, sizeof(struct cg_msg_streamdata_t), 1, fp_ordlog_stream);
				//}

				if(msg->data_size){
					msg->data = malloc(msg->data_size);
					fread(msg->data, msg->data_size, 1, fp_ordlog_stream);
				}else if(msg->data)
					printf("msg->data");

				ordlog_event(NULL, NULL, msg, NULL);

				char need_commit = 0;
				struct cg_msg_streamdata_t *replmsg = (struct cg_msg_streamdata_t*)msg;
				struct orders_log *ordlog = (struct orders_log*)replmsg->data;
				if(((ordlog->xstatus & 0x1000) == 0x1000) && ordlog->id_ord > 674104877) need_commit = 1;
				//if((ordlog->xstatus & 0x1000) == 0x1000) need_commit = 1;

				free(msg->data);
				msg->data = NULL;
				msg->data_size = 0;

				if(is_offline && ss_ordref_tbl->shift){
					msg->type = CG_MSG_P2REPL_ONLINE;
					ordlog_event(NULL, NULL, msg, NULL);
					is_offline = 0;
				}

				if(need_commit){
					msg->type = CG_MSG_TN_COMMIT;
					ordlog_event(NULL, NULL, msg, NULL);
				}

				while(snt_trans_tbl->data_size){
					nanosleep(&tim, NULL);
				}

				cnter++;
				if(cnter == 100000){
					cnter = 100000;
				}
			}

			fgetpos(fp_ordlog_stream, &fp_curr);
		}

		free(msg);
	}

#ifdef UNITTEST
	int it;
	for(it = 0; it < portfolios_tbl->cnt; it ++){
		struct portfolio_t *portfolio = portfolios_tbl->data[it];

		int64_t estimate = LLONG_MIN;
		if(portfolio->zero_wbids_num == 0 && portfolio->zero_wasks_num == 0)
			estimate = (portfolio->wbid + portfolio->wask) / 2;
		else if(portfolio->zero_wbids_num == 0)
			estimate = portfolio->wbid;
		else if(portfolio->zero_wasks_num == 0)
			estimate = portfolio->wask;

		if(estimate != LLONG_MIN){
			estimate = estimate * portfolio->amount + portfolio->ut_volume;
			printf("P(%d, %d, %ld). buy_at = %ld, sell_at = %ld, sl_cnt = %d\n", portfolio->portfolio_id, portfolio->amount, estimate, portfolio->buy_at, portfolio->sell_at, portfolio->sl_cnt);

		}
	}
#endif


	thread_func_forts_ordlog_other_job();


	fclose(fp_futinfo_stream);
	fclose(fp_ordlog_stream);

	printf("UNITTEST FIN. Tot records = %ld\n", cnter);
}


void thread_func_forts_ordlog(void)
{
	CHECK_FAIL(cg_conn_new(cfg_ordlog_conn, &ordlog_conn), "cg_conn_new");

	CHECK_FAIL(cg_lsn_new(ordlog_conn, cfg_futinfo_listener, &futinfo_event, 0, &futinfo_listener), "cg_lsn_new");
#ifndef NO_OPTIONS
	CHECK_FAIL(cg_lsn_new(ordlog_conn, cfg_optinfo_listener, &optinfo_event, 0, &optinfo_listener), "cg_lsn_new");
#endif
	CHECK_FAIL(cg_lsn_new(ordlog_conn, cfg_ordlog_listener, &ordlog_event, 0, &ordlog_listener), "cg_lsn_new");



	while (ordlog_proceed)
	{

		uint32_t state;

		// query connection state
		WARN_FAIL(cg_conn_getstate(ordlog_conn, &state), "cg_conn_getstate");

		if (state == CG_STATE_ERROR)
		{
			// if connection is in ERROR state
			// then close connection
			WARN_FAIL(cg_conn_close(ordlog_conn), "cg_conn_close");
		}
		else if (state == CG_STATE_CLOSED)
		{
			// if connection is in CLOSED state
			// then open connection
			WARN_FAIL(cg_conn_open(ordlog_conn, 0), "cg_conn_open");
		}
		else if (state == CG_STATE_ACTIVE)
		{
			// if connection is ACTIVE then
			// process with connection messages

			uint32_t result = cg_conn_process(ordlog_conn, 0, 0);
			if (result != CG_ERR_OK && result != CG_ERR_TIMEOUT)
			{
				cg_log_info("Warning: connection state request failed: %X", result);
			}


			if(futinfo_listen){
				// FUTINFO query listener state
				WARN_FAIL(cg_lsn_getstate(futinfo_listener, &state), "cg_lsn_getstate");

				switch (state)
				{
				case CG_STATE_CLOSED: /* closed */

					// listener is CLOSED so open it
					//WARN_FAIL(cg_lsn_open(futinfo_listener, ""), "cg_lsn_open");
					WARN_FAIL(cg_lsn_open(futinfo_listener, futinfo_replstate), "cg_lsn_open");
					/**/
					break;
				case CG_STATE_ERROR: /* error */
					// listener is in ERROR state so close it

					WARN_FAIL(cg_lsn_close(futinfo_listener), "cg_lsn_close");
					break;
				}
			}

#ifndef NO_OPTIONS
			if(optinfo_listen){
				// OPTINFO query listener state
				WARN_FAIL(cg_lsn_getstate(optinfo_listener, &state), "cg_lsn_getstate");

				switch (state)
				{
				case CG_STATE_CLOSED: /* closed */

					// listener is CLOSED so open it
					WARN_FAIL(cg_lsn_open(optinfo_listener, ""), "cg_lsn_open");
					/**/
					break;
				case CG_STATE_ERROR: /* error */
					// listener is in ERROR state so close it

					WARN_FAIL(cg_lsn_close(optinfo_listener), "cg_lsn_close");
					break;
				}
			}
#endif

			if(ordlog_listen){
				// ORDLOG query listener state
				WARN_FAIL(cg_lsn_getstate(ordlog_listener, &state), "cg_lsn_getstate");

				switch (state)
				{
				case CG_STATE_CLOSED: /* closed */

					// listener is CLOSED so open it
					//WARN_FAIL(cg_lsn_open(ordlog_listener, ""), "cg_lsn_open");
					WARN_FAIL(cg_lsn_open(ordlog_listener, ordlog_replstate), "cg_lsn_open");
					/**/
					break;
				case CG_STATE_ERROR: /* error */
					// listener is in ERROR state so close it

					WARN_FAIL(cg_lsn_close(ordlog_listener), "cg_lsn_close");
					break;
				}
			}

			thread_func_forts_ordlog_other_job();
			if(modified_fields_tbl->cnt) ordlog_event_external_events();

		}
	}

	CHECK_FAIL(cg_conn_close(ordlog_conn), "cg_conn_close");

	CHECK_FAIL(cg_lsn_destroy(futinfo_listener), "cg_lsn_destroy");
#ifndef NO_OPTIONS
	CHECK_FAIL(cg_lsn_destroy(optinfo_listener), "cg_lsn_destroy");
#endif
	CHECK_FAIL(cg_lsn_destroy(ordlog_listener), "cg_lsn_destroy");

	CHECK_FAIL(cg_conn_destroy(ordlog_conn), "cg_conn_destroy");

}

void thread_func_forts_ordlog_other_job(void)
{
	if(!alive){
		save_portfolios();
		ordlog_proceed = 0;
	}
}



CG_RESULT ordlog_event(cg_conn_t* conn, cg_listener_t* listener, struct cg_msg_t* msg, void* data)
{
#if defined(STREAMS_LOG) && !defined(REPLY)
	pthread_mutex_lock(&io_order_mutex);
#endif

	switch(msg->type)
	{
	case CG_MSG_OPEN:
	{
		/*
		Сообщение приходит в момент активации потока данных. Это событие гарантированно
		возникает до прихода каких либо данных по данной подписке. Для потоков репликации
		приход сообщения означает, что схема данных согласована и готова для использования
		(Подробнее см. Схемы данных) Данное сообщение не содержит дополнительных данных
		и его поля data и data_size не используются.
		*/

		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));

		DEBUG_FUNC(printf_stream_newstate(3, msg->type, 0));
		ordlog_listener_state = CG_MSG_OPEN;

	}
	break;
	case CG_MSG_CLOSE:
	{
		/*
		Сообщение приходит в момент закрытия потока данных. Приход сообщения означает, что
		поток был закрыт пользователем или системой. В поле data содержится указатель на int,
		по указанному адресу хранится информация о причине закрытия подписчика. Возможны
		следующие причины:
		• CG_REASON_UNDEFINED - не определена;
		• CG_REASON_USER - пользователь вернул ошибку в callback подписчика;
		• CG_REASON_ERROR - внутренняя ошибка;
		• CG_REASON_DONE - вызван метод cg_lsn_destroy;
		• CG_REASON_SNAPSHOT_DONE - снэпшот получен.
		*/

		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t), msg->data, msg->data_size));

		DEBUG_FUNC(printf_stream_newstate(3, msg->type, *(int*)(msg->data)));
		ordlog_listener_state = CG_MSG_CLOSE;

		ordlog_event_close();


		/*
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg->data, msg->data_size));
		*/

	}
	break;
	case CG_MSG_TN_BEGIN:
	{
		/*
		Означает момент начала получения очередного блока данных. В паре со следующим со-
		общением может быть использовано логикой ПО для контроля целостности данных. Дан-
		ное сообщение не содержит дополнительных данных и его поля data и data_size не ис-
		пользуются.
		*/
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));

		clock_gettime (CLOCK_MONOTONIC, &ordlog_listener_TN_BEGIN);

	}
	break;
	case CG_MSG_TN_COMMIT:
	{
		/*
		Означает момент завершения получения очередного блока данных. К моменту прихода
		этого сообщения можно считать, что данные полученные по данной подписке, находят-
		ся в непротиворечивом состоянии и отражают таблицы в синхронизированном между со-
		бой состоянии. Данное сообщение не содержит дополнительных данных и его поля data
		и data_size не используются.
		*/

#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
		if(started_tmp && !system_started){
			msg->data_size = sizeof(char);
			msg->data = malloc(msg->data_size);
			msg->data = memcpy(msg->data, &started_tmp, msg->data_size);
			system_started = started_tmp;
		}
#endif

		//STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));
		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t), msg->data, msg->data_size));

#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
		if(msg->data_size){
			free(msg->data);
			msg->data_size = 0;
			msg->data = NULL;
		}
#endif

#if defined(REPLY)
		if(msg->data_size)
			started_tmp = *(char*)msg->data;
#endif

		clock_gettime (CLOCK_MONOTONIC, &ordlog_listener_TN_COMMIT);

		if(ordlog_listener_state == CG_MSG_P2REPL_ONLINE){
			ordlog_event_commit();
			ordlog_event_commit_portfolios();
#ifdef UNITTEST
			if(ordrs_thr_approved_orders_tbl->cnt) ordlog_event_integrate_active_UNITTEST();
#else
			if(ordrs_thr_approved_orders_tbl->cnt) ordlog_event_integrate_active();
#endif
			if(modified_orders_tbl->cnt) ordlog_event_check_modified_orders();
			if(session_tbl->state != FINISHED_SESST) ordlog_event_check_modified_portfolios();

			ordlog_event_check_active();
		}

	}
	break;
	case CG_MSG_STREAM_DATA:
	{

		/*
		Сообщение прихода потоковых данных. Поле data_size содержит размер полученных дан-
		ных, data указывает на сами данные. Само сообщение содержит дополнительные поля,
		которые описываются структурой cg_msg_streamdata_t.
		*/

		/*
		struct cg_msg_streamdata_t
		{
			uint32_t type; 			// Тип сообщения. Всегда CG_MSG_STREAM_DATA для данного сообщения
			size_t data_size;		// Размер данных
			void* data;				// Указатель на данные

			size_t msg_index; 		// Номер описания сообщения в активной схеме
			uint32_t msg_id;		// Уникальный идентификатор типа сообщения

			const char* msg_name; 	// Имя сообщения в активной схеме
			int64_t rev;			// Ревизия записи

			size_t num_nulls;		// Размер массива байт
			const uint8_t* nulls; 	// Массив байт, содержащий 1 для каждого поля, являющегося NULL-ом.
		};
		*/

		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_streamdata_t), msg->data, msg->data_size));

		struct cg_msg_streamdata_t *replmsg = (struct cg_msg_streamdata_t*)msg;


		// Таблица заявок
		if(replmsg->msg_index == orders_log_index){
			if(ordlog_listener_state == CG_MSG_P2REPL_ONLINE){
				ordlog_event_online(replmsg);						// Обновление данных в режиме онлайн
			}else
				ordlog_event_snapshot(replmsg);						// Первоначальное получение данных
		}



		/*
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_streamdata_t)));
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg->data, msg->data_size));
		*/

		// Здесь происоходит обработка полученных данных и отсев ненужных данных.
	}
	break;
	case CG_MSG_P2REPL_ONLINE:
	{
		/*
		Переход потока в состояние online - это означает, что получение начального среза было
		завершено и следующие сообщения CG_MSG_STREAM_DATA будут нести данные он-лайн. Данное
		сообщение не содержит дополнительных данных и его поля data и data_size не используются.
		*/

		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));



		DEBUG_FUNC(printf_stream_newstate(3, msg->type, 0));
		ordlog_listener_state = CG_MSG_P2REPL_ONLINE;

		ordlog_event_set_online();
	}
	break;
	case CG_MSG_P2REPL_LIFENUM:
	{
		/*
		Изменен номер жизни схемы. Такое сообщение означает, что предыдущие данные, полу-
		ченные по потоку, не актуальны и должны быть очищены. При этом произойдёт повторная
		трансляция данных по новому номеру жизни схемы данных. Поле data сообщения указы-
		вает на целочисленное значение, содержащее новый номер жизни схемы; поле data_size
		содержит размер целочисленного типа. Подробнее про обработку номера жизни схемы в
		конце данного раздела
		*/
		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t), msg->data, msg->data_size));

		DEBUG_FUNC(printf_stream_newstate(3, msg->type, 0));
		ordlog_listener_state = CG_MSG_P2REPL_LIFENUM;

		struct timespec ts_start, ts_stop;

		clock_gettime (CLOCK_MONOTONIC, &ts_start);

		ordlog_event_lifenum();

		clock_gettime (CLOCK_MONOTONIC, &ts_stop);



		/*
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg->data, msg->data_size));
		*/
	}
	break;
	case CG_MSG_P2REPL_CLEARDELETED:
	{
		/*
		Произошла операция массового удаления устаревших данных. Поле data сообщения ука-
		зывает на структуру cg_data_cleardeleted_t, в которой указан номер таблицы и номер ре-
		визии, до которой данные в указанной таблице считаются удаленными. Если ревизия в
		cg_data_cleardeleted_t == CG_MAX_REVISON, то последующие ревизии продолжатся с 1.
		*/

		/*
		struct cg_data_cleardeleted_t {
			uint32_t table_idx;
			int64_t table_rev;
		};
		*/
		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t), msg->data, msg->data_size));

		DEBUG_FUNC(printf_stream_newstate(3, msg->type, 0));
		ordlog_listener_state = CG_MSG_P2REPL_CLEARDELETED;

		struct cg_data_cleardeleted_t *replmsg = (struct cg_data_cleardeleted_t*)msg->data;
		struct timespec ts_start, ts_stop;

		clock_gettime (CLOCK_MONOTONIC, &ts_start);

		ordlog_event_deleted(replmsg);

		clock_gettime (CLOCK_MONOTONIC, &ts_stop);

		/*
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg->data, msg->data_size));
		*/
	}
	break;
	case CG_MSG_P2REPL_REPLSTATE:
	{
		/*
		Сообщение содержит состояние потока данных; присылается перед закрытием потока.
		Поле data сообщения указывает на строку, которая в закодированном виде содержит со-
		стояние потока данных на момент прихода сообщения - сохраняются схема данных, но-
		мера ревизий таблиц и номер жизни схемы на момент последнего CG_MSG_TN_COMMIT
		(Внимание: при переоткрытии потока с replstate ревизии, полученные после последнего
		CG_MSG_TN_COMMIT, будут присланы повторно!) Эта строка может быть передана в вы-
		зов cg_lsn_open в качестве параметра "replstate" по этому же потоку в следующий раз, что
		обеспечит продолжение получения данных с момента остановки потока.
		*/

		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t), msg->data, msg->data_size));

		DEBUG_FUNC(printf_stream_newstate(3, msg->type, 0));
		ordlog_listener_state = CG_MSG_P2REPL_REPLSTATE;

		ordlog_replstate = realloc(ordlog_replstate, msg->data_size + 10);
		strcpy(ordlog_replstate, "replstate=");
		memcpy(&ordlog_replstate[10], msg->data, msg->data_size);


		/*
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(ORDERS_STREAM_LOG_ID, msg->data, msg->data_size));
		*/
	}
	break;
	}


#if defined(STREAMS_LOG) && !defined(REPLY)
	pthread_mutex_unlock(&io_order_mutex);
#endif

	return CG_ERR_OK;
}

void ordlog_event_check_active(void)
{
	struct active_order_t *active = active_orders_tbl;
	while(active){
		if(active->state == ORDER_ACTIVE){
			switch(active->order_type){
			case ORDERTYPE_STOP_LOSS:{
				if(active->dir == 1){
					if(active->quote->bid_tgt > active->price){
						LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "--SL = %lld is moving. dir = %d, price = %ld, new_price = %ld. TR=%lld\n", active->order_id, active->dir, active->price, active->quote->bid_tgt + active->quote->minstep, trans_tbl->cnt));
						ordlog_event_move_order(active, active->quote->bid_tgt + active->quote->minstep);

					}
				}else if(active->dir == 2){
					if(active->quote->ask_tgt < active->price){
						LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "--SL = %lld is moving. dir = %d, price = %ld, new_price = %ld. TR=%lld\n", active->order_id, active->dir, active->price, active->quote->ask_tgt - active->quote->minstep, trans_tbl->cnt));
						ordlog_event_move_order(active, active->quote->ask_tgt - active->quote->minstep);
					}
				}
			}break;
/*
			case ORDERTYPE_FLOAT_QUOTE:{


				if(active->dir == 1){
					int64_t price_ch = abs(active->quote->bid_tgt - active->price);
					if(price_ch >= 105 || price_ch <= 95){
						LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "--FQ = %lld is moving. dir = %d, price = %ld, new_price = %ld. TR=%lld\n", active->order_id, active->dir, active->price, active->quote->bid_tgt + active->quote->minstep, trans_tbl->cnt));
						ordlog_event_move_order(active, active->quote->bid_tgt - active->quote->minstep * 100);

					}
				}else if(active->dir == 2){
					int64_t price_ch = abs(active->quote->ask_tgt - active->price);
					if(price_ch >= 105 || price_ch <= 95){
						LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "--FQ = %lld is moving. dir = %d, price = %ld, new_price = %ld. TR=%lld\n", active->order_id, active->dir, active->price, active->quote->ask_tgt - active->quote->minstep, trans_tbl->cnt));
						ordlog_event_move_order(active, active->quote->ask_tgt + active->quote->minstep * 100);
					}
				}

			}break;
*/
			}
		}


		active = active->next;
	}


	if(snt_trans_tbl->data_size == 0){
		new_trans_tbl->first = 0;

		struct trans_pointers_tbl_t *trans_tbl = new_trans_tbl;
		new_trans_tbl = snt_trans_tbl;
		snt_trans_tbl = trans_tbl;
	}



	LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));
}



void ordlog_event_snapshot(struct cg_msg_streamdata_t *replmsg)
{
	if(ordref_tbl->data_size == 0)
		ordlog_event_initial_snapshot(replmsg);
	else{
		/*
		unsigned long long modified_orders_tbl_cnt = modified_orders_tbl->cnt;
		*/

		signed int affected_tbl_cnt = affected_tbl->cnt;


		ordlog_event_online(replmsg);

		/*
		modified_orders_tbl->cnt = modified_orders_tbl_cnt;
		*/

		affected_tbl->cnt = affected_tbl_cnt;

	}
}

/*
int ordlog_event_snapshot_cnt = 0;
int ordlog_event_snapshot_num = 0;
struct timespec ordlog_event_snapshot_ts;
*/

void ordlog_event_initial_snapshot(struct cg_msg_streamdata_t *replmsg)
{
	struct orders_log *ordlog = (struct orders_log*)replmsg->data;

	if((ordlog->xstatus & 0x04) == 0x04) return;
	if(ordlog->replRev <= ordref_tbl->replRev) return;


#ifdef ORDERS_LOG
	int64_t price_int;
	int8_t price_scale;
	cg_bcd_get(ordlog->price, &price_int, &price_scale);
	ORDERSLOG_FUNC(logsaver(ORDERS_LOG_ID, "%lld,%lld,%lld,%lld,%d,%d,%d,%d,%lld,%ld,%d\n", ordlog->replID, ordlog->replRev, ordlog->replAct, ordlog->id_ord, ordlog->amount_rest, ordlog->sess_id, ordlog->isin_id, ordlog->dir, ordlog->xstatus, price_int, price_scale));

	ORDERSLOG_FUNC(thread_func_logger_flush(ORDERS_LOG_ID));
#endif

	signed int isin_id = ordlog->isin_id - isins_tbl->shift;

#ifdef IGNORE_UNKNOWN_ORDERS
	if(isin_id < 0 || isin_id >= isins_tbl->data_size || isins_tbl->data[isin_id] == NULL) return;

	signed int sess_id = ordlog->sess_id - isins_tbl->sess_id.min;
	if(sess_id < 0 || sess_id >= isins_tbl->data[isin_id]->data_size) return;
#endif

	if(integr_state_tmp == 2 && isins_tbl->data[isin_id]->quotes_tbl == NULL) return;


	char useful = (ordlog->replAct == 0);


	if(useful){
		struct orders_log_ex_t *ordlog_ex = malloc(sizeof(struct orders_log_ex_t));
		if(ordlog_ex == NULL) exit_memory_alloc_error(__LINE__, __func__);

		memcpy(ordlog_ex, ordlog, STRUCT_ORDLOG_SNAPSHOT_SIZE);

		cg_bcd_get(ordlog->price, &ordlog_ex->price_int, &ordlog_ex->price_scale);

		if(ordlog->id_ord < ss_ordref_tbl->order_id.min)
			ss_ordref_tbl->order_id.min = ordlog->id_ord;

		if(ordlog->id_ord > ss_ordref_tbl->order_id.max)
			ss_ordref_tbl->order_id.max = ordlog->id_ord;
/*
		if(ordlog->replRev < ss_ordref_tbl->repl_rev.min)
			ss_ordref_tbl->repl_rev.min = ordlog->replRev;

		if(ordlog->replRev > ss_ordref_tbl->repl_rev.max)
			ss_ordref_tbl->repl_rev.max = ordlog->replRev;
*/

		ordlog_ex->next = NULL;

		if(ss_ordref_tbl->data_size == 0) ss_ordref_tbl->shift = ss_ordref_tbl->order_id.min;
		else if(ss_ordref_tbl->order_id.min < ss_ordref_tbl->shift){
			long long mltp = (ss_ordref_tbl->shift - ss_ordref_tbl->order_id.min) / ORDLOGSIZE_INCREMENT + 1;

			ss_ordref_tbl->data_size += ORDLOGSIZE_INCREMENT * mltp;
			ss_ordref_tbl->data = realloc(ss_ordref_tbl->data, sizeof(struct orders_log_ex_t *) * ss_ordref_tbl->data_size);

			memmove(&ss_ordref_tbl->data[ORDLOGSIZE_INCREMENT * mltp], ss_ordref_tbl->data, ss_ordref_tbl->data_size - ORDLOGSIZE_INCREMENT * mltp);
			memset(ss_ordref_tbl->data, 0, ss_ordref_tbl->data_size - ORDLOGSIZE_INCREMENT * mltp);

			ss_ordref_tbl->shift -= ORDLOGSIZE_INCREMENT * mltp;
		}

		int order_id = ordlog->id_ord - ss_ordref_tbl->shift;
		if(order_id >= ss_ordref_tbl->data_size){
			long long mltp = (order_id - ss_ordref_tbl->data_size) / ORDLOGSIZE_INCREMENT + 1;
			ss_ordref_tbl->data_size += ORDLOGSIZE_INCREMENT * mltp;
			ss_ordref_tbl->data = realloc(ss_ordref_tbl->data, sizeof(struct orders_log_ex_t *) * ss_ordref_tbl->data_size);
			if(ss_ordref_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

			int it;
			for(it = ss_ordref_tbl->data_size - ORDLOGSIZE_INCREMENT * mltp; it < ss_ordref_tbl->data_size; it++){
				ss_ordref_tbl->data[it] = NULL;
			}
		}

		if(ss_ordref_tbl->data[order_id] == NULL)
			ss_ordref_tbl->data[order_id] = ordlog_ex;
		else if(ss_ordref_tbl->data[order_id]->replRev < ordlog_ex->replRev){
			free(ss_ordref_tbl->data[order_id]);
			ss_ordref_tbl->data[order_id] = ordlog_ex;
		}
		else
			free(ordlog_ex);

	}

	/*
	ordlog_event_snapshot_cnt++;
	if(ordlog_event_snapshot_cnt > 20000){
		struct timespec curr_tm;
		clock_gettime (CLOCK_MONOTONIC, &curr_tm);
		long long rtt = (curr_tm.tv_sec - ordlog_event_snapshot_ts.tv_sec) * 1000000000L + (curr_tm.tv_nsec - ordlog_event_snapshot_ts.tv_nsec);


		ordlog_event_snapshot_cnt = 0;
		ordlog_event_snapshot_num++;
		printf("SS_NUM (%d) = %lld\n", ordlog_event_snapshot_num, rtt);
	}
	*/
}


void ordlog_event_integrate_active_UNITTEST()
{
	int it;
	for (it = 0; it < ordrs_thr_approved_orders_tbl->cnt; it++){
		struct active_order_t *active = ordrs_thr_approved_orders_tbl->data[it];


		struct ordref_ex_t *ordref = malloc(sizeof(struct ordref_ex_t));
		ordref->replRev = -1;
		ordref->amount_rest = 0;
		ordref->active = NULL;


		struct ordref_list_t *ordref_list = malloc(sizeof(struct ordref_list_t));
		ordref_list->ordref = ordref;
		ordref_list->prev = ordref_list;
#ifdef UNITTEST
		ordref_list->next = ordref_tbl_UNITTEST;
		if(ordref_tbl_UNITTEST) ordref_tbl_UNITTEST->prev = ordref_list;
		ordref_tbl_UNITTEST = ordref_list;
#endif

		//if(active->state == ORDER_KILLED){
//		if(active->state == MOVE_NOT_FOUND || active->state == KILL_NOT_FOUND){
		if(active->state == ORDER_MOVING || active->state == ORDER_KILLING){
			active->state = ORDER_KILLED;
			//modified_orders_tbl->data[modified_orders_tbl->cnt++] = ordref;
			if(ordref->active == NULL) ordrs_thr_empty_orders_tbl->data[ordrs_thr_empty_orders_tbl->cnt++] = active;
		}else{

#ifdef ORDLOG_DEBUG
			if(ordref->active){
				printf("ordref->active != NULL\n");
				exit_ordlog_debug_error(__LINE__, __func__);
			}
#endif
			ordref->active = active;


			if(ordref->replRev > -1){
				active->price = ordref->agp_position * ordref->sess_data->cont->minstep_adj + ordref->sess_data->cont->limit_down_adj;
				modified_orders_tbl->data[modified_orders_tbl->cnt++] = ordref;

			}

			active->state = ORDER_ACTIVE;

			switch(active->order_type){
			case ORDERTYPE_MARKET_MAKER:{
				struct portfolio_t *portfolio = active->quote->portfolio;
				if(portfolio->active_orders_tbl){
					active->next = portfolio->active_orders_tbl;
					portfolio->active_orders_tbl->prev = active;
					portfolio->active_orders_tbl = active;
				}
				else
					portfolio->active_orders_tbl = active;
			}break;
			case ORDERTYPE_STOP_LOSS:{
				if(active_orders_tbl){
					active->next = active_orders_tbl;
					active_orders_tbl->prev = active;
					active_orders_tbl = active;
				}
				else
					active_orders_tbl = active;
			}break;
			}

		}
	}

	ordrs_thr_approved_orders_tbl->cnt = 0;
}

void ordlog_event_integrate_active()
{
	int it;
	for (it = 0; it < ordrs_thr_approved_orders_tbl->cnt; it++){
		struct active_order_t *active = ordrs_thr_approved_orders_tbl->data[it];

		signed long long order_id = active->order_id - ordref_tbl->shift;

#ifdef ORDLOG_DEBUG
		if(order_id < 0){
			printf("order_id < 0! id_ord = %lld\n", active->order_id);
			exit_ordlog_debug_error(__LINE__, __func__);
		}
#endif


		if(order_id >= ordref_tbl->data_size){
			long long mltp = (order_id - ordref_tbl->data_size) / ORDLOGSIZE_INCREMENT + 1;
			ordref_tbl->data_size += ORDLOGSIZE_INCREMENT * mltp;
			ordref_tbl->data = realloc(ordref_tbl->data, sizeof(struct ordref_ex_t *) * ordref_tbl->data_size);
			if(ordref_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

#ifdef ORDLOG_DEBUG
			printf("order_id = %lld, data_size = %lld, shift = %lld\n", order_id, ordref_tbl->data_size, ordref_tbl->shift);
			printf("order_id >= ordref_tbl->data_size!. ordref_tbl->data_size = %lld\n", ordref_tbl->data_size);
#endif

			int it;
			for(it = ordref_tbl->data_size - ORDLOGSIZE_INCREMENT * mltp; it < ordref_tbl->data_size; it++){
				struct ordref_ex_t *ordref = malloc(sizeof(struct ordref_ex_t));
				if(ordref == NULL) exit_memory_alloc_error(__LINE__, __func__);
				ordref_tbl->data[it] = ordref;

				ordref->replRev = -1;
				ordref->amount_rest = 0;
				ordref->active = NULL;
			}
		}



		struct ordref_ex_t *ordref = ordref_tbl->data[order_id];
		//if(active->state == ORDER_KILLED){
//		if(active->state == MOVE_NOT_FOUND || active->state == KILL_NOT_FOUND){
		if(active->state == ORDER_MOVING || active->state == ORDER_KILLING){
			active->state = ORDER_KILLED;
			if(ordref->active == NULL) ordrs_thr_empty_orders_tbl->data[ordrs_thr_empty_orders_tbl->cnt++] = active;
		}else if(active->state != ORDER_KILLED){

#ifdef ORDLOG_DEBUG
			if(ordref->active){
				printf("ordref->active != NULL\n");
				exit_ordlog_debug_error(__LINE__, __func__);
			}
#endif
			ordref->active = active;

			if(ordref->replRev > -1){
				modified_orders_tbl->data[modified_orders_tbl->cnt++] = ordref;
				active->price = ordref->agp_position * ordref->sess_data->cont->minstep_adj + ordref->sess_data->cont->limit_down_adj;

				active->state = ORDER_ACTIVE;
			}


			switch(active->order_type){
			case ORDERTYPE_TAKE_PROFIT:
			case ORDERTYPE_MARKET_MAKER:{
				struct portfolio_t *portfolio = active->quote->portfolio;
				if(portfolio->active_orders_tbl){
					active->next = portfolio->active_orders_tbl;
					portfolio->active_orders_tbl->prev = active;
					portfolio->active_orders_tbl = active;
				}
				else
					portfolio->active_orders_tbl = active;
			}break;
			case ORDERTYPE_STOP_LOSS:{
				if(active_orders_tbl){
					active->next = active_orders_tbl;
					active_orders_tbl->prev = active;
					active_orders_tbl = active;
				}
				else
					active_orders_tbl = active;
			}break;
			}

		}
	}

	ordrs_thr_approved_orders_tbl->cnt = 0;
}


void ordlog_event_check_modified_portfolios(void)
{
	char modified = 0;
	uint it;
	for(it = 0; it < portfolios_tbl->cnt; it ++){
		struct portfolio_t *portfolio = portfolios_tbl->data[it];
		if(portfolio->used){
			modified = 1;

			switch(portfolio->work_type){
			case PORTFOLIOWORKTYPE_PATTERN:{
				switch(portfolio->state){
				case MM_STATE:{
					//portfolio->used = 0;


					if(portfolio->best_buy_trd != LLONG_MAX){
						portfolio->same_inn = portfolio->best_buy - (portfolio->need_sell_amt + portfolio->killed_buy_amt) * portfolio->shift - portfolio->best_buy_trd + portfolio->shift;
						if(portfolio->same_inn){
							portfolio->same_inn_dir = 1;
							portfolio->same_inn = portfolio->same_inn / portfolio->shift;
							int kill_amt = ordlog_event_kill_portfolio_orders_rev(portfolio, portfolio->best_buy_trd, 1);
							portfolio->state = INN_STATE;
							portfolio->used = 0;
							LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "P (%d, %d). Killing crossing Bids = %d, cross_cnt = %d, best_buy = %ld, from = %ld, need_sell = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, kill_amt, portfolio->same_inn, portfolio->best_buy, portfolio->best_buy_trd, portfolio->need_sell_amt, trans_tbl->cnt - kill_amt));
						}
					}

					if(portfolio->same_inn == 0 && portfolio->best_sell_trd != LLONG_MIN){
						portfolio->same_inn = portfolio->best_sell_trd - (portfolio->need_buy_amt + portfolio->killed_sell_amt) * portfolio->shift - portfolio->best_sell + portfolio->shift;
						if(portfolio->same_inn){
							portfolio->same_inn_dir = 2;
							portfolio->same_inn = portfolio->same_inn / portfolio->shift;
							int kill_amt = ordlog_event_kill_portfolio_orders_rev(portfolio, portfolio->best_sell_trd, 2);
							portfolio->state = INN_STATE;
							portfolio->used = 0;
							LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "P (%d, %d). Killing crossing Asks = %d, cross_cnt = %d, best_sell = %ld, from = %ld, need_buy = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, kill_amt, portfolio->same_inn, portfolio->best_sell, portfolio->best_sell_trd, portfolio->need_buy_amt, trans_tbl->cnt - kill_amt));
						}
					}

					if(portfolio->same_inn == 0){
						if(portfolio->need_buy_amt || portfolio->need_sell_amt){
							if(portfolio->best_sell_trd != LLONG_MIN){
								portfolio->best_sell = portfolio->best_sell_trd + portfolio->shift;
								portfolio->best_sell_trd = LLONG_MIN;
							}

							if(portfolio->best_buy_trd != LLONG_MAX){
								portfolio->best_buy = portfolio->best_buy_trd - portfolio->shift;
								portfolio->best_buy_trd = LLONG_MAX;
							}


							portfolio->need_sell_amt += portfolio->killed_sell_amt;
							portfolio->killed_sell_amt = 0;

							portfolio->need_buy_amt += portfolio->killed_buy_amt;
							portfolio->killed_buy_amt = 0;


							struct quote_t *quote = portfolio->quotes_tbl->data[0];
							int it;

							LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "##P (%d, %d, %d). B: worst = %ld, best = %ld, cnt = %d, trd = %d, last = %ld. A: worst = %ld, best = %ld, cnt = %d, trd = %d, last = %ld. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, portfolio->iceberg_cnt, portfolio->buy_at, portfolio->best_buy, portfolio->active_bids, portfolio->need_buy_amt, portfolio->best_buy_trd,  portfolio->sell_at, portfolio->best_sell, portfolio->active_asks, portfolio->need_sell_amt, portfolio->best_sell_trd, trans_tbl->cnt));
		//					LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "##P (%d, %d). B: worst = %ld, best = %ld, cnt = %d, trd = %d. A: worst = %ld, best = %ld, cnt = %d, trd = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, portfolio->buy_at, portfolio->best_buy, portfolio->active_bids, portfolio->need_buy_amt, portfolio->sell_at, portfolio->best_sell, portfolio->active_asks, portfolio->need_sell_amt, trans_tbl->cnt));

							int64_t price;

							portfolio->best_buy += portfolio->need_buy_amt * portfolio->shift;
							price = portfolio->best_buy;

							for(it = 0; it < portfolio->need_buy_amt; it++){
								ordlog_event_send_order(quote, portfolio->trd_amt, 1, price, ORDERTYPE_MARKET_MAKER);
								price -= portfolio->shift;
							}
							portfolio->need_buy_amt = 0;

							portfolio->best_sell -= portfolio->need_sell_amt * portfolio->shift;
							price = portfolio->best_sell;
							for(it = 0; it < portfolio->need_sell_amt; it++){
								ordlog_event_send_order(quote, portfolio->trd_amt, 2, price, ORDERTYPE_MARKET_MAKER);
								price += portfolio->shift;
							}
							portfolio->need_sell_amt = 0;
						}//else if(portfolio->active_bids == 0 && portfolio->buy){

						if(portfolio->active_bids == 0 && portfolio->buy){
#ifdef ORDLOG_DEBUG
							if(proceed && portfolio->best_buy != portfolio->buy_at){
								printf("portfolio->best_buy != portfolio->buy_at\n");
								LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "portfolio->best_buy != portfolio->buy_at\n"));
								//exit_ordlog_debug_error(__LINE__, __func__);
							}
#endif
							portfolio->iceberg_cnt++;
							portfolio->sell = 1;
							if(portfolio->iceberg_cnt == portfolio->iceberg_limit){
								portfolio->buy = 0;
								portfolio->sl_cnt++;

								//Start Hedge

								struct portfolio_t *hedge_portfolio = portfolio->hedge_portfolio;
								if(hedge_portfolio){
									hedge_portfolio->trade = 1;
									hedge_portfolio->work_type = PORTFOLIOWORKTYPE_FOLLOW_TREND;
									hedge_portfolio->sell = 1;
									hedge_portfolio->buy = 0;
									hedge_portfolio->buy_at = hedge_portfolio->wask;
									hedge_portfolio->best_buy = hedge_portfolio->buy_at;
									hedge_portfolio->sell_at = hedge_portfolio->buy_at + hedge_portfolio->spread;
									hedge_portfolio->best_sell = hedge_portfolio->sell_at;
									hedge_portfolio->trd_amt = portfolio->hedge_mltp * portfolio->trd_amt;

									portfolio->amount += hedge_portfolio->amount;
									hedge_portfolio->amount = 0;
								}

								break;

								portfolio->iceberg_cnt = 0;

								struct quote_t *quote = portfolio->quotes_tbl->data[0];

								LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "Active bids is zero! Sending STOP LOSS. sl_cnt = %d, dir = 2. TR=%lld. ", portfolio->sl_cnt, trans_tbl->cnt));
								ordlog_event_send_order(quote, portfolio->trd_limit * portfolio->iceberg_limit * portfolio->trd_amt / 2, 2, portfolio->wbid, ORDERTYPE_STOP_LOSS);
								portfolio->sl_order = 1;
							}else
								portfolio->sl_order = 0;


							LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "Killing asks with price >= %ld. ", portfolio->sell_at - portfolio->trd_limit / 2 * portfolio->shift));
							int killed_amt = ordlog_event_kill_portfolio_orders(portfolio, portfolio->sell_at - portfolio->trd_limit / 2 * portfolio->shift, 2);

#ifdef ORDLOG_DEBUG
							if(proceed && killed_amt != portfolio->trd_limit / 2){
								printf("killed_amt != portfolio->trd_limit / 2\n");
								LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "killed_amt != portfolio->trd_limit / 2\n"));
								//exit_ordlog_debug_error(__LINE__, __func__);
							}
#endif

							LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "Amount = %d. ", killed_amt));
							LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "P (%d, %d). B: worst = %ld, best = %ld, cnt = %d, trd = %d, last = %ld, killed = %d. A: worst = %ld, best = %ld, cnt = %d, trd = %d, last = %ld, killed = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, portfolio->buy_at, portfolio->best_buy, portfolio->active_bids, portfolio->need_buy_amt, portfolio->best_buy_trd, portfolio->killed_buy_amt,  portfolio->sell_at, portfolio->best_sell, portfolio->active_asks, portfolio->need_sell_amt, portfolio->best_sell_trd, portfolio->killed_sell_amt, trans_tbl->cnt));


							//portfolio->sl = killed_amt;
							portfolio->sl = portfolio->trd_limit / 2;
							portfolio->sl_dir = 2;
							portfolio->state = SL_STATE;


						}else if(portfolio->active_asks == 0 && portfolio->sell){
#ifdef ORDLOG_DEBUG
							if(proceed && portfolio->best_sell != portfolio->sell_at){
								printf("portfolio->best_sell != portfolio->sell_at\n");
								LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "portfolio->best_sell != portfolio->sell_at\n"));
								//exit_ordlog_debug_error(__LINE__, __func__);
							}
#endif
							portfolio->iceberg_cnt--;
							portfolio->buy = 1;
							if(portfolio->iceberg_cnt == -portfolio->iceberg_limit){
								portfolio->sell = 0;
								portfolio->sl_cnt++;

								//Start hedge portfolio

								struct portfolio_t *hedge_portfolio = portfolio->hedge_portfolio;
								if(hedge_portfolio){
									hedge_portfolio->trade = 1;
									hedge_portfolio->work_type = PORTFOLIOWORKTYPE_FOLLOW_TREND;
									hedge_portfolio->sell = 0;
									hedge_portfolio->buy = 1;
									hedge_portfolio->sell_at = hedge_portfolio->wbid;
									hedge_portfolio->best_sell = hedge_portfolio->sell_at;
									hedge_portfolio->buy_at = hedge_portfolio->sell_at - hedge_portfolio->spread;
									hedge_portfolio->best_buy = hedge_portfolio->buy_at;
									hedge_portfolio->trd_amt = portfolio->hedge_mltp * portfolio->trd_amt;

									portfolio->amount += hedge_portfolio->amount;
									hedge_portfolio->amount = 0;
								}

								break;

								portfolio->iceberg_cnt = 0;

								struct quote_t *quote = portfolio->quotes_tbl->data[0];

								LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "Active asks is zero! Sending STOP LOSS. sl_cnt = %d, dir = 1. TR=%lld. ", portfolio->sl_cnt, trans_tbl->cnt));
								ordlog_event_send_order(quote, portfolio->trd_limit * portfolio->iceberg_limit * portfolio->trd_amt / 2, 1, portfolio->wask, ORDERTYPE_STOP_LOSS);
								portfolio->sl_order = 1;
							}else
								portfolio->sl_order = 0;


							LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "Killing bids with price <= %ld. ", portfolio->buy_at + portfolio->trd_limit / 2 * portfolio->shift));
							int killed_amt = ordlog_event_kill_portfolio_orders(portfolio, portfolio->buy_at + portfolio->trd_limit / 2 * portfolio->shift, 1);

#ifdef ORDLOG_DEBUG
							if(proceed && killed_amt != portfolio->trd_limit / 2){
								printf("killed_amt != portfolio->trd_limit / 2\n");
								LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "killed_amt != portfolio->trd_limit / 2\n"));
								//exit_ordlog_debug_error(__LINE__, __func__);
							}
#endif

							LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "Amount = %d. ", killed_amt));
							LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "P (%d, %d). B: worst = %ld, best = %ld, cnt = %d, trd = %d, last = %ld, killed = %d. A: worst = %ld, best = %ld, cnt = %d, trd = %d, last = %ld, killed = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, portfolio->buy_at, portfolio->best_buy, portfolio->active_bids, portfolio->need_buy_amt, portfolio->best_buy_trd, portfolio->killed_buy_amt,  portfolio->sell_at, portfolio->best_sell, portfolio->active_asks, portfolio->need_sell_amt, portfolio->best_sell_trd, portfolio->killed_sell_amt, trans_tbl->cnt));


							//portfolio->sl = killed_amt;
							portfolio->sl = portfolio->trd_limit / 2;
							portfolio->sl_dir = 1;
							portfolio->state = SL_STATE;
						}
					}
				}break;
				case INN_STATE:{
					if(portfolio->same_inn_dir == 1){
						portfolio->same_inn = portfolio->best_buy - (portfolio->need_sell_amt + portfolio->killed_buy_amt) * portfolio->shift - portfolio->best_buy_trd + portfolio->shift;
						if(portfolio->same_inn){
							portfolio->same_inn = portfolio->same_inn / portfolio->shift;
							int kill_amt = ordlog_event_kill_portfolio_orders_rev(portfolio, portfolio->best_buy_trd, 1);

							LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "P (%d, %d). Killing crossing Bids = %d, cross_cnt = %d, best_buy = %ld, from = %ld, need_sell = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, kill_amt, portfolio->same_inn, portfolio->best_buy, portfolio->best_buy_trd, portfolio->need_sell_amt, trans_tbl->cnt - kill_amt));
							portfolio->used = 0;
						}else{
							portfolio->same_inn_dir = 0;
							portfolio->state = MM_STATE;
						}

					}else{
						portfolio->same_inn = portfolio->best_sell_trd - (portfolio->need_buy_amt + portfolio->killed_sell_amt) * portfolio->shift - portfolio->best_sell + portfolio->shift;
						if(portfolio->same_inn){
							portfolio->same_inn = portfolio->same_inn / portfolio->shift;
							int kill_amt = ordlog_event_kill_portfolio_orders_rev(portfolio, portfolio->best_sell_trd, 2);

							LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "P (%d, %d). Killing crossing Asks = %d, cross_cnt = %d, best_sell = %ld, from = %ld, need_buy = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, kill_amt, portfolio->same_inn, portfolio->best_sell, portfolio->best_sell_trd, portfolio->need_buy_amt, trans_tbl->cnt - kill_amt));
							portfolio->used = 0;
						}else{
							portfolio->same_inn_dir = 0;
							portfolio->state = MM_STATE;
						}
					}
				}break;
				case SL_STATE:{
					if(portfolio->sl_order == 0){
						if((portfolio->sl_dir == 1 && portfolio->sl == portfolio->killed_buy_amt) || (portfolio->sl_dir == 2 && portfolio->sl == portfolio->killed_sell_amt))
							ordlog_event_commit_portfolios_send_additional_portfolio_orders_minstep_trd(portfolio);

						portfolio->used = 0;
					}
				}break;
				case SLFIN_STATE:{
					if(portfolio->sl_order == 0){
						if((portfolio->sl_dir == 1 && portfolio->sl == portfolio->killed_buy_amt) || (portfolio->sl_dir == 2 && portfolio->sl == portfolio->killed_sell_amt)){
#ifdef ORDLOG_DEBUG
							if(portfolio->active_asks != 0 || portfolio->active_bids != 0){
								printf("portfolio->active_asks != 0 || portfolio->active_bids != 0\n");
								exit_ordlog_debug_error(__LINE__, __func__);
							}
#endif

							portfolio->sl_dir = 0;
							portfolio->sl = 0;
							portfolio->state = WAIT_STATE;
						}
					}

				}break;
				case NEED_STOP_STATE:{
					if(portfolio->active_asks == 0 && portfolio->active_bids == 0){
						portfolio->state = WAIT_STATE;
					}
				}break;
				case STOP_AND_CLOSE_STATE:{
					if(portfolio->active_asks == 0 && portfolio->active_bids == 0){
						portfolio->state = WAIT_STATE;

						int amount = 0;
						struct portfolio_t *hedge_portfolio = portfolio->hedge_portfolio;
						if(hedge_portfolio){
							amount = hedge_portfolio->amount;
							hedge_portfolio->amount = 0;
							hedge_portfolio->trade = 0;
						}
						amount += portfolio->amount;

						portfolio->amount = amount;

						struct quote_t *quote = portfolio->quotes_tbl->data[0];

						if(amount > 0) ordlog_event_send_order(quote, amount, 2, portfolio->wask - quote->minstep, ORDERTYPE_STOP_LOSS);
						else if(amount < 0) ordlog_event_send_order(quote, -amount, 1, portfolio->wbid + quote->minstep, ORDERTYPE_STOP_LOSS);
					}
				}break;
				case RESET_AND_STOP_STATE:{
					if(portfolio->active_asks == 0 && portfolio->active_bids == 0){
						portfolio->state = WAIT_STATE;

						portfolio->amount = 0;
						portfolio->iceberg_cnt = 0;
					}
				}break;

				}
			}break;
			case PORTFOLIOWORKTYPE_FOLLOW_TREND:{
				switch(portfolio->state){
				case STOP_AND_CLOSE_STATE:{
					if(portfolio->active_asks == 0 && portfolio->active_bids == 0){
						portfolio->state = WAIT_STATE;

						int amount = portfolio->amount;

						struct quote_t *quote = portfolio->quotes_tbl->data[0];

						if(amount > 0) ordlog_event_send_order(quote, amount, 2, portfolio->wask - quote->minstep, ORDERTYPE_STOP_LOSS);
						else if(amount < 0) ordlog_event_send_order(quote, -amount, 1, portfolio->wbid + quote->minstep, ORDERTYPE_STOP_LOSS);
					}
				}break;
				}

			}break;

			}
		}
	}

	if(modified){
		if(trans_thr_empty_orders_tbl->cnt == 0){
			struct active_orders_tbl_t *trans_tbl = ordrs_thr_empty_orders_tbl;
			ordrs_thr_empty_orders_tbl = trans_thr_empty_orders_tbl;
			trans_thr_empty_orders_tbl = trans_tbl;
		}

		if(snt_trans_tbl->data_size == 0){
			new_trans_tbl->first = 0;

			struct trans_pointers_tbl_t *trans_tbl = new_trans_tbl;
			new_trans_tbl = snt_trans_tbl;
			snt_trans_tbl = trans_tbl;
		}

		LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));
	}
}


void ordlog_event_portfolio_stop_trade(struct portfolio_t *portfolio)
{
	portfolio->trade = 0;

	ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
	ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);


	//portfolio->state = WAIT_STATE;
}

void portfolio_reset_trd_settings(struct portfolio_t *portfolio)
{
	switch(portfolio->work_type){
	case PORTFOLIOWORKTYPE_FOLLOW_TREND:{
		if(portfolio->zero_wasks_num == 0 && portfolio->zero_wbids_num == 0){
			int64_t mkt_spread = (portfolio->wask - portfolio->wbid - portfolio->spread) / 2;

			portfolio->buy_at = portfolio->wbid + mkt_spread;
			portfolio->sell_at = portfolio->buy_at + portfolio->spread;
		}else if(portfolio->zero_wbids_num == 0){
			portfolio->buy_at = portfolio->wbid;
			portfolio->sell_at = portfolio->buy_at + portfolio->spread;
		}else if(portfolio->zero_wasks_num == 0){
			portfolio->sell_at = portfolio->wask;
			portfolio->buy_at = portfolio->sell_at - portfolio->spread;
		}

		portfolio->amount = 0;
	}break;
	}

	if(portfolio->trade)return;

	if(portfolio->zero_wasks_num == 0 && portfolio->zero_wbids_num == 0){
		int64_t mkt_spread = (portfolio->wask - portfolio->wbid - portfolio->spread) / 2;

		portfolio->buy_at = portfolio->wbid + mkt_spread;
		portfolio->sell_at = portfolio->buy_at + portfolio->spread;
	}else if(portfolio->zero_wbids_num == 0){
		portfolio->buy_at = portfolio->wbid;
		portfolio->sell_at = portfolio->buy_at + portfolio->spread;
	}else if(portfolio->zero_wasks_num == 0){
		portfolio->sell_at = portfolio->wask;
		portfolio->buy_at = portfolio->sell_at - portfolio->spread;
	}

	portfolio->iceberg_cnt = 0;
	portfolio->sl_cnt = 0;
	portfolio->amount = 0;
}

void ordlog_event_check_modified_orders(void)
{
	unsigned long long it;
	for(it = 0; it < modified_orders_tbl->cnt; it++){
		struct ordref_ex_t *ordref = modified_orders_tbl->data[it];

		if(ordref->active){
			struct active_order_t *active = ordref->active;
			struct portfolio_t *portfolio = active->quote->portfolio;
			portfolio->used = 1;

			LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "  >ORD = %lld, dir = %d, amount = %d, price = %ld, state = %d\n", active->order_id, active->dir, ordref->amount_rest, active->price, active->state));

			int amount = active->amount_rest - ordref->amount_rest;
			if(amount){
				if(active->dir == 1) portfolio->amount += amount;
				else portfolio->amount -= amount;

#ifdef UNITTEST
				if(active->dir == 1) portfolio->ut_volume -= amount * active->price;
				else portfolio->ut_volume += amount * active->price;
#endif
			}


			if(session_tbl->state == FINISHED_SESST){
#ifdef ORDLOG_DEBUG
				if(active->state == ORDER_KILLED || active->state == ORDER_KILLING || active->state == ORDER_MOVING){
					printf("FINISHED_SESST! AND active->state == %d\n", active->state);
					//exit_ordlog_debug_error(__LINE__, __func__);
				}
#endif
				if(active->state == ORDER_ACTIVE) active->state = ORDER_KILLED;
			}else

//			if(active->state == MOVE_NOT_FOUND || active->state == KILL_NOT_FOUND)
//				active->state = ORDER_KILLED;



			switch(portfolio->work_type){
			case PORTFOLIOWORKTYPE_PATTERN:{
				switch(active->order_type){
				case ORDERTYPE_MARKET_MAKER:{
					LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "--MM = %lld, dir = %d, amount = %d, price = %ld, state = %d\n", active->order_id, active->dir, ordref->amount_rest, active->price, active->state));

					if(ordref->amount_rest == 0){
						if(active->dir == 1){
							portfolio->need_sell_amt++;
						}
						else{
							portfolio->need_buy_amt++;
						}
					}

					if(active->state == ORDER_ACTIVE){
						if(active->dir == 1){
							if((portfolio->same_inn_dir == 1 && active->price > portfolio->best_buy_trd) || (portfolio->sl_dir == 1 && active->price <= portfolio->buy_at)){
								if(ordref->amount_rest) ordlog_event_kill_order(active);
								else active->state = ORDER_KILLED;
							}
						}else if(active->dir == 2){
							if((portfolio->same_inn_dir == 2 && active->price < portfolio->best_sell_trd) || (portfolio->sl_dir == 2 && active->price >= portfolio->sell_at)){
								if(ordref->amount_rest) ordlog_event_kill_order(active);
								else active->state = ORDER_KILLED;
							}
						}
					}


					switch(active->state){
					case ORDER_ACTIVE:{
						if(ordref->amount_rest == 0){
							if(active->dir == 1){
								portfolio->active_bids--;
								if(active->price < portfolio->best_buy_trd) portfolio->best_buy_trd = active->price;
							}else{
								portfolio->active_asks--;
								if(active->price > portfolio->best_sell_trd) portfolio->best_sell_trd = active->price;
							}
						}
					}break;
					case ORDER_KILLING:{
						if(ordref->amount_rest == 0){
							if(active->dir == 1){
								portfolio->active_bids--;

								if(portfolio->sl_dir == 1) portfolio->sl--;
							}else{
								portfolio->active_asks--;

								if(portfolio->sl_dir == 2) portfolio->sl--;
							}
						}
					}break;
//					case MOVE_NOT_FOUND:
//					case KILL_NOT_FOUND:
					case ORDER_KILLED:{
						if(active->dir == 1){
							portfolio->active_bids--;
							if(ordref->amount_rest) portfolio->killed_buy_amt++;
						}else{
							portfolio->active_asks--;
							if(ordref->amount_rest) portfolio->killed_sell_amt++;
						}
					}break;
					}
				}break;

				case ORDERTYPE_STOP_LOSS:{
					LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "--SL = %lld, dir = %d, amount = %d, price = %ld, state = %d\n", active->order_id, active->dir, ordref->amount_rest, active->price, active->state));

					//int amount = active->amount_rest - ordref->amount_rest;
					if(ordref->amount_rest == 0){
						if(active->dir == 1){
							portfolio->active_bids--;
						}else{
							portfolio->active_asks--;
						}
						portfolio->sl_order = 0;
						LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "--SL = %lld, dir = %d, price = %ld is killed. amount = %d, bids = %d, asks = %d. Adding additional orders\n", active->order_id, active->dir, active->price, portfolio->amount, portfolio->active_bids, portfolio->active_asks));
					}
				}break;
				}
			}break;
			case PORTFOLIOWORKTYPE_FOLLOW_TREND:{
				switch(active->order_type){
				case ORDERTYPE_STOP_LOSS:{
					if(ordref->amount_rest == 0){
						if(active->dir == 1){
							portfolio->active_bids--;
						}else{
							portfolio->active_asks--;
						}
					}
				}break;
				case ORDERTYPE_TAKE_PROFIT:{
					if(ordref->amount_rest == 0 || active->state == ORDER_KILLED){
						if(active->dir == 1){
							portfolio->active_bids--;
							portfolio->sell = 1;
							portfolio->best_sell = LLONG_MAX;
						}else{
							portfolio->active_asks--;
							portfolio->buy = 1;
							portfolio->best_buy = LLONG_MIN;
						}

						if(ordref->amount_rest == 0){
							portfolio->iceberg_cnt = portfolio->amount % portfolio->trd_amt;
							if(active->dir == 1)
								portfolio->sell_at = portfolio->buy_at + portfolio->spread;
							else
								portfolio->buy_at = portfolio->sell_at - portfolio->spread;
						}
					}
				}break;
				}
			}break;

			default:{
				LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "--?? = %lld, dir = %d, amount = %d, price = %ld, state = %d\n", active->order_id, active->dir, ordref->amount_rest, active->price, active->state));
			}break;
			}
			active->amount_rest = ordref->amount_rest;


			if(ordref->amount_rest == 0 || active->state == ORDER_KILLED){
				LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "  <ORD = %lld, dir = %d, amount = %d, price = %ld, state = %d\n", active->order_id, active->dir, ordref->amount_rest, active->price, active->state));


				struct active_order_t *prev = active->prev;
				struct active_order_t *next = active->next;


				switch(active->order_type){
				case ORDERTYPE_TAKE_PROFIT:
				case ORDERTYPE_MARKET_MAKER:{
					if(prev){
						prev->next = next;
						if(next) next->prev = prev;
					}
					else{
						portfolio->active_orders_tbl = next;
						if(portfolio->active_orders_tbl) portfolio->active_orders_tbl->prev = NULL;
					}
				}break;
				case ORDERTYPE_STOP_LOSS:{
					if(prev){
						prev->next = next;
						if(next) next->prev = prev;
					}
					else{
						active_orders_tbl = next;
						if(active_orders_tbl) active_orders_tbl->prev = NULL;
					}
				}break;
				}


				active->prev = NULL;
				active->next = NULL;
				ordref->active = NULL;

				if(active->state != ORDER_MOVING && active->state != ORDER_KILLING)
					ordrs_thr_empty_orders_tbl->data[ordrs_thr_empty_orders_tbl->cnt++] = active;
			}else
				LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "  <ORD = %lld, dir = %d, amount = %d, price = %ld, state = %d.\n", active->order_id, active->dir, ordref->amount_rest, active->price, active->state));

		}
	}
	modified_orders_tbl->cnt = 0;


	if(trans_thr_empty_orders_tbl->cnt == 0){
		struct active_orders_tbl_t *trans_tbl = ordrs_thr_empty_orders_tbl;
		ordrs_thr_empty_orders_tbl = trans_thr_empty_orders_tbl;
		trans_thr_empty_orders_tbl = trans_tbl;
	}

	if(snt_trans_tbl->data_size == 0){
		new_trans_tbl->first = 0;

		struct trans_pointers_tbl_t *trans_tbl = new_trans_tbl;
		new_trans_tbl = snt_trans_tbl;
		snt_trans_tbl = trans_tbl;
	}


#if defined(LOG_SAVER) && defined(LOGIC_LOG)
	for(it = 0; it < portfolios_tbl->cnt; it++){
		struct portfolio_t *portfolio = portfolios_tbl->data[it];
		if(portfolio->used){
			LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "\tP(%d): state=%d, trade=%d, iceberg_cnt=%d, wbid=%ld, lbid=%ld, rbid=%ld, buy=%d, best_buy=%ld, buy_at=%ld, best_buy_trd=%ld, active_bids=%d, need_buy_amt=%d, killed_buy_amt=%d, zero_wbids_num=%d, sell=%d, wask=%ld, lask=%ld, rask=%ld, best_sell=%ld, sell_at=%ld, best_sell_trd=%ld, active_asks=%d, need_sell_amt=%d, killed_sell_amt=%d, zero_wasks_num=%d, sl=%d, sl_dir=%d, sl_order=%d, sl_cnt=%d, same_inn=%d, same_inn_dir=%d, same_inn_ch=%d\n",
				portfolio->portfolio_id, portfolio->state, portfolio->trade, portfolio->iceberg_cnt, portfolio->wbid, portfolio->lbid, portfolio->rbid, portfolio->buy, portfolio->best_buy, portfolio->buy_at, portfolio->best_buy_trd, portfolio->active_bids, portfolio->need_buy_amt, portfolio->killed_buy_amt, portfolio->zero_wbids_num, portfolio->sell, portfolio->wask, portfolio->lask, portfolio->rask, portfolio->best_sell, portfolio->sell_at, portfolio->best_sell_trd, portfolio->active_asks, portfolio->need_sell_amt, portfolio->killed_sell_amt, portfolio->zero_wasks_num, portfolio->sl, portfolio->sl_dir, portfolio->sl_order, portfolio->sl_cnt, portfolio->same_inn, portfolio->same_inn_dir, portfolio->same_inn_ch));
		}
	}
#endif


	LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));

}



void ordlog_event_send_order(struct quote_t *quote, int amount, int dir, int64_t price, int order_type)
{
//	if(!quote->portfolio->trade) return;
	if(!proceed) return;

	if(dir == 1)quote->portfolio->active_bids++;
	else quote->portfolio->active_asks++;

	long trans_id = trans_tbl->cnt++;
	struct trans_t *trans = trans_tbl->data[trans_id];

#ifdef RTT_STAT
	trans->ordlog_listener_TN_COMMIT = ordlog_listener_TN_COMMIT;
	trans->ordlog_listener_TN_BEGIN = ordlog_listener_TN_BEGIN;
#endif


	//Fill transaction
	trans->p2msg = quote->isin_data->msg_data;
	quote->isin_data->msg_data = quote->isin_data->msg_data->next;

	struct cg_msg_data_ex_t *p2msg = trans->p2msg;

	p2msg->origin->user_id = trans_id;
	*p2msg->amount = amount;
	*p2msg->dir = dir;
	*p2msg->type = 1;

	price_to_str_map(price, p2msg->price, quote->price_scale);

	trans->active_order = orders_pool_tbl->first;
	orders_pool_tbl->first = orders_pool_tbl->first->next;
	trans->active_order->quote = quote;
	trans->active_order->order_type = order_type;

	////////////////
	new_trans_tbl->data[new_trans_tbl->data_size++] = trans_id;

#ifdef UNITTEST
		trans->active_order->price = price;
#endif
}

void price_to_str_map(uint64_t value, char* buffer, int8_t scale)
{
#ifdef ORDLOG_DEBUG
	if(scale >= price_to_str_tbl->data_size){
		printf("scale >= price_to_str_tbl->data_size\n");
		exit_ordlog_debug_error(__LINE__, __func__);
	}

	uint64_t value_deb = value - price_to_str_tbl->data[scale]->shift;
	if(value_deb < 0 || value_deb >= price_to_str_tbl->data[scale]->data_size){
		printf("price < 0 || price >= price_to_str_tbl->data[scale]->data_size. Price = %ld, Scale = %d\n", value, scale);
		exit_ordlog_debug_error(__LINE__, __func__);
	}
#endif

	value -= price_to_str_tbl->data[scale]->shift;
	memcpy(buffer, price_to_str_tbl->data[scale]->data[value], price_to_str_tbl->size_of_price);
}

void ordlog_event_new_session(signed int new_sess_id)
{


	int it;
	for (it = 0; it < futsesscont_tbl->data_size; it++){
		struct sesscont_ex_t *sesscont_ex = futsesscont_tbl->data[it];
		if(sesscont_ex){
			signed int isin_id = sesscont_ex->origin.isin_id;
			signed int sess_id = new_sess_id;
			create_isins_tbl_entry(&isin_id, &sess_id);

			struct sess_data_row_t *sess_data = isins_tbl->data[isin_id]->data[sess_id];
			isins_tbl->ext_links++;


			sess_data->cont = sesscont_ex;
			fill_isins_tbl_entry(isins_tbl->data[isin_id], sess_data);
		}
	}
}



void ordlog_event_online(struct cg_msg_streamdata_t *replmsg)
{
	struct orders_log *ordlog = (struct orders_log*)replmsg->data;
	ordref_tbl->replRev = ordlog->replRev;
	if((ordlog->xstatus & 0x04) == 0x04) return;

#ifdef ORDERS_LOG
	int64_t price_int;
	int8_t price_scale;
	cg_bcd_get(ordlog->price, &price_int, &price_scale);
	ORDERSLOG_FUNC(logsaver(ORDERS_LOG_ID, "%lld,%lld,%lld,%lld,%d,%d,%d,%d,%lld,%ld,%d\n", ordlog->replID, ordlog->replRev, ordlog->replAct, ordlog->id_ord, ordlog->amount_rest, ordlog->sess_id, ordlog->isin_id, ordlog->dir, ordlog->xstatus, price_int, price_scale));

	ORDERSLOG_FUNC(thread_func_logger_flush(ORDERS_LOG_ID));
#endif

	signed int isin_id = ordlog->isin_id - isins_tbl->shift;

#ifdef IGNORE_UNKNOWN_ORDERS
	//Ignore option's market data and ????
	if(isin_id < 0 || isin_id >= isins_tbl->data_size || isins_tbl->data[isin_id] == NULL) return;
#endif

	if(integr_state_tmp == 2 && isins_tbl->data[isin_id]->quotes_tbl == NULL) return;

	signed int sess_id = ordlog->sess_id - isins_tbl->sess_id.min;
	if(sess_id >= isins_tbl->data[isin_id]->data_size){
		LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "ordlog data before instruments data: no such session (%d)!. Adding it.\n", ordlog->sess_id));
		ordlog_event_new_session(ordlog->sess_id);
	}


#ifdef ORDLOG_DEBUG
		signed int sess_id_deb = ordlog->sess_id - isins_tbl->sess_id.min;
		if(sess_id_deb < 0){
			printf("ordlog data before instruments data: lower session! (%d)!\n", ordlog->sess_id);
			exit_ordlog_debug_error(__LINE__, __func__);
		}else if(sess_id_deb >= isins_tbl->data[isin_id]->data_size){
			printf("ordlog data before instruments data: higher session! (%d)!\n", ordlog->sess_id);
			exit_ordlog_debug_error(__LINE__, __func__);
		}else if(isins_tbl->data[isin_id]->data[sess_id_deb] == NULL){
			printf("ordlog data before instruments data: missed session! (%d)!\n", ordlog->sess_id);
			exit_ordlog_debug_error(__LINE__, __func__);
		}
#endif



	if(isins_tbl->data[isin_id]->data[sess_id]->destroyed) return;





	char useful = (ordlog->replAct == 0);

	char isin_affected = 0;

	signed long long order_id = ordlog->id_ord - ordref_tbl->shift;

#ifdef ORDLOG_DEBUG
	if(order_id < 0){
		printf("order_id < 0! id_ord = %lld, amount_rest = %d\n", ordlog->id_ord, ordlog->amount_rest);
		if(ordlog->amount_rest)
			exit_ordlog_debug_error(__LINE__, __func__);
		else
			return;
	}
#endif


	if(order_id >= ordref_tbl->data_size){
		long long mltp = (order_id - ordref_tbl->data_size) / ORDLOGSIZE_INCREMENT + 1;
		ordref_tbl->data_size += ORDLOGSIZE_INCREMENT * mltp;
		ordref_tbl->data = realloc(ordref_tbl->data, sizeof(struct ordref_ex_t *) * ordref_tbl->data_size);
		if(ordref_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

		LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "order_id = %lld, data_size = %lld, shift = %lld\n", order_id, ordref_tbl->data_size, ordref_tbl->shift));
		LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "order_id >= ordref_tbl->data_size!. ordref_tbl->data_size = %lld\n", ordref_tbl->data_size));

		int it;
		for(it = ordref_tbl->data_size - ORDLOGSIZE_INCREMENT * mltp; it < ordref_tbl->data_size; it++){
			struct ordref_ex_t *ordref = malloc(sizeof(struct ordref_ex_t));
			if(ordref == NULL) exit_memory_alloc_error(__LINE__, __func__);
			ordref_tbl->data[it] = ordref;

			ordref->replRev = -1;
			ordref->amount_rest = 0;
			ordref->active = NULL;
		}
	}

	struct ordref_ex_t *ordref = ordref_tbl->data[order_id];

#ifdef ORDLOG_DEBUG
	if(ordref == NULL){
		printf("ordref = NULL!. id_ord = %lld, amount_rest = %d, shift = %lld, data_size = %lld\n", ordlog->id_ord, ordlog->amount_rest, ordref_tbl->shift, ordref_tbl->data_size);
		//exit_ordlog_debug_error(__LINE__, __func__);
		return;
	}

	ordref->order_id = ordlog->id_ord;
#endif


	if(useful){
		if(ordref->replRev == -1){
#ifdef ORDLOG_DEBUG
			if(ordlog->amount_rest == 0 && ordlog_listener_state == CG_MSG_P2REPL_ONLINE){
				//printf("ordlog->amount_rest == 0\n");
				//exit_ordlog_debug_error(__LINE__, __func__);
			}
#endif

			if(ordref_tbl->movingorder && (ordlog->xstatus & 0x100000)){
				ordref->active = ordref_tbl->movingorder;
				ordref->active->order_id = ordlog->id_ord;
				ordref_tbl->movingorder = NULL;
			}

			signed int isin_id = ordlog->isin_id - isins_tbl->shift;
			signed int sess_id = ordlog->sess_id - isins_tbl->sess_id.min;

			int64_t price_int;
			int8_t price_scale;

			cg_bcd_get(ordlog->price, &price_int, &price_scale);


			struct sess_data_row_t *sess_data = isins_tbl->data[isin_id]->data[sess_id];


			int agp_position = (price_int - sess_data->cont->limit_down_int) / sess_data->cont->minstep_int;

			if(agp_position >= 0 && agp_position < sess_data->bidEx->data_size){


				sess_data->ords_cnt++;
				struct price_aggr_t *price_aggr;

				if(ordlog->dir == 1){
					price_aggr = sess_data->bidEx->data[agp_position];
					if(agp_position > sess_data->bidEx->first){
						if(agp_position > sess_data->askEx->first && ordlog->amount_rest > sess_data->askEx->data[sess_data->askEx->first]->amount_rest){
							sess_data->bidEx->first = agp_position;
						}
						else
							sess_data->bidEx->first = sess_data->askEx->first;
					}

					if(agp_position >= sess_data->bidEx->last) isin_affected = 1;
				}else{
					price_aggr = sess_data->askEx->data[agp_position];

					if(agp_position < sess_data->askEx->first){
						if(agp_position < sess_data->bidEx->first && ordlog->amount_rest > sess_data->bidEx->data[sess_data->bidEx->first]->amount_rest){
							sess_data->askEx->first = agp_position;
						}
						else
							sess_data->askEx->first = sess_data->bidEx->first;

					}

					if(agp_position <= sess_data->askEx->last) isin_affected = 1;
				}


				price_aggr->amount_rest += ordlog->amount_rest;

				ordref->replID = ordlog->replID;
				ordref->replRev = ordlog->replRev;
				ordref->amount_rest = ordlog->amount_rest;
				ordref->price_aggr = price_aggr;
				ordref->sess_data = sess_data;
				ordref->agp_position = agp_position;


				if(ordref->active){
					modified_orders_tbl->data[modified_orders_tbl->cnt++] = ordref;
					ordref->active->price = ordref->agp_position * ordref->sess_data->cont->minstep_adj + ordref->sess_data->cont->limit_down_adj;
/*
#ifdef ORDLOG_DEBUG
					if(ordref->active->state != ORDER_ADDING || ordref->active->state != ORDER_MOVING){
						printf("ordref->active->state = %d\n", ordref->active->state);
						exit_ordlog_debug_error(__LINE__, __func__);
					}
#endif
*/
					ordref->active->state = ORDER_ACTIVE;
				}

			}else{
				LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "price = %ld outside the bounds! limit_down = %ld, limit_up = %ld\n", price_int, sess_data->cont->limit_down_int, sess_data->cont->limit_up_int));
			}
		}else {
#ifdef ORDLOG_DEBUG
			if(ordlog->replRev <= ordref->replRev){
				printf("ordlog->replRev <= ordref->replRev\n");
				exit_ordlog_debug_error(__LINE__, __func__);
			}
#endif

			if(ordref->active){
				if(ordlog->xstatus & 0x100000){
					ordref_tbl->movingorder = ordref->active;
					ordref->active = NULL;

#ifdef ORDLOG_DEBUG
					if(ordlog->action){
						printf("ordlog->action = %d\n", ordlog->action);
						exit_ordlog_debug_error(__LINE__, __func__);
					}
#endif

				}else{
					if(ordlog->action == 0)
						ordref->active->state = ORDER_KILLED;

					modified_orders_tbl->data[modified_orders_tbl->cnt++] = ordref;
				}
			}

			if((ordlog->dir == 1 && ordref->agp_position >= ordref->sess_data->bidEx->last)
					|| (ordlog->dir == 2 && ordref->agp_position <= ordref->sess_data->askEx->last)) isin_affected = 1;

			struct price_aggr_t *price_aggr = ordref->price_aggr;
			if(ordlog->amount_rest == 0){

				price_aggr->amount_rest -= ordref->amount_rest;

				// Trade!
				if(ordlog->action == 2) ordref->amount_rest = 0;

				//ordref->action = ordlog->action;

				ordref->replID = ordlog->replID;
				ordref->replRev = ordlog->replRev;

				/*
				if(ordref->active == NULL){
					free(ordref);
					ordref_tbl->data[order_id] = NULL;
				}
				*/
			}else{

				price_aggr->amount_rest -= ordref->amount_rest;
				price_aggr->amount_rest += ordlog->amount_rest;

				ordref->amount_rest = ordlog->amount_rest;
				ordref->replID = ordlog->replID;
				ordref->replRev = ordlog->replRev;

			}

#ifdef ORDLOG_DEBUG
			if(price_aggr->amount_rest < 0){
				printf("price_aggr->amount_rest < 0\n");
				exit_ordlog_debug_error(__LINE__, __func__);
			}
#endif
		}
	}else{
		if(ordref->replID == ordlog->replID){
			printf("ordref->replID = ordlog->replID && ordlog->replAct\n");
			exit_ordlog_debug_error(__LINE__, __func__);

			ordref->sess_data->ords_cnt--;
			free(ordref);
			ordref_tbl->data[order_id] = NULL;
		}else{
			LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "ordlog->replAct\n"));
		}
	}


	LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));




	if(isin_affected){
		if(affected_tbl->cnt >= affected_tbl->data_size){

			affected_tbl->data_size += AFFECTED_ISINS;
			affected_tbl->data = realloc(affected_tbl->data, sizeof(signed int) * affected_tbl->data_size);
			if(affected_tbl->data == NULL)
				exit_memory_alloc_error(__LINE__, __func__);
		}
		affected_tbl->data[affected_tbl->cnt++] = ordlog->isin_id;
	}


}

void ordlog_event_set_online(void)
{
	if(ordref_tbl->data_size != 0) return;

	unsigned long long it;
	for(it = 0; it < ss_ordref_tbl->data_size; it++){
		struct orders_log_ex_t *curr = ss_ordref_tbl->data[it];
		if(curr && curr->amount_rest){
			ordref_tbl->shift = curr->id_ord;
			break;
		}
	}

	if(ordref_tbl->shift == 0) ordref_tbl->shift = ss_ordref_tbl->shift;

	int order_id = ss_ordref_tbl->order_id.max - ordref_tbl->shift;

	if(order_id >= ordref_tbl->data_size){
		long long mltp = (order_id - ordref_tbl->data_size) / ORDLOGSIZE_INCREMENT + 1;
		ordref_tbl->data_size += ORDLOGSIZE_INCREMENT * mltp;
		ordref_tbl->data = realloc(ordref_tbl->data, sizeof(struct ordref_ex_t *) * ordref_tbl->data_size);
		if(ordref_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

		unsigned long long it;
		for(it = ordref_tbl->data_size - ORDLOGSIZE_INCREMENT * mltp; it < ordref_tbl->data_size; it++){
			struct ordref_ex_t *ordref = malloc(sizeof(struct ordref_ex_t));
			if(ordref == NULL) exit_memory_alloc_error(__LINE__, __func__);
			ordref_tbl->data[it] = ordref;

			ordref->replRev = -1;
			ordref->amount_rest = 0;
			ordref->active = NULL;
		}
	}


	for(it = 0; it < ss_ordref_tbl->data_size; it++){
		struct orders_log_ex_t *curr = ss_ordref_tbl->data[it];
		if(curr){
			if(curr->amount_rest){
				signed long long id_ord = curr->id_ord - ordref_tbl->shift;

				signed int isin_id = curr->isin_id - isins_tbl->shift;
				signed int sess_id = curr->sess_id - isins_tbl->sess_id.min;



#ifdef ORDLOG_DEBUG
				if(isin_id < 0 || isin_id >= isins_tbl->data_size){
					printf("ordlog data before instruments data\n");
					exit_ordlog_debug_error(__LINE__, __func__);
				}

#ifdef IGNORE_UNKNOWN_ORDERS
				if(isins_tbl->data[isin_id] == NULL){
					struct orders_log_ex_t *to_free = curr;
					curr = curr->next;
					free(to_free);

					continue;
				}
#endif
				if(sess_id < 0 || sess_id >= isins_tbl->data[isin_id]->data_size){
					printf("ordlog data before instruments data\n");
					exit_ordlog_debug_error(__LINE__, __func__);
				}
#endif

#ifdef IGNORE_UNKNOWN_ORDERS
				if(isins_tbl->data[isin_id] == NULL){
					struct orders_log_ex_t *to_free = curr;
					curr = curr->next;
					free(to_free);

					continue;
				}
#endif

				struct sess_data_row_t *sess_data = isins_tbl->data[isin_id]->data[sess_id];
				int agp_position = (curr->price_int - sess_data->cont->limit_down_int) / sess_data->cont->minstep_int;

				if(agp_position >= 0 && agp_position < sess_data->bidEx->data_size){

					struct ordref_ex_t *ordref = ordref_tbl->data[id_ord];
					if(ordref == NULL){
						ordref = malloc(sizeof(struct ordref_ex_t));
						if(ordref == NULL) exit_memory_alloc_error(__LINE__, __func__);

						ordref_tbl->data[id_ord] = ordref;
					}

					struct price_aggr_t *price_aggr;

					if(curr->dir == 1){
						price_aggr = sess_data->bidEx->data[agp_position];
						if(agp_position > sess_data->bidEx->first) sess_data->bidEx->first = agp_position;
					}else{
						price_aggr = sess_data->askEx->data[agp_position];
						if(agp_position < sess_data->askEx->first) sess_data->askEx->first = agp_position;
					}

					price_aggr->amount_rest += curr->amount_rest;

					ordref->replID = curr->replID;
					ordref->replRev = curr->replRev;
					ordref->amount_rest = curr->amount_rest;
					ordref->price_aggr = price_aggr;
					ordref->sess_data = sess_data;
					ordref->agp_position = agp_position;
					ordref->active = NULL;


#ifdef ORDLOG_DEBUG
					if(price_aggr->amount_rest < 0){
						printf("price_aggr->amount_rest = %d\n", price_aggr->amount_rest);
						exit_ordlog_debug_error(__LINE__, __func__);
					}
#endif
				}


//	printf("%lld,%lld,%lld,%lld,%d,%d,%d,%d,%lld,%ld,%d\n", curr->replID, curr->replRev, curr->replAct, curr->id_ord, curr->amount_rest, curr->sess_id, curr->isin_id, curr->dir, 0, curr->price_int, curr->price_scale);
			}

			free(curr);
		}
	}

	free(ss_ordref_tbl->data);
	ss_ordref_tbl->data = NULL;
	ss_ordref_tbl->data_size = 0;

	ss_ordref_tbl->order_id.min = LLONG_MAX;
	ss_ordref_tbl->order_id.max = LLONG_MIN;

	printf("--------ONLINE!\n");
}

void ordlog_event_external_events(void)
{
	uint it;
	for(it = 0; it < modified_fields_tbl->cnt; it++){
		struct field_value_t *filed_value = modified_fields_tbl->data[it];

		switch(filed_value->owner){
		case PORTFOLIOS_OWNER:
			ordlog_event_portfolio_field_changed(filed_value);
			//printf("Owner = %d, owner_id = %d, field = %d, data_size = %zu\n", filed_value->owner, filed_value->owner_id, filed_value->field, filed_value->data_size);
			break;
		}
	}

	modified_fields_tbl->cnt = 0;
}

void ordlog_event_portfolio_field_changed(struct field_value_t *filed_value)
{
	struct portfolio_t *portfolio = portfolios_tbl->data[filed_value->owner_id];

	switch(filed_value->field){
	case COMMAND_FIELD:{
		char command;
		memcpy(&command, filed_value->data, sizeof(char));

		switch(command){
		case SYNCHRONIZE_COMMAND:{
			ordref_tbl->sync_replRev = ((ordref_tbl->replRev / 20000) + 1) * 20000;
		}break;
		case NEED_START_COMMAND:{
			if(portfolio->state == WAIT_STATE){
				portfolio->trade = 1;
				portfolio->buy = 1;
				portfolio->sell = 1;

				clear_portfolio_trd_sate(portfolio);
			}
		}break;
		case NEED_STOP_COMMAND:{
			if(portfolio->trade){
				portfolio->trade = 0;
				portfolio->state = NEED_STOP_STATE;
				portfolio->used = 1;

				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);
			}
		}break;
		case SET_MM_WORKTYPE_COMMAND:{
			if(portfolio->work_type == PORTFOLIOWORKTYPE_PATTERN){
				if(portfolio->hedge_portfolio && portfolio->hedge_portfolio->trade){
					portfolio->hedge_portfolio->trade = 0;
					portfolio->hedge_portfolio->buy = 0;
					portfolio->hedge_portfolio->sell = 0;

					portfolio->amount += portfolio->hedge_portfolio->amount;
					portfolio->hedge_portfolio->amount = 0;

					if(portfolio->trd_limit)
						portfolio->iceberg_cnt = portfolio->amount / portfolio->trd_limit;

					portfolio->state = NEED_STOP_STATE;
					portfolio->trade = 1;
					portfolio->buy = 1;
					portfolio->sell = 1;
					portfolio->used = 1;
					ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
					ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);
				}else if(portfolio->trd_limit)
					portfolio->iceberg_cnt = portfolio->amount / portfolio->trd_limit;
			}
		}break;
		case SET_FR_WORKTYPE_COMMAND:{
			if(portfolio->work_type == PORTFOLIOWORKTYPE_PATTERN)
				if(portfolio->hedge_portfolio){// && !portfolio->hedge_portfolio->trade){

					portfolio->trade = 0;
					portfolio->state = NEED_STOP_STATE;
					portfolio->used = 1;
					ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
					ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);


					struct portfolio_t *hedge_portfolio = portfolio->hedge_portfolio;
					hedge_portfolio->trade = 1;
					hedge_portfolio->work_type = PORTFOLIOWORKTYPE_FOLLOW_TREND;
					hedge_portfolio->sell = 1;
					hedge_portfolio->buy = 1;
					hedge_portfolio->sell_at = hedge_portfolio->wbid;
					hedge_portfolio->best_sell = hedge_portfolio->sell_at;
					hedge_portfolio->buy_at = hedge_portfolio->sell_at - hedge_portfolio->spread;
					hedge_portfolio->best_buy = hedge_portfolio->buy_at;
					hedge_portfolio->trd_amt = portfolio->hedge_mltp * portfolio->trd_amt;
					hedge_portfolio->iceberg_cnt = 0;

					portfolio->amount += hedge_portfolio->amount;
					hedge_portfolio->amount = 0;
				}
		}break;
		case STOP_AND_CLOSE_COMMAND:{
			if(portfolio->trade || portfolio->state == WAIT_STATE){
				if(portfolio->hedge_portfolio && portfolio->hedge_portfolio->trade){
					portfolio->hedge_portfolio->trade = 0;
					portfolio->hedge_portfolio->state = STOP_AND_CLOSE_STATE;
					portfolio->hedge_portfolio->used = 1;

					portfolio->state = NEED_STOP_STATE;
					portfolio->trade = 0;
//					portfolio->buy = 1;
//					portfolio->sell = 1;
					portfolio->used = 1;
					ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
					ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);
				}else{
					portfolio->trade = 0;
					portfolio->state = STOP_AND_CLOSE_STATE;
					portfolio->used = 1;

					ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
					ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);
				}
			}

			/*
			if(portfolio->trade || portfolio->state == WAIT_STATE){
				portfolio->trade = 0;
				portfolio->state = STOP_AND_CLOSE_STATE;
				portfolio->used = 1;

				if(portfolio->hedge_portfolio) portfolio->hedge_portfolio->trade = 0;

				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);
			}
			*/
		}break;
		case RESET_AND_STOP_COMMAND:{
			//if(portfolio->trade || portfolio->state == WAIT_STATE){
				portfolio->trade = 0;
				portfolio->state = RESET_AND_STOP_STATE;
				portfolio->used = 1;

				if(portfolio->hedge_portfolio) portfolio->hedge_portfolio->trade = 0;

				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);
			//}
		}break;
		case SYSTEM_START_COMMAND:{
			started_tmp = 1;
		}break;
		case SYSTEM_STOP_COMMAND:{
			started_tmp = 0;
		}break;

		}
	}break;
	}
}



void ordlog_event_portfolio_field_changed_old(struct field_value_t *filed_value)
{
	struct portfolio_t *portfolio = portfolios_tbl->data[filed_value->owner_id];

	switch(filed_value->field){
	case WORKTYPE_FIELD:{
		if(portfolio->work_type != PORTFOLIOWORKTYPE_PATTERN) return;

		int work_type;
		memcpy(&work_type, filed_value->data, sizeof(int));

		switch(work_type){
		case PORTFOLIOWORKTYPE_PATTERN:
			if(portfolio->hedge_portfolio && portfolio->hedge_portfolio->trade){
				portfolio->hedge_portfolio->trade = 0;

				portfolio->amount += portfolio->hedge_portfolio->amount;


				portfolio->state = NEED_STOP_STATE;
				portfolio->trade = 1;
				portfolio->used = 1;
				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);
			}

			break;
		case PORTFOLIOWORKTYPE_FOLLOW_TREND:
			if(portfolio->hedge_portfolio){// && !portfolio->hedge_portfolio->trade){
				portfolio->trade = 0;
				portfolio->state = NEED_STOP_STATE;
				portfolio->used = 1;
				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);


				struct portfolio_t *hedge_portfolio = portfolio->hedge_portfolio;
				hedge_portfolio->trade = 1;
				hedge_portfolio->work_type = PORTFOLIOWORKTYPE_FOLLOW_TREND;
				hedge_portfolio->sell = 1;
				hedge_portfolio->buy = 1;
				hedge_portfolio->sell_at = hedge_portfolio->wbid;
				hedge_portfolio->best_sell = hedge_portfolio->sell_at;
				hedge_portfolio->buy_at = hedge_portfolio->sell_at - hedge_portfolio->spread;
				hedge_portfolio->best_buy = hedge_portfolio->buy_at;
				hedge_portfolio->trd_amt = portfolio->hedge_mltp * portfolio->trd_amt;

				portfolio->amount += hedge_portfolio->amount;
				hedge_portfolio->amount = 0;
			}
			break;
		}
	}break;
	case STATE_FIELD:{
		char state = 0;
		memcpy(&state, filed_value->data, sizeof(char));

		switch(state){
		case NEED_STOP_STATE:
			if(portfolio->trade){
				portfolio->trade = 0;
				portfolio->state = NEED_STOP_STATE;
				portfolio->used = 1;

				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);
			}
			break;
		case STOP_AND_CLOSE_STATE:
			if(portfolio->trade || portfolio->state == WAIT_STATE){
				portfolio->trade = 0;
				portfolio->state = STOP_AND_CLOSE_STATE;
				portfolio->used = 1;

				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);
			}
			break;
		case RESET_AND_STOP_STATE:
			if(portfolio->trade || portfolio->state == WAIT_STATE){
				portfolio->trade = 0;
				portfolio->state = RESET_AND_STOP_STATE;
				portfolio->used = 1;

				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
				ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);
			}
			break;
		}

	}break;
	}
}


void ordlog_event_commit_portfolios(void)
{
#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
	if(!system_started) return;
#else
	if(!started_tmp) return;
#endif


	int it;
	for(it = 0; it < pts_affected_tbl->cnt; it++){
		struct portfolio_t *portfolio = portfolios_tbl->data[pts_affected_tbl->data[it]];
		portfolio->not_affected = 1;

		if(portfolio->trade){
			switch(portfolio->work_type){
			case PORTFOLIOWORKTYPE_FOLLOW_TREND:{
				if(portfolio->wbid > portfolio->best_buy && portfolio->wbid > portfolio->sell_at && portfolio->zero_wasks_num == 0){
					if(portfolio->buy){
						ordlog_event_send_portfolio_orders(portfolio, portfolio->trd_amt, 2, ORDERTYPE_COMMON);
						LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "##P (%d, %d, %d). B: buy_at = %ld, cnt = %d. A: sell_at = %ld, cnt = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, portfolio->iceberg_cnt, portfolio->buy_at, portfolio->active_bids,  portfolio->sell_at, portfolio->active_asks, trans_tbl->cnt - 1));
						LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));
						portfolio->best_buy = portfolio->wbid;

						portfolio->iceberg_cnt++;
						if(portfolio->iceberg_cnt == portfolio->iceberg_limit){
							int amount = portfolio->amount;
							if(amount < 0) amount = 0;
							//amount += portfolio->active_bids * portfolio->trd_amt;

							if(amount){
								struct quote_t *quote = portfolio->quotes_tbl->data[0];
								ordlog_event_send_order(quote, amount, 2, quote->ask_tgt + quote->minstep, ORDERTYPE_TAKE_PROFIT);

								//ordlog_event_send_portfolio_orders(portfolio, -amount, 1, ORDERTYPE_TAKE_PROFIT);
								portfolio->buy = 0;
							}else
								portfolio->iceberg_cnt--;

						}

						if(!portfolio->sell){
							ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MIN, 1);
						}

					}

				}else if(portfolio->wask < portfolio->best_sell && portfolio->wask < portfolio->buy_at && portfolio->zero_wbids_num == 0){
					if(portfolio->sell){
						ordlog_event_send_portfolio_orders(portfolio, -portfolio->trd_amt, 2, ORDERTYPE_COMMON);
						LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "##P (%d, %d, %d). B: buy_at = %ld, cnt = %d. A: sell_at = %ld, cnt = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, portfolio->iceberg_cnt, portfolio->buy_at, portfolio->active_bids,  portfolio->sell_at, portfolio->active_asks, trans_tbl->cnt - 1));
						LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));

						portfolio->best_sell = portfolio->wask;
						portfolio->iceberg_cnt--;
						if(portfolio->iceberg_cnt == -portfolio->iceberg_limit){
							int amount = portfolio->amount;
							if(amount > 0) amount = 0;

							//amount -= portfolio->active_asks * portfolio->trd_amt;

							if(amount){
								struct quote_t *quote = portfolio->quotes_tbl->data[0];
								ordlog_event_send_order(quote, -amount, 1, quote->bid_tgt - quote->minstep, ORDERTYPE_TAKE_PROFIT);
								//ordlog_event_send_portfolio_orders(portfolio, -amount, 1, ORDERTYPE_TAKE_PROFIT);
								portfolio->sell = 0;
							}else
								portfolio->iceberg_cnt++;
						}

						if(!portfolio->buy){
							ordlog_event_kill_portfolio_orders_rev(portfolio, LLONG_MAX, 2);
						}
					}

				}
			}break;
			case PORTFOLIOWORKTYPE_PATTERN:{
				if(portfolio->state == WAIT_STATE && portfolio->active_asks == 0 && portfolio->active_bids == 0 && portfolio->zero_wasks_num == 0 && portfolio->zero_wbids_num == 0){
					ordlog_event_commit_portfolios_send_portfolio_orders_minstep_trd(portfolio);
				}
			}break;
			case PORTFOLIOWORKTYPE_PINGPONG:{
				ordlog_event_commit_portfolios_send_portfolio_orders_pingpong(portfolio);
			}break;
	#ifdef RTT_STAT
			case PORTFOLIOWORKTYPE_RTT:{
				if(ordlog_listener_TN_COMMIT.tv_sec - portfolio->ordlog_listener_TN_COMMIT.tv_sec >= 1)
					ordlog_event_commit_portfolios_send_portfolio_orders_rtt(portfolio);
			}break;
	#endif
			}
		}
	}

	pts_affected_tbl->cnt = 0;

	if(snt_trans_tbl->data_size == 0){
		new_trans_tbl->first = 0;

		struct trans_pointers_tbl_t *trans_tbl = new_trans_tbl;
		new_trans_tbl = snt_trans_tbl;
		snt_trans_tbl = trans_tbl;
	}
}




void ordlog_event_commit_portfolios_old(void)
{
#if defined(STREAMS_LOG) && !defined(UNITTEST) && !defined(REPLY)
	if(!system_started) return;
#else
	if(!started_tmp) return;
#endif


	int it;
	for(it = 0; it < pts_affected_tbl->cnt; it++){
		struct portfolio_t *portfolio = portfolios_tbl->data[pts_affected_tbl->data[it]];
		portfolio->not_affected = 1;

		if(portfolio->trade){
			switch(portfolio->work_type){
			case PORTFOLIOWORKTYPE_FOLLOW_TREND:{
				if(portfolio->active_bids == 0 && portfolio->wbid > portfolio->sell_at && portfolio->zero_wasks_num == 0){
					if(portfolio->buy){
						ordlog_event_send_portfolio_orders(portfolio, portfolio->trd_amt, 2, ORDERTYPE_COMMON);
						LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "##P (%d, %d, %d). B: buy_at = %ld, cnt = %d. A: sell_at = %ld, cnt = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, portfolio->iceberg_cnt, portfolio->buy_at, portfolio->active_bids,  portfolio->sell_at, portfolio->active_asks, trans_tbl->cnt - 1));
						LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));
					}else if(portfolio->amount < 0){
						int amount = -portfolio->amount;
						amount = amount < portfolio->trd_amt ? amount : portfolio->trd_amt;

						ordlog_event_send_portfolio_orders(portfolio, amount, 2, ORDERTYPE_COMMON);
						LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "##P (%d, %d, %d). B: buy_at = %ld, cnt = %d. A: sell_at = %ld, cnt = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, portfolio->iceberg_cnt, portfolio->buy_at, portfolio->active_bids,  portfolio->sell_at, portfolio->active_asks, trans_tbl->cnt - 1));
						LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));
					}else{
						portfolio->sell_at = portfolio->wbid;
						if(portfolio->sell_at > portfolio->best_sell) portfolio->sell_at = portfolio->best_sell;
						portfolio->buy_at = portfolio->sell_at - portfolio->spread;
					}

				}else if(portfolio->active_asks == 0 && portfolio->wask < portfolio->buy_at && portfolio->zero_wbids_num == 0){
					if(portfolio->sell){
						ordlog_event_send_portfolio_orders(portfolio, -portfolio->trd_amt, 2, ORDERTYPE_COMMON);
						LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "##P (%d, %d, %d). B: buy_at = %ld, cnt = %d. A: sell_at = %ld, cnt = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, portfolio->iceberg_cnt, portfolio->buy_at, portfolio->active_bids,  portfolio->sell_at, portfolio->active_asks, trans_tbl->cnt - 1));
						LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));
					}else if(portfolio->amount > 0){
						int amount = portfolio->amount;
						amount = amount < portfolio->trd_amt ? amount : portfolio->trd_amt;

						ordlog_event_send_portfolio_orders(portfolio, -amount, 2, ORDERTYPE_COMMON);
						LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "##P (%d, %d, %d). B: buy_at = %ld, cnt = %d. A: sell_at = %ld, cnt = %d. TR=%lld\n", portfolio->portfolio_id, portfolio->amount, portfolio->iceberg_cnt, portfolio->buy_at, portfolio->active_bids,  portfolio->sell_at, portfolio->active_asks, trans_tbl->cnt - 1));
						LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));
					}else{
						portfolio->buy_at = portfolio->wask;
						if(portfolio->buy_at < portfolio->best_buy) portfolio->buy_at = portfolio->best_buy;
						portfolio->sell_at = portfolio->buy_at + portfolio->spread;
					}
				}
			}break;
			case PORTFOLIOWORKTYPE_PATTERN:{
				if(portfolio->state == WAIT_STATE && portfolio->active_asks == 0 && portfolio->active_bids == 0 && portfolio->zero_wasks_num == 0 && portfolio->zero_wbids_num == 0){
					ordlog_event_commit_portfolios_send_portfolio_orders_minstep_trd(portfolio);
				}
			}break;
			case PORTFOLIOWORKTYPE_PINGPONG:{
				ordlog_event_commit_portfolios_send_portfolio_orders_pingpong(portfolio);
			}break;
	#ifdef RTT_STAT
			case PORTFOLIOWORKTYPE_RTT:{
				if(ordlog_listener_TN_COMMIT.tv_sec - portfolio->ordlog_listener_TN_COMMIT.tv_sec >= 1)
					ordlog_event_commit_portfolios_send_portfolio_orders_rtt(portfolio);
			}break;
	#endif
			}
		}
	}

	pts_affected_tbl->cnt = 0;

	if(snt_trans_tbl->data_size == 0){
		new_trans_tbl->first = 0;

		struct trans_pointers_tbl_t *trans_tbl = new_trans_tbl;
		new_trans_tbl = snt_trans_tbl;
		snt_trans_tbl = trans_tbl;
	}
}


void ordlog_event_commit_portfolios_send_portfolio_orders_rtt(struct portfolio_t *portfolio)
{
#ifdef RTT_STAT
	portfolio->ordlog_listener_TN_COMMIT = ordlog_listener_TN_COMMIT;
#endif

	int it;
	for(it = 0; it < portfolio->quotes_tbl->cnt; it++){
		struct quote_t *quote = portfolio->quotes_tbl->data[it];

		int wamount = quote->weight;
		int64_t price = 0;
		char dir;

		if(wamount > 0){
			price = quote->bid_tgt - 10 * quote->minstep;
			dir = 1;
			portfolio->active_bids++;
		}
		else{
			price = quote->ask_tgt + 10 * quote->minstep;
			dir = 2;
			wamount = -wamount;
			portfolio->active_asks++;
		}


		long trans_id = trans_tbl->cnt++;
		struct trans_t *trans = trans_tbl->data[trans_id];

#ifdef RTT_STAT
		trans->ordlog_listener_TN_COMMIT = ordlog_listener_TN_COMMIT;
		trans->ordlog_listener_TN_BEGIN = ordlog_listener_TN_BEGIN;
#endif


		//Fill transaction
		trans->p2msg = quote->isin_data->msg_data;
		quote->isin_data->msg_data = quote->isin_data->msg_data->next;

		struct cg_msg_data_ex_t *p2msg = trans->p2msg;

		p2msg->origin->user_id = trans_id;
		*p2msg->amount = wamount;
		*p2msg->dir = dir;
		*p2msg->type = 3;

		price_to_str_map(price, p2msg->price, quote->price_scale);


		trans->active_order = orders_pool_tbl->first;
		orders_pool_tbl->first = orders_pool_tbl->first->next;
		trans->active_order->quote = quote;
		trans->active_order->order_type = ORDERTYPE_COMMON;

		////////////////
		new_trans_tbl->data[new_trans_tbl->data_size++] = trans_id;

#ifdef UNITTEST
		trans->active_order->price = price;
#endif
	}
}


void ordlog_event_commit_portfolios_send_portfolio_orders_followtrend(struct portfolio_t *portfolio)
{

}
void ordlog_event_commit_portfolios_send_additional_portfolio_orders_minstep_trd(struct portfolio_t *portfolio)
{
	struct quote_t *quote = portfolio->quotes_tbl->data[0];

	if(portfolio->sl_dir == 2){
		portfolio->killed_sell_amt = 0;

		if(portfolio->wask <= portfolio->buy_at){
			int bid_amt = (portfolio->buy_at - portfolio->wask) / portfolio->shift + 1;

			if(bid_amt >= portfolio->trd_limit / 2){

				/*
				int iceburg_cnt = portfolio->iceberg_cnt + 2 * bid_amt / portfolio->trd_limit;

				if(iceburg_cnt >= portfolio->iceberg_limit){
					iceburg_cnt = portfolio->iceberg_limit;
					portfolio->buy = 0;
				}
				portfolio->iceberg_cnt = iceburg_cnt;
				*/

				printf("bid_amt >= portfolio->trd_limit / 2\n");
				LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "bid_amt >= portfolio->trd_limit / 2\n"));
				//portfolio->iceberg_cnt = portfolio->iceberg_limit - 1;


				portfolio->buy = 0;
				portfolio->sl_cnt++;

				//Start Hedge

				struct portfolio_t *hedge_portfolio = portfolio->hedge_portfolio;
				if(hedge_portfolio){
					hedge_portfolio->trade = 1;
					hedge_portfolio->work_type = PORTFOLIOWORKTYPE_FOLLOW_TREND;
					hedge_portfolio->sell = 1;
					hedge_portfolio->buy = 0;
					hedge_portfolio->buy_at = hedge_portfolio->wask;
					hedge_portfolio->best_buy = hedge_portfolio->buy_at;
					hedge_portfolio->sell_at = hedge_portfolio->buy_at + hedge_portfolio->spread;
					hedge_portfolio->best_sell = hedge_portfolio->sell_at;
					hedge_portfolio->trd_amt = portfolio->hedge_mltp * portfolio->trd_amt;

					portfolio->amount += hedge_portfolio->amount;
					hedge_portfolio->amount = 0;
				}

				portfolio->sl = 0;
				portfolio->state = MM_STATE;
				portfolio->sl_dir = 0;

				int sell_amt = 2 * portfolio->trd_limit - portfolio->active_asks;
				int it;
				for(it = 0; it < sell_amt; it ++)
					ordlog_event_send_order(quote, portfolio->trd_amt, 2, portfolio->sell_at + it * portfolio->shift, ORDERTYPE_MARKET_MAKER);
				portfolio->sell_at += sell_amt * portfolio->shift;

			}else{
				if(portfolio->active_asks == 0){
					portfolio->state = WAIT_STATE;
					LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "\t Active asks is zero. Setting WAIT_STATE\n"));
				}else{
					int sell_amt = bid_amt + portfolio->need_sell_amt;
					portfolio->need_sell_amt = sell_amt;
					portfolio->best_buy = portfolio->best_sell - portfolio->spread - (sell_amt + 1) * portfolio->shift;

					bid_amt = portfolio->trd_limit / 2 - bid_amt;
					int it;
					for(it = 0; it < bid_amt; it++)
						ordlog_event_send_order(quote, portfolio->trd_amt, 1, portfolio->best_buy - it * portfolio->shift, ORDERTYPE_MARKET_MAKER);

					portfolio->buy_at = portfolio->best_buy - portfolio->shift * bid_amt;

					LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "\t New MM bids. buy_at = %ld, bid_amt = %d. best_buy = %ld\n", portfolio->buy_at, bid_amt, portfolio->best_buy));


					if(portfolio->best_sell_trd == LLONG_MIN) portfolio->best_sell_trd = portfolio->best_sell - portfolio->shift;
					portfolio->best_sell -= portfolio->need_sell_amt * portfolio->shift;
					portfolio->killed_sell_amt = portfolio->need_sell_amt;
					portfolio->need_sell_amt = 0;

					portfolio->state = MM_STATE;
				}

				portfolio->sl = 0;
				portfolio->sl_dir = 0;
			}
		}else{
			if(portfolio->active_asks == 0){
				portfolio->state = WAIT_STATE;
				LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "\t Active asks is zero. Setting WAIT_STATE\n"));
			}else{
				portfolio->best_buy = portfolio->buy_at;

				int bid_amt = portfolio->trd_limit / 2;
				int it;
				for(it = 0; it < bid_amt; it++)
					ordlog_event_send_order(quote, portfolio->trd_amt, 1, portfolio->best_buy - it * portfolio->shift, ORDERTYPE_MARKET_MAKER);

				portfolio->buy_at = portfolio->best_buy - portfolio->shift * bid_amt;

				LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "\t New MM bids. buy_at = %ld, bid_amt = %d. best_buy = %ld\n", portfolio->buy_at, bid_amt, portfolio->best_buy));

				portfolio->state = MM_STATE;
			}

			portfolio->sl = 0;
			portfolio->sl_dir = 0;
		}
	}else{
		portfolio->killed_buy_amt = 0;

		if(portfolio->wbid >= portfolio->sell_at){
			int ask_amt = (portfolio->wbid - portfolio->sell_at) / portfolio->shift + 1;

			if(ask_amt >= portfolio->trd_limit / 2){

				/*
				int iceburg_cnt = portfolio->iceberg_cnt - 2 * ask_amt / portfolio->trd_limit;
				if(iceburg_cnt <= -portfolio->iceberg_limit){
					iceburg_cnt = -portfolio->iceberg_limit;
					portfolio->sell = 0;
				}

				portfolio->iceberg_cnt = iceburg_cnt;
				*/

				printf("ask_amt >= portfolio->trd_limit / 2\n");
				LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "ask_amt >= portfolio->trd_limit / 2\n"));
				//portfolio->iceberg_cnt = -portfolio->iceberg_limit + 1;


				portfolio->sell = 0;
				portfolio->sl_cnt++;

				//Start hedge portfolio

				struct portfolio_t *hedge_portfolio = portfolio->hedge_portfolio;
				if(hedge_portfolio){
					hedge_portfolio->trade = 1;
					hedge_portfolio->work_type = PORTFOLIOWORKTYPE_FOLLOW_TREND;
					hedge_portfolio->sell = 0;
					hedge_portfolio->buy = 1;
					hedge_portfolio->sell_at = hedge_portfolio->wbid;
					hedge_portfolio->best_sell = hedge_portfolio->sell_at;
					hedge_portfolio->buy_at = hedge_portfolio->sell_at - hedge_portfolio->spread;
					hedge_portfolio->best_buy = hedge_portfolio->buy_at;
					hedge_portfolio->trd_amt = portfolio->hedge_mltp * portfolio->trd_amt;

					portfolio->amount += hedge_portfolio->amount;
					hedge_portfolio->amount = 0;
				}


				portfolio->sl = 0;
				portfolio->state = MM_STATE;
				portfolio->sl_dir = 0;

				int buy_amt = 2 * portfolio->trd_limit - portfolio->active_bids;
				int it;
				for(it = 0; it < buy_amt; it ++)
					ordlog_event_send_order(quote, portfolio->trd_amt, 1, portfolio->buy_at - it * portfolio->shift, ORDERTYPE_MARKET_MAKER);
				portfolio->buy_at -= buy_amt * portfolio->shift;


			}else{
				if(portfolio->active_bids == 0){
					portfolio->state = WAIT_STATE;
					LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "\t Active bids is zero. Setting WAIT_STATE\n"));
				}else{
					int buy_amt = ask_amt + portfolio->need_buy_amt;
					portfolio->need_buy_amt = buy_amt;
					portfolio->best_sell = portfolio->best_buy + portfolio->spread + (buy_amt + 1) * portfolio->shift;

					ask_amt = portfolio->trd_limit / 2 - ask_amt;
					int it;
					for(it = 0; it < ask_amt; it++)
						ordlog_event_send_order(quote, portfolio->trd_amt, 2, portfolio->best_sell + it * portfolio->shift, ORDERTYPE_MARKET_MAKER);

					portfolio->sell_at = portfolio->best_sell + portfolio->shift * ask_amt;
					LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "\t New MM asks. sell_at = %ld, sell_amt = %d. best_sell = %ld\n", portfolio->sell_at, ask_amt, portfolio->best_sell));


					if(portfolio->best_buy_trd == LLONG_MAX) portfolio->best_buy_trd = portfolio->best_buy + portfolio->shift;
					portfolio->best_buy += portfolio->need_buy_amt * portfolio->shift;
					portfolio->killed_buy_amt = portfolio->need_buy_amt;
					portfolio->need_buy_amt = 0;

					portfolio->state = MM_STATE;
				}
				portfolio->sl = 0;
				portfolio->sl_dir = 0;
			}

		}else{
			if(portfolio->active_bids == 0){
				portfolio->state = WAIT_STATE;
				LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "\t Active bids is zero. Setting WAIT_STATE\n"));
			}else{
				portfolio->best_sell = portfolio->sell_at;

				int ask_amt = portfolio->trd_limit / 2;
				int it;
				for(it = 0; it < ask_amt; it++)
					ordlog_event_send_order(quote, portfolio->trd_amt, 2, portfolio->best_sell + it * portfolio->shift, ORDERTYPE_MARKET_MAKER);

				portfolio->sell_at = portfolio->best_sell + portfolio->shift * ask_amt;

				LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "\t New MM asks. sell_at = %ld, sell_amt = %d. best_sell = %ld\n", portfolio->sell_at, ask_amt, portfolio->best_sell));

				portfolio->state = MM_STATE;
			}
			portfolio->sl = 0;
			portfolio->sl_dir = 0;
		}
	}

	LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));
}



void ordlog_event_commit_portfolios_send_portfolio_orders_pingpong(struct portfolio_t *portfolio)
{
	////////
	if(ordref_tbl->replRev > ordref_tbl->sync_replRev){
		//ordref_tbl->sync_replRev = LLONG_MAX;
		if(ordref_tbl->sync == -1){
			rnd_values_tbl->first = ordref_tbl->sync_replRev % (MAX_RND_VALUES * 3 / 4);
			ordref_tbl->sync = 1;
		}
	}
	////////

	if(ordref_tbl->sync != 1) return;
	//ordref_tbl->sync = 0;

	struct quote_t *quote = portfolio->quotes_tbl->data[0];
	if(portfolio->wask - portfolio->wbid == quote->minstep) return;

	//ordref_tbl->sync_replRev = ((ordref_tbl->replRev / 20000) + 1) * 20000;

	//


	int tot_trds = MAX_FLOOD_MSGS;
	struct timespec curr_tm;
	curr_tm.tv_nsec = 0;
	curr_tm.tv_sec = 0;

	clock_gettime (CLOCK_MONOTONIC, &curr_tm);

	while(pp_snt_messages_tbl->data_size != 0 && ((curr_tm.tv_sec - pp_snt_messages_tbl->data[pp_snt_messages_tbl->first].tv_sec > FLOOD_PERIOD_SEC) || ((curr_tm.tv_sec - pp_snt_messages_tbl->data[pp_snt_messages_tbl->first].tv_sec == FLOOD_PERIOD_SEC) && (curr_tm.tv_nsec > pp_snt_messages_tbl->data[pp_snt_messages_tbl->first].tv_nsec)))){
		pp_snt_messages_tbl->first = (pp_snt_messages_tbl->first + 1) % TRANSACTIONS_CNT;
		pp_snt_messages_tbl->data_size--;
	}

	uint msgs_cnt = MAX_FLOOD_MSGS - pp_snt_messages_tbl->data_size;
	if(msgs_cnt > 0){
		if(msgs_cnt > tot_trds) msgs_cnt = tot_trds;
		//pp_snt_messages_tbl->data_size += msgs_cnt;

		tot_trds = msgs_cnt;
	}else
		tot_trds = 0;

	//if(tot_trds <= 0) return;



	int tot_a_trds = 0;
	int tot_b_trds = 0;

	if(portfolio->wbid > portfolio->sell_at){
		LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "##P (%d, %d, %d). B: buy_at = %ld, wbid = %ld. A: sell_at = %ld, wask = %ld.\n", portfolio->portfolio_id, portfolio->amount, portfolio->iceberg_cnt, portfolio->buy_at, portfolio->wbid, portfolio->sell_at, portfolio->wask));


		int tot_trds = rnd_values_tbl->data[rnd_values_tbl->first++] % MAX_PINGPONG_TRADES + 1;
		int tot_a_amt = 0;
		int a_it;
		for(a_it = 0; a_it < portfolio->type_a_cnt; a_it++){
			int64_t curr = portfolio->wbid + quote->minstep;
			int t_it = 0;

			while(curr < portfolio->wask){
				int is_trade = rnd_values_tbl->data[rnd_values_tbl->first++] % 10;
				if(is_trade < 5){
					int trd_amt = rnd_values_tbl->data[rnd_values_tbl->first++] % 10 + 1;
					if(portfolio->self_type == 1 && portfolio->type_id == a_it)
						ordlog_event_send_order(quote, trd_amt, 2, curr, ORDERTYPE_COMMON);

					tot_a_amt += trd_amt;
					tot_a_trds++;
				}
				curr += quote->minstep;

				pingpongtrades_tbl[t_it++] = tot_a_amt;
				if(t_it == tot_trds) break;
			}
		}

		int tot_b_amt = 0;
		int b_it;
		for(b_it = 0; b_it < portfolio->type_b_cnt; b_it++){
			int64_t curr = portfolio->wbid + quote->minstep;
			int t_it = 0;
			while(curr < portfolio->wask){
				int is_trade = rnd_values_tbl->data[rnd_values_tbl->first++] % 10;
				if(is_trade < 5){
					int trd_amt = rnd_values_tbl->data[rnd_values_tbl->first++] % 10 + 1;
					trd_amt = trd_amt < pingpongtrades_tbl[t_it] - tot_b_amt ? trd_amt : pingpongtrades_tbl[t_it] - tot_b_amt;

					if(trd_amt){
						if(portfolio->self_type == 2 && portfolio->type_id == b_it)
							ordlog_event_send_order(quote, trd_amt, 1, curr, ORDERTYPE_COMMON);

						tot_b_amt += trd_amt;
						tot_a_amt -= trd_amt;
						pingpongtrades_tbl[t_it] -= trd_amt;
						tot_b_trds++;
					}
				}

				if(++t_it == tot_trds) break;
				if(tot_a_amt == 0) break;
				curr += quote->minstep;
			}
			if(tot_a_amt == 0) break;
		}

		if(tot_a_amt > 0){
			b_it = rnd_values_tbl->data[rnd_values_tbl->first++] % portfolio->type_b_cnt;
			int64_t curr = portfolio->wask - quote->minstep;
			if(portfolio->self_type == 2 && portfolio->type_id == b_it)
				ordlog_event_send_order(quote, tot_a_amt, 1, curr, ORDERTYPE_COMMON);

			tot_b_trds++;
		}

		portfolio->sell_at = portfolio->wbid;
		portfolio->buy_at = portfolio->sell_at - portfolio->spread;
		//ordref_tbl->sync_replRev = ((ordref_tbl->replRev / 20000) + 1) * 20000;
		LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));
	}else if(portfolio->wask < portfolio->buy_at){
		LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "##P (%d, %d, %d). B: buy_at = %ld, wbid = %ld. A: sell_at = %ld, wask = %ld.\n", portfolio->portfolio_id, portfolio->amount, portfolio->iceberg_cnt, portfolio->buy_at, portfolio->wbid, portfolio->sell_at, portfolio->wask));

		int tot_trds = rnd_values_tbl->data[rnd_values_tbl->first++] % MAX_PINGPONG_TRADES + 1;

		int tot_a_amt = 0;
		int a_it;
		for(a_it = 0; a_it < portfolio->type_a_cnt; a_it++){
			int64_t curr = portfolio->wask - quote->minstep;
			int t_it = 0;

			while(curr > portfolio->wbid){
				int is_trade = rnd_values_tbl->data[rnd_values_tbl->first++] % 10;
				if(is_trade < 5){
					int trd_amt = rnd_values_tbl->data[rnd_values_tbl->first++] % 10 + 1;
					if(portfolio->self_type == 1 && portfolio->type_id == a_it)
						ordlog_event_send_order(quote, trd_amt, 1, curr, ORDERTYPE_COMMON);
					tot_a_amt += trd_amt;
					tot_a_trds++;
				}
				curr -= quote->minstep;

				pingpongtrades_tbl[t_it++] = tot_a_amt;
				if(t_it == tot_trds) break;
			}
		}

		int tot_b_amt = 0;
		int b_it;
		for(b_it = 0; b_it < portfolio->type_b_cnt; b_it++){
			int64_t curr = portfolio->wask - quote->minstep;
			int t_it = 0;
			while(curr > portfolio->wbid){
				int is_trade = rnd_values_tbl->data[rnd_values_tbl->first++] % 10;
				if(is_trade < 5){
					int trd_amt = rnd_values_tbl->data[rnd_values_tbl->first++] % 10 + 1;
					trd_amt = trd_amt < pingpongtrades_tbl[t_it] - tot_b_amt? trd_amt : pingpongtrades_tbl[t_it] - tot_b_amt;

					if(trd_amt){
						if(portfolio->self_type == 2 && portfolio->type_id == b_it)
							ordlog_event_send_order(quote, trd_amt, 2, curr, ORDERTYPE_COMMON);

						tot_b_amt += trd_amt;
						tot_a_amt -= trd_amt;
						pingpongtrades_tbl[t_it] -= trd_amt;
						tot_b_trds++;
					}
				}
				if(++t_it == tot_trds) break;
				if(tot_a_amt == 0) break;
				curr -= quote->minstep;
			}
			if(tot_a_amt == 0) break;
		}

		if(tot_a_amt > 0){
			b_it = rnd_values_tbl->data[rnd_values_tbl->first++] % portfolio->type_b_cnt;
			int64_t curr = portfolio->wbid + quote->minstep;
			if(portfolio->self_type == 2 && portfolio->type_id == b_it)
				ordlog_event_send_order(quote, tot_a_amt, 2, curr, ORDERTYPE_COMMON);
			tot_b_trds++;
		}

		portfolio->buy_at = portfolio->wask;
		portfolio->sell_at = portfolio->buy_at + portfolio->spread;
		//ordref_tbl->sync_replRev = ((ordref_tbl->replRev / 20000) + 1) * 20000;
		LOGICLOG_FUNC(thread_func_logger_flush(LOGIC_LOG_ID));
	}

	/*
	tot_trds = tot_a_trds > tot_b_trds ? tot_a_trds : tot_b_trds;
	pp_snt_messages_tbl->data_size += tot_trds;
	int it;
	for(it = 0; it < tot_trds; it++){
		struct timespec *snt_ts = &pp_snt_messages_tbl->data[(pp_snt_messages_tbl->first+it) % TRANSACTIONS_CNT];
		clock_gettime (CLOCK_MONOTONIC, snt_ts);
	}
	*/

	if(MAX_RND_VALUES - rnd_values_tbl->first < 1000)
		rnd_values_tbl->first = rnd_values_tbl->data[rnd_values_tbl->first] % (MAX_RND_VALUES * 3 / 4);
}

void ordlog_event_commit_portfolios_send_portfolio_orders_minstep_trd(struct portfolio_t *portfolio)
{
	portfolio->state = MM_STATE;

	portfolio->killed_buy_amt = 0;
	portfolio->killed_sell_amt = 0;

	portfolio->need_buy_amt = 0;
	portfolio->need_sell_amt = 0;

	portfolio->sell_at = portfolio->wask;
	portfolio->buy_at = portfolio->sell_at - portfolio->spread - portfolio->shift;

	portfolio->best_sell = portfolio->sell_at;
	portfolio->best_buy = portfolio->buy_at;

	portfolio->best_buy_trd = LLONG_MAX;
	portfolio->best_sell_trd = LLONG_MIN;

	struct quote_t *quote = portfolio->quotes_tbl->data[0];

	int buy_amt = 0;
	int sell_amt = 0;

	int residual = portfolio->amount % portfolio->trd_amt;

	if(portfolio->iceberg_cnt >= portfolio->iceberg_limit){
		buy_amt = 0;
		sell_amt = 2 * portfolio->trd_limit;
	}else if(portfolio->iceberg_cnt <= -portfolio->iceberg_limit){
		buy_amt = 2 * portfolio->trd_limit;
		sell_amt = 0;
	}else{
		buy_amt = portfolio->trd_limit;
		sell_amt = portfolio->trd_limit;
	}


	int buy_it = 0;
	int sell_it = 0;

	if(residual > 0){
		ordlog_event_send_order(quote, residual, 2, portfolio->sell_at, ORDERTYPE_MARKET_MAKER);
		sell_it = 1;
	}else if(residual < 0){
		ordlog_event_send_order(quote, -residual, 1, portfolio->buy_at, ORDERTYPE_MARKET_MAKER);
		buy_it = 1;
	}

	int it;
	for(it = buy_it; it < buy_amt; it++)
		ordlog_event_send_order(quote, portfolio->trd_amt, 1, portfolio->buy_at - portfolio->shift * it, ORDERTYPE_MARKET_MAKER);
	portfolio->buy_at -= portfolio->shift * buy_amt;

	for(it = sell_it; it < sell_amt; it++)
		ordlog_event_send_order(quote, portfolio->trd_amt, 2, portfolio->sell_at + portfolio->shift * it, ORDERTYPE_MARKET_MAKER);
	portfolio->sell_at += portfolio->shift * sell_amt;

#ifdef RANDOM_MOVE
	if(portfolio->hedge_portfolio)
	{
		portfolio = portfolio->hedge_portfolio;
		quote = portfolio->quotes_tbl->data[0];
		ordlog_event_send_order(quote, 1, 1, portfolio->buy_at - quote->minstep * 100, ORDERTYPE_FLOAT_QUOTE);
	}
#endif
}

int ordlog_event_kill_portfolio_orders_rev(struct portfolio_t *portfolio, int64_t price, signed int dir)
{
	int killed_amt = 0;

	struct active_order_t *active = portfolio->active_orders_tbl;
	while(active){
		if(active->state == ORDER_ACTIVE){
			if(dir == 1){
				if(active->dir == dir && active->price > price){
					ordlog_event_kill_order(active);
					killed_amt++;
				}
			}else{
				if(active->dir == dir && active->price < price){
					ordlog_event_kill_order(active);
					killed_amt++;
				}
			}
		}

		active = active->next;
	}


	return killed_amt;
}

int ordlog_event_kill_portfolio_orders(struct portfolio_t *portfolio, int64_t price, signed int dir)
{

	int killed_amt = 0;

	struct active_order_t *active = portfolio->active_orders_tbl;
	while(active){
		if(active->state == ORDER_ACTIVE){
			if(dir == 1){
				if(active->dir == dir && active->price <= price){
					ordlog_event_kill_order(active);
					killed_amt++;
				}
			}else{
				if(active->dir == dir && active->price >= price){
					ordlog_event_kill_order(active);
					killed_amt++;
				}
			}
		}

		active = active->next;
	}

	if(dir == 1){
		portfolio->buy_at = price;
	}else{
		portfolio->sell_at = price;
	}



	return killed_amt;
}

int ordlog_event_kill_portfolio_orders_range(struct portfolio_t *portfolio, int64_t from, int64_t till, signed int dir)
{
	int killed_amt = 0;

	struct active_order_t *active = portfolio->active_orders_tbl;
	while(active){
		if(active->state == ORDER_ACTIVE && active->amount_rest){
			if(dir == 1){
				if(active->dir == dir && active->price > from && active->price <= till){
					ordlog_event_kill_order(active);
					killed_amt++;
				}
			}else{
				if(active->dir == dir && active->price >= from && active->price < till){
					ordlog_event_kill_order(active);
					killed_amt++;
				}
			}
		}

		active = active->next;
	}


	return killed_amt;
}


void ordlog_event_move_order(struct active_order_t *active, int64_t price)
{
	if(!proceed) return;
	active->state = ORDER_MOVING;

	long trans_id = trans_tbl->cnt++;
	struct trans_t *trans = trans_tbl->data[trans_id];

#ifdef RTT_STAT
	trans->ordlog_listener_TN_COMMIT = ordlog_listener_TN_COMMIT;
	trans->ordlog_listener_TN_BEGIN = ordlog_listener_TN_BEGIN;
#endif

	if(active->option){
		trans->p2msg = optmovemsgs_tbl->msg_data;
		optmovemsgs_tbl->msg_data = optmovemsgs_tbl->msg_data->next;
	}else{
		trans->p2msg = futmovemsgs_tbl->msg_data;
		futmovemsgs_tbl->msg_data = futmovemsgs_tbl->msg_data->next;
	}

	struct cg_msg_data_ex_t *p2msg = trans->p2msg;
	p2msg->origin->user_id = trans_id;

	*p2msg->order_id = active->order_id;
	price_to_str_map(price, p2msg->price, active->quote->price_scale);


	trans->active_order = active;
	new_trans_tbl->data[new_trans_tbl->data_size++] = trans_id;
}

void ordlog_event_kill_order(struct active_order_t *active)
{
	if(!proceed) return;
	active->state = ORDER_KILLING;


	long trans_id = trans_tbl->cnt++;
	struct trans_t *trans = trans_tbl->data[trans_id];

#ifdef RTT_STAT
	trans->ordlog_listener_TN_COMMIT = ordlog_listener_TN_COMMIT;
	trans->ordlog_listener_TN_BEGIN = ordlog_listener_TN_BEGIN;
#endif

	if(active->option){
		trans->p2msg = optdelmsgs_tbl->msg_data;
		optdelmsgs_tbl->msg_data = optdelmsgs_tbl->msg_data->next;
	}else{
		trans->p2msg = futdelmsgs_tbl->msg_data;
		futdelmsgs_tbl->msg_data = futdelmsgs_tbl->msg_data->next;
	}

	struct cg_msg_data_ex_t *p2msg = trans->p2msg;
	p2msg->origin->user_id = trans_id;

	*p2msg->order_id = active->order_id;

	trans->active_order = active;
	new_trans_tbl->data[new_trans_tbl->data_size++] = trans_id;

#ifdef UNITTEST
	struct ordref_list_t *ordref_list = ordref_tbl_UNITTEST;
	while(ordref_list){
		struct ordref_ex_t *ordref = ordref_list->ordref;
		if(ordref && ordref->active == active){
			struct ordref_list_t *prev = ordref_list->prev;
			struct ordref_list_t *next = ordref_list->next;
			prev->next = next;
			if(next) next->prev = prev;

			active->state = ORDER_KILLED;
			modified_orders_tbl->data[modified_orders_tbl->cnt++] = ordref;
			ordref->amount_rest = active->amount;

			free(ordref_list);
			break;
		}
		ordref_list = ordref_list->next;
	}
#endif
}

void ordlog_event_send_portfolio_orders(struct portfolio_t *portfolio, int amount, signed int p2type, int order_type)
{

	int it;
	for(it = 0; it < portfolio->quotes_tbl->cnt; it++){
		struct quote_t *quote = portfolio->quotes_tbl->data[it];

		int wamount = amount * quote->weight;
		int64_t price = 0;
		char dir;

		if(wamount > 0){
			price = quote->ask_tgt;
			dir = 1;
			portfolio->active_bids++;

		}
		else{
			price = quote->bid_tgt;
			dir = 2;
			wamount = -wamount;
			portfolio->active_asks++;
		}


		long trans_id = trans_tbl->cnt++;
		struct trans_t *trans = trans_tbl->data[trans_id];

#ifdef RTT_STAT
		trans->ordlog_listener_TN_COMMIT = ordlog_listener_TN_COMMIT;
		trans->ordlog_listener_TN_BEGIN = ordlog_listener_TN_BEGIN;
#endif


		//Fill transaction
		trans->p2msg = quote->isin_data->msg_data;
		quote->isin_data->msg_data = quote->isin_data->msg_data->next;

		struct cg_msg_data_ex_t *p2msg = trans->p2msg;

		p2msg->origin->user_id = trans_id;
		*p2msg->amount = wamount;
		*p2msg->dir = dir;
		*p2msg->type = p2type;

		price_to_str_map(price, p2msg->price, quote->price_scale);


		trans->active_order = orders_pool_tbl->first;
		orders_pool_tbl->first = orders_pool_tbl->first->next;
		trans->active_order->quote = quote;
		trans->active_order->order_type = order_type;

		////////////////
		new_trans_tbl->data[new_trans_tbl->data_size++] = trans_id;


#ifdef UNITTEST
		trans->active_order->price = price;
#endif
	}

}

void ordlog_event_commit(void)
{
	if(session_tbl->current == NULL){
		return;

		printf("session_tbl->current = NULL\n");
		exit_ordlog_debug_error(__LINE__, __func__);
	}


	signed int futsess_id = session_tbl->current->origin.sess_id - isins_tbl->sess_id.min;
	signed int optsess_id = session_tbl->current->origin.opt_sess_id - isins_tbl->sess_id.min;

	int it;
	for(it = 0; it < affected_tbl->cnt; it++){
		signed int isin_id = affected_tbl->data[it] - isins_tbl->shift;

		struct sess_data_row_t *sess = isins_tbl->data[isin_id]->data[futsess_id];
		if(sess == NULL)sess = isins_tbl->data[isin_id]->data[optsess_id];

		if(sess == NULL || sess->used)continue;
		sess->used = 1;

#ifdef ORDLOG_DEBUG
		int bid_first = sess->bidEx->data_size - 1;
		while(sess->bidEx->data[bid_first]->amount_rest == 0){
			bid_first--;
			if(bid_first == 0) break;
		}

		int ask_first = 0;
		while(sess->askEx->data[ask_first]->amount_rest == 0){
			ask_first++;
			if(ask_first == sess->askEx->data_size){
				ask_first--;
				break;
			}
		}

		if(bid_first > sess->bidEx->first){
			printf("bid_first = (%ld, %d), bidEx->first = (%ld, %d)\n"
					, bid_first * sess->cont->minstep_adj + sess->cont->limit_down_adj
					, sess->bidEx->data[bid_first]->amount_rest
					, sess->bidEx->first * sess->cont->minstep_adj + sess->cont->limit_down_adj
					, sess->bidEx->data[sess->bidEx->first]->amount_rest
					);
			exit_ordlog_debug_error(__LINE__, __func__);
		}

		if(ask_first < sess->askEx->first){
			printf("ask_first = (%ld, %d), askEx->first = (%ld, %d)\n"
					, ask_first * sess->cont->minstep_adj + sess->cont->limit_down_adj
					, sess->askEx->data[ask_first]->amount_rest
					, sess->askEx->first * sess->cont->minstep_adj + sess->cont->limit_down_adj
					, sess->askEx->data[sess->askEx->first]->amount_rest
					);
			exit_ordlog_debug_error(__LINE__, __func__);
		}
#endif



		//Change Portfolio Quotes
		//
		// Inline function.
		if(isins_tbl->data[isin_id]->quotes_tbl){
			struct quotes_tbl_t *quotes_tbl = isins_tbl->data[isin_id]->quotes_tbl;

			signed int qty;
			int64_t vol;

			int jt;
			char needfirst;
			int shift;
			int weight;

			qty = 0;
			vol = 0;
			jt = 0;
			needfirst = 1;
			shift = sess->bidEx->first;
			weight = abs(quotes_tbl->data[jt]->weight);

			int64_t bid_price = shift * sess->cont->minstep_adj + sess->cont->limit_down_adj;
			while(shift >= 0){
				struct price_aggr_t *bidEx = sess->bidEx->data[shift];
				if(bidEx->amount_rest){
					if(needfirst){
						sess->bidEx->first = shift;
						needfirst = 0;
					}

					if(bidEx->amount_rest + qty >= weight){
						ordlog_event_commit_quote_bid_change(quotes_tbl->data[jt], sess->cont->scale_adj, bid_price, vol, qty);

						if(++jt == quotes_tbl->cnt) break;
						weight = abs(quotes_tbl->data[jt]->weight);
					}else{
						qty += bidEx->amount_rest;
						vol += bid_price * bidEx->amount_rest;

						shift--;
						bid_price -= sess->cont->minstep_adj;

					}
				}else{
					shift--;
					bid_price -= sess->cont->minstep_adj;

				}
			}
			if(shift < 0)
				while(jt < quotes_tbl->cnt){
					ordlog_event_commit_quote_bid_change(quotes_tbl->data[jt], 0, 0, 0, 0);
					jt++;
				}
			sess->bidEx->last = shift;



			needfirst = 1;
			shift = sess->askEx->first;
			int ask_size = sess->askEx->data_size;

			qty = 0;
			vol = 0;
			jt = 0;
			weight = abs(quotes_tbl->data[jt]->weight);


			int64_t ask_price = shift * sess->cont->minstep_adj + sess->cont->limit_down_adj;
			while(shift < ask_size){
				struct price_aggr_t *askEx = sess->askEx->data[shift];
				if(askEx->amount_rest){
					if(needfirst){
						sess->askEx->first = shift;
						needfirst = 0;
					}

					if(askEx->amount_rest + qty >= weight){
						ordlog_event_commit_quote_ask_change(quotes_tbl->data[jt], sess->cont->scale_adj, ask_price, vol, qty);
						if(++jt == quotes_tbl->cnt) break;
						weight = abs(quotes_tbl->data[jt]->weight);
					}else{
						qty += askEx->amount_rest;
						vol += ask_price * askEx->amount_rest;

						shift++;
						ask_price += sess->cont->minstep_adj;

					}
				}else{
					shift++;
					ask_price += sess->cont->minstep_adj;

				}
			}
			if(shift == ask_size)
				while(jt < quotes_tbl->cnt){
					ordlog_event_commit_quote_ask_change(quotes_tbl->data[jt], 0, 0, 0, 0);
					jt++;
				}
			sess->askEx->last = shift;
		}
	}

	/*
#ifdef RTT_STAT
	struct timespec curr_tm;
	clock_gettime (CLOCK_MONOTONIC, &curr_tm);

	long long begin = (curr_tm.tv_sec - ordlog_listener_TN_BEGIN.tv_sec) * 1000000000L + (curr_tm.tv_nsec - ordlog_listener_TN_BEGIN.tv_nsec);
	long long commit = (curr_tm.tv_sec - ordlog_listener_TN_COMMIT.tv_sec) * 1000000000L + (curr_tm.tv_nsec - ordlog_listener_TN_COMMIT.tv_nsec);

	printf("Commit: begin = %lld, comiit = %lld\n", begin, commit);
#endif
	*/


#ifdef ORDLOG_DEBUG
	if(affected_tbl->cnt)
		for(it = 0; it < portfolios_tbl->cnt; it++){
			struct portfolio_t *portfolio = portfolios_tbl->data[it];
			struct quotes_tbl_t *quotes = portfolio->quotes_tbl;

			int64_t wbid = 0;
			int64_t wask = 0;

			int jt;
			for(jt = 0; jt < quotes->cnt; jt ++){
				int64_t q_wbid = 0;
				int64_t q_wask = 0;


				struct quote_t *quote = quotes->data[jt];

				signed int isin_id = quote->isin_id - isins_tbl->shift;
				struct sess_data_row_t *sess = isins_tbl->data[isin_id]->data[futsess_id];
				if(sess == NULL)sess = isins_tbl->data[isin_id]->data[optsess_id];

				int64_t mltp = ipow(10, portfolio->scale - quote->price_scale - quote->ptv_scale);


				int shift;
				signed int qty;
				int64_t vol;
				int weight = quote->weight;

				qty = 0;
				vol = 0;
				shift = sess->bidEx->first;

				int64_t bid_price = shift * sess->cont->minstep_adj + sess->cont->limit_down_adj;
				while(shift >= 0){
					struct price_aggr_t *bidEx = sess->bidEx->data[shift];

					if(bidEx->amount_rest + qty >= abs(weight)){
						if(weight > 0)
							q_wbid = (vol + (abs(weight) - qty) * bid_price);
						else
							q_wask = -(vol + (abs(weight) - qty) * bid_price);

						break;
					}else{
						qty += bidEx->amount_rest;
						vol += bid_price * bidEx->amount_rest;
					}
					shift--;
					bid_price -= sess->cont->minstep_adj;
				}

				int ask_size = sess->askEx->data_size;

				qty = 0;
				vol = 0;
				shift = sess->askEx->first;

				int64_t ask_price = shift * sess->cont->minstep_adj + sess->cont->limit_down_adj;
				while(shift < ask_size){
					struct price_aggr_t *askEx = sess->askEx->data[shift];
					if(askEx->amount_rest + qty >= abs(weight)){
						if(weight < 0)
							q_wbid = -(vol + (abs(weight) - qty) * ask_price);
						else
							q_wask = (vol + (abs(weight) - qty) * ask_price);
						break;
					}else{
						qty += askEx->amount_rest;
						vol += ask_price * askEx->amount_rest;
					}
					shift++;
					ask_price += sess->cont->minstep_adj;
				}

				if((q_wbid != quote->wbid && quote->wbid)
						|| (q_wask != quote->wask && quote->wask)){
					printf("Q(%d). wbid = %ld, wask = %ld, quote->wbid = %ld, quote->wask = %ld\n", quote->quote_id, q_wbid, q_wask, quote->wbid, quote->wask);
				}

				wbid += q_wbid * quote->pt_value * mltp;
				wask += q_wask * quote->pt_value * mltp;
			}

			if((portfolio->wbid != wbid && portfolio->zero_wbids_num == 0) || (portfolio->wask != wask && portfolio->zero_wasks_num == 0)){
				printf("P(%d). wbid = %ld, wask = %ld\n", portfolio->portfolio_id, wbid, wask);
				printf("P(%d, %d, %d). wbid = %ld, wask = %ld. scale = %d, bid = %f, ask = %f\n", portfolio->portfolio_id, portfolio->zero_wbids_num, portfolio->zero_wasks_num, portfolio->wbid, portfolio->wask, portfolio->scale, (float)portfolio->wbid / ipow(10, portfolio->scale) / portfolio->divisor + portfolio->coeff, (float)portfolio->wask / ipow(10, portfolio->scale) / portfolio->divisor + portfolio->coeff);
			}
		}
#endif

#ifdef PORTFOLIOSTAT_LOG
	for(it = 0; it < portfolios_tbl->cnt; it++){
		struct portfolio_t *portfolio = portfolios_tbl->data[it];
		if(portfolio->wbid != portfolio->pstat_wbid || portfolio->wask != portfolio->pstat_wask){
			PSTAT_FUNC(logsaver(PORTFOLIOSTAT_LOG_ID, "%d\t%ld\t%ld\n", portfolio->portfolio_id, portfolio->wbid, portfolio->wask));

			portfolio->pstat_wbid = portfolio->wbid;
			portfolio->pstat_wask = portfolio->wask;
		}
		break;
	}
	PSTAT_FUNC(thread_func_logger_flush(PORTFOLIOSTAT_LOG_ID));
#endif

#ifdef UNITTEST
	struct ordref_list_t *ordref_list = ordref_tbl_UNITTEST;
	while(ordref_list){
		struct ordref_ex_t *ordref = ordref_list->ordref;
		if(ordref && ordref->active){
			struct active_order_t *active = ordref->active;
			struct quote_t *quote = active->quote;
			if((active->dir == 1 && quote->wask <= active->price && quote->wask != 0) || (active->dir == 2 && quote->wbid >= active->price)){
				ordref->amount_rest = 0;
				modified_orders_tbl->data[modified_orders_tbl->cnt++] = ordref;
			}
		}
		ordref_list = ordref_list->next;
	}
#endif



	for(it = 0; it < affected_tbl->cnt; it++){
		signed int isin_id = affected_tbl->data[it] - isins_tbl->shift;

		struct sess_data_row_t *sess = isins_tbl->data[isin_id]->data[futsess_id];
		if(sess == NULL)sess = isins_tbl->data[isin_id]->data[optsess_id];

		if(sess == NULL)continue;
		sess->used = 0;
	}
	affected_tbl->cnt = 0;



	for(it = 0; it < qnt_affected_tbl->cnt; it++){
		signed int isin_id = qnt_affected_tbl->data[it] - isins_tbl->shift;

		struct sess_data_row_t *sess = isins_tbl->data[isin_id]->data[futsess_id];
		if(sess == NULL)sess = isins_tbl->data[isin_id]->data[optsess_id];

		if(sess == NULL || sess->used)continue;
		sess->used = 1;


		if(isins_tbl->data[isin_id]->quotes_tbl){
			struct quotes_tbl_t *quotes_tbl = isins_tbl->data[isin_id]->quotes_tbl;
			int jt;
			for(jt = 0; jt < quotes_tbl->cnt; jt++)
				ordlog_event_commit_quote_ptv_change(quotes_tbl->data[jt], sess->cont);
		}
	}

	for(it = 0; it < qnt_affected_tbl->cnt; it++){
		signed int isin_id = qnt_affected_tbl->data[it] - isins_tbl->shift;

		struct sess_data_row_t *sess = isins_tbl->data[isin_id]->data[futsess_id];
		if(sess == NULL) sess = isins_tbl->data[isin_id]->data[optsess_id];
		if(sess == NULL) continue;

		sess->used = 0;
	}
	qnt_affected_tbl->cnt = 0;
}


void ordlog_event_commit_quote_bid_change(struct quote_t *quote, int8_t bid_scale, int64_t bid_price, int64_t volume, signed int qty)
{
	struct portfolio_t *portfolio = quote->portfolio;

	int64_t price = 0;

	if(bid_price){
		quote->price_scale = bid_scale;

		if(quote->ptv_scale + quote->price_scale > portfolio->scale){
			int64_t mltp = ipow(10, quote->ptv_scale + quote->price_scale - portfolio->scale) - 1;

			int it;
			struct quotes_tbl_t *quotes_tbl = portfolio->quotes_tbl;
			for(it = 0; it < quotes_tbl->cnt; it++){
				struct quote_t *quote = quotes_tbl->data[it];
				int64_t w_bid_ch = mltp * quote->wbid * quote->pt_value;
				int64_t w_ask_ch = mltp * quote->wask * quote->pt_value;

				portfolio->wbid += w_bid_ch;
				portfolio->wask += w_ask_ch;
			}

			portfolio->scale = quote->ptv_scale + quote->price_scale;
		}

		price = bid_price;
	}

	int64_t mltp = ipow(10, portfolio->scale - quote->price_scale - quote->ptv_scale);


	int64_t w_bid_ch = 0;
	int64_t w_ask_ch = 0;

	if(quote->weight > 0){
		volume = (volume + (quote->weight - qty) * price);
		w_bid_ch = (volume - quote->wbid) * quote->pt_value;

		if(quote->wbid == 0 && w_bid_ch)portfolio->zero_wbids_num--;
		else if(volume == 0 && w_bid_ch)portfolio->zero_wbids_num++;

		quote->wbid = volume;
		portfolio->wbid += w_bid_ch * mltp;

		quote->bid_tgt = bid_price;
	}else{
		volume = (volume + (-quote->weight - qty) * price);
		w_ask_ch = (-volume - quote->wask) * quote->pt_value;

		if(quote->wask == 0 && w_ask_ch)portfolio->zero_wasks_num--;
		else if(volume == 0 && w_ask_ch)portfolio->zero_wasks_num++;

		quote->wask = -volume;
		portfolio->wask += w_ask_ch * mltp;

		quote->ask_tgt = bid_price;
	}


	if(portfolio->not_affected){
		portfolio->not_affected = 0;
		pts_affected_tbl->data[pts_affected_tbl->cnt++] = portfolio->portfolio_id;
	}


#ifdef ORDLOG_DEBUG
	if(quote->weight == 1){
		if(quote->wbid != quote->bid_tgt || quote->wbid != portfolio->wbid){
			printf("quote->wbid (%ld) != quote->bid_tgt (%ld) || quote->wbid (%ld) != portfolio->wbid (%ld)\n", quote->wbid, quote->bid_tgt, quote->wbid, portfolio->wbid);
		}
		if(quote->wask != quote->ask_tgt || quote->wask != portfolio->wask){
			printf("quote->wask (%ld) != quote->ask_tgt (%ld) || quote->wask (%ld) != portfolio->wask (%ld)\n", quote->wask, quote->ask_tgt, quote->wask, portfolio->wask);
		}
	}

#endif
}


void ordlog_event_commit_quote_ask_change(struct quote_t *quote, int8_t ask_scale, int64_t ask_price, int64_t volume, signed int qty)
{


	struct portfolio_t *portfolio = quote->portfolio;

	int64_t price = 0;

	if(ask_price){
		quote->price_scale = ask_scale;

		if(quote->ptv_scale + quote->price_scale > portfolio->scale){
			int64_t mltp = ipow(10, quote->ptv_scale + quote->price_scale - portfolio->scale) - 1;

			int it;
			struct quotes_tbl_t *quotes_tbl = portfolio->quotes_tbl;
			for(it = 0; it < quotes_tbl->cnt; it++){
				struct quote_t *quote = quotes_tbl->data[it];
				int64_t w_bid_ch = mltp * quote->wbid * quote->pt_value;
				int64_t w_ask_ch = mltp * quote->wask * quote->pt_value;

				portfolio->wbid += w_bid_ch;
				portfolio->wask += w_ask_ch;
			}

			portfolio->scale = quote->ptv_scale + quote->price_scale;
		}

		price = ask_price;
	}

	int64_t mltp = ipow(10, portfolio->scale - quote->price_scale - quote->ptv_scale);
	int64_t w_bid_ch = 0;
	int64_t w_ask_ch = 0;


	if(quote->weight < 0){

		volume = (volume + (-quote->weight - qty) * price);
		w_bid_ch = (-volume - quote->wbid) * quote->pt_value;

		if(quote->wbid == 0 && w_bid_ch)portfolio->zero_wbids_num--;
		else if(volume == 0 && w_bid_ch)portfolio->zero_wbids_num++;

		quote->wbid = -volume;
		portfolio->wbid += w_bid_ch * mltp;

		quote->bid_tgt = ask_price;
	}else{

		volume = (volume + (quote->weight - qty) * price);
		w_ask_ch = (volume - quote->wask) * quote->pt_value;

		if(quote->wask == 0 && w_ask_ch)portfolio->zero_wasks_num--;
		else if(volume == 0 && w_ask_ch)portfolio->zero_wasks_num++;

		quote->wask = volume;
		portfolio->wask += w_ask_ch * mltp;

		quote->ask_tgt = ask_price;
	}

	if(portfolio->not_affected){
		portfolio->not_affected = 0;
		pts_affected_tbl->data[pts_affected_tbl->cnt++] = portfolio->portfolio_id;
	}

#ifdef ORDLOG_DEBUG
	if(quote->weight == 1){
		if(quote->wbid != quote->bid_tgt || quote->wbid != portfolio->wbid){
			printf("quote->wbid (%ld) != quote->bid_tgt (%ld) || quote->wbid (%ld) != portfolio->wbid (%ld)\n", quote->wbid, quote->bid_tgt, quote->wbid, portfolio->wbid);
		}
		if(quote->wask != quote->ask_tgt || quote->wask != portfolio->wask){
			printf("quote->wask (%ld) != quote->ask_tgt (%ld) || quote->wask (%ld) != portfolio->wask (%ld)\n", quote->wask, quote->ask_tgt, quote->wask, portfolio->wask);
		}
	}
#endif

}

void ordlog_event_commit_quote_ptv_change(struct quote_t *quote, struct sesscont_ex_t *sess)
{

	quote->minstep = sess->minstep_adj;

	struct portfolio_t *portfolio = quote->portfolio;

	int64_t step_price;
	if(quote->is_quanto){
		step_price = sess->step_price_int;
	}
	else{
		step_price = sess->step_price_curr_int;
	}

	if(quote->qnt_type == QNTTYPE_STEPPRICE_EQ_MINSTEP){
		step_price = sess->minstep_int;
	}


	int8_t scale = 0;
	while(step_price % sess->minstep_int){
		step_price *= 10;
		scale++;
	}


	int64_t pt_value = step_price / sess->minstep_int;
	quote->ptv_scale = scale;


	if(quote->price_scale + quote->ptv_scale > portfolio->scale){
		int64_t mltp = ipow(10, quote->price_scale + quote->ptv_scale - portfolio->scale) - 1;

		int it;
		struct quotes_tbl_t *quotes_tbl = portfolio->quotes_tbl;
		for(it = 0; it < quotes_tbl->cnt; it++){
			struct quote_t *quote = quotes_tbl->data[it];
			int64_t w_bid_ch = mltp * quote->wbid * quote->pt_value;
			int64_t w_ask_ch = mltp * quote->wask * quote->pt_value;

			portfolio->wbid += w_bid_ch;
			portfolio->wask += w_ask_ch;
		}

		portfolio->scale = quote->price_scale + quote->ptv_scale;
	}

	int64_t mltp = ipow(10, portfolio->scale - quote->price_scale - quote->ptv_scale);

	int64_t w_bid_ch = (pt_value - quote->pt_value) * quote->wbid;
	if(quote->pt_value == 0 && w_bid_ch)portfolio->zero_wbids_num--;
	else if(pt_value == 0 && w_bid_ch)portfolio->zero_wbids_num++;

	int64_t w_ask_ch = (pt_value - quote->pt_value) * quote->wask;
	if(quote->pt_value == 0 && w_ask_ch)portfolio->zero_wasks_num--;
	else if(pt_value == 0 && w_ask_ch)portfolio->zero_wasks_num++;

	portfolio->wbid += w_bid_ch * mltp;
	portfolio->wask += w_ask_ch * mltp;
	quote->pt_value = pt_value;


	if(portfolio->not_affected){
		portfolio->not_affected = 0;
		pts_affected_tbl->data[pts_affected_tbl->cnt++] = portfolio->portfolio_id;
	}

}

void ordlog_event_lifenum(void)
{

	int it;
	// Таблица заявок
	for(it = 0; it < ordref_tbl->data_size; it++){
		struct ordref_ex_t *ordref = ordref_tbl->data[it];

		if(ordref){
			if(ordref->replRev >= 0){
				if(ordref->sess_data->destroyed == 0){
					struct price_aggr_t *price_aggr = ordref->price_aggr;
					price_aggr->amount_rest -= ordref->amount_rest;
				}

				ordref->replRev = -1;
				ordref->amount_rest = 0;
				ordref->active = NULL;
				ordref->sess_data->ords_cnt--;
			}

			free(ordref);
			ordref_tbl->data[it] = NULL;
		}
	}

	free(ordref_tbl->data);
	ordref_tbl->data = NULL;
	ordref_tbl->data_size = 0;
	ordref_tbl->shift = 0;
	ordref_tbl->cnt = 0;
	ordref_tbl->replRev = 0;
}

void ordlog_event_close(void)
{

}

void ordlog_event_deleted(struct cg_data_cleardeleted_t *replmsg)
{
#ifdef ORDLOG_DEBUG
	int cnt = 0;
#endif

	// Таблица заявок
	if(replmsg->table_idx == orders_log_index){
		int it;
		for(it = 0; it < ordref_tbl->data_size; it++){

			struct ordref_ex_t *ordref = ordref_tbl->data[it];
			if(ordref && ordref->replRev >= 0 && ordref->replRev < replmsg->table_rev){

				if(ordref->sess_data->destroyed == 0){
					struct price_aggr_t *price_aggr = ordref->price_aggr;
					price_aggr->amount_rest -= ordref->amount_rest;
				}


				ordref->replRev = -1;
				ordref->amount_rest = 0;
				ordref->active = NULL;
				ordref->sess_data->ords_cnt--;

				ordref_tbl->data[it] = NULL;
				free(ordref);

#ifdef ORDLOG_DEBUG
				cnt++;
#endif
			}
		}
	}

#ifdef ORDLOG_DEBUG
	if(cnt){
		printf("ordlog_event_deleted = %d\n", cnt);
	}
#endif
}


void create_isins_tbl_entry(signed int *isin_id_p, signed int *sess_id_p)
{
	signed int isin_id = *isin_id_p - isins_tbl->shift;

#ifdef ORDLOG_DEBUG
	if(isin_id < 0){
		printf("isin_id < 0\n");
		exit_ordlog_debug_error(__LINE__, __func__);
	}
#endif


	if(isin_id >= isins_tbl->data_size){
		int mltp = (isin_id - isins_tbl->data_size) / ISINSSIZE_INCREMENT + 1;
		// Увеличиваем размер массива для хранения различных инструментов
		isins_tbl->data_size += ISINSSIZE_INCREMENT * mltp;
		isins_tbl->data = realloc(isins_tbl->data, sizeof(struct isin_data_tbl_t*) * isins_tbl->data_size);
		if(isins_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

		//printf("isins_tbl->data_size = %ld\n", isins_tbl->data_size);

		int it;
		for(it = isins_tbl->data_size - ISINSSIZE_INCREMENT * mltp; it < isins_tbl->data_size; it++)
			isins_tbl->data[it] = NULL;
	}


	if(isins_tbl->data[isin_id] == NULL){
		//printf("new isins_tbl->data[isin_id], %d\n", isin_id);


		struct isin_data_tbl_t *sess_tbl = malloc(sizeof(struct isin_data_tbl_t));
		if(sess_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
		isins_tbl->data[isin_id] = sess_tbl;


		isins_tbl->cnt++;

		sess_tbl->cnt = 0;
		sess_tbl->shift = isins_tbl->sess_id.min;
		sess_tbl->data = NULL;
		sess_tbl->quotes_tbl = NULL;
		sess_tbl->qnt_quotes_tbl = NULL;
		sess_tbl->msg_data = NULL;
		sess_tbl->msg_data_last = NULL;



		int mltp = (isins_tbl->sess_id.max - isins_tbl->sess_id.min) / SESSSIZE_INCREMENT + 1;
		sess_tbl->data_size = SESSSIZE_INCREMENT * mltp;
		sess_tbl->data = malloc(sizeof(struct sess_data_row_t*) * sess_tbl->data_size);
		if(sess_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

		int it;
		for(it = 0; it < sess_tbl->data_size; it++)
			sess_tbl->data[it] = NULL;
	}
	*isin_id_p = isin_id;

	if(sess_id_p == NULL) return;


	struct isin_data_tbl_t *sess_tbl = isins_tbl->data[isin_id];
	signed int sess_id = *sess_id_p - sess_tbl->shift;

#ifdef ORDLOG_DEBUG
	if(sess_id < 0){
		printf("sess_id < 0\n");
		exit_ordlog_debug_error(__LINE__, __func__);
	}
#endif

	if(sess_id >= sess_tbl->data_size){
		int mltp = (sess_id - sess_tbl->data_size) / SESSSIZE_INCREMENT + 1;
		// Увеличиваем размер массива для хранения сессионных данных (для разных сессий)
		sess_tbl->data_size += SESSSIZE_INCREMENT * mltp;
		sess_tbl->data = realloc(sess_tbl->data, sizeof(struct sess_data_row_t*) * sess_tbl->data_size);
		if(sess_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

//		printf("sess_tbl->data_size = %ld\n", sess_tbl->data_size);

		int it;
		for(it = sess_tbl->data_size - SESSSIZE_INCREMENT * mltp; it < sess_tbl->data_size; it++)
			sess_tbl->data[it] = NULL;
	}

	if(sess_tbl->data[sess_id] == NULL){
		struct sess_data_row_t *sess_data = malloc(sizeof(struct sess_data_row_t));
		if(sess_data == NULL) exit_memory_alloc_error(__LINE__, __func__);

		sess_tbl->data[sess_id] = sess_data;
		sess_tbl->cnt++;

		sess_data->cont = NULL;
		sess_data->bidEx = NULL;
		sess_data->askEx = NULL;
		sess_data->limit_down = 0;
		sess_data->limit_up = 0;
		sess_data->ords_cnt = 0;
		sess_data->used = 0;

		sess_data->destroyed = 0;
	}

	*sess_id_p = sess_id;
}



CG_RESULT futinfo_event(cg_conn_t* conn, cg_listener_t* listener, struct cg_msg_t* msg, void* data)
{
	switch(msg->type)
	{
	case CG_MSG_OPEN:
	{
		/*
		Сообщение приходит в момент активации потока данных. Это событие гарантированно
		возникает до прихода каких либо данных по данной подписке. Для потоков репликации
		приход сообщения означает, что схема данных согласована и готова для использования
		(Подробнее см. Схемы данных) Данное сообщение не содержит дополнительных данных
		и его поля data и data_size не используются.
		*/
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));

		DEBUG_FUNC(printf_stream_newstate(1, msg->type, 0));
		futinfo_listener_state = CG_MSG_OPEN;

	}
	break;
	case CG_MSG_CLOSE:
	{
		/*
		Сообщение приходит в момент закрытия потока данных. Приход сообщения означает, что
		поток был закрыт пользователем или системой. В поле data содержится указатель на int,
		по указанному адресу хранится информация о причине закрытия подписчика. Возможны
		следующие причины:
		• CG_REASON_UNDEFINED - не определена;
		• CG_REASON_USER - пользователь вернул ошибку в callback подписчика;
		• CG_REASON_ERROR - внутренняя ошибка;
		• CG_REASON_DONE - вызван метод cg_lsn_destroy;
		• CG_REASON_SNAPSHOT_DONE - снэпшот получен.
		*/
		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t), msg->data, msg->data_size));

		DEBUG_FUNC(printf_stream_newstate(1, msg->type, *(int*)(msg->data)));
		futinfo_listener_state = CG_MSG_CLOSE;


		/*
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg->data, msg->data_size));
		*/

	}
	break;
	case CG_MSG_TN_BEGIN:
	{
		/*
		Означает момент начала получения очередного блока данных. В паре со следующим со-
		общением может быть использовано логикой ПО для контроля целостности данных. Дан-
		ное сообщение не содержит дополнительных данных и его поля data и data_size не ис-
		пользуются.
		*/
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));

		clock_gettime (CLOCK_MONOTONIC, &futinfo_listener_TN_BEGIN);
	}
	break;
	case CG_MSG_TN_COMMIT:
	{
		/*
		Означает момент завершения получения очередного блока данных. К моменту прихода
		этого сообщения можно считать, что данные полученные по данной подписке, находят-
		ся в непротиворечивом состоянии и отражают таблицы в синхронизированном между со-
		бой состоянии. Данное сообщение не содержит дополнительных данных и его поля data
		и data_size не используются.
		*/
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));

		clock_gettime (CLOCK_MONOTONIC, &futinfo_listener_TN_COMMIT);

		if(futinfo_listener_state == CG_MSG_P2REPL_ONLINE){
//			DEBUG_FUNC(printf_stream_newstate(1, msg->type, 0));
//			printf("FUTINFO:Новые данные получены за: %ld\n", (futinfo_listener_TN_COMMIT.tv_sec - futinfo_listener_TN_BEGIN.tv_sec)*1000000000 + (futinfo_listener_TN_COMMIT.tv_nsec - futinfo_listener_TN_BEGIN.tv_nsec));
		}


	}
	break;
	case CG_MSG_STREAM_DATA:
	{
		/*
		Сообщение прихода потоковых данных. Поле data_size содержит размер полученных дан-
		ных, data указывает на сами данные. Само сообщение содержит дополнительные поля,
		которые описываются структурой cg_msg_streamdata_t.
		*/

		// Здесь происоходит обработка полученных данных и отсев ненужных данных.

		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_streamdata_t), msg->data, msg->data_size));


		struct cg_msg_streamdata_t *replmsg = (struct cg_msg_streamdata_t*)msg;

		if(futinfo_listener_state == CG_MSG_P2REPL_ONLINE)
			futinfo_event_online(replmsg);						// Обновление данных в режиме онлайн
		else
			futinfo_event_snapshot(replmsg);					// Первоначальное получение данных



		/*
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_streamdata_t)));
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg->data, msg->data_size));
		*/

	}
	break;
	case CG_MSG_P2REPL_ONLINE:
	{
		/*
		Переход потока в состояние online - это означает, что получение начального среза было
		завершено и следующие сообщения CG_MSG_STREAM_DATA будут нести данные он-лайн. Данное
		сообщение не содержит дополнительных данных и его поля data и data_size не используются.
		*/
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));

		DEBUG_FUNC(printf_stream_newstate(1, msg->type, 0));
		futinfo_listener_state = CG_MSG_P2REPL_ONLINE;

		futinfo_event_set_online();

	}
	break;
	case CG_MSG_P2REPL_LIFENUM:
	{
		/*
		Изменен номер жизни схемы. Такое сообщение означает, что предыдущие данные, полу-
		ченные по потоку, не актуальны и должны быть очищены. При этом произойдёт повторная
		трансляция данных по новому номеру жизни схемы данных. Поле data сообщения указы-
		вает на целочисленное значение, содержащее новый номер жизни схемы; поле data_size
		содержит размер целочисленного типа. Подробнее про обработку номера жизни схемы в
		конце данного раздела
		*/


		//Vot zdes, voobshe govorya nugno polnostyu vse chistit
		//1. Stop trade
		//2. Try to remove all active orders
		//3. free all internal structures!

		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t), msg->data, msg->data_size));

		DEBUG_FUNC(printf_stream_newstate(1, msg->type, 0));
		futinfo_listener_state = CG_MSG_P2REPL_LIFENUM;


		struct timespec ts_start, ts_stop;

		clock_gettime (CLOCK_MONOTONIC, &ts_start);
		futinfo_event_lifenum();
		clock_gettime (CLOCK_MONOTONIC, &ts_stop);


		/*
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg->data, msg->data_size));
		*/

	}
	break;
	case CG_MSG_P2REPL_CLEARDELETED:
	{
		/*
		Произошла операция массового удаления устаревших данных. Поле data сообщения ука-
		зывает на структуру cg_data_cleardeleted_t, в которой указан номер таблицы и номер ре-
		визии, до которой данные в указанной таблице считаются удаленными. Если ревизия в
		cg_data_cleardeleted_t == CG_MAX_REVISON, то последующие ревизии продолжатся с 1.
		*/


		/*
		struct cg_data_cleardeleted_t {
			uint32_t table_idx;
			int64_t table_rev;
		};
		*/
		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t), msg->data, msg->data_size));

		DEBUG_FUNC(printf_stream_newstate(1, msg->type, 0));
		futinfo_listener_state = CG_MSG_P2REPL_CLEARDELETED;

		struct cg_data_cleardeleted_t *replmsg = (struct cg_data_cleardeleted_t*)msg->data;
		struct timespec ts_start, ts_stop;

		clock_gettime (CLOCK_MONOTONIC, &ts_start);
		futinfo_event_deleted(replmsg);
		clock_gettime (CLOCK_MONOTONIC, &ts_stop);


		/*
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg->data, msg->data_size));
		*/
	}
	break;
	case CG_MSG_P2REPL_REPLSTATE:
	{
		/*
		Сообщение содержит состояние потока данных; присылается перед закрытием потока.
		Поле data сообщения указывает на строку, которая в закодированном виде содержит со-
		стояние потока данных на момент прихода сообщения - сохраняются схема данных, но-
		мера ревизий таблиц и номер жизни схемы на момент последнего CG_MSG_TN_COMMIT
		(Внимание: при переоткрытии потока с replstate ревизии, полученные после последнего
		CG_MSG_TN_COMMIT, будут присланы повторно!) Эта строка может быть передана в вы-
		зов cg_lsn_open в качестве параметра "replstate" по этому же потоку в следующий раз, что
		обеспечит продолжение получения данных с момента остановки потока.
		*/
		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t), msg->data, msg->data_size));

		DEBUG_FUNC(printf_stream_newstate(1, msg->type, 0));
		futinfo_listener_state = CG_MSG_P2REPL_REPLSTATE;
		//printf("FUTINFO:CG_MSG_P2REPL_REPLSTATE\n");


		futinfo_replstate = realloc(futinfo_replstate, msg->data_size + 10);
		strcpy(futinfo_replstate, "replstate=");
		memcpy(&futinfo_replstate[10], msg->data, msg->data_size);


		/*
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));
		STREAMLOG_FUNC(thread_func_logger_save_immidiete(FUTINFO_STREAM_LOG_ID, msg->data, msg->data_size));
		*/
	}
	break;
	}

	STREAMLOG_FUNC(thread_func_logger_flush(FUTINFO_STREAM_LOG_ID));
	return CG_ERR_OK;
}



void futinfo_event_snapshot(struct cg_msg_streamdata_t *replmsg)
{
	// Таблица сессионных инструментов
	if(replmsg->msg_index == fut_sess_contents_index){
		struct fut_sess_contents *sesscont = (struct fut_sess_contents*)replmsg->data;
		FUTINFO_FUNC(futinfo_event_sescontlog(sesscont));

		//Если replAct ненулевой — запись удалена
		char useful = (sesscont->replAct == 0);

		signed long long replID = sesscont->replID;


		if(useful){
			if(replID < ss_futsesscont_tbl->repl_id.min)
				ss_futsesscont_tbl->repl_id.min = replID;

			if(replID > ss_futsesscont_tbl->repl_id.max)
				ss_futsesscont_tbl->repl_id.max = replID;

			if(sesscont->isin_id < ss_futsesscont_tbl->isin_id.min)
				ss_futsesscont_tbl->isin_id.min = sesscont->isin_id;

			if(sesscont->isin_id > ss_futsesscont_tbl->isin_id.max)
				ss_futsesscont_tbl->isin_id.max = sesscont->isin_id;

			if(sesscont->sess_id < ss_futsesscont_tbl->sess_id.min)
				ss_futsesscont_tbl->sess_id.min = sesscont->sess_id;

			if(sesscont->sess_id > ss_futsesscont_tbl->sess_id.max)
				ss_futsesscont_tbl->sess_id.max = sesscont->sess_id;


			if(ss_futsesscont_tbl->cnt >= ss_futsesscont_tbl->data_size){
				int mltp = (ss_futsesscont_tbl->cnt - ss_futsesscont_tbl->data_size) / SESSCONTSIZE_INCREMENT + 1;
				ss_futsesscont_tbl->data_size += SESSCONTSIZE_INCREMENT * mltp;
				ss_futsesscont_tbl->data = realloc(ss_futsesscont_tbl->data, sizeof(struct sesscont_ex_t *) * ss_futsesscont_tbl->data_size);
				if(ss_futsesscont_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

				int it;
				for(it = ss_futsesscont_tbl->data_size - SESSCONTSIZE_INCREMENT * mltp; it < ss_futsesscont_tbl->data_size; it++)
					ss_futsesscont_tbl->data[it] = NULL;
			}

			struct sesscont_ex_t *sesscont_ex = malloc(sizeof(struct sesscont_ex_t));
			if(sesscont_ex == NULL) exit_memory_alloc_error(__LINE__, __func__);
			ss_futsesscont_tbl->data[ss_futsesscont_tbl->cnt] = sesscont_ex;


			memcpy(sesscont_ex, sesscont, replmsg->data_size);
			sesscont_ex->origin.option = 0;

			WARN_FAIL(cg_bcd_get(sesscont->min_step, &sesscont_ex->minstep_int, &sesscont_ex->minstep_scale), "bcd_get");
			WARN_FAIL(cg_bcd_get(sesscont->step_price, &sesscont_ex->step_price_int, &sesscont_ex->step_price_scale), "bcd_get");
			WARN_FAIL(cg_bcd_get(sesscont->step_price_curr, &sesscont_ex->step_price_curr_int, &sesscont_ex->step_price_curr_scale), "bcd_get");
			WARN_FAIL(cg_bcd_get(sesscont->limit_up, &sesscont_ex->limit_up_int, &sesscont_ex->limit_up_scale), "bcd_get");
			WARN_FAIL(cg_bcd_get(sesscont->limit_down, &sesscont_ex->limit_down_int, &sesscont_ex->limit_down_scale), "bcd_get");
			WARN_FAIL(cg_bcd_get(sesscont->last_cl_quote, &sesscont_ex->last_cl_quote_int, &sesscont_ex->last_cl_quote_scale), "bcd_get");

			if(sesscont->is_limited){
				sesscont_ex->limit_down_int = sesscont_ex->last_cl_quote_int - sesscont_ex->limit_down_int;
				sesscont_ex->limit_up_int = sesscont_ex->last_cl_quote_int + sesscont_ex->limit_up_int;
			}else{
				sesscont_ex->limit_down_int = (sesscont_ex->last_cl_quote_int * 0.9) / sesscont_ex->minstep_int;
				sesscont_ex->limit_down_int *= sesscont_ex->minstep_int;

				sesscont_ex->limit_up_int = (sesscont_ex->last_cl_quote_int * 1.1) / sesscont_ex->minstep_int;
				sesscont_ex->limit_up_int *= sesscont_ex->minstep_int;
			}

			int64_t div = ipow(10, sesscont_ex->minstep_scale - sesscont->roundto);
			sesscont_ex->minstep_adj = sesscont_ex->minstep_int / div;
			sesscont_ex->limit_down_adj = sesscont_ex->limit_down_int / div;
			sesscont_ex->scale_adj = sesscont->roundto;

			int64_t limit_up_adj = sesscont_ex->limit_up_int / div;

			adjust_prices_map_tbl(limit_up_adj, sesscont_ex->limit_down_adj, sesscont_ex->scale_adj);

			//printf("Isin = %s(%d), min_step = %ld(%d), step_price = %ld(%d), round = %d, limit_down = %ld, limit_up = %ld, n_steps = %ld\n", sesscont->isin, sesscont->isin_id, sesscont_ex->minstep_int, sesscont_ex->minstep_scale, sesscont_ex->step_price_curr_int, sesscont_ex->step_price_curr_scale, sesscont_ex->origin.roundto, sesscont_ex->limit_down_int, sesscont_ex->limit_up_int, (sesscont_ex->limit_up_int - sesscont_ex->limit_down_int) / sesscont_ex->minstep_int + 1);
			//printf("Isin = %s(%d, %d), min_step_adj = %ld, scale_adj = %d, limit_down_adj = %ld, limit_up_adj = %ld\n", sesscont->isin, sesscont->isin_id, sesscont->sess_id, sesscont_ex->minstep_adj, sesscont_ex->scale_adj, sesscont_ex->limit_down_adj, limit_up_adj);

			ss_futsesscont_tbl->cnt++;
		}
	}else if(replmsg->msg_index == session_index){
		struct session *session = (struct session*)replmsg->data;
		FUTINFO_FUNC(futinfo_event_seslog(session));

		char useful = (session->replAct == 0);
		/*
		replAct — признак того, что запись удалена.
		replAct != 0 — запись удалена.
		replAct  = 0 — запись активна.
		*/

		signed long long replID = session->replID;


		if(useful){
			if(replID < ss_session_tbl->repl_id.min)
				ss_session_tbl->repl_id.min = replID;

			if(replID > ss_session_tbl->repl_id.max)
				ss_session_tbl->repl_id.max = replID;

			if(session->sess_id < ss_session_tbl->sess_id.min)
				ss_session_tbl->sess_id.min = session->sess_id;

			if(session->sess_id > ss_session_tbl->sess_id.max)
				ss_session_tbl->sess_id.max = session->sess_id;


			if(ss_session_tbl->cnt >= ss_session_tbl->data_size){
				int mltp = (ss_session_tbl->cnt - ss_session_tbl->data_size) / SESSSIZE_INCREMENT + 1;
				// Увеличиваем размер массива
				ss_session_tbl->data_size += SESSSIZE_INCREMENT * mltp;
				ss_session_tbl->data = realloc(ss_session_tbl->data, sizeof(struct session_ex_t *) * ss_session_tbl->data_size);
				if(ss_session_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

				int it;
				for(it = ss_session_tbl->data_size - SESSSIZE_INCREMENT * mltp; it < ss_session_tbl->data_size; it++)
					ss_session_tbl->data[it] = NULL;
			}


			struct session_ex_t *session_ex = malloc(sizeof(struct session_ex_t));
			if(session_ex == NULL) exit_memory_alloc_error(__LINE__, __func__);

			ss_session_tbl->data[ss_session_tbl->cnt] = session_ex;

			memcpy(session_ex, session, replmsg->data_size);

			ss_session_tbl->cnt++;
		}
	}
}



void futinfo_event_seslog(struct session *sess)
{
    char begin[24];
    char end[24];
    char inter_cl_begin[24];
    char inter_cl_end[24];
    char eve_begin[24];
    char eve_end[24];
    char mon_begin[24];
    char mon_end[24];
    char pos_transfer_begin[24];
    char pos_transfer_end[24];

    memset(&begin, 0, 24);
    memset(&end, 0, 24);
    memset(&inter_cl_begin, 0, 24);
    memset(&inter_cl_end, 0, 24);
    memset(&eve_begin, 0, 24);
    memset(&eve_end, 0, 24);
    memset(&mon_begin, 0, 24);
    memset(&mon_end, 0, 24);
    memset(&pos_transfer_begin, 0, 24);
    memset(&pos_transfer_end, 0, 24);


    size_t bufsize = 24;
    if(cg_getstr("t", &sess->begin, begin, &bufsize) != CG_ERR_OK) printf("Small buffer size! (%zd)\n", bufsize);
    if(cg_getstr("t", &sess->end, end, &bufsize) != CG_ERR_OK) printf("Small buffer size! (%zd)\n", bufsize);
    if(cg_getstr("t", &sess->inter_cl_begin, inter_cl_begin, &bufsize) != CG_ERR_OK) printf("Small buffer size! (%zd)\n", bufsize);
    if(cg_getstr("t", &sess->inter_cl_end, inter_cl_end, &bufsize) != CG_ERR_OK) printf("Small buffer size! (%zd)\n", bufsize);
    if(cg_getstr("t", &sess->eve_begin, eve_begin, &bufsize) != CG_ERR_OK) printf("Small buffer size! (%zd)\n", bufsize);
    if(cg_getstr("t", &sess->eve_end, eve_end, &bufsize) != CG_ERR_OK) printf("Small buffer size! (%zd)\n", bufsize);
    if(cg_getstr("t", &sess->mon_begin, mon_begin, &bufsize) != CG_ERR_OK) printf("Small buffer size! (%zd)\n", bufsize);
    if(cg_getstr("t", &sess->mon_end, mon_end, &bufsize) != CG_ERR_OK) printf("Small buffer size! (%zd)\n", bufsize);
    if(cg_getstr("t", &sess->pos_transfer_begin, pos_transfer_begin, &bufsize) != CG_ERR_OK) printf("Small buffer size! (%zd)\n", bufsize);
    if(cg_getstr("t", &sess->pos_transfer_end, pos_transfer_end, &bufsize) != CG_ERR_OK) printf("Small buffer size! (%zd)\n", bufsize);

#ifdef FUTINFO_LOG
	logsaver(FUTINFO_LOG_ID, "%lld,%lld,%lld,%d,%s,%s,%d,%d,%s,%s,%d,%d,%s,%s,%d,%s,%s,%s,%s\n", sess->replID, sess->replRev, sess->replAct, sess->sess_id, begin, end, sess->state, sess->opt_sess_id, inter_cl_begin, inter_cl_end, sess->inter_cl_state, sess->eve_on, eve_begin, eve_end, sess->mon_on, mon_begin, mon_end, pos_transfer_begin, pos_transfer_end);
    //printf("%lld,%lld,%lld,%d,%s,%s,%d,%d,%s,%s,%d,%d,%s,%s,%d,%s,%s,%s,%s\n", sess->replID, sess->replRev, sess->replAct, sess->sess_id, begin, end, sess->state, sess->opt_sess_id, inter_cl_begin, inter_cl_end, sess->inter_cl_state, sess->eve_on, eve_begin, eve_end, sess->mon_on, mon_begin, mon_end, pos_transfer_begin, pos_transfer_end);
	thread_func_logger_flush(FUTINFO_LOG_ID);
#endif

}

void futinfo_event_sescontlog(struct fut_sess_contents *sesscont)
{
	int64_t min_step_int;
	int8_t min_step_scale;
	int64_t step_price_int;
	int8_t step_price_scale;
	int64_t limit_up_int;
	int8_t limit_up_scale;
	int64_t limit_down_int;
	int8_t limit_down_scale;
	int64_t last_cl_quote_int;
	int8_t last_cl_quote_scale;
	int64_t step_price_curr_int;
	int8_t step_price_curr_scale;

	WARN_FAIL(cg_bcd_get(sesscont->min_step, &min_step_int, &min_step_scale), "bcd_get");
	WARN_FAIL(cg_bcd_get(sesscont->step_price, &step_price_int, &step_price_scale), "bcd_get");
	WARN_FAIL(cg_bcd_get(sesscont->step_price_curr, &step_price_curr_int, &step_price_curr_scale), "bcd_get");
	WARN_FAIL(cg_bcd_get(sesscont->limit_up, &limit_up_int, &limit_up_scale), "bcd_get");
	WARN_FAIL(cg_bcd_get(sesscont->limit_down, &limit_down_int, &limit_down_scale), "bcd_get");
	WARN_FAIL(cg_bcd_get(sesscont->last_cl_quote, &last_cl_quote_int, &last_cl_quote_scale), "bcd_get");

#ifdef FUTINFO_LOG
	logsaver(FUTINFO_LOG_ID, "%lld,%lld,%lld,%d,%d,%s,%d,%ld,%d,%ld,%d,%d,%d,%ld,%d,%ld,%d,%ld,%d,%ld,%d\n", sesscont->replID, sesscont->replRev, sesscont->replAct, sesscont->sess_id, sesscont->isin_id, sesscont->isin, sesscont->roundto, min_step_int, min_step_scale, step_price_int, step_price_scale, sesscont->signs, sesscont->is_limited, limit_up_int, limit_up_scale, limit_down_int, limit_down_scale, last_cl_quote_int, last_cl_quote_scale, step_price_curr_int, step_price_curr_scale);
	//printf("%lld,%lld,%lld,%d,%d,%s,%d,%ld,%d,%ld,%d,%d,%d,%ld,%d,%ld,%d,%ld,%d,%ld,%d\n", sesscont->replID, sesscont->replRev, sesscont->replAct, sesscont->sess_id, sesscont->isin_id, sesscont->isin, sesscont->roundto, min_step_int, min_step_scale, step_price_int, step_price_scale, sesscont->signs, sesscont->is_limited, limit_up_int, limit_up_scale, limit_down_int, limit_down_scale, last_cl_quote_int, last_cl_quote_scale, step_price_curr_int, step_price_curr_scale);
	thread_func_logger_flush(FUTINFO_LOG_ID);
#endif
}

void futinfo_event_online(struct cg_msg_streamdata_t *replmsg)
{
	if(replmsg->msg_index == fut_sess_contents_index){
		signed int affected_isin_id = -1;

		struct fut_sess_contents *sesscont = (struct fut_sess_contents*)replmsg->data;

		FUTINFO_FUNC(futinfo_event_sescontlog(sesscont));

		char useful = (sesscont->replAct == 0);
		/*
		Критерий полезности этой заявки
		0 - заявка бесполезная
		1 - заявку нужно добавить в систему
		*/

		signed long long replID = sesscont->replID;

		if(useful){
			replID -= futsesscont_tbl->shift;
			if(replID >= futsesscont_tbl->data_size){
				int mltp = (replID - futsesscont_tbl->data_size) / SESSCONTSIZE_INCREMENT + 1;
				// Увеличиваем размер массива для хранения заявок
				futsesscont_tbl->data_size += SESSCONTSIZE_INCREMENT * mltp;
				futsesscont_tbl->data = realloc(futsesscont_tbl->data, sizeof(struct sesscont_ex_t *) * futsesscont_tbl->data_size);
				if(futsesscont_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

//				printf("futsesscont_tbl->data_size = %lld\n", futsesscont_tbl->data_size);

				int it;
				for(it = futsesscont_tbl->data_size - SESSCONTSIZE_INCREMENT * mltp; it < futsesscont_tbl->data_size; it++)
					futsesscont_tbl->data[it] = NULL;
			}

			if(futsesscont_tbl->data[replID] == NULL){
				futsesscont_tbl->data[replID] = malloc(sizeof(struct sesscont_ex_t));
				struct sesscont_ex_t *sesscont_ex = futsesscont_tbl->data[replID];
				if(sesscont_ex == NULL) exit_memory_alloc_error(__LINE__, __func__);

				memcpy(sesscont_ex, sesscont, replmsg->data_size);
				sesscont_ex->origin.option = 0;

				WARN_FAIL(cg_bcd_get(sesscont->min_step, &sesscont_ex->minstep_int, &sesscont_ex->minstep_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->step_price, &sesscont_ex->step_price_int, &sesscont_ex->step_price_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->step_price_curr, &sesscont_ex->step_price_curr_int, &sesscont_ex->step_price_curr_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->limit_up, &sesscont_ex->limit_up_int, &sesscont_ex->limit_up_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->limit_down, &sesscont_ex->limit_down_int, &sesscont_ex->limit_down_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->last_cl_quote, &sesscont_ex->last_cl_quote_int, &sesscont_ex->last_cl_quote_scale), "bcd_get");

				if(sesscont->is_limited){
					sesscont_ex->limit_down_int = sesscont_ex->last_cl_quote_int - sesscont_ex->limit_down_int;
					sesscont_ex->limit_up_int = sesscont_ex->last_cl_quote_int + sesscont_ex->limit_up_int;
				}else{
					sesscont_ex->limit_down_int = (sesscont_ex->last_cl_quote_int * 0.9) / sesscont_ex->minstep_int;
					sesscont_ex->limit_down_int *= sesscont_ex->minstep_int;

					sesscont_ex->limit_up_int = (sesscont_ex->last_cl_quote_int * 1.1) / sesscont_ex->minstep_int;
					sesscont_ex->limit_up_int *= sesscont_ex->minstep_int;
				}

				affected_isin_id =  sesscont->isin_id;

				int64_t div = ipow(10, sesscont_ex->minstep_scale - sesscont->roundto);
				sesscont_ex->minstep_adj = sesscont_ex->minstep_int / div;
				sesscont_ex->limit_down_adj = sesscont_ex->limit_down_int / div;
				sesscont_ex->scale_adj = sesscont->roundto;
				int64_t limit_up_adj = sesscont_ex->limit_up_int / div;

				adjust_prices_map_tbl(limit_up_adj, sesscont_ex->limit_down_adj, sesscont_ex->scale_adj);
				rebuild_prices_map_tbl();



#ifdef ORDLOG_DEBUG
				//printf("NEW FUT_SES_CONT\n");
				if(sesscont_ex->minstep_scale != sesscont_ex->step_price_scale || sesscont_ex->minstep_scale != sesscont_ex->step_price_curr_scale
						 || sesscont_ex->minstep_scale != sesscont_ex->limit_up_scale
						 || sesscont_ex->minstep_scale != sesscont_ex->limit_down_scale){
					printf("minstep_scale = %d, step_price_scale = %d, step_price_curr_scale = %d\n", sesscont_ex->minstep_scale, sesscont_ex->step_price_scale, sesscont_ex->step_price_curr_scale);
					exit_ordlog_debug_error(__LINE__, __func__);
				}
#endif

				futsesscont_tbl->cnt++;


				signed int isin_id = sesscont->isin_id;
				signed int sess_id = sesscont->sess_id;
				create_isins_tbl_entry(&isin_id, &sess_id);

				struct sess_data_row_t *sess_data = isins_tbl->data[isin_id]->data[sess_id];
				isins_tbl->ext_links++;

				sess_data->cont = sesscont_ex;

				fill_isins_tbl_entry(isins_tbl->data[isin_id], sess_data);




			}else{
				struct sesscont_ex_t *sesscont_ex = futsesscont_tbl->data[replID];
				memcpy(sesscont_ex, sesscont, replmsg->data_size);

#ifdef ORDLOG_DEBUG
				//printf("UPDATE FUT_SES_CONT\n");
				int64_t minstep_int = sesscont_ex->minstep_int;
				int8_t minstep_scale = sesscont_ex->minstep_scale;

				int64_t step_price_curr_int = sesscont_ex->step_price_curr_int;
				//int8_t step_price_curr_scale = sesscont_ex->step_price_curr_scale;
#endif
				int64_t step_price_int = sesscont_ex->step_price_int;
				//int8_t step_price_scale = sesscont_ex->step_price_scale;

				int64_t limit_up_int = sesscont_ex->limit_up_int;
				//int8_t limit_up_scale = sesscont_ex->limit_up_scale;

				int64_t limit_down_int = sesscont_ex->limit_down_int;
				//int8_t limit_down_scale = sesscont_ex->limit_down_scale;

				WARN_FAIL(cg_bcd_get(sesscont->min_step, &sesscont_ex->minstep_int, &sesscont_ex->minstep_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->step_price, &sesscont_ex->step_price_int, &sesscont_ex->step_price_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->step_price_curr, &sesscont_ex->step_price_curr_int, &sesscont_ex->step_price_curr_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->limit_up, &sesscont_ex->limit_up_int, &sesscont_ex->limit_up_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->limit_down, &sesscont_ex->limit_down_int, &sesscont_ex->limit_down_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->last_cl_quote, &sesscont_ex->last_cl_quote_int, &sesscont_ex->last_cl_quote_scale), "bcd_get");

				if(sesscont->is_limited){
					sesscont_ex->limit_down_int = sesscont_ex->last_cl_quote_int - sesscont_ex->limit_down_int;
					sesscont_ex->limit_up_int = sesscont_ex->last_cl_quote_int + sesscont_ex->limit_up_int;
				}else{
					sesscont_ex->limit_down_int = (sesscont_ex->last_cl_quote_int * 0.9) / sesscont_ex->minstep_int;
					sesscont_ex->limit_down_int *= sesscont_ex->minstep_int;

					sesscont_ex->limit_up_int = (sesscont_ex->last_cl_quote_int * 1.1) / sesscont_ex->minstep_int;
					sesscont_ex->limit_up_int *= sesscont_ex->minstep_int;
				}

				if(step_price_int != sesscont_ex->step_price_int)
					affected_isin_id =  sesscont->isin_id;

				if(limit_up_int != sesscont_ex->limit_up_int || limit_down_int != sesscont_ex->limit_down_int){
					signed int isin_id = sesscont->isin_id - isins_tbl->shift;
					signed int sess_id = sesscont->sess_id - isins_tbl->sess_id.min;

					int shift = (sesscont_ex->limit_down_int - limit_down_int) / sesscont_ex->minstep_int;

					limit_up_int = limit_up_int > sesscont_ex->limit_up_int ? limit_up_int : sesscont_ex->limit_up_int;
					limit_down_int = limit_down_int < sesscont_ex->limit_down_int ? limit_down_int : sesscont_ex->limit_down_int;

					int data_size = (limit_up_int - limit_down_int) / sesscont_ex->minstep_int + 1;
					struct price_aggr_t **bid = malloc(sizeof(struct price_aggr_t *) * data_size);
					struct price_aggr_t **ask = malloc(sizeof(struct price_aggr_t *) * data_size);

					struct sess_data_row_t *sess_data = isins_tbl->data[isin_id]->data[sess_id];
					int it;
					int jt = 0;
					for(it = 0; it < data_size; it++){
						if((it + shift) >= 0 && jt < sess_data->askEx->data_size){
							bid[it] = sess_data->bidEx->data[jt];
							ask[it] = sess_data->askEx->data[jt];
							jt++;
						}else{
							struct price_aggr_t *bid_aggr = malloc(sizeof(struct price_aggr_t));
							bid_aggr->amount_rest = 0;
//							bid_aggr->lc_amount_rest = 0;
//							bid_aggr->fr_amount_rest = 0;
							bid[it] = bid_aggr;

							struct price_aggr_t *ask_aggr = malloc(sizeof(struct price_aggr_t));
							ask_aggr->amount_rest = 0;
//							ask_aggr->lc_amount_rest = 0;
//							ask_aggr->fr_amount_rest = 0;
							ask[it] = ask_aggr;
						}
					}


					free(sess_data->bidEx->data);
					free(sess_data->askEx->data);

					sess_data->bidEx->data_size = data_size;
					sess_data->bidEx->data = bid;
					sess_data->bidEx->first = 0;
					sess_data->bidEx->last = 0;

					sess_data->askEx->data_size = data_size;
					sess_data->askEx->data = ask;
					sess_data->askEx->first = data_size - 1;
					sess_data->askEx->last = data_size - 1;


					int bid_first = sess_data->bidEx->data_size - 1;
					while(sess_data->bidEx->data[bid_first]->amount_rest == 0){
						bid_first--;
						if(bid_first == 0) break;
					}

					int ask_first = 0;
					while(sess_data->askEx->data[ask_first]->amount_rest == 0){
						ask_first++;
						if(ask_first == sess_data->askEx->data_size){
							ask_first--;
							break;
						}
					}

					sess_data->bidEx->first = bid_first;
					sess_data->askEx->first = ask_first;


					sesscont_ex->limit_down_int = limit_down_int;
					sesscont_ex->limit_up_int = limit_up_int;

					sess_data->limit_down = limit_down_int;
					sess_data->limit_up = limit_up_int;


					int64_t div = ipow(10, sesscont_ex->minstep_scale - sesscont->roundto);
					sesscont_ex->minstep_adj = sesscont_ex->minstep_int / div;
					sesscont_ex->limit_down_adj = sesscont_ex->limit_down_int / div;
					sesscont_ex->scale_adj = sesscont->roundto;
					int64_t limit_up_adj = sesscont_ex->limit_up_int / div;

					adjust_prices_map_tbl(limit_up_adj, sesscont_ex->limit_down_adj, sesscont_ex->scale_adj);
					rebuild_prices_map_tbl();


				}

#ifdef ORDLOG_DEBUG
				if(minstep_int != sesscont_ex->minstep_int){
					printf("minstep_int (%ld) != sesscont_ex->minstep_int (%ld)\n", minstep_int, sesscont_ex->minstep_int);
					exit_ordlog_debug_error(__LINE__, __func__);
				}

				if(step_price_curr_int != sesscont_ex->step_price_curr_int){
					printf("step_price_curr_int (%ld) != sesscont_ex->step_price_curr_int (%ld)\n", step_price_curr_int, sesscont_ex->step_price_curr_int);
					exit_ordlog_debug_error(__LINE__, __func__);
				}

				if(minstep_scale != sesscont_ex->minstep_scale
						|| sesscont_ex->minstep_scale != sesscont_ex->step_price_scale
						|| sesscont_ex->minstep_scale != sesscont_ex->step_price_curr_scale
						|| sesscont_ex->minstep_scale != sesscont_ex->limit_up_scale
						|| sesscont_ex->minstep_scale != sesscont_ex->limit_down_scale
						){
					printf("minstep_scale = %d, sess_cont->minstep_scale = %d, sess_cont->step_price_scale = %d, sess_cont->step_price_curr_scale = %d\n", minstep_scale,  sesscont_ex->minstep_scale, sesscont_ex->step_price_scale, sesscont_ex->step_price_curr_scale);
					exit_ordlog_debug_error(__LINE__, __func__);
				}
#endif


			}
		}else{
			replID -= futsesscont_tbl->shift;

#ifdef ORDLOG_DEBUG
			if(replID < 0 || replID >= futsesscont_tbl->data_size){
				printf("replID is out of the range\n");
				exit_ordlog_debug_error(__LINE__, __func__);
			}
#endif

			struct sesscont_ex_t *sesscont_ex = futsesscont_tbl->data[replID];
			destroy_isins_tbl_entry(sesscont_ex->origin.isin_id, sesscont_ex->origin.sess_id);

			free(sesscont_ex);
			futsesscont_tbl->data[replID] = NULL;

			futsesscont_tbl->cnt--;
			isins_tbl->ext_links--;
		}

		if(affected_isin_id >= 0){
			if(qnt_affected_tbl->cnt >= qnt_affected_tbl->data_size){
				qnt_affected_tbl->data_size += AFFECTED_ISINS;
				qnt_affected_tbl->data = realloc(qnt_affected_tbl->data, sizeof(signed int) * qnt_affected_tbl->data_size);
				if(qnt_affected_tbl->data == NULL)
					exit_memory_alloc_error(__LINE__, __func__);
			}
			qnt_affected_tbl->data[qnt_affected_tbl->cnt++] = affected_isin_id;
		}


	}else if(replmsg->msg_index == session_index){
		struct session *session = (struct session*)replmsg->data;

		FUTINFO_FUNC(futinfo_event_seslog(session));

		char useful = (session->replAct == 0);
		/*
		Критерий полезности этой заявки
		0 - заявка бесполезная
		1 - заявку нужно добавить в систему
		*/

		signed long long replID = session->replID;

		if(useful){
			replID -= session_tbl->shift;
			if(replID >= session_tbl->data_size){
				int mltp = (replID - session_tbl->data_size) / SESSSIZE_INCREMENT + 1;
				// Увеличиваем размер массива для хранения заявок
				session_tbl->data_size += SESSSIZE_INCREMENT * mltp;
				session_tbl->data = realloc(session_tbl->data, sizeof(struct session_ex_t *) * session_tbl->data_size);
				if(session_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

				int it;
				for(it = session_tbl->data_size - SESSSIZE_INCREMENT * mltp; it < session_tbl->data_size; it++)
					session_tbl->data[it] = NULL;
			}

			if(session_tbl->data[replID] == NULL){
				session_tbl->data[replID] = malloc(sizeof(struct session_ex_t));
				struct session_ex_t *session_ex = session_tbl->data[replID];
				if(session_ex == NULL) exit_memory_alloc_error(__LINE__, __func__);

				memcpy(session_ex, session, replmsg->data_size);

				session_tbl->cnt++;

				futinfo_event_online_sesstate_changed(session_ex);
				//if(session->state == 1) session_tbl->current = session_ex;

			}else{
				memcpy(session_tbl->data[replID], session, replmsg->data_size);

				futinfo_event_online_sesstate_changed(session_tbl->data[replID]);
			}
		}else{
			replID -= session_tbl->shift;

			struct session_ex_t *session_ex = session_tbl->data[replID];

			if(replID < session_tbl->data_size && session_ex){
				if(session_ex->origin.state == 1){
					//session_tbl->current = NULL;
					futinfo_event_online_sesstate_changed(NULL);
				}

				free(session_ex);
				session_tbl->data[replID] = NULL;

			}
		}


	}
}

time_t cg_time_to_time_t(struct cg_time_t *cg_time)
{
	struct tm tm;
	memset(&tm, 0, sizeof(struct tm));
	tm.tm_year = cg_time->year - 1900;
	tm.tm_mon = cg_time->month - 1;
	tm.tm_mday = cg_time->day;
	tm.tm_hour = cg_time->hour;
	tm.tm_min = cg_time->minute;
	tm.tm_sec = cg_time->second;

	return mktime(&tm);
}

void futinfo_event_online_sesstate_changed(struct session_ex_t *session_ex)
{
	if(session_ex == NULL){
		session_tbl->current = NULL;
		printf("session_ex == NULL, clear_portfolios_market_data\n");
		LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "session_ex == NULL, clear_portfolios_market_data\n"));
		clear_portfolios_market_data();
		return;
	}else if(session_ex->origin.state == 1){
		session_tbl->current = session_ex;
		// Reset portfolio
		printf("session_ex->state == 1, clear_portfolios_trd_sate, clear_portfolios_market_data\n");
		LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "session_ex->state == 1, clear_portfolios_trd_sate, clear_portfolios_market_data\n"));
		clear_portfolios_trd_sate();
		clear_portfolios_market_data();
	}else if(session_tbl->current && session_tbl->current->origin.sess_id == session_ex->origin.sess_id){
		session_tbl->current = NULL;
		// Reset portfolio
		printf("session_ex->state changed, clear_portfolios_market_data\n");
		LOGICLOG_FUNC(logsaver(LOGIC_LOG_ID, "session_ex->state changed, clear_portfolios_market_data\n"));
		clear_portfolios_market_data();
	}

	/*
	struct timespec curr_tm;
	clock_gettime (CLOCK_REALTIME, &curr_tm);

	time_t ses_begin = cg_time_to_time_t(&session_ex->origin.begin);
	time_t ses_end = cg_time_to_time_t(&session_ex->origin.end);

	time_t eve_begin = cg_time_to_time_t(&session_ex->origin.eve_begin);
	time_t eve_end = cg_time_to_time_t(&session_ex->origin.eve_end);


	if((curr_tm.tv_sec >= ses_begin && curr_tm.tv_sec <= ses_end) || (curr_tm.tv_sec >= eve_begin && curr_tm.tv_sec <= eve_end)){
		session_tbl->current = session_ex;
	}
	*/


#ifdef UNITTEST
	if(session_tbl->current && session_tbl->current->origin.sess_id < session_ex->origin.sess_id)
		session_tbl->current = session_ex;
	else if(session_tbl->current == NULL)
		session_tbl->current = session_ex;
#endif

	session_ex = session_tbl->current;

	if(session_ex == NULL || session_tbl->state == session_ex->origin.state) return;


	switch(session_tbl->state){
	case SETTED_SESST:{
		switch(session_ex->origin.state){
		case ACTIVE_SESST:{

		}break;
		case SUSPENDED_SESST:{

		}break;
		case FORCEDFINISHED_SESST:{

		}break;
		case FINISHED_SESST:{

		}break;
		}
	}break;
	case ACTIVE_SESST:{
		switch(session_ex->origin.state){
		case SUSPENDED_SESST:{

		}break;
		case FORCEDFINISHED_SESST:
		case FINISHED_SESST:{
			susp_trans_tbl->release = 0;
			susp_trans_tbl->free = 1;
		}break;
		}
	}break;
	case SUSPENDED_SESST:{
		switch(session_ex->origin.state){
		case ACTIVE_SESST:{
			susp_trans_tbl->release = 1;
			susp_trans_tbl->free = 0;
		}break;
		case FORCEDFINISHED_SESST:
		case FINISHED_SESST:{
			susp_trans_tbl->release = 0;
			susp_trans_tbl->free = 1;
		}break;
		}
	}break;
	case FINISHED_SESST:{
		susp_trans_tbl->release = 0;
		susp_trans_tbl->free = 0;

		printf("FINISHED_SESST\n");

		clear_portfolios_trd_sate();

	}break;
	}


	session_tbl->state = session_ex->origin.state;
	if(session_tbl->state == FORCEDFINISHED_SESST) session_tbl->state = FINISHED_SESST;
}

void rebuild_prices_map_tbl()
{
	price_to_str_tbl->size_of_price = sizeof(((struct FutAddOrder *)0)->price);
	int it;
	for(it = 0; it < price_to_str_tbl->data_size; it++){
		struct prices_map_tbl_t *prices_map = price_to_str_tbl->data[it];

		if(prices_map && (prices_map->price.min != prices_map->range.min || prices_map->price.max != prices_map->range.max)){
			long long int min_price = prices_map->range.min < prices_map->price.min ? prices_map->range.min : prices_map->price.min;
			long long int max_price = prices_map->range.max > prices_map->price.max ? prices_map->range.max : prices_map->price.max;

			unsigned long long data_size = max_price - min_price + 1;
			char **data = malloc(sizeof(char *) * data_size);

			int shift;
			if(prices_map->price.min == LLONG_MAX) shift = 0;
			else shift = prices_map->range.min - prices_map->price.min;

			prices_map->shift = min_price;

			int jt;
			int kt = 0;
			for(jt = 0; jt < data_size; jt ++){
				if((jt + shift) >= 0 && kt < prices_map->data_size){
					data[jt] = prices_map->data[kt++];
				}else{
					char *price_str = malloc(price_to_str_tbl->size_of_price);
					if(price_str == NULL) exit_memory_alloc_error(__LINE__, __func__);

					i64toa_branchlut_ex(prices_map->shift + jt, price_str, it);
					data[jt] = price_str;
				}
			}

			free(prices_map->data);
			prices_map->data = data;
			prices_map->data_size = data_size;
			prices_map->price = prices_map->range;
		}
	}
}

void adjust_prices_map_tbl(int64_t limit_up_adj, int64_t limit_down_adj, int64_t scale_adj)
{
	if(scale_adj >= price_to_str_tbl->data_size){
		int data_size = scale_adj + 1;
		price_to_str_tbl->data = realloc(price_to_str_tbl->data, sizeof(struct prices_map_tbl_t *) * data_size);
		if(price_to_str_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

		int it;
		for(it = price_to_str_tbl->data_size; it < data_size; it++)
			price_to_str_tbl->data[it] = NULL;

		price_to_str_tbl->data_size = data_size;
	}

	if(price_to_str_tbl->data[scale_adj] == NULL){
		struct prices_map_tbl_t *prices_map_tbl = malloc(sizeof(struct prices_map_tbl_t));
		if(prices_map_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
		prices_map_tbl->range.max = LLONG_MIN;
		prices_map_tbl->range.min = LLONG_MAX;

		prices_map_tbl->price = prices_map_tbl->range;

		prices_map_tbl->data = NULL;
		prices_map_tbl->data_size = 0;
		prices_map_tbl->shift = LLONG_MAX;

		price_to_str_tbl->data[scale_adj] = prices_map_tbl;
	}

	struct prices_map_tbl_t *prices_map_tbl = price_to_str_tbl->data[scale_adj];
	if(limit_down_adj < prices_map_tbl->range.min) prices_map_tbl->range.min = limit_down_adj;
	if(limit_up_adj > prices_map_tbl->range.max) prices_map_tbl->range.max = limit_up_adj;
}

void clear_portfolios_market_data()
{
	uint it;
	for(it = 0; it < portfolios_tbl->cnt; it++){
		struct portfolio_t *portfolio = portfolios_tbl->data[it];
		struct quotes_tbl_t *quotes_tbl = portfolio->quotes_tbl;

		portfolio->wask = 0;
		portfolio->wbid = 0;

		portfolio->zero_wasks_num = quotes_tbl->cnt;
		portfolio->zero_wbids_num = quotes_tbl->cnt;

		uint jt;
		for(jt = 0; jt < quotes_tbl->cnt; jt++){
			struct quote_t *quote = quotes_tbl->data[jt];
			quote->bid_tgt = 0;
			quote->wbid = 0;
			quote->ask_tgt = 0;
			quote->wask = 0;
		}

	}
}

void clear_portfolios_trd_sate()
{
	uint it;
	for(it = 0; it < portfolios_tbl->cnt; it++){
		struct portfolio_t *portfolio = portfolios_tbl->data[it];
		clear_portfolio_trd_sate(portfolio);
	}

	struct active_order_t *active = active_orders_tbl;
	while(active){
		struct active_order_t *prev = active;

		ordrs_thr_empty_orders_tbl->data[ordrs_thr_empty_orders_tbl->cnt++] = active;
		active = active->next;

		prev->next = NULL;
		prev->prev = NULL;
	}

	active_orders_tbl = NULL;
}

void clear_portfolio_trd_sate(struct portfolio_t *portfolio)
{
	portfolio->active_bids = 0;
	portfolio->active_asks = 0;
	portfolio->need_buy_amt = 0;
	portfolio->need_sell_amt = 0;
	//portfolio->buy_at = 0;
	//portfolio->sell_at = 0;
	portfolio->best_buy = LLONG_MIN;
	portfolio->best_sell = LLONG_MAX;
	portfolio->same_inn = 0;
	portfolio->same_inn_ch = 0;
	portfolio->same_inn_dir = 0;
	portfolio->killed_buy_amt = 0;
	portfolio->killed_sell_amt = 0;
	portfolio->sl = 0;
	portfolio->sl_cnt = 0;
	portfolio->sl_dir = 0;
	portfolio->sl_order = 0;
	portfolio->best_buy_trd = LLONG_MAX;
	portfolio->best_sell_trd = LLONG_MIN;

	struct active_order_t *active = portfolio->active_orders_tbl;
	while(active){
		struct active_order_t *prev = active;
		ordrs_thr_empty_orders_tbl->data[ordrs_thr_empty_orders_tbl->cnt++] = active;
		active = active->next;

		prev->next = NULL;
		prev->prev = NULL;
	}

	portfolio->active_orders_tbl = NULL;


	portfolio->state = WAIT_STATE;
}

void destroy_isins_tbl_entry(signed int isin_id, signed int sess_id)
{
//	ordlog_need_snapshot = 1;

	isin_id -= isins_tbl->shift;
	sess_id -= isins_tbl->sess_id.min;

	struct sess_data_row_t *sess_data = isins_tbl->data[isin_id]->data[sess_id];
	sess_data->destroyed = 1;
	//isins_tbl->data[isin_id]->data[sess_id] = NULL;

	int it;
	for(it = 0; it < sess_data->bidEx->data_size; it++){
		free(sess_data->bidEx->data[it]);
		free(sess_data->askEx->data[it]);
	}

//	isins_tbl->bytes_used -= 2 * sess_data->bidEx->data_size * sizeof(struct price_aggr_t);


	free(sess_data->bidEx->data);
	free(sess_data->askEx->data);

	free(sess_data->bidEx);
	free(sess_data->askEx);

	sess_data->bidEx = NULL;
	sess_data->askEx = NULL;

	//free(sess_data);
}



void futinfo_event_set_online_sescont(void)
{
	if(futsesscont_tbl->data_size == 0) futsesscont_tbl->shift = ss_futsesscont_tbl->repl_id.min;
	signed long long replID = ss_futsesscont_tbl->repl_id.max - futsesscont_tbl->shift;

	if(replID >= futsesscont_tbl->data_size){
		int mltp = (replID - futsesscont_tbl->data_size) / SESSCONTSIZE_INCREMENT + 1;
		// Увеличиваем размер массива для хранения заявок
		futsesscont_tbl->data_size += SESSCONTSIZE_INCREMENT * mltp;
		futsesscont_tbl->data = realloc(futsesscont_tbl->data, sizeof(struct sesscont_ex_t *) * futsesscont_tbl->data_size);
		if(futsesscont_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

		int it;
		for(it = futsesscont_tbl->data_size - SESSCONTSIZE_INCREMENT * mltp; it < futsesscont_tbl->data_size; it++)
			futsesscont_tbl->data[it] = NULL;
	}

	struct sesscont_ex_t **data = ss_futsesscont_tbl->data;
	ss_futsesscont_tbl->data = NULL;

	int it;
	for(it = 0; it < ss_futsesscont_tbl->cnt; it++){
		signed long long replID = data[it]->origin.replID - futsesscont_tbl->shift;
		if(futsesscont_tbl->data[replID])free(futsesscont_tbl->data[replID]);
		futsesscont_tbl->data[replID] = data[it];

		struct sesscont_ex_t *sesscont_ex = data[it];
		signed int isin_id = sesscont_ex->origin.isin_id;
		signed int sess_id = sesscont_ex->origin.sess_id;
		create_isins_tbl_entry(&isin_id, &sess_id);

		struct sess_data_row_t *sess_data = isins_tbl->data[isin_id]->data[sess_id];
		sess_data->cont = sesscont_ex;

		fill_isins_tbl_entry(isins_tbl->data[isin_id], sess_data);
		isins_tbl->ext_links++;
	}
		//*/
	free(data);

	ss_futsesscont_tbl->data_size = 0;
	ss_futsesscont_tbl->cnt = 0;
	ss_futsesscont_tbl->repl_id.min = LLONG_MAX;
	ss_futsesscont_tbl->sess_id.min = LLONG_MAX;
	ss_futsesscont_tbl->isin_id.min = LLONG_MAX;
	ss_futsesscont_tbl->repl_id.max = LLONG_MIN;
	ss_futsesscont_tbl->sess_id.max = LLONG_MIN;
	ss_futsesscont_tbl->isin_id.max = LLONG_MIN;
}

void futinfo_event_set_online_session(void)
{
	if(session_tbl->data_size == 0) session_tbl->shift = ss_session_tbl->repl_id.min;
	signed long long replID = ss_session_tbl->repl_id.max - session_tbl->shift;

	if(replID >= session_tbl->data_size){
		int mltp = (replID - session_tbl->data_size) / SESSSIZE_INCREMENT + 1;
		// Увеличиваем размер массива для хранения заявок
		session_tbl->data_size += SESSSIZE_INCREMENT * mltp;
		session_tbl->data = realloc(session_tbl->data, sizeof(struct session_ex_t *) * session_tbl->data_size);
		if(session_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

		int it;
		for(it = session_tbl->data_size - SESSSIZE_INCREMENT * mltp; it < session_tbl->data_size; it++)
			session_tbl->data[it] = NULL;
	}

	struct session_ex_t **data = ss_session_tbl->data;
	ss_session_tbl->data = NULL;

	int it;
	for(it = 0; it < ss_session_tbl->cnt; it++){
		signed long long replID = data[it]->origin.replID - session_tbl->shift;
		if(session_tbl->data[replID]) free(session_tbl->data[replID]);
		session_tbl->data[replID] = data[it];

		struct session_ex_t *session_ex = data[it];

		futinfo_event_online_sesstate_changed(session_ex);

		/*
		if(session_ex->origin.state == 1){
			session_tbl->current = session_ex;
		}
		*/

	}

	free(data);

	ss_session_tbl->data_size = 0;
	ss_session_tbl->cnt = 0;
	ss_session_tbl->repl_id.min = LLONG_MAX;
	ss_session_tbl->repl_id.max = LLONG_MIN;
	ss_session_tbl->sess_id.min = LLONG_MAX;
	ss_session_tbl->sess_id.max = LLONG_MIN;
}
void futinfo_event_set_online(void)
{
	futinfo_event_set_online_session();

	if(isins_tbl->data == NULL){
		int mltp = (ss_futsesscont_tbl->isin_id.max - ss_futsesscont_tbl->isin_id.min) / ISINSSIZE_INCREMENT + 1;
		isins_tbl->data_size = ISINSSIZE_INCREMENT * mltp;

		isins_tbl->data = malloc(sizeof(struct isin_data_tbl_t*) * isins_tbl->data_size);
		if(isins_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
		isins_tbl->shift = ss_futsesscont_tbl->isin_id.min;
		isins_tbl->sess_id = ss_futsesscont_tbl->sess_id;

		int it;
		for(it = 0; it < isins_tbl->data_size; it++)
			isins_tbl->data[it] = NULL;
	}

	futinfo_event_set_online_sescont();


#ifdef NO_OPTIONS
	rebuild_prices_map_tbl();
	if(!integr_state_tmp) integrate_portfolios();
#endif


#ifndef NO_OPTIONS
	optinfo_listen = 1;
#else
	ordlog_listen = 1;
#endif


}

void fill_isins_tbl_entry(struct isin_data_tbl_t *isin_data, struct sess_data_row_t *sess_data)
{
	struct sesscont_ex_t *sesscont_ex = sess_data->cont;

	if(isin_data->msg_data == NULL){
		int jt;
		if(sesscont_ex->origin.option)
			for(jt = 0; jt < SIMULTANEOUSLY_SENT_TRANS; jt++){
				struct cg_msg_data_t *msg_data = NULL;

#if !defined(UNITTEST) && !defined(REPLY)
				if(cg_pub_msgnew(msg_publisher, CG_KEY_NAME, "OptAddOrder", (struct cg_msg_t**)&msg_data) != CG_ERR_OK)
					exit(CG_PUB_MSGNEW_ERROR);
#else
				msg_data = malloc(sizeof(struct cg_msg_data_t));
				msg_data->data = malloc(sizeof(struct OptAddOrder));
#endif

				struct cg_msg_data_ex_t *msg_data_ex = malloc(sizeof(struct cg_msg_data_ex_t));
				msg_data_ex->origin = msg_data;

				struct OptAddOrder *ord = (struct OptAddOrder*) msg_data->data;
				strcpy(ord->isin, sesscont_ex->origin.isin);
				strcpy(ord->client_code, kern_settings.ccode);
				ord->type = 1;

				msg_data_ex->option = 1;
				msg_data_ex->type = &ord->type;
				msg_data_ex->dir = &ord->dir;
				msg_data_ex->amount = &ord->amount;
				msg_data_ex->price = ord->price;
				msg_data_ex->next = isin_data->msg_data;
				if(msg_data_ex->next == NULL)	isin_data->msg_data_last = msg_data_ex;

				isin_data->msg_data = msg_data_ex;

				//isins_tbl->bytes_used += sizeof_OptAddOrder;
			}
		else
			for(jt = 0; jt < SIMULTANEOUSLY_SENT_TRANS; jt++){
				struct cg_msg_data_t *msg_data = NULL;

#if !defined(UNITTEST) && !defined(REPLY)
				if(cg_pub_msgnew(msg_publisher, CG_KEY_NAME, "FutAddOrder", (struct cg_msg_t**)&msg_data) != CG_ERR_OK)
					exit(CG_PUB_MSGNEW_ERROR);
#else
				msg_data = malloc(sizeof(struct cg_msg_data_t));
				msg_data->data = malloc(sizeof(struct FutAddOrder));
#endif

				struct cg_msg_data_ex_t *msg_data_ex = malloc(sizeof(struct cg_msg_data_ex_t));
				msg_data_ex->origin = msg_data;

				struct FutAddOrder *ord = (struct FutAddOrder*) msg_data->data;
				strcpy(ord->isin, sesscont_ex->origin.isin);
				strcpy(ord->client_code, kern_settings.ccode);
				ord->type = 1;

				msg_data_ex->option = 0;
				msg_data_ex->type = &ord->type;
				msg_data_ex->dir = &ord->dir;
				msg_data_ex->amount = &ord->amount;
				msg_data_ex->price = ord->price;
				msg_data_ex->next = isin_data->msg_data;
				if(msg_data_ex->next == NULL)	isin_data->msg_data_last = msg_data_ex;

				isin_data->msg_data = msg_data_ex;

				//isins_tbl->bytes_used += sizeof_FutAddOrder;


			}
	}

	if(sess_data->bidEx == NULL)
	{
		int jt;
		struct prices_tbl_t *bid = malloc(sizeof(struct prices_tbl_t));
		bid->data_size = (sesscont_ex->limit_up_int - sesscont_ex->limit_down_int) / sesscont_ex->minstep_int + 1;
		bid->data = malloc(sizeof(struct price_aggr_t *) * bid->data_size);
		bid->first = 0;
		bid->last = 0;
		for(jt = 0; jt < bid->data_size; jt++){
			bid->data[jt] = malloc(sizeof(struct price_aggr_t));
			bid->data[jt]->amount_rest = 0;
	//		bid->data[jt]->fr_amount_rest = 0;
	//		bid->data[jt]->lc_amount_rest = 0;
		}
		sess_data->bidEx = bid;


		struct prices_tbl_t *ask = malloc(sizeof(struct prices_tbl_t));
		ask->data_size = (sesscont_ex->limit_up_int - sesscont_ex->limit_down_int) / sesscont_ex->minstep_int + 1;
		ask->data = malloc(sizeof(struct price_aggr_t *) * ask->data_size);
		ask->first = ask->data_size - 1;
		ask->last = ask->data_size - 1;
		for(jt = 0; jt < ask->data_size; jt++){
			ask->data[jt] = malloc(sizeof(struct price_aggr_t));
			ask->data[jt]->amount_rest = 0;
	//		ask->data[jt]->fr_amount_rest = 0;
	//		ask->data[jt]->lc_amount_rest = 0;
		}
		sess_data->askEx = ask;
		//

		//isins_tbl->bytes_used += 2 * ask->data_size * sizeof(struct price_aggr_t);

		sess_data->limit_down = sesscont_ex->limit_down_int;
		sess_data->limit_up = sesscont_ex->limit_up_int;

	}else if(sess_data->limit_up != sesscont_ex->limit_up_int || sess_data->limit_down != sesscont_ex->limit_down_int){
	//////////////////////

		int shift = (sesscont_ex->limit_down_int - sess_data->limit_down) / sesscont_ex->minstep_int;

		int64_t limit_up_int = sess_data->limit_up > sesscont_ex->limit_up_int ? sess_data->limit_up : sesscont_ex->limit_up_int;
		int64_t limit_down_int = sess_data->limit_down < sesscont_ex->limit_down_int ? sess_data->limit_down : sesscont_ex->limit_down_int;

		int data_size = (limit_up_int - limit_down_int) / sesscont_ex->minstep_int + 1;
		struct price_aggr_t **bid = malloc(sizeof(struct price_aggr_t *) * data_size);
		struct price_aggr_t **ask = malloc(sizeof(struct price_aggr_t *) * data_size);

		int it;
		int jt = 0;
		for(it = 0; it < data_size; it++){
			if((it + shift) >= 0 && jt < sess_data->askEx->data_size){
				bid[it] = sess_data->bidEx->data[jt];
				ask[it] = sess_data->askEx->data[jt];
				jt++;
			}else{
				struct price_aggr_t *bid_aggr = malloc(sizeof(struct price_aggr_t));
				bid_aggr->amount_rest = 0;
//							bid_aggr->lc_amount_rest = 0;
//							bid_aggr->fr_amount_rest = 0;
				bid[it] = bid_aggr;

				struct price_aggr_t *ask_aggr = malloc(sizeof(struct price_aggr_t));
				ask_aggr->amount_rest = 0;
//							ask_aggr->lc_amount_rest = 0;
//							ask_aggr->fr_amount_rest = 0;
				ask[it] = ask_aggr;
			}
		}


		free(sess_data->bidEx->data);
		free(sess_data->askEx->data);

		sess_data->bidEx->data_size = data_size;
		sess_data->bidEx->data = bid;
		sess_data->bidEx->first = 0;
		sess_data->bidEx->last = 0;

		sess_data->askEx->data_size = data_size;
		sess_data->askEx->data = ask;
		sess_data->askEx->first = data_size - 1;
		sess_data->askEx->last = data_size - 1;


		int bid_first = sess_data->bidEx->data_size - 1;
		while(sess_data->bidEx->data[bid_first]->amount_rest == 0){
			bid_first--;
			if(bid_first == 0) break;
		}

		int ask_first = 0;
		while(sess_data->askEx->data[ask_first]->amount_rest == 0){
			ask_first++;
			if(ask_first == sess_data->askEx->data_size){
				ask_first--;
				break;
			}
		}

		sess_data->bidEx->first = bid_first;
		sess_data->askEx->first = ask_first;


		sesscont_ex->limit_down_int = limit_down_int;
		sesscont_ex->limit_up_int = limit_up_int;

		sess_data->limit_down = limit_down_int;
		sess_data->limit_up = limit_up_int;

	//////////////////////
	}
}

void futinfo_event_deleted(struct cg_data_cleardeleted_t *replmsg)
{
	if(replmsg->table_idx == fut_sess_contents_index){
#ifdef ORDLOG_DEBUG
		int cnt = 0;
#endif
		int it;
		for(it = 0; it < futsesscont_tbl->data_size; it++)
			if(futsesscont_tbl->data[it] && futsesscont_tbl->data[it]->origin.replRev < replmsg->table_rev){

				struct sesscont_ex_t *sesscont_ex = futsesscont_tbl->data[it];
				destroy_isins_tbl_entry(sesscont_ex->origin.isin_id, sesscont_ex->origin.sess_id);

				free(sesscont_ex);
				futsesscont_tbl->data[it] = NULL;
				futsesscont_tbl->cnt--;

				isins_tbl->ext_links--;
#ifdef ORDLOG_DEBUG
				cnt++;
#endif
			}

#ifdef ORDLOG_DEBUG
		if(cnt) printf("fut_sess_contents deleted = %d\n", cnt);
#endif

	}else if(replmsg->table_idx == session_index){
#ifdef ORDLOG_DEBUG
		int cnt = 0;
#endif

		int it;
		for(it = 0; it < session_tbl->data_size; it++)
			if(session_tbl->data[it] && session_tbl->data[it]->origin.replRev < replmsg->table_rev){

				struct session_ex_t *session_ex = session_tbl->data[it];
				if(session_ex->origin.state == 1)
					session_tbl->current = NULL;

				free(session_tbl->data[it]);
				session_tbl->data[it] = NULL;

#ifdef ORDLOG_DEBUG
				cnt++;
#endif
			}


#ifdef ORDLOG_DEBUG
		if(cnt) printf("sess deleted = %d\n", cnt);
#endif
	}

	//if(isins_tbl->ext_links == 0) isins_tbl->cnt = 0;

}
void futinfo_event_lifenum(void)
{
	int it;

	// Таблица fut_sess_contents
	for(it = 0; it < futsesscont_tbl->data_size; it++)
		if(futsesscont_tbl->data[it]){
			struct sesscont_ex_t *sesscont_ex = futsesscont_tbl->data[it];
			destroy_isins_tbl_entry(sesscont_ex->origin.isin_id, sesscont_ex->origin.sess_id);

			free(sesscont_ex);
			futsesscont_tbl->data[it] = NULL;

			isins_tbl->ext_links--;
		}

	free(futsesscont_tbl->data);
	futsesscont_tbl->data = NULL;
	futsesscont_tbl->data_size = 0;
	futsesscont_tbl->shift = 0;
	futsesscont_tbl->cnt = 0;

	//________________________________________________


	// Таблица session
	for(it = 0; it < session_tbl->data_size; it++)
		if(session_tbl->data[it]){

			free(session_tbl->data[it]);
			session_tbl->data[it] = NULL;

		}

	free(session_tbl->data);
	session_tbl->data = NULL;
	session_tbl->data_size = 0;
	session_tbl->shift = 0;
	session_tbl->cnt = 0;
	session_tbl->current = NULL;
	//________________________________________________


	if(isins_tbl->ext_links == 0) isins_tbl->cnt = 0;
}


CG_RESULT optinfo_event(cg_conn_t* conn, cg_listener_t* listener, struct cg_msg_t* msg, void* data)
{
	switch(msg->type)
	{
	case CG_MSG_OPEN:
	{
		/*
		Сообщение приходит в момент активации потока данных. Это событие гарантированно
		возникает до прихода каких либо данных по данной подписке. Для потоков репликации
		приход сообщения означает, что схема данных согласована и готова для использования
		(Подробнее см. Схемы данных) Данное сообщение не содержит дополнительных данных
		и его поля data и data_size не используются.
		*/

		//printf("OPTINFO:CG_MSG_OPEN\n");
		DEBUG_FUNC(printf_stream_newstate(2, msg->type, 0));
		optinfo_listener_state = CG_MSG_OPEN;
	}
	break;
	case CG_MSG_CLOSE:
	{
		/*
		Сообщение приходит в момент закрытия потока данных. Приход сообщения означает, что
		поток был закрыт пользователем или системой. В поле data содержится указатель на int,
		по указанному адресу хранится информация о причине закрытия подписчика. Возможны
		следующие причины:
		• CG_REASON_UNDEFINED - не определена;
		• CG_REASON_USER - пользователь вернул ошибку в callback подписчика;
		• CG_REASON_ERROR - внутренняя ошибка;
		• CG_REASON_DONE - вызван метод cg_lsn_destroy;
		• CG_REASON_SNAPSHOT_DONE - снэпшот получен.
		*/

		//printf("OPTINFO:CG_MSG_CLOSE = %d\n", *(int*)(msg->data));
		DEBUG_FUNC(printf_stream_newstate(2, msg->type, *(int*)(msg->data)));
		optinfo_listener_state = CG_MSG_CLOSE;
	}
	break;
	case CG_MSG_TN_BEGIN:
	{
		/*
		Означает момент начала получения очередного блока данных. В паре со следующим со-
		общением может быть использовано логикой ПО для контроля целостности данных. Дан-
		ное сообщение не содержит дополнительных данных и его поля data и data_size не ис-
		пользуются.
		*/
		clock_gettime (CLOCK_MONOTONIC, &optinfo_listener_TN_BEGIN);

	}
	break;
	case CG_MSG_TN_COMMIT:
	{
		/*
		Означает момент завершения получения очередного блока данных. К моменту прихода
		этого сообщения можно считать, что данные полученные по данной подписке, находят-
		ся в непротиворечивом состоянии и отражают таблицы в синхронизированном между со-
		бой состоянии. Данное сообщение не содержит дополнительных данных и его поля data
		и data_size не используются.
		*/
		clock_gettime (CLOCK_MONOTONIC, &optinfo_listener_TN_COMMIT);

		if(optinfo_listener_state == CG_MSG_P2REPL_ONLINE)
			DEBUG_FUNC(printf_stream_newstate(2, msg->type, 0));
//			printf("OPTINFO:Новые данные получены за: %ld\n", (optinfo_listener_TN_COMMIT.tv_sec - optinfo_listener_TN_BEGIN.tv_sec)*1000000000 + (optinfo_listener_TN_COMMIT.tv_nsec - optinfo_listener_TN_BEGIN.tv_nsec));

		// Здесь мы запускаем процесс отдачи данных во вне.
	}
	break;
	case CG_MSG_STREAM_DATA:
	{
		/*
		Сообщение прихода потоковых данных. Поле data_size содержит размер полученных дан-
		ных, data указывает на сами данные. Само сообщение содержит дополнительные поля,
		которые описываются структурой cg_msg_streamdata_t.
		*/

		/*
		struct cg_msg_streamdata_t
		{
			uint32_t type; 			// Тип сообщения. Всегда CG_MSG_STREAM_DATA для данного сообщения
			size_t data_size;		// Размер данных
			void* data;				// Указатель на данные

			size_t msg_index; 		// Номер описания сообщения в активной схеме
			uint32_t msg_id;		// Уникальный идентификатор типа сообщения

			const char* msg_name; 	// Имя сообщения в активной схеме
			int64_t rev;			// Ревизия записи

			size_t num_nulls;		// Размер массива байт
			const uint8_t* nulls; 	// Массив байт, содержащий 1 для каждого поля, являющегося NULL-ом.
		};
		*/


		struct cg_msg_streamdata_t *replmsg = (struct cg_msg_streamdata_t*)msg;

		if(optinfo_listener_state == CG_MSG_P2REPL_ONLINE)
			optinfo_event_online(replmsg);						// Обновление данных в режиме онлайн
		else
			optinfo_event_snapshot(replmsg);					// Первоначальное получение данных




		// Здесь происоходит обработка полученных данных и отсев ненужных данных.
	}
	break;
	case CG_MSG_P2REPL_ONLINE:
	{
		/*
		Переход потока в состояние online - это означает, что получение начального среза было
		завершено и следующие сообщения CG_MSG_STREAM_DATA будут нести данные он-лайн. Данное
		сообщение не содержит дополнительных данных и его поля data и data_size не используются.
		*/


		//printf("OPTINFO:CG_MSG_P2REPL_ONLINE\n");

		optinfo_event_set_online();

		DEBUG_FUNC(printf_stream_newstate(2, msg->type, 0));
		optinfo_listener_state = CG_MSG_P2REPL_ONLINE;

	}
	break;
	case CG_MSG_P2REPL_LIFENUM:
	{
		/*
		Изменен номер жизни схемы. Такое сообщение означает, что предыдущие данные, полу-
		ченные по потоку, не актуальны и должны быть очищены. При этом произойдёт повторная
		трансляция данных по новому номеру жизни схемы данных. Поле data сообщения указы-
		вает на целочисленное значение, содержащее новый номер жизни схемы; поле data_size
		содержит размер целочисленного типа. Подробнее про обработку номера жизни схемы в
		конце данного раздела
		*/


		struct timespec ts_start, ts_stop;

		clock_gettime (CLOCK_MONOTONIC, &ts_start);

		optinfo_event_lifenum();

		clock_gettime (CLOCK_MONOTONIC, &ts_stop);


		//printf("OPTINFO:CG_MSG_P2REPL_LIFENUM = %ld\n", (ts_stop.tv_sec - ts_start.tv_sec)*1000000000 + (ts_stop.tv_nsec - ts_start.tv_nsec));
		DEBUG_FUNC(printf_stream_newstate(2, msg->type, 0));

		optinfo_listener_state = CG_MSG_P2REPL_LIFENUM;


	}
	break;
	case CG_MSG_P2REPL_CLEARDELETED:
	{
		/*
		Произошла операция массового удаления устаревших данных. Поле data сообщения ука-
		зывает на структуру cg_data_cleardeleted_t, в которой указан номер таблицы и номер ре-
		визии, до которой данные в указанной таблице считаются удаленными. Если ревизия в
		cg_data_cleardeleted_t == CG_MAX_REVISON, то последующие ревизии продолжатся с 1.
		*/


		/*
		struct cg_data_cleardeleted_t {
			uint32_t table_idx;
			int64_t table_rev;
		};
		*/


		struct cg_data_cleardeleted_t *replmsg = (struct cg_data_cleardeleted_t*)msg;
		struct timespec ts_start, ts_stop;

		clock_gettime (CLOCK_MONOTONIC, &ts_start);

		optinfo_event_deleted(replmsg);

		clock_gettime (CLOCK_MONOTONIC, &ts_stop);


		//printf("OPTINFO:CG_MSG_P2REPL_CLEARDELETED = %ld\n", (ts_stop.tv_sec - ts_start.tv_sec)*1000000000 + (ts_stop.tv_nsec - ts_start.tv_nsec));
		DEBUG_FUNC(printf_stream_newstate(2, msg->type, 0));
		optinfo_listener_state = CG_MSG_P2REPL_CLEARDELETED;

	}
	break;
	case CG_MSG_P2REPL_REPLSTATE:
	{
		/*
		Сообщение содержит состояние потока данных; присылается перед закрытием потока.
		Поле data сообщения указывает на строку, которая в закодированном виде содержит со-
		стояние потока данных на момент прихода сообщения - сохраняются схема данных, но-
		мера ревизий таблиц и номер жизни схемы на момент последнего CG_MSG_TN_COMMIT
		(Внимание: при переоткрытии потока с replstate ревизии, полученные после последнего
		CG_MSG_TN_COMMIT, будут присланы повторно!) Эта строка может быть передана в вы-
		зов cg_lsn_open в качестве параметра "replstate" по этому же потоку в следующий раз, что
		обеспечит продолжение получения данных с момента остановки потока.
		*/
		//printf("OPTINFO:CG_MSG_P2REPL_REPLSTATE\n");
		DEBUG_FUNC(printf_stream_newstate(2, msg->type, 0));
		optinfo_listener_state = CG_MSG_P2REPL_REPLSTATE;

	}
	break;

	}
	return CG_ERR_OK;
}

void optinfo_event_snapshot(struct cg_msg_streamdata_t *replmsg)
{

	// Таблица сессионных инструментов
	if(replmsg->msg_index == opt_sess_contents_index){
		struct opt_sess_contents *sesscont = (struct opt_sess_contents*)replmsg->data;

		char useful = (sesscont->replAct == 0);
		/*
		replAct — признак того, что запись удалена.
		replAct != 0 — запись удалена.
		replAct  = 0 — запись активна.
		*/

		signed long long replID = sesscont->replID;


		if(useful){
			if(replID < ss_optsesscont_tbl->repl_id.min)
				ss_optsesscont_tbl->repl_id.min = replID;

			if(replID > ss_optsesscont_tbl->repl_id.max)
				ss_optsesscont_tbl->repl_id.max = replID;

			if(sesscont->isin_id < ss_optsesscont_tbl->isin_id.min)
				ss_optsesscont_tbl->isin_id.min = sesscont->isin_id;

			if(sesscont->isin_id > ss_optsesscont_tbl->isin_id.max)
				ss_optsesscont_tbl->isin_id.max = sesscont->isin_id;

			if(sesscont->sess_id < ss_optsesscont_tbl->sess_id.min)
				ss_optsesscont_tbl->sess_id.min = sesscont->sess_id;

			if(sesscont->sess_id > ss_optsesscont_tbl->sess_id.max)
				ss_optsesscont_tbl->sess_id.max = sesscont->sess_id;


			if(ss_optsesscont_tbl->cnt >= ss_optsesscont_tbl->data_size){
				int mltp = (ss_optsesscont_tbl->cnt - ss_optsesscont_tbl->data_size) / SESSCONTSIZE_INCREMENT + 1;
				// Увеличиваем размер массива
				ss_optsesscont_tbl->data_size += SESSCONTSIZE_INCREMENT * mltp;
				ss_optsesscont_tbl->data = realloc(ss_optsesscont_tbl->data, sizeof(struct sesscont_ex_t *) * ss_optsesscont_tbl->data_size);
				if(ss_optsesscont_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

				int it;
				for(it = ss_optsesscont_tbl->data_size - SESSCONTSIZE_INCREMENT * mltp; it < ss_optsesscont_tbl->data_size; it++)
					ss_optsesscont_tbl->data[it] = NULL;
			}


			ss_optsesscont_tbl->data[ss_optsesscont_tbl->cnt] = malloc(sizeof(struct sesscont_ex_t));
			struct sesscont_ex_t *sesscont_ex = ss_optsesscont_tbl->data[ss_optsesscont_tbl->cnt];
			if(sesscont_ex == NULL) exit_memory_alloc_error(__LINE__, __func__);

			memcpy(sesscont_ex, sesscont, replmsg->data_size);
			sesscont_ex->origin.option = 1;

			WARN_FAIL(cg_bcd_get(sesscont->min_step, &sesscont_ex->minstep_int, &sesscont_ex->minstep_scale), "bcd_get");
			WARN_FAIL(cg_bcd_get(sesscont->step_price, &sesscont_ex->step_price_int, &sesscont_ex->step_price_scale), "bcd_get");
//			WARN_FAIL(cg_bcd_get(sesscont->step_price_curr, &sesscont_ex->step_price_curr_int, &sesscont_ex->step_price_curr_scale), "bcd_get");
			WARN_FAIL(cg_bcd_get(sesscont->limit_up, &sesscont_ex->limit_up_int, &sesscont_ex->limit_up_scale), "bcd_get");
			WARN_FAIL(cg_bcd_get(sesscont->limit_down, &sesscont_ex->limit_down_int, &sesscont_ex->limit_down_scale), "bcd_get");
			WARN_FAIL(cg_bcd_get(sesscont->last_cl_quote, &sesscont_ex->last_cl_quote_int, &sesscont_ex->last_cl_quote_scale), "bcd_get");

			if(sesscont->is_limited){
				sesscont_ex->limit_down_int = sesscont_ex->last_cl_quote_int - sesscont_ex->limit_down_int;
				sesscont_ex->limit_up_int = sesscont_ex->last_cl_quote_int + sesscont_ex->limit_up_int;
			}else if(sesscont_ex->origin.option){
				int64_t strike_int;
				int8_t strike_scale;

				WARN_FAIL(cg_bcd_get(sesscont->strike, &strike_int, &strike_scale), "bcd_get");
				strike_int /= 10; // 10% of base asset price change

				sesscont_ex->limit_down_int = sesscont_ex->last_cl_quote_int - strike_int;
				if(sesscont_ex->limit_down_int < 0) sesscont_ex->limit_down_int = 0;
				sesscont_ex->limit_up_int = sesscont_ex->last_cl_quote_int + strike_int;
			}else{
				sesscont_ex->limit_down_int = (sesscont_ex->last_cl_quote_int * 0.9) / sesscont_ex->minstep_int;
				sesscont_ex->limit_down_int *= sesscont_ex->minstep_int;

				sesscont_ex->limit_up_int = (sesscont_ex->last_cl_quote_int * 1.1) / sesscont_ex->minstep_int;
				sesscont_ex->limit_up_int *= sesscont_ex->minstep_int;
			}

			int64_t div = ipow(10, sesscont_ex->minstep_scale - sesscont->roundto);
			sesscont_ex->minstep_adj = sesscont_ex->minstep_int / div;
			sesscont_ex->limit_down_adj = sesscont_ex->limit_down_int / div;
			sesscont_ex->scale_adj = sesscont->roundto;

			int64_t limit_up_adj = sesscont_ex->limit_up_int / div;
			if(sesscont_ex->scale_adj >= price_to_str_tbl->data_size){
				price_to_str_tbl->data = realloc(price_to_str_tbl->data, sizeof(struct prices_map_tbl_t *) * sesscont_ex->scale_adj);
				if(price_to_str_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

				int it;
				for(it = price_to_str_tbl->data_size; it < sesscont_ex->scale_adj; it++)
					price_to_str_tbl->data[it] = NULL;

				price_to_str_tbl->data_size = sesscont_ex->scale_adj;
			}

			if(price_to_str_tbl->data[sesscont_ex->scale_adj] == NULL){
				struct prices_map_tbl_t *prices_map_tbl = malloc(sizeof(struct prices_map_tbl_t));
				if(prices_map_tbl == NULL) exit_memory_alloc_error(__LINE__, __func__);
				prices_map_tbl->range.max = LLONG_MIN;
				prices_map_tbl->range.min = LLONG_MAX;
				price_to_str_tbl->data[sesscont_ex->scale_adj] = prices_map_tbl;
			}

			struct prices_map_tbl_t *prices_map_tbl = price_to_str_tbl->data[sesscont_ex->scale_adj];
			if(sesscont_ex->limit_down_adj < prices_map_tbl->range.min) prices_map_tbl->range.min = sesscont_ex->limit_down_adj;
			if(limit_up_adj > prices_map_tbl->range.max) prices_map_tbl->range.max = limit_up_adj;


//printf("Isin = %s(%d), min_step = %ld(%d), step_price = %ld(%d), last_cl_quote_int = %d, limit_down = %ld, limit_up = %ld, n_steps = %d\n", sesscont->isin, sesscont->isin_id, sesscont_ex->minstep_int, sesscont_ex->minstep_scale, sesscont_ex->step_price_int, sesscont_ex->step_price_scale, sesscont_ex->last_cl_quote_int, sesscont_ex->limit_down_int, sesscont_ex->limit_up_int, (sesscont_ex->limit_up_int - sesscont_ex->limit_down_int) / sesscont_ex->minstep_int + 1);

			ss_optsesscont_tbl->cnt++;
		}
	}

}
void optinfo_event_set_online(void)
{
	if(isins_tbl->data == NULL){
		int mltp = (ss_optsesscont_tbl->isin_id.max - ss_optsesscont_tbl->isin_id.min) / ISINSSIZE_INCREMENT + 1;
		isins_tbl->data_size = ISINSSIZE_INCREMENT * mltp;

		isins_tbl->data = realloc(isins_tbl->data, sizeof(struct isin_data_tbl_t*) * isins_tbl->data_size);
		if(isins_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);
		isins_tbl->shift = ss_optsesscont_tbl->isin_id.min;
		isins_tbl->sess_id = ss_optsesscont_tbl->sess_id;

//		printf("isins_tbl->data_size = %ld\n", isins_tbl->data_size);

		int it;
		for(it = 0; it < isins_tbl->data_size; it++)
			isins_tbl->data[it] = NULL;
	}


	{
		struct sesscont_ex_t **data = ss_optsesscont_tbl->data;
		ss_optsesscont_tbl->data = NULL;

		int mltp = (ss_optsesscont_tbl->repl_id.max - ss_optsesscont_tbl->repl_id.min) / SESSCONTSIZE_INCREMENT + 1;
		// Увеличиваем размер массива для хранения заявок
		ss_optsesscont_tbl->data_size = SESSCONTSIZE_INCREMENT * mltp;
		ss_optsesscont_tbl->data = realloc(ss_optsesscont_tbl->data, sizeof(struct sesscont_ex_t *) * ss_optsesscont_tbl->data_size);
		if(ss_optsesscont_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

//		printf("ss_optsesscont_tbl->data_size = %lld\n", ss_optsesscont_tbl->data_size);

		int it;
		for(it = 0; it < ss_optsesscont_tbl->data_size; it++)
			ss_optsesscont_tbl->data[it] = NULL;

		ss_optsesscont_tbl->shift = ss_optsesscont_tbl->repl_id.min;


		int cnt = ss_optsesscont_tbl->cnt;
		for(it = 0; it < cnt; it++){
			signed long long replID = data[it]->origin.replID - ss_optsesscont_tbl->shift;
			ss_optsesscont_tbl->data[replID] = data[it];

			struct sesscont_ex_t *sesscont_ex = data[it];
			signed int isin_id = sesscont_ex->origin.isin_id;
			signed int sess_id = sesscont_ex->origin.sess_id;
			create_isins_tbl_entry(&isin_id, &sess_id);

			struct sess_data_row_t *sess_data = isins_tbl->data[isin_id]->data[sess_id];
			sess_data->cont = sesscont_ex;

			fill_isins_tbl_entry(isins_tbl->data[isin_id], sess_data);
			isins_tbl->ext_links++;
		}
			//*/
		free(data);
	}


#ifndef NO_OPTIONS
	{
		price_to_str_tbl->size_of_price = sizeof(((struct FutAddOrder *)0)->price);

		int it;
		for(it = 0; it < price_to_str_tbl->data_size; it++){
			struct prices_map_tbl_t *prices_map = price_to_str_tbl->data[it];

			if(prices_map){
				prices_map->shift = prices_map->range.min;
				prices_map->data_size = prices_map->range.max - prices_map->range.min + 1;

				prices_map->data = malloc(sizeof(char *) * prices_map->data_size);
				if(prices_map->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

				int jt;
				for(jt = 0; jt < prices_map->data_size; jt++){
					char *price_str = malloc(price_to_str_tbl->size_of_price);
					if(price_str == NULL) exit_memory_alloc_error(__LINE__, __func__);

					i64toa_branchlut_ex(prices_map->shift + jt, price_str, it);
					prices_map->data[jt] = price_str;
				}
			}
		}
	}
#endif


	ordlog_listen = 1;

}
void optinfo_event_online(struct cg_msg_streamdata_t *replmsg)
{

	if(replmsg->msg_index == opt_sess_contents_index){
		signed int affected_isin_id = -1;

		struct opt_sess_contents *sesscont = (struct opt_sess_contents*)replmsg->data;

		char useful = (sesscont->replAct == 0);
		/*
		Критерий полезности этой заявки
		0 - заявка бесполезная
		1 - заявку нужно добавить в систему
		*/

		signed long long replID = sesscont->replID;

		if(useful){
			replID -= optsesscont_tbl->shift;
			if(replID >= optsesscont_tbl->data_size){
				int mltp = (replID - optsesscont_tbl->data_size) / SESSCONTSIZE_INCREMENT + 1;
				// Увеличиваем размер массива для хранения заявок
				optsesscont_tbl->data_size += SESSCONTSIZE_INCREMENT * mltp;
				optsesscont_tbl->data = realloc(optsesscont_tbl->data, sizeof(struct sesscont_ex_t *) * optsesscont_tbl->data_size);
				if(optsesscont_tbl->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

//				printf("optsesscont_tbl->data_size = %lld\n", optsesscont_tbl->data_size);

				int it;
				for(it = optsesscont_tbl->data_size - SESSCONTSIZE_INCREMENT * mltp; it < optsesscont_tbl->data_size; it++)
					optsesscont_tbl->data[it] = NULL;
			}

			if(optsesscont_tbl->data[replID] == NULL){
				optsesscont_tbl->data[replID] = malloc(sizeof(struct sesscont_ex_t));
				struct sesscont_ex_t *sesscont_ex = optsesscont_tbl->data[replID];
				if(sesscont_ex == NULL) exit_memory_alloc_error(__LINE__, __func__);

				memcpy(sesscont_ex, sesscont, replmsg->data_size);
				sesscont_ex->origin.option = 1;

				WARN_FAIL(cg_bcd_get(sesscont->min_step, &sesscont_ex->minstep_int, &sesscont_ex->minstep_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->step_price, &sesscont_ex->step_price_int, &sesscont_ex->step_price_scale), "bcd_get");
				//WARN_FAIL(cg_bcd_get(sesscont->step_price_curr, &sesscont_ex->step_price_curr_int, &sesscont_ex->step_price_curr_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->limit_up, &sesscont_ex->limit_up_int, &sesscont_ex->limit_up_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->limit_down, &sesscont_ex->limit_down_int, &sesscont_ex->limit_down_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->last_cl_quote, &sesscont_ex->last_cl_quote_int, &sesscont_ex->last_cl_quote_scale), "bcd_get");

				sesscont_ex->step_price_curr_int = 0;
				sesscont_ex->step_price_curr_scale = sesscont_ex->minstep_scale;


				if(sesscont->is_limited){
					sesscont_ex->limit_down_int = sesscont_ex->last_cl_quote_int - sesscont_ex->limit_down_int;
					sesscont_ex->limit_up_int = sesscont_ex->last_cl_quote_int + sesscont_ex->limit_up_int;
				}else if(sesscont_ex->origin.option){
					int64_t strike_int;
					int8_t strike_scale;

					WARN_FAIL(cg_bcd_get(sesscont->strike, &strike_int, &strike_scale), "bcd_get");
					strike_int /= 10; // 10% of base asset price change

					sesscont_ex->limit_down_int = sesscont_ex->last_cl_quote_int - strike_int;
					if(sesscont_ex->limit_down_int < 0) sesscont_ex->limit_down_int = 0;
					sesscont_ex->limit_up_int = sesscont_ex->last_cl_quote_int + strike_int;
				}else{
					sesscont_ex->limit_down_int = (sesscont_ex->last_cl_quote_int * 0.9) / sesscont_ex->minstep_int;
					sesscont_ex->limit_down_int *= sesscont_ex->minstep_int;

					sesscont_ex->limit_up_int = (sesscont_ex->last_cl_quote_int * 1.1) / sesscont_ex->minstep_int;
					sesscont_ex->limit_up_int *= sesscont_ex->minstep_int;
				}


				affected_isin_id =  sesscont->isin_id;

				int64_t div = ipow(10, sesscont_ex->minstep_scale - sesscont->roundto);
				sesscont_ex->minstep_adj = sesscont_ex->minstep_int / div;
				sesscont_ex->limit_down_adj = sesscont_ex->limit_down_int / div;
				sesscont_ex->scale_adj = sesscont->roundto;


#ifdef ORDLOG_DEBUG
				printf("NEW OPT_SES_CONT\n");
				if(sesscont_ex->minstep_scale != sesscont_ex->step_price_scale || sesscont_ex->minstep_scale != sesscont_ex->step_price_curr_scale
						 || sesscont_ex->minstep_scale != sesscont_ex->limit_up_scale
						 || sesscont_ex->minstep_scale != sesscont_ex->limit_down_scale){
					printf("minstep_scale = %d, step_price_scale = %d, step_price_curr_scale = %d\n", sesscont_ex->minstep_scale, sesscont_ex->step_price_scale, sesscont_ex->step_price_curr_scale);
					exit_ordlog_debug_error(__LINE__, __func__);
				}
#endif

				optsesscont_tbl->cnt++;


				signed int isin_id = sesscont->isin_id;
				signed int sess_id = sesscont->sess_id;
				create_isins_tbl_entry(&isin_id, &sess_id);

				struct sess_data_row_t *sess_data = isins_tbl->data[isin_id]->data[sess_id];
				isins_tbl->ext_links++;

				sess_data->cont = sesscont_ex;

				fill_isins_tbl_entry(isins_tbl->data[isin_id], sess_data);



			}else{
				struct sesscont_ex_t *sesscont_ex = optsesscont_tbl->data[replID];
				memcpy(sesscont_ex, sesscont, replmsg->data_size);

#ifdef ORDLOG_DEBUG
				printf("UPDATE OPT_SES_CONT\n");
				int64_t minstep_int = sesscont_ex->minstep_int;
				int8_t minstep_scale = sesscont_ex->minstep_scale;

				int64_t step_price_curr_int = sesscont_ex->step_price_curr_int;
				//int8_t step_price_curr_scale = sesscont_ex->step_price_curr_scale;
#endif
				int64_t step_price_int = sesscont_ex->step_price_int;
				//int8_t step_price_scale = sesscont_ex->step_price_scale;

				int64_t limit_up_int = sesscont_ex->limit_up_int;
				//int8_t limit_up_scale = sesscont_ex->limit_up_scale;

				int64_t limit_down_int = sesscont_ex->limit_down_int;
				//int8_t limit_down_scale = sesscont_ex->limit_down_scale;

				WARN_FAIL(cg_bcd_get(sesscont->min_step, &sesscont_ex->minstep_int, &sesscont_ex->minstep_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->step_price, &sesscont_ex->step_price_int, &sesscont_ex->step_price_scale), "bcd_get");
			//	WARN_FAIL(cg_bcd_get(sesscont->step_price_curr, &sesscont_ex->step_price_curr_int, &sesscont_ex->step_price_curr_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->limit_up, &sesscont_ex->limit_up_int, &sesscont_ex->limit_up_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->limit_down, &sesscont_ex->limit_down_int, &sesscont_ex->limit_down_scale), "bcd_get");
				WARN_FAIL(cg_bcd_get(sesscont->last_cl_quote, &sesscont_ex->last_cl_quote_int, &sesscont_ex->last_cl_quote_scale), "bcd_get");

				sesscont_ex->step_price_curr_int = 0;
				sesscont_ex->step_price_curr_scale = sesscont_ex->minstep_scale;



				if(sesscont->is_limited){
					sesscont_ex->limit_down_int = sesscont_ex->last_cl_quote_int - sesscont_ex->limit_down_int;
					sesscont_ex->limit_up_int = sesscont_ex->last_cl_quote_int + sesscont_ex->limit_up_int;
				}else if(sesscont_ex->origin.option){
					int64_t strike_int;
					int8_t strike_scale;

					WARN_FAIL(cg_bcd_get(sesscont->strike, &strike_int, &strike_scale), "bcd_get");
					strike_int /= 10; // 10% of base asset price change

					sesscont_ex->limit_down_int = sesscont_ex->last_cl_quote_int - strike_int;
					if(sesscont_ex->limit_down_int < 0) sesscont_ex->limit_down_int = 0;
					sesscont_ex->limit_up_int = sesscont_ex->last_cl_quote_int + strike_int;
				}else{
					sesscont_ex->limit_down_int = (sesscont_ex->last_cl_quote_int * 0.9) / sesscont_ex->minstep_int;
					sesscont_ex->limit_down_int *= sesscont_ex->minstep_int;

					sesscont_ex->limit_up_int = (sesscont_ex->last_cl_quote_int * 1.1) / sesscont_ex->minstep_int;
					sesscont_ex->limit_up_int *= sesscont_ex->minstep_int;
				}

				if(step_price_int != sesscont_ex->step_price_int)
					affected_isin_id =  sesscont->isin_id;

				if(limit_up_int != sesscont_ex->limit_up_int || limit_down_int != sesscont_ex->limit_down_int){
					signed int isin_id = sesscont->isin_id - isins_tbl->shift;
					signed int sess_id = sesscont->sess_id - isins_tbl->sess_id.min;

					int shift = (sesscont_ex->limit_down_int - limit_down_int) / sesscont_ex->minstep_int;

					limit_up_int = limit_up_int > sesscont_ex->limit_up_int ? limit_up_int : sesscont_ex->limit_up_int;
					limit_down_int = limit_down_int < sesscont_ex->limit_down_int ? limit_down_int : sesscont_ex->limit_down_int;

					int data_size = (limit_up_int - limit_down_int) / sesscont_ex->minstep_int + 1;
					struct price_aggr_t **bid = malloc(sizeof(struct price_aggr_t *) * data_size);
					struct price_aggr_t **ask = malloc(sizeof(struct price_aggr_t *) * data_size);

					struct sess_data_row_t *sess_data = isins_tbl->data[isin_id]->data[sess_id];
					int it;
					for(it = 0; it < data_size; it++){
						if(shift > 0 && shift < sess_data->askEx->data_size){
							bid[it] = sess_data->bidEx->data[shift];
							ask[it] = sess_data->askEx->data[shift];
							shift++;
						}else{
							struct price_aggr_t *bid_aggr = malloc(sizeof(struct price_aggr_t));
							bid_aggr->amount_rest = 0;
//							bid_aggr->lc_amount_rest = 0;
//							bid_aggr->fr_amount_rest = 0;
							bid[it] = bid_aggr;

							struct price_aggr_t *ask_aggr = malloc(sizeof(struct price_aggr_t));
							ask_aggr->amount_rest = 0;
//							ask_aggr->lc_amount_rest = 0;
//							ask_aggr->fr_amount_rest = 0;
							ask[it] = ask_aggr;
						}
					}

					free(sess_data->bidEx->data);
					free(sess_data->askEx->data);

					sess_data->bidEx->data_size = data_size;
					sess_data->bidEx->data = bid;
					sess_data->bidEx->first = 0;

					sess_data->askEx->data_size = data_size;
					sess_data->askEx->data = ask;
					sess_data->askEx->first = 0;
				}

#ifdef ORDLOG_DEBUG
				if(minstep_int != sesscont_ex->minstep_int){
					printf("minstep_int (%ld) != sesscont_ex->minstep_int (%ld)\n", minstep_int, sesscont_ex->minstep_int);
					exit_ordlog_debug_error(__LINE__, __func__);
				}

				if(step_price_curr_int != sesscont_ex->step_price_curr_int){
					printf("step_price_curr_int (%ld) != sesscont_ex->step_price_curr_int (%ld)\n", step_price_curr_int, sesscont_ex->step_price_curr_int);
					exit_ordlog_debug_error(__LINE__, __func__);
				}

				if(minstep_scale != sesscont_ex->minstep_scale
						|| sesscont_ex->minstep_scale != sesscont_ex->step_price_scale
						|| sesscont_ex->minstep_scale != sesscont_ex->step_price_curr_scale
						|| sesscont_ex->minstep_scale != sesscont_ex->limit_up_scale
						|| sesscont_ex->minstep_scale != sesscont_ex->limit_down_scale
						){
					printf("minstep_scale = %d, sess_cont->minstep_scale = %d, sess_cont->step_price_scale = %d, sess_cont->step_price_curr_scale = %d\n", minstep_scale,  sesscont_ex->minstep_scale, sesscont_ex->step_price_scale, sesscont_ex->step_price_curr_scale);
					exit_ordlog_debug_error(__LINE__, __func__);
				}
#endif
			}
		}else{
			replID -= optsesscont_tbl->shift;

#ifdef ORDLOG_DEBUG
			if(replID < 0 || replID >= optsesscont_tbl->data_size){
				printf("replID is out of the range\n");
				exit_ordlog_debug_error(__LINE__, __func__);
			}
#endif

			struct sesscont_ex_t *sesscont_ex = optsesscont_tbl->data[replID];
			destroy_isins_tbl_entry(sesscont_ex->origin.isin_id, sesscont_ex->origin.sess_id);

			free(sesscont_ex);
			optsesscont_tbl->data[replID] = NULL;

			optsesscont_tbl->cnt--;
			isins_tbl->ext_links--;
		}

		if(qnt_affected_tbl->cnt >= qnt_affected_tbl->data_size){
			qnt_affected_tbl->data_size += AFFECTED_ISINS;
			qnt_affected_tbl->data = realloc(qnt_affected_tbl->data, sizeof(signed int) * qnt_affected_tbl->data_size);
			if(qnt_affected_tbl->data == NULL)
				exit_memory_alloc_error(__LINE__, __func__);
		}
		qnt_affected_tbl->data[qnt_affected_tbl->cnt++] = affected_isin_id;
	}


}
void optinfo_event_deleted(struct cg_data_cleardeleted_t *replmsg)
{


	if(replmsg->table_idx == opt_sess_contents_index){
		int it;
		for(it = 0; it < optsesscont_tbl->data_size; it++)
			if(optsesscont_tbl->data[it] && optsesscont_tbl->data[it]->origin.replRev < replmsg->table_rev){
				struct sesscont_ex_t *sesscont_ex = optsesscont_tbl->data[it];
				destroy_isins_tbl_entry(sesscont_ex->origin.isin_id, sesscont_ex->origin.sess_id);

				free(sesscont_ex);
				optsesscont_tbl->data[it] = NULL;

				optsesscont_tbl->cnt--;
				isins_tbl->ext_links--;
			}
	}

}
void optinfo_event_lifenum(void)
{

	int it;

	// Таблица opt_sess_contents
	for(it = 0; it < optsesscont_tbl->data_size; it++)
		if(optsesscont_tbl->data[it]){
			struct sesscont_ex_t *sesscont_ex = optsesscont_tbl->data[it];
			destroy_isins_tbl_entry(sesscont_ex->origin.isin_id, sesscont_ex->origin.sess_id);

			free(sesscont_ex);
			optsesscont_tbl->data[it] = NULL;

			isins_tbl->ext_links--;
		}
	free(optsesscont_tbl->data);
	optsesscont_tbl->data = NULL;
	optsesscont_tbl->data_size = 0;
	optsesscont_tbl->shift = 0;
	optsesscont_tbl->cnt = 0;
}

CG_RESULT repl_event(cg_conn_t* conn, cg_listener_t* listener, struct cg_msg_t* msg, void* data)
{
#if defined(STREAMS_LOG) && !defined(REPLY)
	pthread_mutex_lock(&io_order_mutex);
#endif

	switch (msg->type)
	{
	case CG_MSG_DATA:
	{
		/*
		Сообщение содержит ответ на ранее отосланное состояние. Поле data ука-
		зывает на данные, а поле data_size содержит размер блока данных. Сооб-
		щение описывается структурой cg_msg_data_t и содержит дополнительные
		поля, позволяющие идентифицировать исходное сообщение, а также инфор-
		мацию о схеме данных. Подробнее см. раздел Отправка команд и получение
		ответов.
		*/

		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(TRANS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_data_t), msg->data, msg->data_size));



		//uint32_t* data = (uint32_t*)msg->data;
		/*
		printf("Client received reply [id:%d, data: %d, user-id: %d, name: %s]\n",
			((struct cg_msg_data_t*)msg)->msg_id,
			*((uint32_t*)msg->data),
			((struct cg_msg_data_t*)msg)->user_id,
			((struct cg_msg_data_t*)msg)->msg_name);
		*/

		repl_event_data((struct cg_msg_data_t*)msg);


		//printf("Message CG_MSG_DATA (0x%X)\n", msg->type);
	}
	break;
	case CG_MSG_P2MQ_TIMEOUT:
	{
		/*
		Сообщение приходит в том случае, если ответ на отправленное ранее сооб-
		щение не был получен в течение указанного в соответствующем publisher вре-
		мени. Сообщение описывается структурой cg_msg_data_t и содержит значе-
		ние user_id, задаваемое при отправке исходного сообщения.
		*/

		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(TRANS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_data_t), msg->data, msg->data_size));


		printf("Message CG_MSG_P2MQ_TIMEOUT (0x%X)\n", msg->type);
		repl_event_timeout((struct cg_msg_data_t*)msg);
	}
	break;
	case CG_MSG_CLOSE:
	{
		/*
		Сообщение приходит в момент закрытия потока данных. Приход сообщения
		означает, что поток был закрыт пользователем или системой. В поле data со-
		держится указатель на int, по указанному адресу хранится информация о при-
		чине закрытия подписчика. Возможны следующие причины:
		• CG_REASON_UNDEFINED - не определена.
		• CG_REASON_USER - пользователь вернул ошибку в callback слушателя.
		• CG_REASON_ERROR - внутренняя ошибка.
		• CG_REASON_DONE - вызван метод cg_lsn_destroy.
		• CG_REASON_SNAPSHOT_DONE - снэпшот получен.
		*/

		STREAMLOG_FUNC(thread_func_logger_save_immidiete_duos(TRANS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t), msg->data, msg->data_size));

		printf("Message CG_MSG_CLOSE (0x%X)\n", msg->type);
	}
	break;
	case CG_MSG_OPEN:
	{
		/*
		Сообщение приходит в момент активации потока данных. Это событие гаран-
		тированно возникает до прихода каких либо данных по данной подписке. Для
		потоков репликации приход сообщения означает, что схема данных согласо-
		вана и готова для использования (Подробнее см. Схемы данных) Данное со-
		общение не содержит дополнительных данных и его поля data и data_size не
		используются
		*/

		STREAMLOG_FUNC(thread_func_logger_save_immidiete(TRANS_STREAM_LOG_ID, msg, sizeof(struct cg_msg_t)));

		printf("Message CG_MSG_OPEN (0x%X)\n", msg->type);
	}
	break;
	default:
		printf("Message 0x%X\n", msg->type);
	}

#if defined(STREAMS_LOG) && !defined(REPLY)
	pthread_mutex_unlock(&io_order_mutex);
#endif

	return CG_ERR_OK;
}

void repl_event_timeout(struct cg_msg_data_t* replmsg)
{
	struct trans_t *trans = trans_tbl->data[replmsg->user_id];
	struct active_order_t *active = trans->active_order;

	switch(active->state){
	case ORDER_ADDING:
	{
		if(*trans->p2msg->type == 1){
			if(*trans->p2msg->dir == 1) active->quote->portfolio->active_bids--;
			else  active->quote->portfolio->active_asks--;
		}

		active->quote->isin_data->msg_data_last->next = trans->p2msg;
		active->quote->isin_data->msg_data_last = trans->p2msg;
		trans->p2msg->next = NULL;
		trans->p2msg = NULL;

		orders_pool_tbl->last->next = active;
		orders_pool_tbl->last = active;
		trans->active_order = NULL;
	}
	break;
	case ORDER_MOVING:
	{
		if(active->option){
			optmovemsgs_tbl->msg_data_last->next = trans->p2msg;
			optmovemsgs_tbl->msg_data_last = trans->p2msg;
			trans->p2msg->next = NULL;
			trans->p2msg = NULL;
		}else{
			futmovemsgs_tbl->msg_data_last->next = trans->p2msg;
			futmovemsgs_tbl->msg_data_last = trans->p2msg;
			trans->p2msg->next = NULL;
			trans->p2msg = NULL;
		}

		active->state = ORDER_ACTIVE;
	}
	break;
	case ORDER_KILLING:
	{
		if(active->option){
			optdelmsgs_tbl->msg_data_last->next = trans->p2msg;
			optdelmsgs_tbl->msg_data_last = trans->p2msg;
			trans->p2msg->next = NULL;
			trans->p2msg = NULL;
		}else{
			futdelmsgs_tbl->msg_data_last->next = trans->p2msg;
			futdelmsgs_tbl->msg_data_last = trans->p2msg;
			trans->p2msg->next = NULL;
			trans->p2msg = NULL;
		}

		active->state = ORDER_ACTIVE;
	}
	break;
	}
}

void repl_event_data(struct cg_msg_data_t* replmsg)
{
//	struct trans_t *trans = trans_tbl->data[replmsg->user_id];
	long long int rtt = 0;



	switch(replmsg->msg_id){
	case 100:
		{
			struct FORTS_MSG100 *msg = (struct FORTS_MSG100 *)replmsg->data;

			if(msg->code == 10006){
				struct trans_t *trans = trans_tbl->data[replmsg->user_id];
#ifdef RTT_STAT
				struct timespec curr_tm;
				clock_gettime (CLOCK_MONOTONIC, &curr_tm);
				rtt = (curr_tm.tv_sec - trans->ordlog_listener_TN_COMMIT.tv_sec) * 1000000000L + (curr_tm.tv_nsec - trans->ordlog_listener_TN_COMMIT.tv_nsec);
#endif


				struct active_order_t *active = trans->active_order;
				struct cg_msg_data_t *p2msg = trans->p2msg->origin;

				switch(active->state){
				case ORDER_ADDING:
				{
					if(*trans->p2msg->type == 1){
						if(*trans->p2msg->dir == 1) active->quote->portfolio->active_bids--;
						else  active->quote->portfolio->active_asks--;
					}


					struct OptAddOrder *ord =  (struct OptAddOrder *)p2msg->data;

					TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. AddOrder (FAIL): Isin = %s, Price = %s, Amount = %d, Dir = %d, CCode = %s, rtt = %lld\n", replmsg->user_id, ord->isin, ord->price, ord->amount, ord->dir, ord->client_code, rtt));

					active->quote->isin_data->msg_data_last->next = trans->p2msg;
					active->quote->isin_data->msg_data_last = trans->p2msg;
					trans->p2msg->next = NULL;
					trans->p2msg = NULL;

					orders_pool_tbl->last->next = active;
					orders_pool_tbl->last = active;
					trans->active_order = NULL;
				}
				break;
				case ORDER_MOVING:
				{
					struct OptMoveOrder *ord =  (struct OptMoveOrder *)p2msg->data;

					TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. MoveOrder (FAIL): OrderId = %lld, Price = %s, Amount = %d, rtt = %lld\n", replmsg->user_id, ord->order_id1, ord->price1, ord->amount1, rtt));

					if(active->option){
						optmovemsgs_tbl->msg_data_last->next = trans->p2msg;
						optmovemsgs_tbl->msg_data_last = trans->p2msg;
						trans->p2msg->next = NULL;
						trans->p2msg = NULL;
					}else{
						futmovemsgs_tbl->msg_data_last->next = trans->p2msg;
						futmovemsgs_tbl->msg_data_last = trans->p2msg;
						trans->p2msg->next = NULL;
						trans->p2msg = NULL;
					}

					active->state = ORDER_ACTIVE;
				}
				break;
				case ORDER_KILLING:
				{
					struct OptDelOrder *ord =  (struct OptDelOrder *)p2msg->data;
					TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. DelOrder (FAIL): OrderId = %lld, rtt = %lld\n", replmsg->user_id, ord->order_id, rtt));


					if(active->option){
						optdelmsgs_tbl->msg_data_last->next = trans->p2msg;
						optdelmsgs_tbl->msg_data_last = trans->p2msg;
						trans->p2msg->next = NULL;
						trans->p2msg = NULL;
					}else{
						futdelmsgs_tbl->msg_data_last->next = trans->p2msg;
						futdelmsgs_tbl->msg_data_last = trans->p2msg;
						trans->p2msg->next = NULL;
						trans->p2msg = NULL;
					}

					active->state = ORDER_ACTIVE;
				}
				break;
				}
			}else{
				TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. Order (FAIL): code = %d, message = %s, rtt = %lld\n", replmsg->user_id, msg->code, msg->message, rtt));
			}
		}
		break;
	case 109:
	case 101:
	{
		//FutAddOrder, OptAddOrder
		struct FORTS_MSG101 *msg = (struct FORTS_MSG101 *)replmsg->data;
		struct trans_t *trans = trans_tbl->data[replmsg->user_id];

#ifdef RTT_STAT
		rtt = (trans->sent_moment.tv_sec - trans->ordlog_listener_TN_COMMIT.tv_sec) * 1000000000L + (trans->sent_moment.tv_nsec - trans->ordlog_listener_TN_COMMIT.tv_nsec);
		if(rtt > trans_tbl->commit_rtt.max) trans_tbl->commit_rtt.max = rtt;
		else if(rtt < trans_tbl->commit_rtt.min) trans_tbl->commit_rtt.min = rtt;
		trans_tbl->commit_rtt.avg += rtt;
		trans_tbl->commit_rtt.cnt++;

		rtt = (trans->sent_moment.tv_sec - trans->ordlog_listener_TN_BEGIN.tv_sec) * 1000000000L + (trans->sent_moment.tv_nsec - trans->ordlog_listener_TN_BEGIN.tv_nsec);
		if(rtt > trans_tbl->begin_rtt.max) trans_tbl->begin_rtt.max = rtt;
		else if(rtt < trans_tbl->begin_rtt.min) trans_tbl->begin_rtt.min = rtt;
		trans_tbl->begin_rtt.avg += rtt;
		trans_tbl->begin_rtt.cnt++;


		struct timespec curr_tm;
		clock_gettime (CLOCK_MONOTONIC, &curr_tm);


		rtt = (curr_tm.tv_sec - trans->ordlog_listener_TN_COMMIT.tv_sec) * 1000000000L + (curr_tm.tv_nsec - trans->ordlog_listener_TN_COMMIT.tv_nsec);
		if(rtt > trans_tbl->sent_rtt.max) trans_tbl->sent_rtt.max = rtt;
		else if(rtt < trans_tbl->sent_rtt.min) trans_tbl->sent_rtt.min = rtt;
		trans_tbl->sent_rtt.avg += rtt;
		trans_tbl->sent_rtt.cnt++;

#endif

		struct active_order_t *active = trans->active_order;


		if(msg->code == 0){
			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. AddOrder (SUCSESS). order_id = %lld, rtt = %lld\n", replmsg->user_id, msg->order_id, rtt));

			struct portfolio_t *portfolio = trans->active_order->quote->portfolio;

			switch(*trans->p2msg->type){
			case 1:{
				active->prev = NULL;
				active->next = NULL;
				active->order_id = msg->order_id;

				active->option = trans->p2msg->option;
				active->amount = *trans->p2msg->amount;
				active->amount_rest = active->amount;
				active->dir = *trans->p2msg->dir;

				trans_thr_approved_orders_tbl->data[trans_thr_approved_orders_tbl->cnt++] = active;
			}break;
			case 2:{
				if(*trans->p2msg->dir == 1){

					if(portfolio->amount >= 0){
						portfolio->sell_at = portfolio->wbid;
						portfolio->buy_at += portfolio->shift;
					}else{
						portfolio->sell_at += portfolio->shift;

						portfolio->sl_cnt++;
					}
					portfolio->active_bids--;
					if(portfolio->active_bids == 0)
						portfolio->best_buy = LLONG_MIN;
					/*
					portfolio->active_bids--;
					portfolio->sell_at = portfolio->wbid;
					//portfolio->buy_at = portfolio->sell_at - portfolio->spread;
					portfolio->buy_at += portfolio->shift;
					if(portfolio->amount <= 0) portfolio->sl_cnt++;
					*/
				}else{

					if(portfolio->amount <= 0){
						portfolio->buy_at = portfolio->wask;
						portfolio->sell_at -= portfolio->shift;
					}else{
						portfolio->buy_at -= portfolio->shift;

						portfolio->sl_cnt++;
					}
					portfolio->active_asks--;
					if(portfolio->active_asks == 0)
						portfolio->best_sell = LLONG_MAX;

					/*
					portfolio->active_asks--;
					portfolio->buy_at = portfolio->wask;
					//portfolio->sell_at = portfolio->buy_at + portfolio->spread;
					portfolio->sell_at -= portfolio->shift;
					if(portfolio->amount >= 0) portfolio->sl_cnt++;
					*/
				}

				active->prev = NULL;
				active->next = NULL;
				active->order_id = msg->order_id;

				active->option = trans->p2msg->option;
				active->amount = *trans->p2msg->amount;
				active->amount_rest = active->amount;
				active->dir = *trans->p2msg->dir;

				trans_thr_approved_orders_tbl->data[trans_thr_approved_orders_tbl->cnt++] = active;

			}break;
			case 3:{
				if(*trans->p2msg->dir == 1){
					portfolio->amount += *trans->p2msg->amount;
					portfolio->active_bids--;
					portfolio->sell_at = portfolio->wbid;
					portfolio->buy_at = portfolio->sell_at - portfolio->spread;

					if(portfolio->amount <= 0) portfolio->sl_cnt++;
				}else{
					portfolio->amount -= *trans->p2msg->amount;
					portfolio->active_asks--;
					portfolio->buy_at = portfolio->wask;
					portfolio->sell_at = portfolio->buy_at + portfolio->spread;
					if(portfolio->amount >= 0) portfolio->sl_cnt++;
				}
				RELEASE_ACTIVE_ORDER
			}break;
			}

			/*
			if(*trans->p2msg->type == 3 || *trans->p2msg->type == 2){
				if(*trans->p2msg->dir == 1){
					portfolio->amount += *trans->p2msg->amount;
					portfolio->active_bids--;
					portfolio->sell_at = portfolio->wbid;
					portfolio->buy_at = portfolio->sell_at - portfolio->spread;

					if(portfolio->amount <= 0) portfolio->sl_cnt++;
				}else{
					portfolio->amount -= *trans->p2msg->amount;
					portfolio->active_asks--;
					portfolio->buy_at = portfolio->wask;
					portfolio->sell_at = portfolio->buy_at + portfolio->spread;
					if(portfolio->amount >= 0) portfolio->sl_cnt++;
				}
			}


			if(*trans->p2msg->type == 1){
				active->prev = NULL;
				active->next = NULL;
				active->order_id = msg->order_id;

				active->option = trans->p2msg->option;
				active->amount = *trans->p2msg->amount;
				active->amount_rest = active->amount;
				active->dir = *trans->p2msg->dir;

				trans_thr_approved_orders_tbl->data[trans_thr_approved_orders_tbl->cnt++] = active;
			}else{
				RELEASE_ACTIVE_ORDER
			}
			*/

			RELEASE_ADD_TRANS
		}else{

			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. AddOrder (FAIL). code = %d, message = %s, rtt = %lld\n", replmsg->user_id, msg->code, msg->message, rtt));

			struct portfolio_t *portfolio = trans->active_order->quote->portfolio;


			switch(msg->code){
			case 4103:{													// Partial FOK trade
				if(*trans->p2msg->dir == 1){
					portfolio->active_bids--;
					//printf("P(%d), wbid = %ld, wask = %ld\n", portfolio->portfolio_id, portfolio->wbid, portfolio->wask);
					//portfolio->sell_at -= portfolio->shift;
					//portfolio->buy_at -= portfolio->shift;
				}else{
					portfolio->active_asks--;
					//printf("P(%d), wbid = %ld, wask = %ld\n", portfolio->portfolio_id, portfolio->wbid, portfolio->wask);
					//portfolio->sell_at += portfolio->shift;
					//portfolio->buy_at += portfolio->shift;
				}

				RELEASE_ACTIVE_ORDER
				RELEASE_ADD_TRANS
			}break;
			case 3: 													// Now this session is not running
			case 4:{													// Session suspended
				trans->ttype = ADDORDER_TTYPE;
				susp_trans_tbl->data[susp_trans_tbl->data_size++] = replmsg->user_id;
				susp_trans_tbl->release = 0;
			}break;
			default:{
				if(*trans->p2msg->dir == 1){
					portfolio->active_bids--;
					//portfolio->need_buy_amt++;
					if(portfolio->work_type == PORTFOLIOWORKTYPE_PATTERN) portfolio->killed_buy_amt++;
				}
				else{
					portfolio->active_asks--;
					//portfolio->need_sell_amt++;
					if(portfolio->work_type == PORTFOLIOWORKTYPE_PATTERN) portfolio->killed_sell_amt++;
				}


				RELEASE_ACTIVE_ORDER
				RELEASE_ADD_TRANS
			}break;
			}
		}
	}
	break;
	case 113:
	case 105:
	{
		//FutMoveOrders, OptMoveOrders
		struct FORTS_MSG105 *msg = (struct FORTS_MSG105 *)replmsg->data;
		struct trans_t *trans = trans_tbl->data[replmsg->user_id];

#ifdef RTT_STAT
		struct timespec curr_tm;
		clock_gettime (CLOCK_MONOTONIC, &curr_tm);
		rtt = (curr_tm.tv_sec - trans->ordlog_listener_TN_COMMIT.tv_sec) * 1000000000L + (curr_tm.tv_nsec - trans->ordlog_listener_TN_COMMIT.tv_nsec);
#endif

		struct active_order_t *active = trans->active_order;

		switch(msg->code)
		{
		case 0:
		{
			if(msg->order_id1){
				trans->active_order->order_id = msg->order_id1;
				//trans->active_order->state = ORDER_ACTIVE;
			}
			if(msg->order_id2){
				trans->active_order2->order_id = msg->order_id2;
				//trans->active_order2->state = ORDER_ACTIVE;
			}

			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. MoveOrders (SUCSESS). order_id1 = %lld, order_id2 = %lld, rtt = %lld\n", replmsg->user_id, msg->order_id1, msg->order_id2, rtt));

			RELEASE_MOVE_TRANS
		}
		break;

		case 3: 													// Now this session is not running
		case 4:{													// Session suspended

			trans->ttype = MOVEORDER_TTYPE;
			susp_trans_tbl->data[susp_trans_tbl->data_size++] = replmsg->user_id;
			susp_trans_tbl->release = 0;
		}break;

		case 50:
		{
			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. MoveOrders (FAIL). code = %d, message = %s, rtt = %lld\n", replmsg->user_id, msg->code, msg->message, rtt));
			//active->state = MOVE_NOT_FOUND;
			trans_thr_approved_orders_tbl->data[trans_thr_approved_orders_tbl->cnt++] = active;

			RELEASE_MOVE_TRANS
		}
		break;

		default:
		{
			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. MoveOrders (FAIL). code = %d, message = %s, rtt = %lld\n", replmsg->user_id, msg->code, msg->message, rtt));
			RELEASE_MOVE_TRANS
		}
		break;
		}
	}
	break;
	case 110:
	case 102:
	{
		//FutDelOrder, OptDelOrder

		struct FORTS_MSG102 *msg = (struct FORTS_MSG102 *)replmsg->data;
		struct trans_t *trans = trans_tbl->data[replmsg->user_id];
#ifdef RTT_STAT
		struct timespec curr_tm;
		clock_gettime (CLOCK_MONOTONIC, &curr_tm);
		rtt = (curr_tm.tv_sec - trans->ordlog_listener_TN_COMMIT.tv_sec) * 1000000000L + (curr_tm.tv_nsec - trans->ordlog_listener_TN_COMMIT.tv_nsec);
#endif

		struct active_order_t *active = trans->active_order;
		//struct portfolio_t *portfolio = active->quote->portfolio;

		switch(msg->code)
		{
		case 0:{
			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. DelOrder (SUCSESS). residual = %d, rtt = %lld\n", replmsg->user_id, msg->amount, rtt));
			RELEASE_DEL_TRANS
		}break;

		case 3: 													// Now this session is not running
		case 4:{													// Session suspended

			trans->ttype = DELORDER_TTYPE;
			susp_trans_tbl->data[susp_trans_tbl->data_size++] = replmsg->user_id;
			susp_trans_tbl->release = 0;
		}break;

		case 14:
		{
			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. DelOrder (FAIL). code = %d, residual = %d, rtt = %lld\n", replmsg->user_id, msg->code, msg->amount, rtt));

			//active->state = KILL_NOT_FOUND;
			trans_thr_approved_orders_tbl->data[trans_thr_approved_orders_tbl->cnt++] = active;
			RELEASE_DEL_TRANS
		}break;

		default:
		{
			TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. DelOrder (FAIL). code = %d, message = %s, rtt = %lld\n", replmsg->user_id, msg->code, msg->message, rtt));
			RELEASE_DEL_TRANS
		}break;
		}
	}
	break;
	case 103:
	{
		struct FORTS_MSG103 *msg = (struct FORTS_MSG103 *)replmsg->data;
		alive += replmsg->user_id;

		TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "%s. Num removed orders = %d, rtt = %lld\n", msg->message, msg->num_orders, rtt));
	}
	break;
	case 111:
	{
		struct FORTS_MSG111 *msg = (struct FORTS_MSG111 *)replmsg->data;
		alive += replmsg->user_id;
		TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "%s. Num removed orders = %d, rtt = %lld\n", msg->message, msg->num_orders, rtt));
	}
	break;
	case 99:
	{
		struct FORTS_MSG99 *msg = (struct FORTS_MSG99 *)replmsg->data;

		flood_trans_tbl->data[flood_trans_tbl->data_size++] = replmsg->user_id;

		TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "TR=%d. Queue = %d, PenaltyRemain = %d. Message = %s, rtt = %lld\n", replmsg->user_id, msg->queue_size, msg->penalty_remain, msg->message, rtt));
	}
	break;
	default:
	{
		TRANSLOG_FUNC(logsaver(TRANS_LOG_ID, "UNKNOWN!!!: msg_id = %d, rtt = %lld\n", replmsg->msg_id, rtt));
	}
	break;
	}

	TRANSLOG_FUNC(thread_func_logger_flush(TRANS_LOG_ID));
}

void load_portfolios(void)
{
	config_t cfg;
	config_init(&cfg);

	/* Read the file. If there is an error, report it and exit. */
	if(!config_read_file(&cfg, "settings.cfg"))
	{
		fprintf(stderr, "%s:%d - %s\n", config_error_file(&cfg),
		config_error_line(&cfg), config_error_text(&cfg));
		config_destroy(&cfg);
		return;
	}

	int quote_id = 0;
	int portfolio_id = 0;

	struct quote_t *quote;
	struct portfolio_t *portfolio;
	struct quotes_tbl_t *quotes;

	const char *ccode;
	const char *port;


	if(config_lookup_string(&cfg, "ccode", &ccode))
		strncpy(kern_settings.ccode, ccode, 3);


	if(config_lookup_string(&cfg, "port", &port)){
		int len = strlen(port);
		strncpy(kern_settings.port, port, len);
		strncpy(&cfg_ordlog_conn[18], port, len);
		strncpy(&cfg_p2msg_conn[18], port, len);
	}


//	printf("ccode = %s\n", kern_settings.ccode);

	config_setting_t *setting = config_lookup(&cfg, "portfolios");
	if(setting != NULL)
	{
		int portfolios_cnt = config_setting_length(setting);

		int it;
		for(it = 0; it < portfolios_cnt; ++it)
		{
			config_setting_t *portfolio_cs = config_setting_get_elem(setting, it);

			long long int buy_at = 0;
			long long int sell_at = 0;
			int work_type = 0;
			int amount = 0;
			int divisor = 1;
			long long int coeff = 0;
			int shift = 1;
			int type_a_cnt = 0;
			int type_b_cnt = 0;
			int type_c_cnt = 0;
			int self_type = 0;
			int type_id = 0;
			int sl_cnt = 0;
			int mkt_spread = 0;
			int trade = 0;
			int buy = 0;
			int sell = 0;
			int trd_amt = 1;
			int trd_limit = 0;
			//int iceberg_cnt = 0;
			int iceberg_limit = 1;
			int hedge_portfolio_id = -1;
			int hedge_mltp = 1;

			config_setting_lookup_int64(portfolio_cs, "buy_at", &buy_at);
			config_setting_lookup_int64(portfolio_cs, "sell_at", &sell_at);
			config_setting_lookup_int(portfolio_cs, "work_type", &work_type);
			config_setting_lookup_int(portfolio_cs, "amount", &amount);
			config_setting_lookup_int64(portfolio_cs, "coeff", &coeff);
			config_setting_lookup_int(portfolio_cs, "divisor", &divisor);
			config_setting_lookup_int(portfolio_cs, "shift", &shift);
			config_setting_lookup_int(portfolio_cs, "type_a_cnt", &type_a_cnt);
			config_setting_lookup_int(portfolio_cs, "type_b_cnt", &type_b_cnt);
			config_setting_lookup_int(portfolio_cs, "type_c_cnt", &type_c_cnt);
			config_setting_lookup_int(portfolio_cs, "self_type", &self_type);
			config_setting_lookup_int(portfolio_cs, "type_id", &type_id);
			config_setting_lookup_int(portfolio_cs, "sl_cnt", &sl_cnt);
			config_setting_lookup_int(portfolio_cs, "mkt_spread", &mkt_spread);
			config_setting_lookup_int(portfolio_cs, "trade", &trade);
			config_setting_lookup_int(portfolio_cs, "buy", &buy);
			config_setting_lookup_int(portfolio_cs, "sell", &sell);
			config_setting_lookup_int(portfolio_cs, "trd_amt", &trd_amt);
			config_setting_lookup_int(portfolio_cs, "trd_limit", &trd_limit);
			//config_setting_lookup_int(portfolio_cs, "iceberg_cnt", &iceberg_cnt);
			config_setting_lookup_int(portfolio_cs, "iceberg_limit", &iceberg_limit);
			config_setting_lookup_int(portfolio_cs, "hedge_portfolio_id", &hedge_portfolio_id);
			config_setting_lookup_int(portfolio_cs, "hedge_mltp", &hedge_mltp);

			config_setting_t *q_setting = config_setting_get_member(portfolio_cs, "quotes");
			if(q_setting){
				int quotes_cnt = config_setting_length(q_setting);

				portfolio = load_portfolios_new_portfolio(portfolio_id, divisor, coeff, quotes_cnt);
				portfolio->work_type = work_type;
				portfolio->buy_at = buy_at;
				portfolio->sell_at = sell_at;
				portfolio->amount = amount;
				portfolio->shift = shift;
				portfolio->type_a_cnt = type_a_cnt;
				portfolio->type_b_cnt = type_b_cnt;
				portfolio->type_c_cnt = type_c_cnt;
				portfolio->self_type = self_type;
				portfolio->type_id = type_id;
				portfolio->sl_cnt = sl_cnt;
				portfolio->mkt_spread = mkt_spread;
				portfolio->trade = trade;
				portfolio->buy = buy;
				portfolio->sell = sell;
				portfolio->trd_amt = trd_amt;
				portfolio->trd_limit = trd_limit;
				if(trd_limit) portfolio->iceberg_cnt = amount / trd_limit;
				portfolio->iceberg_limit = iceberg_limit;
				portfolio->hedge_portfolio_id = hedge_portfolio_id;
				portfolio->hedge_mltp = hedge_mltp;

				quotes = portfolio->quotes_tbl;

				int jt;
				for(jt = 0; jt < quotes_cnt; ++jt){
					config_setting_t *quote_cs = config_setting_get_elem(q_setting, jt);

					int isin_id = 0;
					int is_left = 1;
					int weight = 1;
					int is_quanto = 0;

					config_setting_lookup_int(quote_cs, "isin_id", &isin_id);
					config_setting_lookup_int(quote_cs, "is_left", &is_left);
					config_setting_lookup_int(quote_cs, "weight", &weight);
					config_setting_lookup_int(quote_cs, "is_quanto", &is_quanto);

					quote = load_portfolios_new_quote(portfolio, quote_id++, isin_id, is_left, weight, is_quanto);
					quotes->data[quotes->cnt++] = quote;
					quotes_tbl->data[quote->quote_id] = quote;
					quotes_tbl->cnt++;
				}

				portfolios_tbl->data[portfolio_id++] = portfolio;
				portfolios_tbl->cnt++;
			}
		}
	}
	config_destroy(&cfg);

	int it;
	for(it = 0; it < portfolios_tbl->cnt; it++){
		struct portfolio_t *portfolio = portfolios_tbl->data[it];
		printf("P(%d, %d). buy_at = %ld, sell_at = %ld, sl_cnt = %d\n", portfolio->portfolio_id, portfolio->amount, portfolio->buy_at, portfolio->sell_at, portfolio->sl_cnt);

		if(portfolio->hedge_portfolio_id >= 0 && portfolio->hedge_portfolio_id <  portfolios_tbl->cnt){
			portfolio->hedge_portfolio = portfolios_tbl->data[portfolio->hedge_portfolio_id];
			portfolio->hedge_portfolio->trade = 0;
		}

#ifdef UNITTEST
		portfolio->amount = 0;
		portfolio->iceberg_cnt = 0;
		portfolio->sl_cnt = 0;
#endif

	}
}


struct quote_t *load_portfolios_new_quote(struct portfolio_t *portfolio, int quote_id, signed int isin_id, char is_left, int weight, char is_quanto)
{
	struct quote_t *quote = malloc(sizeof(struct quote_t));
	if(quote == NULL) exit_memory_alloc_error(__LINE__, __func__);

	quote->quote_id = quote_id;
	quote->portfolio_id = portfolio->portfolio_id;
	quote->isin_id = isin_id;
	quote->weight = weight;
	quote->is_quanto = is_quanto;
	quote->is_left = is_left;
	quote->wbid = 0;
	quote->wask = 0;
	quote->bid_tgt = 0;
	quote->ask_tgt = 0;
	quote->next = NULL;
	quote->portfolio = portfolio;
	quote->price_scale = 0;
	quote->ptv_scale = 0;
	quote->pt_value = 0;
	quote->qnt_type = 0;

	return quote;
}

struct portfolio_t *load_portfolios_new_portfolio(int portfolio_id, int divisor, int coeff, int quotes_num)
{
	struct portfolio_t *portfolio = malloc(sizeof(struct portfolio_t));
	if(portfolio == NULL) exit_memory_alloc_error(__LINE__, __func__);

	struct quotes_tbl_t *quotes = malloc(sizeof(struct quotes_tbl_t));
	if(quotes == NULL) exit_memory_alloc_error(__LINE__, __func__);
	quotes->data_size = quotes_num;
	quotes->data = malloc(sizeof(struct quote_t *) * quotes->data_size);
	if(quotes->data == NULL) exit_memory_alloc_error(__LINE__, __func__);

	quotes->cnt = 0;

	int it;
	for(it = 0; it < quotes->data_size; it++)quotes->data[it] = NULL;


	portfolio->quotes_tbl = quotes;
	portfolio->zero_wasks_num = quotes_num;
	portfolio->zero_wbids_num = quotes_num;
	portfolio->coeff = coeff;
	portfolio->divisor = divisor;
	portfolio->portfolio_id = portfolio_id;
	portfolio->best_buy_trd = LLONG_MAX;
	portfolio->best_sell_trd = LLONG_MIN;
	portfolio->state = WAIT_STATE;
	portfolio->not_affected = 1;

	portfolio->wbid = 0;
	portfolio->wask = 0;
	portfolio->lbid = 0;
	portfolio->lask = 0;
	portfolio->rbid = 0;
	portfolio->rask = 0;
	portfolio->best_buy = LLONG_MIN;
	portfolio->best_sell = LLONG_MAX;
	portfolio->buy_at = 0;
	portfolio->sell_at = 0;
	portfolio->sl_cnt = 0;
	portfolio->trd_amt = 0;
	portfolio->trd_limit = 0;

	portfolio->used = 0;
	portfolio->sl = 0;
	portfolio->sl_dir = 0;
	portfolio->sl_order = 0;
	portfolio->same_inn = 0;
	portfolio->same_inn_ch = 0;
	portfolio->same_inn_dir = 0;
	portfolio->active_bids = 0;
	portfolio->active_asks = 0;
	portfolio->need_buy_amt = 0;
	portfolio->need_sell_amt = 0;
	portfolio->killed_buy_amt = 0;
	portfolio->killed_sell_amt = 0;
	portfolio->amount = 0;
	portfolio->active_orders_tbl = NULL;
	portfolio->work_type = 0;
	portfolio->scale = 0;
	portfolio->spread = 0;
	portfolio->shift = 0;

	portfolio->type_a_cnt = 0;
	portfolio->type_b_cnt = 0;
	portfolio->type_c_cnt = 0;
	portfolio->self_type = 0;
	portfolio->type_id = 0;

	portfolio->mkt_spread = 0;
	portfolio->trades = NULL;
	portfolio->trade = 0;
	portfolio->buy = 0;
	portfolio->sell = 0;

	portfolio->hedge_portfolio_id = -1;
	portfolio->hedge_portfolio = NULL;
	portfolio->hedge_mltp = 1;

#ifdef RTT_STAT
	portfolio->ordlog_listener_TN_COMMIT.tv_nsec = 0;
	portfolio->ordlog_listener_TN_COMMIT.tv_sec = 0;
#endif

#ifdef UNITTEST
	portfolio->ut_volume = 0;
#endif

//////
/*
	portfolio->portfolio_id = portfolio_id;
	portfolio->quotes_tbl = quotes;
	portfolio->coeff = coeff;
	portfolio->best_buy_trd = LLONG_MAX;
	portfolio->best_sell_trd = LLONG_MIN;
	portfolio->divisor = divisor;
	portfolio->zero_wbids_num = quotes_num;
	portfolio->zero_wasks_num = quotes_num;

	portfolio->wbid = 0;
	portfolio->wask = 0;
	portfolio->lbid = 0;
	portfolio->lask = 0;
	portfolio->rbid = 0;
	portfolio->rask = 0;
	portfolio->best_buy = 0;
	portfolio->best_sell = 0;
	portfolio->buy_at = 0;
	portfolio->sell_at = 0;
	portfolio->sl_cnt = 0;
	portfolio->trd_amt = 0;
	portfolio->used = 0;
	portfolio->sl = 0;
	portfolio->sl_dir = 0;
	portfolio->sl_order = 0;
	portfolio->same_inn = 0;
	portfolio->same_inn_ch = 0;
	portfolio->same_inn_dir = 0;
	portfolio->active_bids = 0;
	portfolio->active_asks = 0;
	portfolio->need_buy_amt = 0;
	portfolio->need_sell_amt = 0;
	portfolio->killed_buy_amt = 0;
	portfolio->killed_sell_amt = 0;
	portfolio->amount = 0;
	portfolio->active_orders_tbl = NULL;
	portfolio->work_type = 0;
	portfolio->state = 0;
	portfolio->scale = 0;
	portfolio->spread = 0;
	portfolio->shift = 0;
	portfolio->not_affected = 0;
*/

/////


	return portfolio;
}

void integrate_portfolios(void)
{
	integr_state_tmp = 2;

	signed int futsess_id = -1;
	signed int optsess_id = -1;

	if(session_tbl->current){
		futsess_id = session_tbl->current->origin.sess_id - isins_tbl->sess_id.min;
		optsess_id = session_tbl->current->origin.opt_sess_id - isins_tbl->sess_id.min;
	}

	int it;
	for(it = 0; it < quotes_tbl->data_size; it++){

		struct quote_t *new_quote = quotes_tbl->data[it];
		if(new_quote == NULL) continue;


		signed int isin_id = quotes_tbl->data[it]->isin_id - isins_tbl->shift;

		if(isin_id < 0 && isin_id >= isins_tbl->data_size) continue;

		struct isin_data_tbl_t *sess_data = isins_tbl->data[isin_id];

		if(sess_data){

			if(futsess_id >= 0){
				struct sess_data_row_t *sess = sess_data->data[futsess_id];
				if(sess == NULL)sess = sess_data->data[optsess_id];
				if(sess) ordlog_event_commit_quote_ptv_change(new_quote, sess->cont);
			}


			if(sess_data->quotes_tbl){
				struct quote_t *quote = sess_data->quotes_tbl->data[0];


				if(abs(new_quote->weight) < abs(quote->weight)){
					sess_data->quotes_tbl->data[0] = new_quote;
					new_quote->next = quote;
				}else{
					struct quote_t *prev = quote;
					quote = quote->next;
					while(quote && abs(new_quote->weight) > abs(quote->weight)) quote = quote->next;

					prev->next = new_quote;
					new_quote->next = quote;
				}

				sess_data->quotes_tbl->cnt++;

				if(new_quote->is_quanto)sess_data->quotes_tbl->qnt_cnt++;
			}else{

				sess_data->quotes_tbl = malloc(sizeof(struct quotes_tbl_t));
				sess_data->quotes_tbl->data = malloc(sizeof(struct quote_t*));
				sess_data->quotes_tbl->cnt = 1;
				sess_data->quotes_tbl->data_size = 1;
				sess_data->quotes_tbl->qnt_cnt = 0;
				sess_data->quotes_tbl->data[0] = new_quote;

				if(new_quote->is_quanto)sess_data->quotes_tbl->qnt_cnt++;
			}

		}else{
		}


	}



	for(it = 0; it < quotes_tbl->data_size; it++){

		struct quote_t *new_quote = quotes_tbl->data[it];
		if(new_quote == NULL) continue;

		signed int isin_id = quotes_tbl->data[it]->isin_id - isins_tbl->shift;
		struct isin_data_tbl_t *sess_data = isins_tbl->data[isin_id];

		new_quote->isin_data = sess_data;



		if(sess_data){
			if(sess_data->quotes_tbl->qnt_cnt && sess_data->qnt_quotes_tbl == NULL){
				sess_data->qnt_quotes_tbl = malloc(sizeof(struct quotes_tbl_t));
				sess_data->qnt_quotes_tbl->cnt = sess_data->quotes_tbl->qnt_cnt;
				sess_data->qnt_quotes_tbl->data_size = sess_data->quotes_tbl->qnt_cnt;
				sess_data->qnt_quotes_tbl->data = malloc(sizeof(struct quote_t*) * sess_data->qnt_quotes_tbl->cnt);
			}

			if(sess_data->quotes_tbl->cnt > 1 && sess_data->quotes_tbl->data_size == 1){
				sess_data->quotes_tbl->data = realloc(sess_data->quotes_tbl->data, sizeof(struct quote_t*) * sess_data->quotes_tbl->cnt);
				sess_data->quotes_tbl->data_size = sess_data->quotes_tbl->cnt;

				struct quote_t *quote = sess_data->quotes_tbl->data[0];
				int jt;
				int kt = 0;
				for(jt = 0; jt < sess_data->quotes_tbl->cnt; jt++){
					sess_data->quotes_tbl->data[jt] = quote;
					if(quote->is_quanto) sess_data->qnt_quotes_tbl->data[kt++] = quote;

					quote = quote->next;
					sess_data->quotes_tbl->data[jt]->next = NULL;

				}
			}


		}
	}

	for(it = 0; it < portfolios_tbl->cnt; it++){
		portfolios_tbl->data[it]->spread = portfolios_tbl->data[it]->sell_at - portfolios_tbl->data[it]->buy_at;
	}

#if defined(UNITTEST)
	started_tmp = 1;
#endif

}

long long ipow(int base, int exp){
	long long result = 1;
	while(exp){
		if(exp & 1)
			result *= base;
		exp >>= 1;
		base *= base;
	}

	return result;
}
